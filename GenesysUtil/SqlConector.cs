﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GenesysUtil
{
    public class SqlConector
    {

   public static DataTable getDataTable(string spName, List<string> alParametros, List<string> paran)
    {
        SqlConnection conn = getConnection();
        SqlCommand cmd = getCommand(conn, spName, alParametros, paran);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = 0;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
            DataTable ds = new DataTable();
            da.Fill(ds);

            conn.Close();
            return ds;
        }
    }
        public static DataTable getDataTable(string sql)
        {
            SqlConnection conn = getConnection();
            SqlCommand cmd = getCommand(conn, sql);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            conn.Close();
            return dt;
        }

        public static DataTable getDataTableonlysp(string spName)
    {
        SqlConnection conn = getConnection();
        SqlCommand cmd = getCommand(conn, spName);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = 0;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
            DataTable ds = new DataTable();
            da.Fill(ds);

            conn.Close();
            return ds;
        }
    }

    public static DataSet getDataset(string spName, List<string> alParametros, List<string> paran)
    {
        SqlConnection conn = getConnection();
        SqlCommand cmd = getCommand(conn, spName, alParametros, paran);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = 0;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
            DataSet dsf = new DataSet();
            da.Fill(dsf, "data");

            conn.Close();
            return dsf;
        }
    }

    public static DataSet getDatasetbycs(string spName, List<string> alParametros, List<string> paran, string cs)
    {
        SqlConnection conn = getConnectionbycs(cs);
        SqlCommand cmd = getCommand(conn, spName, alParametros, paran);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = 0;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
            DataSet dsf = new DataSet();
            da.Fill(dsf, "data");

            conn.Close();
            return dsf;
        }
    }

    public static string resultofstore(string spName, List<string> alParametros, List<string> paran)
    {
        string result = null;
        SqlConnection conn = getConnection();
        SqlCommand cmd = getCommand(conn, spName, alParametros, paran);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = 0;
        object o = cmd.ExecuteScalar();

        if (o != null)
        {
            result = o.ToString();
        }

        return result;

    }

    public static SqlConnection getConnection()
    {
        string strcnx = getConnectionString();
        SqlConnection conn = new SqlConnection(strcnx);
        conn.Open();
        return conn;
    }

    public static SqlConnection getConnectionbycs(string cs)
    {
        string strcnx = getConnectionStringbyvar(cs);
        SqlConnection conn = new SqlConnection(strcnx);
        conn.Open();
        return conn;
    }

    public static string getConnectionString()
    {
            return ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
    }
    public static string getConnectionStringbyvar(string cstring)
    {
        return ConfigurationManager.ConnectionStrings[cstring].ConnectionString;
    }

    private static SqlCommand getCommand(SqlConnection conn, string spName, List<string> alParametros, List<string> paran)
    {
        SqlCommand cmd = new SqlCommand(spName, conn);
        int i = 0;
        foreach (string listai in alParametros)
        {
            cmd.Parameters.AddWithValue(paran[i], listai);
            i++;
        }

        return cmd;
    }

    private static SqlCommand getCommand(SqlConnection conn, string sql)
    {
        SqlCommand cmd = new SqlCommand(sql, conn);
        return cmd;
    }


    }
}
