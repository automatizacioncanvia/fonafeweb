﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GenesysUtil
{
    public class Validation
    {

        public string AuthDirectoryEntry(string domain, string userName, string password)
        {

            string respuesta = "";

            if (userName == "" || password == "")
            {
                 return respuesta= "false";
            }

            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain, userName, password);
                DirectorySearcher mySearcher = new DirectorySearcher(entry);
                mySearcher.Filter = "(&(objectClass=user)(|(cn=" + userName + ")(sAMAccountName=" + userName + ")))";
                SearchResult result = mySearcher.FindOne();

                return respuesta = "true";
            }
           catch (Exception exx)
                {
                    if (exx.HResult == -2147023570)
                    {
                        respuesta = "El usuario/contraseña/dominio incorrectos";
                        
                        return respuesta;
                    }
                    else if (exx.HResult == -2147016646)
                    {
                        respuesta = "El servidor no está operativo";
                        return respuesta;
                    }
                    else
                    {
                        respuesta = "Ocurrió el siguiente error: " + Convert.ToString(exx.Message);
                        return respuesta;
                    }
                }
          
        }

        public bool AuthPrincipalContext(string domain, string user, string password)
        {
            bool isValid = false;
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
            {
                // validate the credentials
                isValid = pc.ValidateCredentials(user, password);
            }
            return isValid;
        }

        public string AuthLDAP(string domain, string user, string password)
        {
            try
            {
                LdapConnection connection = new LdapConnection(domain);
                NetworkCredential credential = new NetworkCredential(user, password);
                connection.Credential = credential;
                connection.Bind();
                return "true";
            }
            catch (LdapException lexc)
            {
                String error = lexc.ServerErrorMessage;
                return error;
            }
            catch (Exception exc)
            {
                return exc.ToString();
            }
        }
    }
}
