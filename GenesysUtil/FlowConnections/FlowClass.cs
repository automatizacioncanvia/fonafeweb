﻿using System;
//using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Net;
//using System.Web;

namespace GenesysUtil.FlowConnections
{
    public class FlowClass
    {

        public string GetFlow(string hpuser, string hpopassword, string idexecflow, string sentence, string hpip)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://" + hpip + ":8443/oo/rest/v1/executions/" + idexecflow + "/" + sentence);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            String username = hpuser;
            String password = hpopassword;
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                return result.ToString();
            }

        }

        public string ExecuteFlowNow(string Flowuid, string iphpoo, string inputs, string hpouser, string hpopass)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://" + iphpoo + ":8443/oo/rest/executions/");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            String username = hpouser;
            String password = hpopass;
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            //httpWebRequest.Headers.Add("Content-Type","application/json");
            httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
            //


            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{ \"uuid\":\"" + Flowuid + "\"," +
                                  "\"logLevel\": \"INFO\"," +
                                  "\"inputs\":" + inputs +
                              "}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                return result.ToString();
            }

        }

    }
}

