﻿using GenesysBE;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysDA
{
    public class InventarioDA
    {

        public DataSet GetOPTIONS(Option ioption)
        {
            List<string> list = new List<string>();
            list.Add(ioption.id);
            list.Add(ioption.vista.ToString());
            list.Add(ioption.colab_id.ToString());
            List<string> parametros = new List<string>();
            parametros.Add("@ID");
            parametros.Add("@VISTA");
            parametros.Add("@COLAB_ID");
            using (DataSet ds = GenesysUtil.SqlConector.getDataset("SP_INVOPTIONS", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                return ds;
            }
        }

        public DataSet GetInventario(Format inv) {
 

            List<string> list = new List<string>();
            list.Add(inv.vista);
            list.Add(inv.info);
            Console.WriteLine(list);
            List<string> parametros = new List<string>();
            parametros.Add("@pIdVista");
            parametros.Add("@pInfo");
            using (DataSet ds = GenesysUtil.SqlConector.getDataset("FO_GETINVENTARIO", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                //return ds;
                return ds;

            }


        }

        public string PostInventario(Inventario ioption)
        {
            string retorno = "";

            string store = "FO_INSERTEQUIPO";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;

 
            sqlCmd.Parameters.AddWithValue("@OP", ioption.OP);
            sqlCmd.Parameters.AddWithValue("@ECI_ID", ioption.ECI_ID);//((object)Convert.ToInt32(ioption.ECI_ID)) ?? DBNull.Value
            sqlCmd.Parameters.AddWithValue("@NOMBRE", ioption.NOMBRE);
            sqlCmd.Parameters.AddWithValue("@TIPOS", Convert.ToInt32(ioption.TIPOS));         //ioption.TIPOS
            sqlCmd.Parameters.AddWithValue("@NOMBREV", ((object)ioption.NOMBREV) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@ESTADOE", Convert.ToInt32(ioption.ESTADOE));     //ioption.ESTADOE
            sqlCmd.Parameters.AddWithValue("@FEC_ALTA", ((object)ioption.FEC_ALTA) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@FEC_BAJA", ((object)ioption.FEC_BAJA) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@FAM_ID", Convert.ToInt32(ioption.FAM_ID));       //ioption.FAM_ID
            sqlCmd.Parameters.AddWithValue("@CLASE_ID", Convert.ToInt32(ioption.CLASE_ID));   //ioption.CLASE_ID
            sqlCmd.Parameters.AddWithValue("@COD_CA", ((object)ioption.COD_CA) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@SERIE", ((object)ioption.SERIE) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@PARTNUM", ((object)ioption.PARTNUM) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@MODELO", ((object)ioption.MODELO) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@MARCA", ((object)ioption.MARCA) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@COD_UBI", ((object)Convert.ToInt32(ioption.COD_UBI)) ?? DBNull.Value);     //ioption.COD_UBI
            sqlCmd.Parameters.AddWithValue("@PROYECTO", ((object)Convert.ToInt32(ioption.PROYECTO)) ?? DBNull.Value);   //ioption.PROYECTO
            sqlCmd.Parameters.AddWithValue("@PROPIEDAD", ((object)Convert.ToInt32(ioption.PROPIEDAD)) ?? DBNull.Value); //ioption.PROPIEDAD
            sqlCmd.Parameters.AddWithValue("@ADMINID", ((object)Convert.ToInt32(ioption.ADMINID)) ?? DBNull.Value);     //ioption.ADMINID
            sqlCmd.Parameters.AddWithValue("@COD_ROL", ((object)Convert.ToInt32(ioption.COD_ROL)) ?? DBNull.Value);     //ioption.COD_ROL
            sqlCmd.Parameters.AddWithValue("@COD_AMB", ((object)Convert.ToInt32(ioption.COD_AMB)) ?? DBNull.Value);     //ioption.COD_AMB
            sqlCmd.Parameters.AddWithValue("@COD_CRIT", ((object)Convert.ToInt32(ioption.COD_CRIT)) ?? DBNull.Value);   //ioption.COD_CRIT
            sqlCmd.Parameters.AddWithValue("@DESC", ((object)ioption.DESC) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@SOID", ((object)Convert.ToInt32(ioption.SOID)) ?? DBNull.Value);           //ioption.SOID
            sqlCmd.Parameters.AddWithValue("@VERSION", ((object)ioption.VERSION) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@ELICENCIA", ((object)Convert.ToInt32(ioption.ELICENCIA)) ?? DBNull.Value); //ioption.ELICENCIA
            sqlCmd.Parameters.AddWithValue("@NLICENCIA", ((object)ioption.NLICENCIA) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@ESOPORTE", ((object)ioption.ESOPORTE) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@SOPORTED", ((object)ioption.SOPORTED) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@SOPORTEH", ((object)ioption.SOPORTEH) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@EGARANTIA", ((object)ioption.EGARANTIA) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@GARANTIAD", ((object)ioption.GARANTIAD) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@GARANTIAH", ((object)ioption.GARANTIAH) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@RAM", ((object)Convert.ToDouble(ioption.RAM)) ?? DBNull.Value);            //ioption.RAM
            sqlCmd.Parameters.AddWithValue("@SOCKET", ((object)Convert.ToInt32(ioption.SOCKET)) ?? DBNull.Value );       //ioption.SOCKET
            sqlCmd.Parameters.AddWithValue("@CORE", ((object)Convert.ToDouble(ioption.CORE)) ?? DBNull.Value);           //ioption.CORE
            sqlCmd.Parameters.AddWithValue("@CPU", ((object)Convert.ToDouble(ioption.CPU)) ?? DBNull.Value);            //ioption.CPU
            sqlCmd.Parameters.AddWithValue("@DISCOAP", ((object)ioption.DISCOAP) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@DISCOAS", ((object)Convert.ToDouble(ioption.DISCOAS)) ?? DBNull.Value);    //ioption.DISCOAS
            sqlCmd.Parameters.AddWithValue("@COLAB_ID", ioption.COLAB_ID);
            sqlCmd.Parameters.AddWithValue("@RACK", ((object)ioption.RACK) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@RUD", ((object)ioption.RUD) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@RUH", ((object)ioption.RUH) ?? DBNull.Value);
  
            sqlCmd.Parameters.AddWithValue("@BAD", ((object)ioption.BAD) ?? DBNull.Value);//Agregado por Kenneth
            sqlCmd.Parameters.AddWithValue("@BAH", ((object)ioption.BAH) ?? DBNull.Value);//Agregado por Kenneth
            sqlCmd.Parameters.AddWithValue("@CID", ((object)ioption.CID) ?? DBNull.Value);//Agregado por Kenneth
            sqlCmd.Parameters.AddWithValue("@VCENTER", ((object)ioption.VCENTER) ?? DBNull.Value);

            SqlParameter parm3 = new SqlParameter("@MENSAJE", SqlDbType.VarChar, 200);
            parm3.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add(parm3);
 
            object o = sqlCmd.ExecuteScalar();

            if (o != null)
            {
                retorno = o.ToString();
            }
            myConnection.Close();

            return retorno;
        }

    }
}
