﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenesysBE.ActiveDirectory;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace GenesysDA
{
    public class ActiveDirectoryDA
    {

        public string Operation (User e)
        {

            string idlog = null;
            string store = "FO_ACTIVEDIRECTORY_lOG";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;

     
            sqlCmd.Parameters.AddWithValue("@U_ADID", ((object)e.u_adid) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@O_ID", e.o_id);
            sqlCmd.Parameters.AddWithValue("@EM_ID",((object) e.em_id)??DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_NAME", ((object)e.u_name) ?? DBNull.Value );
            sqlCmd.Parameters.AddWithValue("@U_SURNAME", ((object)e.u_surname) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_SAMANAME", e.u_samaname);
            sqlCmd.Parameters.AddWithValue("@U_PASSWORD", ((object)e.u_password)??DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@OU_PAHT", e.ou_path);
            sqlCmd.Parameters.AddWithValue("@U_OFFICE", ((object)e.u_office) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_TITLE", ((object)e.u_title) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_DEPARTAMENT",((object)e.u_departament) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_COMPANY", ((object)e.u_company) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_HOMEPHONE", ((object)e.u_homephone) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_MOBILEPHONE", ((object)e.u_mobilephone) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_OFFICEPHONE", ((object)e.u_officephone) ?? DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@U_ENABLE", e.u_enable);
            sqlCmd.Parameters.AddWithValue("@U_ACCOUNTEXPIRATIONDATE", ((object)e.u_accountexpirationdate)??DBNull.Value);
            sqlCmd.Parameters.AddWithValue("@CO_ID", e.co_id);
            sqlCmd.Parameters.AddWithValue("@M_TOKEN", e.m_token);
            sqlCmd.Parameters.AddWithValue("@INFO", e.info);
            sqlCmd.Parameters.AddWithValue("@U_CHANGEPASSWORDATLOGON", ((object)e.ul_changepasswordatlogon) ?? DBNull.Value );


            SqlDataAdapter adapta = new SqlDataAdapter(sqlCmd);
            DataTable tabla = new DataTable();
            adapta.Fill(tabla);

            foreach (DataRow row in tabla.Rows)
            {
                idlog = row["RESULT"].ToString();
                
            }

            myConnection.Close();
            return idlog;


        }


        public hpooconfiguration GetHPOOCONFIGURATION()
        {

            hpooconfiguration objecthpoo = new hpooconfiguration();


            string store = "FO_AD_GETHPOOCONFIGURATION";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;


            SqlDataAdapter adapta = new SqlDataAdapter(sqlCmd);
            DataTable tabla = new DataTable();
            adapta.Fill(tabla);

            foreach (DataRow row in tabla.Rows)
            {
                objecthpoo.hp_ip = row["HP_IP"].ToString();
                objecthpoo.hp_flowuuid = row["HP_FLOWUUID"].ToString();
                objecthpoo.hp_user = row["HP_USER"].ToString();
                objecthpoo.hp_password = row["HP_PASSWORD"].ToString();

            }


            return objecthpoo;
           
        }


        public string InsertAtributos(List<UserAttr> attr,int ul_id)
        {
            string result = null;
            string store = "FO_ACTIVEDIRECTORY_ATRIBUTOS_LOG";
            string fonafe = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;

            foreach (var itemattr in attr)
            {

                if (itemattr.model == "")
                {
                    itemattr.model = null;
                }
         
                    SqlConnection myConnection = new SqlConnection(fonafe);
                    myConnection.Open();

                    SqlCommand sqlCmd = new SqlCommand(store, myConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Clear();
                    sqlCmd.Parameters.Add(new SqlParameter("@UL_ID", ul_id));
                    sqlCmd.Parameters.Add(new SqlParameter("@AT_ID", itemattr.aT_ID));
                    sqlCmd.Parameters.Add(new SqlParameter("@DO_ID", itemattr.dO_ID));
                    sqlCmd.Parameters.Add(new SqlParameter("@EM_ID", itemattr.eM_ID));
                    sqlCmd.Parameters.Add(new SqlParameter("@VALOR", ((object)itemattr.model) ?? DBNull.Value ));
                    string getValue = sqlCmd.ExecuteScalar().ToString();
                    if (getValue != null)
                    {
                        string respuesta = getValue.ToString();
                    }

                    myConnection.Close();


            }

            result = "1";
            return result;

        }



        public string GroupOperation(FullGroup e)
        {

            string resutl = null;
            string store = "FO_AD_ADGROUP_LOG";
            string fonafe = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;

            foreach (var gitem in e.Groups)
            {

                SqlConnection myConnection = new SqlConnection(fonafe);
                myConnection.Open();

                SqlCommand sqlCmd = new SqlCommand(store, myConnection);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Clear();
                sqlCmd.Parameters.Add(new SqlParameter("@SID", e.SID));
                sqlCmd.Parameters.Add(new SqlParameter("@CO_ID", e.co_id));
                sqlCmd.Parameters.Add(new SqlParameter("@TOKEN_ID",e.tokenid));
                sqlCmd.Parameters.Add(new SqlParameter("@G_ID", gitem.g_id));
                sqlCmd.Parameters.Add(new SqlParameter("@OPERACION", gitem.operacion));
                sqlCmd.Parameters.Add(new SqlParameter("@EM_ID_ORI", e.em_id));
                sqlCmd.Parameters.Add(new SqlParameter("@U_NAME", ((object)e.u_name) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_SURNAME", ((object)e.u_surname) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_SAMANAME",((object) e.u_samaname)??DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_PASSWORD", ((object)e.u_password) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@OU_PAHT", ((object) e.ou_path) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_OFFICE", ((object)e.u_officephone) ?? DBNull.Value));             
                sqlCmd.Parameters.Add(new SqlParameter("@U_TITLE", ((object)e.u_title) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_DEPARTAMENT", ((object)e.u_departament) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_COMPANY", ((object)e.u_company) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_HOMEPHONE", ((object)e.u_homephone) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_MOBILEPHONE", ((object)e.u_mobilephone) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_OFFICEPHONE", ((object)e.u_officephone) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_ENABLE", ((object)e.u_enable) ?? DBNull.Value));
                sqlCmd.Parameters.Add(new SqlParameter("@U_ACCOUNTEXPIRATIONDATE", ((object)e.u_accountexpirationdate) ?? DBNull.Value));
          
                string getValue = sqlCmd.ExecuteScalar().ToString();
                if (getValue != null)
                {
                   string respuesta = getValue.ToString();
                }

                myConnection.Close();

            }


          

            resutl = "Se inserto Correctamente";
            return resutl;
        }




    }
}
