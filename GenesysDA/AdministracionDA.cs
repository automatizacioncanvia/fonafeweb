﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenesysBE;
using System.Configuration;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Newtonsoft.Json;
using GenesysUtil;


namespace GenesysDA
{
    public class AdministracionDA
    {

        public DataTable GetElementoControl(string parametro)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            List<string> list = new List<string>();
            list.Add(parametro);

            List<string> parametros = new List<string>();
            parametros.Add("@pParametro");
            using (DataTable ds = GenesysUtil.SqlConector.getDataTable("FO_GETELEMENTOCONTROl", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                return ds;
            }
        }

    }
}
