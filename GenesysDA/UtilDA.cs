﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenesysBE;
using System.Configuration;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Newtonsoft.Json;
using GenesysUtil;

namespace GenesysDA
{
    public class UtilDA
    {

        public DataTable GetTablas()
        {
            SqlDataAdapter da = new SqlDataAdapter();

            using (DataTable dt = GenesysUtil.SqlConector.getDataTableonlysp("FO_GETTABLAS"))
            {
                return dt;
            }
        }

        public DataTable GetFiltros(string idcategoria)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            List<string> list = new List<string>();
            list.Add(idcategoria);
            List<string> parametros = new List<string>();
            parametros.Add("@pCodigo");
            using (DataTable ds = GenesysUtil.SqlConector.getDataTable("FO_GETFILTROS", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                return ds;
            }
        }

        public DataTable GetServicios()
        {
            SqlDataAdapter da = new SqlDataAdapter();

            using (DataTable dt = GenesysUtil.SqlConector.getDataTableonlysp("FO_GETSERVICIO"))
            {
                return dt;
            }
        }


        public DataTable getEmpresas() {
            SqlDataAdapter da = new SqlDataAdapter();
            using (DataTable dt = SqlConector.getDataTableonlysp("FO_GETINFOEMPRESA"))
            {
                return dt;
            }
        }

    }
}
