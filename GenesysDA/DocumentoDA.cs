﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenesysBE;
using System.Configuration;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Newtonsoft.Json;
using GenesysUtil;

namespace GenesysDA
{
    public class DocumentoDA
    {
        public DataTable Getdocumento(Format e)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            List<string> list = new List<string>();
            list.Add(e.vista);
            list.Add(e.info);
            List<string> parametros = new List<string>();
            parametros.Add("@pIdVista");
            parametros.Add("@pInfo");
            using (DataTable ds = GenesysUtil.SqlConector.getDataTable("FO_GETDOCUMENTS", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                //return ds;

                return ds;

            }
        }

        public string eliminarDocumento(string nombre) {

            string retorno = "";

            string store = "FO_DELETEDOCUMENTO";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.AddWithValue("@@NOMBRE", nombre);

            object o = sqlCmd.ExecuteScalar();

            if (o != null)
            {
                retorno = o.ToString();
            }
            myConnection.Close();

            return retorno;
        }

        public string insertarDocumento(Documento d) {
            string retorno="";

            string store = "FO_INSERTDOCUMENTO";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.AddWithValue("@NOMBRE", d.nombre);
            sqlCmd.Parameters.AddWithValue("@EM_ID", d.em_id);
            sqlCmd.Parameters.AddWithValue("@SE_ID", d.se_id);
            sqlCmd.Parameters.AddWithValue("@CODIGO", d.codigo);
            sqlCmd.Parameters.AddWithValue("@FECHA", d.fecha);
            sqlCmd.Parameters.AddWithValue("@USER", d.usuario);

            object o = sqlCmd.ExecuteScalar();

            if (o != null)
            {
                retorno = o.ToString();
            }
            myConnection.Close();

            return retorno;
        }


    }
}

