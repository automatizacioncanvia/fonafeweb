﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenesysBE;
using System.Configuration;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Newtonsoft.Json;
using GenesysUtil;


namespace GenesysDA
{
    public class SeguridadDA
    {

        public DataTable GetEmpresaId(int EM_ID)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            List<string> list = new List<string>();
            list.Add(EM_ID.ToString());
            List<string> parametros = new List<string>();
            parametros.Add("@EM_ID");
            DataSet ds = GenesysUtil.SqlConector.getDatasetbycs("FO_GETEMPRESA", list, parametros, "connBDFonafeDev");
            dt = ds.Tables[0];
            return dt;
        }

        public DataTable GetByDataSetByTareaName(string NAME)
        {

            List<string> listi = new List<string>();
            listi.Add(NAME);

            List<string> parametros = new List<string>();
            parametros.Add("@NAME");

            using (DataTable ds = GenesysUtil.SqlConector.getDataTable("GETTAREABYNAME", listi, parametros))
            {
                listi.Clear();
                parametros.Clear();
                //return ds;
                return ds;

            }
        }

        public DataTable GetChartInfo()
        {
            using (DataTable ds = GenesysUtil.SqlConector.getDataTableonlysp("BK_GETCHARTINFO"))
            {
                return  ds;
            }
        }

        public string GetPasswordColaborador(string str)
        {
            string contrasena = null;

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            List<string> list = new List<string>();
            list.Add(str);
            List<string> parametros = new List<string>();
            parametros.Add("@CO_CUENTA");
            DataSet ds = GenesysUtil.SqlConector.getDatasetbycs("FO_GETPASSWORDBYCUENTA", list, parametros, "connBDFonafeDev");
            contrasena =  ds.Tables[0].Rows[0][0].ToString();

            //da.Fill(ds, "data");
            //dt = ds.Tables["data"];
            //foreach (DataRow dr in dt.Rows) {
            //    contrasena = dr["CO_CLAVE"].ToString();
            //}
            //contrasena = ds.Tables(Nombretabla).Rows(0)("CO_CLAVE".ToString);

            try
            {
                //list.Clear();
                //parametros.Clear();
                //DataTable row = ds.
                //contrasena = row["CO_CLAVE"].ToString();
                return contrasena;
            }

            catch(Exception ex){
                String error = Convert.ToString(ex);
                return error;
            }
            
            //return contrasena;
        }
        
        public DataSet GetOPTIONS(string ID, string VISTA)
        {
                List<string> list = new List<string>();
                list.Add(ID);
                list.Add(VISTA);
                List<string> parametros = new List<string>();
                parametros.Add("@ID");
                parametros.Add("@VISTA");
                using (DataSet ds = GenesysUtil.SqlConector.getDataset("BK_OPTIONS", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    return ds;
                }
        }

  
   
        public DataTable GetDataSetByNameFiltro(string valor)
        {
            List<string> list = new List<string>();
            list.Add(valor);

            List<string> parametros = new List<string>();
            parametros.Add("@VALOR");

            using (DataTable dt = GenesysUtil.SqlConector.getDataTable("GETBYFILTRONAME", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                return dt;
            }
        }

        public DataTable GetChartInfoESP(string VISTA, string INFO)
        {

            List<string> list = new List<string>();
            list.Add(VISTA);
            list.Add(INFO);
            List<string> parametros = new List<string>();
            parametros.Add("@VISTA");
            parametros.Add("@INFO");
            using (DataTable ds = GenesysUtil.SqlConector.getDataTable("BK_GETCHARTINFO_DEV", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                //return ds;
                return ds;

            }
        }

        public string GetDominioColaborador(string str)
        {
            string contrasena = null;

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            List<string> list = new List<string>();
            list.Add(str);
            List<string> parametros = new List<string>();
            parametros.Add("@CO_CUENTA");
            DataSet ds = GenesysUtil.SqlConector.getDatasetbycs("FO_GETDOMINIOBYCUENTA", list, parametros, "connBDFonafeDev");
            contrasena = ds.Tables[0].Rows[0][0].ToString();
            try
            {
                return contrasena;
            }
            catch (Exception ex)
            {
                String error = Convert.ToString(ex);
                return error;
            }
        }

        public string GETsdlink()
        {
            string url = null;

            DataTable dt = SqlConector.getDataTableonlysp("FO_GETSDLINK");
            url = dt.Rows[0][0].ToString();
            return url;
        }


        public DataSet GetDash(Format e)
        {
            
            List<string> list = new List<string>();
            list.Add(e.vista);
            list.Add(e.info);
            List<string> parametros = new List<string>();
            parametros.Add("@pIdVista");
            parametros.Add("@pInfo");
            using (DataSet ds = GenesysUtil.SqlConector.getDataset("FO_GETDASHBOARDALL", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                //return ds;
                return ds;

            }
        }

        public DataSet GetMenu(int vista, string info)
        {
            List<string> list = new List<string>();
            list.Add(vista.ToString());
            list.Add(info);
            List<string> parametros = new List<string>();
            parametros.Add("@VISTA");
            parametros.Add("@INFO");
            using (DataSet ds = GenesysUtil.SqlConector.getDataset("FO_GETMENUALL", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                //return ds;
                return ds;

            }
        }

        public string FonafeAutentication(Seguridad seguri)
        {
            string resutl;


            SeguridadDA ouser = new SeguridadDA();
            if (ouser.UserAllow(seguri.usuario, seguri.dominio))
            {
                Validation ovali = new Validation();

                bool flag;

                string respuesta = ovali.AuthDirectoryEntry(seguri.dominio, seguri.usuario, seguri.contrasena);
                if (Boolean.TryParse(respuesta, out flag))
                {
                    if (Boolean.Parse(respuesta))
                    {
                        //Encriptacion Contraseña
                        string contrasenaEncriptada = SecurityManager.EncryptPassword(seguri.contrasena);

                        //Generacion de Token
                        #region ♖ ► (Capturar tiempo en 64bits)
                        Int64 retval = 0;
                        var st = new DateTime(1970, 1, 1);
                        TimeSpan t = (DateTime.Now.ToUniversalTime() - st);
                        retval = (Int64)(t.TotalMilliseconds + 0.5);
                        #endregion
                        long timenow = (retval * 10000) + 621355968000000000;
                        string Token = SecurityManager.GenerateToken(seguri.usuario, seguri.contrasena, "ip", "userAgent", timenow);
                        //insertar token

                        DataTable ds = InsertEcripCredential(seguri.usuario, seguri.dominio, contrasenaEncriptada);

                        string JsonPerfiles = JsonConvert.SerializeObject(ds);
                        //get perfil


                        string rptstringconcate = JsonPerfiles + '¬' + respuesta + '¬' + Token + '¬' + seguri.usuario;

                        resutl = rptstringconcate;

                    }
                    else
                    {

                        resutl = respuesta;
                    }

                }
                else
                {
                    resutl = respuesta;
                }
            }
            else
            {
                resutl = "Usuario sin Acceso";
            }

            return resutl;


        }

        public DataTable InsertEcripCredential(string usuario,string dominio,string credential)
        {
            DataTable resulti = null;
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            
            List<string> list = new List<string>();
            list.Add(usuario);
            list.Add(dominio);
            list.Add(credential);
            list.Add("2");
            List<string> parametros = new List<string>();
            parametros.Add("@CO_CUENTA");
            parametros.Add("@DO_NOMBRE");
            parametros.Add("@CO_CLAVE");
            parametros.Add("@VISTA");

            using (DataTable ds = GenesysUtil.SqlConector.getDataTable("FO_VALIDUSER", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                if (ds != null)
                {
                    return ds;
                }
            }

            return resulti;

        }

        public bool UserAllow(string usuario, string dominio)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            bool result = false;
            List<string> list = new List<string>();
            list.Add(usuario);
            list.Add(dominio);
            list.Add("");
            list.Add("1");
            List<string> parametros = new List<string>();
            parametros.Add("@CO_CUENTA");
            parametros.Add("@DO_NOMBRE");
            parametros.Add("@CO_CLAVE");
            parametros.Add("@VISTA");


            using (DataTable ds = GenesysUtil.SqlConector.getDataTable("FO_VALIDUSER", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                foreach (DataRow dtRow in ds.Rows)
                {

                    if (dtRow["RESULT"].ToString() == "1")
                    {
                        result = true;
                    }

                }
            }


            return result;
        }

    }
}
