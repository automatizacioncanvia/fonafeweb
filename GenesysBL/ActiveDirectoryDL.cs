﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenesysBE.ActiveDirectory;
using GenesysDA;

namespace GenesysBL
{
    public class ActiveDirectoryDL
    {

        public string Operation(User e)
        {
             ActiveDirectoryDA objData = new ActiveDirectoryDA();
             return objData.Operation(e);

          
        }

        public string InserAtributos(List<UserAttr> attr, int ul_id)
        {

            ActiveDirectoryDA objDatai = new ActiveDirectoryDA();
            return objDatai.InsertAtributos(attr,ul_id);


        }
        



        public hpooconfiguration GetHPOOCONFIGURATION()
        {
            ActiveDirectoryDA objhpoo = new ActiveDirectoryDA();
            return objhpoo.GetHPOOCONFIGURATION();
        }

        public string GroupOperation(FullGroup g)
        {
            ActiveDirectoryDA objgp = new ActiveDirectoryDA();
            return objgp.GroupOperation(g);
        }


     }
}
