﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenesysBE;
using GenesysDA;
using Newtonsoft.Json;

namespace GenesysBL
{
    public class UtilBL
    {
        public DataTable GetTablas()
        {
            UtilDA objData = new UtilDA();
            return objData.GetTablas();
        }

        public DataTable GetFiltros(string idcategoria)
        {
            UtilDA objData = new UtilDA();
            return objData.GetFiltros(idcategoria);
        }

        public DataTable GetServicios()
        {
            UtilDA objData = new UtilDA();
            return objData.GetServicios();
        }

        public DataTable GetEmpresas() {
            UtilDA objData = new UtilDA();
            return objData.getEmpresas();
        }

    }
}
