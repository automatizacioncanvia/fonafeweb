﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenesysBE;
using GenesysDA;
using Newtonsoft.Json;

namespace GenesysBL
{
    public class DocumentoBL
    {

        public DataTable Getdocumento(Format e)
        {
            //D
            DocumentoDA objDocumento = new DocumentoDA();
            return objDocumento.Getdocumento(e);
        }

        public string InsertarDocumento(Documento d) {
            DocumentoDA obj = new DocumentoDA();
            return obj.insertarDocumento(d);
        }

        public string eliminarDocumento(string d)
        {
            DocumentoDA obj = new DocumentoDA();
            return obj.eliminarDocumento(d);
        }

    }
}
