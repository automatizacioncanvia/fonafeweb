﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenesysDA;

namespace GenesysBL
{
    public class AdministracionBL
    {
        public DataTable GetElementoControl(string parametro)
        {
            AdministracionDA objData = new AdministracionDA();
            return objData.GetElementoControl(parametro);
        }

    }
}
