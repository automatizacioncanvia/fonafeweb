﻿using GenesysBE;
using GenesysDA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBL
{
    public class InventarioBL
    {
        InventarioDA objInventario = new InventarioDA();
        public DataSet GetOPTIONS(Option ioption)
        {
            return objInventario.GetOPTIONS(ioption);
            
        }

        public DataSet GetInventario(Format inv) {
            return objInventario.GetInventario(inv);
        }

        public string PostInventario(Inventario inv) {
            return objInventario.PostInventario(inv);
        }
    }
}
