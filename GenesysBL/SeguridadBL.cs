﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenesysBE;
using GenesysDA;
using Newtonsoft.Json;

namespace GenesysBL
{
    public class SeguridadBL
    {

        public DataTable GetEmpresaId(int EM_ID)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetEmpresaId(EM_ID);
        }


        public DataTable GetByDataSetByTareaName(string NAME)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetByDataSetByTareaName(NAME);
        }

        public DataTable GetChartInfo()
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetChartInfo();
        }


        public DataTable GetChartInfoESP(string VISTA,string INFO)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetChartInfoESP(VISTA,INFO);
        }



        public DataSet GetOPTIONS(string ID, string VISTA)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetOPTIONS(ID, VISTA);
        }

        public string GetPasswordColaborador(String str)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetPasswordColaborador(str);
        }

   
        public String FonafeAuth(Seguridad seguri)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.FonafeAutentication(seguri);
        }

        public string GetDominioColaborador(string str)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetDominioColaborador(str);
        }

        public string GETsdlink()
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GETsdlink();
        }

        public DataSet GetDash(Format e)
        {
            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetDash(e);
        }

        public DataSet GetMenu(int vista, string info)
        {

            GenesysDA.SeguridadDA objClass1 = new GenesysDA.SeguridadDA();
            return objClass1.GetMenu(vista,info);

        }
    }
}
