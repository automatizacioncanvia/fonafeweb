﻿angular.module('masterGenerysV1App').filter('FilterController', function () {
    return function (string) {
        if (!angular.isString(string)) {
            return string;
        }
        return string.replace(/[\s]/g, '');
    };

});