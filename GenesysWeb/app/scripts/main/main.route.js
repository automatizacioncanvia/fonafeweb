﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
   $stateProvider
            .state('main',
            {
                url: '/',
                views: {
                    'main': {
                        templateUrl: 'app/views/main/main.html',
                        controller: 'MainCtrl',
                        controllerAs: 'main',
                        authorize: true
                    },
                    'content@main': {
                        templateUrl: 'app/views/components/app/app.html',
                        controller: 'AppCtrl',
                        controllerAs: 'app',
                        authorize: true
                    }
                }
            });
});