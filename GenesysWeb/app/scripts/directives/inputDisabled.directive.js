﻿angular.module('masterGenerysV1App')

.directive("inputDisabled", function() {
    return function (scope, element, attrs) {

        scope.$watch(attrs.inputDisabled, function(val) {
            if (val)
                element.removeAttr("disabled");
            else
                element.attr("disabled", "disabled");
        });

    }
});