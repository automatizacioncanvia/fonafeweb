﻿/// <reference path="D:\FonafeGITProyecto\GenesysWeb\Index.html" />
angular.module('masterGenerysV1App')
.directive('contentGroup', function ($compile) {

    var datatem = '  <div class="row">'+
                    '<br />'+
                    '<br />'+
                    '<div class="col-md-12">'+
                    '<legend>{{content.title}}</legend>'+
                        '<div class="col-md-6">'+

                            '<div class="input-group">'+

                                '<ui-select theme="bootstrap" ng-model="content.value"  style="width: 250px;">' +
                                    '<ui-select-match placeholder="Seleccione">{{$select.selected.g_NOMBRE}}</ui-select-match>'+

                                    '<ui-select-choices repeat="grupo in content.group| filter: $select.search">'+
                                        '<div ng-bind-html="grupo.g_NOMBRE | highlight: $select.search"></div>'+
                                    '</ui-select-choices>'+


                                '</ui-select>'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" ng-click="addgroudpi(content.value)" class="btn btn-xs btn-primary btn-fill" ng-disabled="alldata.length==={{content.max}}">' +
                                        '<span>'+
                                          '<i class="pe-7s-right-arrow" style="font-size:19px;"></i>' +
                                        '</span>'+
                                    '</button>'+
                                '</span>'+
                            '</div>'+

                        '</div>'+

                        '<div class="col-md-6">'+
                            '<div class="well" id="well2">'+
                                '<div class="list-group">'+
                                    '<div class="list-group-item"  ng-repeat="group in alldata" layout="row" flex>'+
                                        '<md-input-container style="width:200px;margin:0px;">'+
                                            '<input required name="surname" value="{{group.g_NOMBRE}}" ng-disabled="true">'+
                                        '</md-input-container>'+
                                        '<div>'+
                                            '<md-button class="md-icon-button" ng-click="deletegroupi(group)">'+
                                                '<md-icon class="material-icons">clear</md-icon>'+
                                            '</md-button>'+
                                        '</div>'+

                                    '</div>'+
                                '</div>'+
                            '</div>'+

                        '</div>'+

                    '</div>'+
                '</div>';


    var intitem = '<div class="row">' +

                    '<br />' +
                    '<br />' +
                    '<div class="col-md-12">' +
                       '<legend>{{content.title}}</legend>' +


                        '<div class="col-md-6">' +

                           ' <div class="input-group">' +

                                '<ui-select theme="bootstrap" ng-model="content.value"  style="width: 250px;">' +
                                    '<ui-select-match placeholder="Seleccione">{{$select.selected.g_NOMBRE}}</ui-select-match>' +

                                    '<ui-select-choices repeat="grupo in content.group | filter: $select.search">' +
                                        '<div ng-bind-html="grupo.g_NOMBRE | highlight: $select.search"></div>' +
                                    '</ui-select-choices>' +


                                '</ui-select>' +
                               ' <span class="input-group-btn">' +
                                   ' <button type="button" ng-click="addgroud2(content.value)" class="btn btn-primary btn-fill" ng-disabled="alldata.length==={{content.max}}">' +
                                        '<span class="pe-7s-right-arrow"></span>' +
                                    '</button>' +
                                '</span>' +
                            '</div>' +

                        '</div>' +



                        '<div class="col-md-6">' +
                            '<div class="well" id="well3" style="height: 40px;">' +
                                '<div class="list-group">' +
                                    '<div class="list-group-item" ng-repeat="group in alldata" layout="row" flex>' +
                                       ' <md-input-container style="width:200px">' +
                                            '<input required name="surname" value="{{group.g_NOMBRE}}" ng-disabled="true">' +
                                        '</md-input-container>' +
                                        '<div>' +
                                           ' <md-button class="md-icon-button" ng-click="deletegroup1(group)">' +
                                                '<md-icon class="material-icons">clear</md-icon>' +
                                            '</md-button>' +
                                        '</div>' +

                                   ' </div>' +
                               '</div>' +
                            '</div>' +

                        '</div>' +

                    '</div>' +


                '</div>';







    var getTemplate = function (max) {

        var template = '';

        switch (max) {

            case -1:
                template = datatem;
                break;
            case 1:
                template = datatem;
                break;
        }

        return template;
    }


    var linker = function (scope, element, attrs) {

        element.html(getTemplate(scope.content.max)).show();
        $compile(element.contents())(scope);
    }



    return {

        restrict: "E",
        link: linker,
        scope: {
            content: '=',
            memberof: '='
        },
        controller: function ($scope, Message) { //  equals MyController

            $scope.alldata = [];

           

            function OnStart() {

                for (var j = 0; j < $scope.memberof.length; j++) {

                    var temp = $scope.content.group.filter(function (obj) {

                        return (obj.g_PATH === $scope.memberof[j]);
                    })

                    if (temp.length > 0) {
                        $scope.alldata = $scope.alldata.concat(temp);
                    }
                }


                //$scope.temgroups = JSON.parse(JSON.stringify($scope.alldata));

                $scope.content.arraytengo = JSON.parse(JSON.stringify($scope.alldata));
                $scope.content.gadd = [];
                $scope.content.gdelete = [];
                $scope.content.value = {};
                //alldata es como mi gfilesystem


                //arraytengo = JSON.parse(JSON.stringify(groupsf.concat(groupsi, groupsbd, groupsc)));

            }

            OnStart();
          


        

            $scope.addgroudpi = function (grupo) {

                var g_id = grupo.g_ID;
                var g_nombre = grupo.g_NOMBRE;
                var g_type = grupo.gT_ID;

                var found = $scope.content.arraytengo.filter(function (obj) {
                    return (obj.g_ID === g_id);

                });

             
                if (found.length > 0) {

                    var fi = arrayObjectIndexOf($scope.content.gdelete, g_id, 'g_id');
                    $scope.content.gdelete.splice(fi, 1);

                    var valid = arrayObjectIndexOf($scope.alldata, g_id, 'g_ID');

                    if (valid !== -1) {
                        Message.result(2, "El grupo ya existe");

                    } else {
                        $scope.alldata.push(grupo);

                    }


                } else {


                    var valid = arrayObjectIndexOf($scope.alldata, g_id, 'g_ID');

                    if (valid !== -1) {
                        Message.result(2, "El grupo ya existe");

                    } else {
                        $scope.alldata.push(grupo);
                        var nuevi = new Group(g_id, g_nombre, 9);
                        $scope.content.gadd.push(nuevi);

                    }
                }


                //console.log($scope.content.gadd, "Grupo Agregados");
                //console.log($scope.content.gdelete, "Grupos Eliminass");



            }

            $scope.deletegroupi = function (grupo) {
                //console.log(dat);

                var g_id = grupo.g_ID;
                var g_nombre = grupo.g_NOMBRE;
                var g_type = grupo.gT_ID;

                var found = $scope.content.arraytengo.filter(function (obj) {
                    return (obj.g_ID === g_id);

                });

                if (found.length > 0) {
                    var fi = arrayObjectIndexOf($scope.alldata, g_id, 'g_ID');
                    $scope.alldata.splice(fi, 1);
                    var nuevog = new Group(g_id, g_nombre, 8);
                    $scope.content.gdelete.push(nuevog);
                }


                else {
                    var found2 = $scope.content.gadd.filter(function (obj) {
                        return (obj.g_id === g_id);

                    })

                    if (found2.length > 0) {

                      

                           var fi = arrayObjectIndexOf($scope.alldata, g_id, 'g_ID');
                                  $scope.alldata.splice(fi, 1);
                           

                        }

                    var iadd = arrayObjectIndexOf($scope.content.gadd, g_id, 'g_id');
                    $scope.content.gadd.splice(iadd, 1);



                }

                //console.log($scope.content.gadd, "Grupo Agregados");
                //console.log($scope.content.gdelete, "Grupos Eliminass");



            }





            function arrayObjectIndexOf(myArray, searchTerm, property) {

                for (var i = 0, len = myArray.length; i < len; i++) {

                    if (myArray[i][property] === searchTerm) {

                        return i;
                        break;
                    }
                }
                return -1;
            }


            function Group(g_id, g_nombre, g_operacion) {

                this.g_id = g_id;
                this.g_nombre = g_nombre;
                this.operacion = g_operacion;
            }

          

        }
    };




});