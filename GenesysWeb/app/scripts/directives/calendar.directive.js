﻿angular.module('masterGenerysV1App')
.directive('dateCombo', function(){
    return {
        restrict: 'E',
        scope: false,
        templateUrl: 'app/views/dates.html'
    }
});