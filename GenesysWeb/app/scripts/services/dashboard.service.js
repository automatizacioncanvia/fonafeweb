﻿angular.module('masterGenerysV1App').factory('Apidashboard', function($http, $rootScope) {


    var config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    return {

        getdash: function(data) {
            
            return $http.post('api/dashboard', data, config);
        },

        getdashi: function (periodo, em, idchart, servicio, prioridad, sla, estado, tipoticket,grupo) {
            //MODIFICAR AQUI URGENTE
            var empresa = em;
            var servicio = (servicio === undefined ? -1 : servicio); //servicio
            var prioridad = (prioridad === undefined ? -1 : prioridad);
            var sla = (sla === undefined ? -1 : sla);
            var estado = (estado === undefined ? -1 : estado);
            var tipoticket = (tipoticket === undefined ? -1 : tipoticket);
            var grupo = (grupo === undefined ? -1 : grupo);

            //debugger;
            //servicio,periodo.
            var body = JSON.stringify({
                vista: idchart + "," + periodo + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad + "," + sla + "," + estado + "," + tipoticket + "," + grupo,
                info: "-"
            });
              console.log(body);
            //console.log('entre');
            return $http.post('api/dashboard', body, config);
        },

        getpanel: function (periodo, em, idchart, servicio, prioridad, sla, estado, tipoticket, grupo) {
            //MODIFICAR AQUI URGENTE
            var empresa = em;
            var servicio = (servicio === undefined ? -1 : servicio); //servicio
            var prioridad = (prioridad === undefined ? -1 : prioridad);
            var sla = (sla === undefined ? -1 : sla);
            var estado = (estado === undefined ? -1 : estado);
            var tipoticket = (tipoticket === undefined ? -1 : tipoticket);
            var grupo = (grupo === undefined ? -1 : grupo);

            //debugger;
            //servicio,periodo.
            var body = JSON.stringify({
                vista: idchart + "," + periodo + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad + "," + sla + "," + estado + "," + tipoticket + "," + grupo,
                info: "-"
            });

            //console.log('entre');
            return $http.post('api/panelmando', body, config);
        },

        getdashbydatepanel: function (from, to, em, idchart, servicio, prioridad, sla, estado, tipoticket, grupo) {


            var desde = moment(from).format("YYYYMMDD");
            var hasta = moment(to).format("YYYYMMDD");
            var servicio = (servicio === undefined ? -1 : servicio); //servicio
            var prioridad = (prioridad === undefined ? -1 : prioridad);
            var sla = (sla === undefined ? -1 : sla);
            var estado = (estado === undefined ? -1 : estado);
            var empresa = em;


            var vista = idchart + ",1," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad + "," + sla + "," + estado + "," + tipoticket + "," + grupo;
            var info = "" + desde + "," + hasta;
            var body = JSON.stringify({
                vista: vista,
                info: info
            });


            return $http.post('api/panelmando', body, config);



        },

        getdashbydate: function (from, to, em, idchart, servicio, prioridad, sla, estado, tipoticket, grupo) {


            var desde = moment(from).format("YYYYMMDD");
            var hasta = moment(to).format("YYYYMMDD");
            var servicio = (servicio === undefined ? -1 : servicio); //servicio
            var prioridad = (prioridad === undefined ? -1 : prioridad);
            var sla = (sla === undefined ? -1 : sla);
            var estado = (estado === undefined ? -1 : estado);
            var empresa = em;


            var vista = idchart + ",1," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad + "," + sla + "," + estado + "," + tipoticket + "," + grupo;
            var info = "" + desde + "," + hasta;
            var body = JSON.stringify({
                vista: vista,
                info: info
            });

                    console.log("getdashbydate datos");
                    var myArr1 = [vista, info, body, config];
                    console.log(myArr1);
            return $http.post('api/dashboard', body, config);



        },

        makedashboard1: function(arreglo) {
 
            var arr = this.Create2DArray(arreglo.length);
            var estados = [];
            var data = [];
            var totales = [];
            
            var obkey = Object.keys(arreglo[0]);
            var ln = obkey.length;
            var oi = [];

            function validarreglo(hi) {

                var value = 0;
                for (var i = 0; i < arreglo.length; i++) {

                    if (arreglo[i][obkey[hi]] === 0) {

                        value += 1
                    }
                }

                if (arreglo.length === 1) {

                    if (value === 1) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    if (value === 2) {
                        return true;
                    } else {
                        return false;
                    }


                }
            }
            var acum = 0;
            var acum2 = 0;
			var acum3 = 0;
            var acum4 = 0;
            var acum5 = 0;
			var acum6 = 0;
			var acum7 = 0;
			var acum8 = 0;
			var acum9 = 0;
			var acum10 = 0;
            for (var hi = 0; hi < ln; hi++) {
                
                if (!isNaN(arreglo[0][obkey[hi]])) {
                    
                    var bool = false//validarreglo(hi);
                    if (bool) {} else {
                        oi.push([obkey[hi]]);
                        var j = 0
                        while (j < arreglo.length) {
                            arr[j].push(arreglo[j][obkey[hi]]);
                            if (j == 0) { acum = acum + arreglo[j][obkey[hi]]; }
							else if(j == 1){acum2 = acum2 + arreglo[j][obkey[hi]];}
							else if(j == 2){acum3 = acum3 + arreglo[j][obkey[hi]];}
							else if(j == 3){acum4 = acum4 + arreglo[j][obkey[hi]];}
							else if(j == 4){acum5 = acum5 + arreglo[j][obkey[hi]];}
							else if(j == 5){acum6 = acum6 + arreglo[j][obkey[hi]];}
							else if(j == 6){acum7 = acum7 + arreglo[j][obkey[hi]];}
							else if(j == 7){acum8 = acum8 + arreglo[j][obkey[hi]];}
							else if(j == 8){acum9 = acum9 + arreglo[j][obkey[hi]];}
                            else { acum10 = acum10 + arreglo[j][obkey[hi]]; }
                            j++
                        }
                    }
                } else {

                    var k = 0
                    while (k < arreglo.length) {
                        estados.push(arreglo[k][obkey[hi]]);
                        k++;
                    }
                }

            }
            totales.push(acum)
            totales.push(acum2)
			totales.push(acum3)
			totales.push(acum4)
			totales.push(acum5)
			totales.push(acum6)
            totales.push(acum7)
			totales.push(acum8)
			totales.push(acum9)
			totales.push(acum10)
			
            var obj = {
                labels: oi,
                series: estados,
                data: arr,
                totales: totales
            };

			console.log("makedashboard1");
            var myArr1 = [obkey, ln, arreglo.length];
            console.log(myArr1);

            return obj;


        },

        makedashboard16: function (arreglo) {
            var arr = this.Create2DArray(arreglo.length);
            var estados = [];
            var data = [];
            var totales = [];

            var obkey = Object.keys(arreglo[0]);
            var ln = obkey.length;
            var oi = [];

            function validarreglo(hi) {

                var value = 0;
                for (var i = 0; i < arreglo.length; i++) {

                    if (arreglo[i][obkey[hi]] === 0) {

                        value += 1
                    }
                }

                if (arreglo.length === 1) {

                    if (value === 1) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    if (value === 2) {
                        return true;
                    } else {
                        return false;
                    }


                }
            }
            var acum = 0;
            var acum2 = 0;
			var acum3 = 0;
            var acum4 = 0;
            var acum5 = 0;
			var acum6 = 0;
			var acum7 = 0;
			var acum8 = 0;
			var acum9 = 0;
			var acum10 = 0;
            for (var hi = 0; hi < ln; hi++) {

                if (!isNaN(arreglo[0][obkey[hi]])) {

                    var bool = false//validarreglo(hi);
                    if (bool) { } else {
                        oi.push([obkey[hi]]);
                        var j = 0
                        while (j < arreglo.length) {
                            arr[j].push(arreglo[j][obkey[hi]]);
                            if (j == 0) { acum = acum + arreglo[j][obkey[hi]]; }
                            else if(j == 1){acum2 = acum2 + arreglo[j][obkey[hi]];}
							else if(j == 2){acum3 = acum3 + arreglo[j][obkey[hi]];}
							else if(j == 3){acum4 = acum4 + arreglo[j][obkey[hi]];}
							else if(j == 4){acum5 = acum5 + arreglo[j][obkey[hi]];}
							else if(j == 5){acum6 = acum6 + arreglo[j][obkey[hi]];}
							else if(j == 6){acum7 = acum7 + arreglo[j][obkey[hi]];}
							else if(j == 7){acum8 = acum8 + arreglo[j][obkey[hi]];}
							else if(j == 8){acum9 = acum9 + arreglo[j][obkey[hi]];}
                            else { acum10 = acum10 + arreglo[j][obkey[hi]]; }
                            j++
                        }
                    }
                } else {

                    var k = 0
                    while (k < arreglo.length) {
                        estados.push(arreglo[k][obkey[hi]]);
                        k++;
                    }
                }

            }
            totales.push(acum)
            totales.push(acum2)
			totales.push(acum3)
			totales.push(acum4)
			totales.push(acum5)
			totales.push(acum6)
            totales.push(acum7)
			totales.push(acum8)
			totales.push(acum9)
			totales.push(acum10)

            var obj = {
                labels: oi,
                series: estados,
                data: arr,
                totales: totales
            };



            return obj;
        },

        makedashboard2: function(data) {

            var obkey = Object.keys(data[0]);
            var labels = [];
            obkey.splice(0, 4);
            var ln = obkey.length;
            var final = this.Create2DArray(ln);
            for (var i = 0; i < ln; i++) {

                var k = 0
                while (k < data.length) {

                    if (labels.length < data.length) {

                        labels.push(data[k].periodo);
                    }
                    final[i].push(data[k][obkey[i]]);
                    k++;
                }

            };

            var objecfinal = {

                labels: labels,
                legend: obkey,
                series: obkey,
                data: final
            }

            return objecfinal;

        },

        makedashboard17: function (data) {

            var obkey = Object.keys(data[0]);
            var labels = [];
            obkey.splice(0, 4);
            var ln = obkey.length;
            var final = this.Create2DArray(ln);
            for (var i = 0; i < ln; i++) {
                var k = 0
                while (k < data.length) {

                    if (labels.length < data.length) {

                        labels.push(data[k].periodo);
                    }
                    final[i].push(data[k][obkey[i]]);
                    k++;
                }
            };
            var objecfinal = {
                labels: labels,
                legend: obkey,
                series: obkey,
                data: final
            }
            return objecfinal;
        },

        makedashboard3: function(data) {

            var series = Object.keys(data[0]);
            series.splice(0, 2);

            var label = [];

            var arr = this.Create2DArray(series.length);


            for (var i = 0; i < arr.length; i++) {

                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        var strData = data[k].servicio.toString();
                        label.push(strData);
                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }



            var obj = {
                series: series,
                data: arr,
                labels: label


            }


            return obj

        },

        makedashboard18: function (data) {

            var series = Object.keys(data[0]);
            series.splice(0, 2);

            var label = [];

            var arr = this.Create2DArray(series.length);


            for (var i = 0; i < arr.length; i++) {

                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        var strData = data[k].servicio.toString();
                        label.push(strData);
                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }



            var obj = {
                series: series,
                data: arr,
                labels: label


            }


            return obj
        },

        makedashboard4: function(data) {

            var series = Object.keys(data[0]);
            series.splice(0, 3);

            var label = [];

            var arr = this.Create2DArray(series.length);


            for (var i = 0; i < arr.length; i++) {

                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        var strData = data[k].periodo.toString();
                        label.push(strData);

                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }



            var obj = {
                series: series,
                data: arr,
                labels: label


            }


            return obj

        },

        makedashboard19: function (data) {

            var series = Object.keys(data[0]);
            series.splice(0, 3);

            var label = [];

            var arr = this.Create2DArray(series.length);


            for (var i = 0; i < arr.length; i++) {

                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        var strData = data[k].periodo.toString();
                        label.push(strData);

                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }



            var obj = {
                series: series,
                data: arr,
                labels: label


            }


            return obj

        },

        makedashboard5: function(data) {


            var series = Object.keys(data[0]);

            series.splice(0, 2);
            series.splice(5, 0);

            var arr = this.Create2DArray(series.length);
            var label = [];


            for (var i = 0; i < arr.length; i++) {

                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        var strData = data[k].periodo.toString();
                        var lab = (strData.indexOf('2016') === -1 ? strData.concat("-", data[k].año) : strData);

                        label.push(lab);

                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }



            var obj = {
                series: series,
                data: arr,
                labels: label


            }

            return obj

        },

        makedashboard6: function(data) {


            var series = Object.keys(data[0]);
            series.splice(0, 3);


            var arr = this.Create2DArray(series.length);
            var label = [];


            for (var i = 0; i < arr.length; i++) {

                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        var strData = data[k].periodo.toString();
                        var lab = (strData.indexOf('2016') === -1 ? strData.concat("-", data[k].año) : strData);

                        label.push(lab);

                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }


            var obj = {
                series: series,
                data: arr,
                labels: label


            }

            return obj

        },

        makedashboard7: function(arreglo) {
            var arr = this.Create2DArray(arreglo.length);
            var estados = [];
            var data = [];

            var obkey = Object.keys(arreglo[0]);
            var ln = obkey.length;
            var oi = [];

            var int_acumulaIncAt = 0;
            var int_acumulaReqAt = 0;
            var int_acumulaIncRes = 0;
            var int_acumulaReqRes = 0;

            for (var hi = 0; hi < arreglo.length; hi++) {
                if (arreglo[hi][obkey[0]] === 'S' && arreglo[hi][obkey[1]] === 'Incidente')     { int_acumulaIncAt = int_acumulaIncAt + arreglo[hi][obkey[3]] }
                if (arreglo[hi][obkey[0]] === 'S' && arreglo[hi][obkey[1]] === 'Requerimiento') { int_acumulaReqAt = int_acumulaReqAt + arreglo[hi][obkey[3]] }
                if (arreglo[hi][obkey[0]] === 'R' && arreglo[hi][obkey[1]] === 'Incidente')     { int_acumulaIncRes = int_acumulaIncRes + arreglo[hi][obkey[3]] }
                if (arreglo[hi][obkey[0]] === 'R' && arreglo[hi][obkey[1]] === 'Requerimiento') { int_acumulaReqRes = int_acumulaReqRes + arreglo[hi][obkey[3]] }
            }


            function validarreglo(hi) {

                var value = 0;
                for (var i = 0; i < arreglo.length; i++) {

                    if (arreglo[i][obkey[hi]] === 0) {

                        value += 1
                    }
                }

                if (arreglo.length === 1) {

                    if (value === 1) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    if (value === 2) {
                        return true;
                    } else {
                        return false;
                    }


                }
            }

            for (var hi = 0; hi < ln; hi++) {

                if (!isNaN(arreglo[0][obkey[hi]])) {

                    var bool = validarreglo(hi);
                    if (bool) {} else {
                        oi.push([obkey[hi]]);
                        var j = 0
                        while (j < arreglo.length) {
                            arr[j].push(arreglo[j][obkey[hi]]);
                            j++
                        }

                    }
                } else {

                    var k = 0
                    while (k < arreglo.length) {
                        estados.push(arreglo[k][obkey[hi]]);
                        k++;
                    }
                }

            }


            var obj = {
                labels: oi,
                series: estados,
                data: arr,
                IncAt: int_acumulaIncAt,
                ReqAt: int_acumulaReqAt,
                IncRes: int_acumulaIncRes,
                ReqRes: int_acumulaReqRes
            };
            

            return obj;
        },

        makedashboard8: function(data) {


            var series = Object.keys(data[0]);
                series.splice(0, 3);

            var arr = this.Create2DArray(series.length);
            var label = [];


            for (var i = 0; i < arr.length; i++) {

                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        var strData = data[k].periodo.toString();
                        var lab = strData;

                        label.push(lab);

                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }
            var obj = {
                series: series,
                data: arr,
                labels: label


            }



            return obj

        },

        makedashboard9: function(data) {
            var series = Object.keys(data[0]);
            series.splice(0, 1);
            var label = [];
            var arr = this.Create2DArray(series.length);
            var nuevo = []
            for (var i = 0; i < arr.length; i++) {
                var k = 0
                while (k < data.length) {
                    if (label.length < data.length) {
                        //var strData = data[k].periodo.toString();
                        //var lab = (strData.indexOf('2016') === -1 ? strData.concat("-", data[k].año) : strData);
                        var lab = series[i]
                        label.push(lab);
                    }
                    //arr[i].push(data[k][series[i]]);
                    nuevo = nuevo.concat(data[k][series[i]]);
                    k++;
                }
            }
            var obj = {
                series: [''],
                data: nuevo,
                labels: series
            }
            return obj
        },

        makedashboard10: function(data) {
            var series = Object.keys(data[0]);
            series.splice(0, 1);
            var label = [];
            var arr = this.Create2DArray(series.length);         
            var nuevo = []
            for (var i = 0; i < arr.length; i++) {
                var k = 0
                while (k < data.length) {
                    if (label.length < data.length) {
                        // var strData = data[k].periodo.toString();
                        //var lab = (strData.indexOf('2016') === -1 ? strData.concat("-", data[k].año) : strData);
                        var lab = series[i]
                        label.push(lab);
                    }
                    // arr[i].push(data[k][series[i]]);
                    nuevo = nuevo.concat(data[k][series[i]]);
                    k++;
                }
            }
            var obj = {
                series: [''],
                data: nuevo,
                labels: series
            }

            return obj
        },

        makedashboard11: function(data) {

            var series = Object.keys(data[0]);
            series.splice(0, 1);

            var label = [];

            var arr = this.Create2DArray(series.length);



            for (var i = 0; i < arr.length; i++) {
                var k = 0
                while (k < data.length) {

                    if (label.length < data.length) {

                        //var strData = data[k].periodo.toString();
                        //var lab = (strData.indexOf('2016') === -1 ? strData.concat("-", data[k].año) : strData);
                        var lab = series[k]

                        label.push(lab);

                    }
                    arr[i].push(data[k][series[i]]);
                    k++;
                }
            }



            var obj = {
                series: series,
                data: arr,
                labels: ['']


            }


            return obj

        },

        makedashboard12: function(arreglo) {
            var arr = this.Create2DArray(arreglo.length);
            var estados = [];
            var data = [];

            var obkey = Object.keys(arreglo[0]);
            var ln = obkey.length;
            var oi = [];
            //obkey.splice(0, 1);
            //console.log(arreglo[0][obkey[0]])

            function validarreglo(hi) {

                var value = 0;
                for (var i = 0; i < arreglo.length; i++) {

                    if (arreglo[i][obkey[hi]] === 0) {

                        value += 1
                    }
                }

                if (arreglo.length === 1) {

                    if (value === 1) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    if (value === 2) {
                        return true;
                    } else {
                        return false;
                    }


                }
            }

            for (var hi = 0; hi < ln; hi++) {

                if (!isNaN(arreglo[0][obkey[hi]])) {

                    var bool = validarreglo(hi);
                    if (bool) {} else {
                        oi.push([obkey[hi]]);
                        var j = 0
                        while (j < arreglo.length) {
                            arr[j].push(arreglo[j][obkey[hi]]);
                            j++
                        }

                    }
                } else {

                    var k = 0
                    while (k < arreglo.length) {
                        estados.push(arreglo[k][obkey[hi]]);
                        k++;
                    }
                }

            }


            var obj = {
                labels: oi,
                series: estados,
                data: arr
            };
            return obj;
        },

        makedashboard13: function (arreglo) {
            var arr = this.Create2DArray(arreglo.length);
            var estados = [];
            var data = [];

            var obkey = Object.keys(arreglo[0]);
            var ln = obkey.length;
            var oi = [];
            //obkey.splice(0, 1);
            //console.log(arreglo[0][obkey[0]])

            function validarreglo(hi) {

                var value = 0;
                for (var i = 0; i < arreglo.length; i++) {

                    if (arreglo[i][obkey[hi]] === 0) {

                        value += 1
                    }
                }

                if (arreglo.length === 1) {

                    if (value === 1) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    if (value === 2) {
                        return true;
                    } else {
                        return false;
                    }


                }
            }

            for (var hi = 0; hi < ln; hi++) {

                if (!isNaN(arreglo[0][obkey[hi]])) {

                    var bool = validarreglo(hi);
                    if (bool) { } else {
                        oi.push([obkey[hi]]);
                        var j = 0
                        while (j < arreglo.length) {
                            arr[j].push(arreglo[j][obkey[hi]]);
                            j++
                        }

                    }
                } else {

                    var k = 0
                    while (k < arreglo.length) {
                        estados.push(arreglo[k][obkey[hi]]);
                        k++;
                    }
                }

            }


            var obj = {
                labels: oi,
                series: estados,
                data: arr
            };



            return obj;
        },

        makedashboard21: function (data) {

            var obkey = Object.keys(data[0]);
            var labels = [];
            obkey.splice(0, 4);
            var ln = obkey.length;
            var final = this.Create2DArray(ln);
            var labels2 = [];
            var data2 = [];
            var int_total = 0;
            var int_cumplimineto = 0;
            var int_incumplimiento = 0;
            var cumplimiento = [];
            var incumplimiento = [];
           
            for (var i = 0; i < data.length; i++) {

                int_cumplimineto = (data[i].cumplio * 100) / data[i].total;
                int_incumplimiento = 100 - int_cumplimineto;
                cumplimiento.push(parseFloat(int_cumplimineto).toFixed(2));
                incumplimiento.push(parseFloat(int_incumplimiento).toFixed(2));
            }

            
            data2.push(cumplimiento);
            data2.push(incumplimiento);

            console.log(data2);
            labels2.push('Cumplimiento');
            labels2.push('Inaceptable');

            for (var i = 0; i < ln; i++) {

                var k = 0
                while (k < data.length) {

                    if (labels.length < data.length) {

                        labels.push(data[k].mes);
                    }
                    final[i].push(data[k][obkey[i]]);
                    k++;
                }

            };

            var objecfinal = {

                labels: labels,
                legend: obkey,
                series: obkey,
                data: final,
                labels2: labels2,
                data2: data2
            }

            return objecfinal;

        },

        makedashboard22: function (data) {

            var obkey = Object.keys(data[0]);
            var labels = [];
            obkey.splice(0, 4);
            var ln = obkey.length;
            var final = this.Create2DArray(ln);
            var labels2 = [];
            var data2 = [];
            var int_total = 0;
            var int_cumplimineto = 0;
            var int_incumplimiento = 0;
            var cumplimiento = [];
            var incumplimiento = [];

            for (var i = 0; i < data.length; i++) {

                int_cumplimineto = (data[i].cumplio * 100) / data[i].total;
                int_incumplimiento = 100 - int_cumplimineto;
                cumplimiento.push(parseFloat(int_cumplimineto).toFixed(2));
                incumplimiento.push(parseFloat(int_incumplimiento).toFixed(2));
            }


            data2.push(cumplimiento);
            data2.push(incumplimiento);

            console.log(data2);
            labels2.push('Cumplimiento');
            labels2.push('Inaceptable');

            for (var i = 0; i < ln; i++) {

                var k = 0
                while (k < data.length) {

                    if (labels.length < data.length) {

                        labels.push(data[k].mes);
                    }
                    final[i].push(data[k][obkey[i]]);
                    k++;
                }

            };

            var objecfinal = {

                labels: labels,
                legend: obkey,
                series: obkey,
                data: final,
                labels2: labels2,
                data2: data2
            }

            return objecfinal;

        },

        indexOfespecial: function(data) {

            console.log("IndexOfEspecial");
			console.log(data);console.log($rootScope.useri);
            var index = -1;
            for (var i = 0; i < data.length; i++) {

                if (data[i].eM_ID === $rootScope.useri.empresa_id) {  //data[i].eM_ID === $rootScope.useri.empresa_id
                    console.log(data[i]);
                    index = i;
                    break;
                }
            }

            return index;

        },

        Create2DArray: function(rows) {
            var arr = [];

            for (var i = 0; i < rows; i++) {
                arr[i] = [];
            }

            return arr;
        }

    }

})
