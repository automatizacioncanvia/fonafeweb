﻿angular.module('masterGenerysV1App').factory('AD', function ($http, $rootScope) {


    var config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };


    return {

        operation: function (data) {       
            return $http.post('api/operation', data, config);
        },
      
        getuser: function (data) {

             return $http.post('api/execflow', data, config);
        },

        getFlowStatus :function(idejecucion,sentencia){

            return $http.get('api/getflowsummary/' + idejecucion + '/' + sentencia)
        },
        groupOperation: function (data) {

            return $http.post('api/operationgroup', data, config);
        },
        User:  function User (SID, Operacion, Empresa_id, UserName, UserApellido, UserSama, UserPassword, UserPath, UserOffice, UserTitle, UserDepartment, UserCompany, UserHomephone, UserMoibilePhone, UserOfficePhoen, UserEnable, UserExpirationDate, ColaboradorID, Info,atributos) {

                this.u_adid = SID;
                this.o_id = Operacion;
                this.em_id = Empresa_id;
                this.u_name = UserName;
                this.u_surname = UserApellido;
                this.u_samaname = UserSama,
                this.u_password = UserPassword,
                this.ou_path = UserPath,
                this.u_office = UserOffice,
                this.u_title = UserTitle,
                this.u_departament = UserDepartment,
                this.u_company = UserCompany,
                this.u_homephone = UserHomephone,
                this.u_mobilephone = UserMoibilePhone,
                this.u_officephone = UserOfficePhoen,
                this.u_enable = UserEnable,
                this.u_accountexpirationdate = UserExpirationDate,
                this.co_id = ColaboradorID,
                this.info = Info
                this.atributos = atributos;
            }

    }
})