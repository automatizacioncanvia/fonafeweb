﻿angular.module('masterGenerysV1App').factory('Apimenu', function ($http, $rootScope, Notification) {

    return {

        getimage: function () {
            console.log($rootScope.useri);
            $http.get("api/GetEmpresaId/" + $rootScope.useri.empresa_id).then(function (data) {
                var logo = data.data[0];
                $rootScope.urlimage = logo.eM_LOGO;
                $rootScope.nombreEmpresav = logo.eM_NOMBRE;
                return true;
            });
        },

        getmenu: function (vista,info) {
            console.log('getmenu');
	        console.log(vista+'   -   '+info);
            return $http.get("/api/menu/"+vista+"/"+info);
        },

        validroute: function (data) {

            var i = 0;
            var result = false;
			console.log($rootScope.menuItems);
            for(var j=0; j<$rootScope.menuItems.length;j++) {  
                if ($rootScope.menuItems[j].path === data) {
                    i++;
                }
            };

            if (i != 0) {
               result = true;
            }

            return result;
        }

    }
        
})