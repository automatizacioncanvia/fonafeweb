﻿angular.module('masterGenerysV1App').factory('Apiinventario', function ($http) {

    var config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    return {
        getOption: function (data) {
            //console.log('entre');
            return $http.post('api/ioption', data, config);
        },
        getinventario: function (data) {
            //console.log('entre');
            return $http.post('api/getinventario', data, config);
        },

        insertItem: function (data) {
            //console.log('entre');
            return $http.post('api/createitem', data, config);
        },
    }
})