﻿angular.module('masterGenerysV1App').factory('Auth', function ($http, $location, $rootScope, $window, Apimenu) {

    return {
        //Logueo
        login: function (inputs, config) {
            return $http.post('api/auth', inputs, config);
        },

        //Deslogueo
        logout: function () {
            localStorage.removeItem("Perfiles");
            localStorage.removeItem("Token");
            localStorage.removeItem("Respuesta");
            localStorage.removeItem("Usuario");
            $location.path("login");
            return true;
        },

        //Verificación de Logueo
        islogin: function () {
            if ((localStorage.getItem('Token') === null) ||
                localStorage.getItem('Token') in localStorage) {
                console.log('Error - Acceso Denegado');
                return false;
            }
            else {
                console.log('Ok - Acceso Concebido');
              
                return true;
            }        
        },
         setUserinfo: function () {

            var perfiles = JSON.parse($window.localStorage.getItem('Perfiles'));
            $rootScope.useri = {
                name: perfiles[0].CO_NOMBRE,
                userid: perfiles[0].CO_ID,
                rol: perfiles[0].PE_ID,
                empresa: perfiles[0].EM_NOMBRE,
                alcance: perfiles[0].AL_ID,
                empresa_id:perfiles[0].EM_ID
            };
            var respues = Apimenu.getimage();
            return respues;
           
        },
        removeUserinfo: function () {

            $rootScope.useri = null;

            return true;
        }
    }
});