﻿angular.module('masterGenerysV1App')
.controller('AutorCtrl', function ($timeout, $q, $log) {

    var self = this;

    self.simulateQuery = false;
    self.isDisabled = false;
    self.groups = loadAll();
    self.querySearch = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange = searchTextChange;
    self.newState = newState;




    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */

    function querySearch(query) {
        var results = query ? self.states.filter(createFilterFor(query)) : self.states,
            deferred;
        if (self.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        $log.info('Item changed to ' + JSON.stringify(item));
    }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
       
        var domains = [{ "id": 1, "display": "GMD" }, { "id": 2, "display": "FONAFEMV" }, { "id": 3, "display": "MIVIVIENDA" }, { "id": 4, "display": "DISTRILUZ" }]

        // return allStates.split(/, +/g).map( function (state) {
        //   return {
        //     value: state.toLowerCase(),
        //     display: state
        //   };
        // });


        return domains;


    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var uppercaseQuery = angular.uppercase(query);

        return function filterFn(state) {

            return (state.display.indexOf(uppercaseQuery) === 0);
        };

    }





        
})