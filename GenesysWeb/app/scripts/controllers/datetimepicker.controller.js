﻿
angular.module('masterGenerysV1App').controller('ModalNuevoInventario', function () {

    var vm = this;
    var d = new Date(2016, 0, 1);
    var n = moment();

    var today = new Date();


    vm.date = moment().add(-1, 'd');
    vm.date2 = moment();

    vm.fechaalta = moment().add(-1, 'd');
    vm.fechabaja = moment();

    vm.soportedesde = moment().add(-1, 'd');
    vm.soportehasta = moment();

    vm.garantiadesde = moment().add(-1, 'd');
    vm.garantiahasta = moment();
 

    //vm.options = { format: "YYYY/MM/DD HH:mm" };

    vm.update = function (date, date2) {
        vm.optionsFrom = { format: 'DD/MM/YYYY', maxDate: date2, minDate: d, toolbarPlacement: 'top' };
        vm.optionsTo = { format: 'DD/MM/YYYY', minDate: date, toolbarPlacement: 'top' };
        vm.options = { format: 'DD/MM/YYYY', toolbarPlacement: 'top' };
    };


    vm.update(vm.date, vm.date2);
    vm.update(vm.fechaalta, vm.fechabaja);
    vm.update(vm.soportedesde, vm.soportehasta);
    vm.update(vm.garantiadesde, vm.garantiahasta)
   

    vm.getTime = function () {
        alert('Selected time is:' + vm.date.format("YYYY/MM/DD"));
    };



    vm.addTime = function (val, selector) {
        vm.date = moment(vm.date.add(val, selector));
    };


});
