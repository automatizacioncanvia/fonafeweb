﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
           .state('login', {
               url: '/login',
               views: {
                   'login@': {
                       templateUrl: 'app/views/login/login.html',
                       controller: 'LoginController',
                       controllerAs: 'login',
                       authorize: false
                   }
               }
           });
});