﻿angular.module('masterGenerysV1App')
.filter('example', function () {

    return function (text) {

        if(text!==undefined){
            var final = text[0];
 
        return final;
        }
        else{

        return "";

        }
    }
});

angular.module('masterGenerysV1App')
.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

angular.module('masterGenerysV1App')
.directive('clickAndDisable', function () {
    return {
        scope: {
            clickAndDisable: '&'
        },
        link: function (scope, iElement, iAttrs) {
            iElement.bind('click', function () {
                iElement.prop('disabled', true);
                scope.clickAndDisable().finally(function () {
                    iElement.prop('disabled', false);
                })
            });
        }
    };
});