﻿angular.module('masterGenerysV1App')
    .controller('DashboardsCtrl', function ($scope, $state, Apilink, $rootScope, Apimenu, $parse,Notification, Apifonfe, $window, Apidashboard, $sce, $stateParams) {

        $scope.nodata0010 = false;
        $scope.nodata0011 = false;
        $scope.variablereporte2 = false;
 
        var comboanio = function () {
            var date = new Date,
                years = [],
                year = date.getFullYear();

            for (var i = year; i > year - 5; i--) {
                years.push(i);
            }

            return years;
        };

        $scope.years = comboanio();

        $scope.dash10 = "0010";
        $scope.dash11 = "0011";

        $scope.form = {};

        var today = moment();
        $scope.form.expirationdate = today;
        $scope.options = { format: 'DD/MM/YYYY', showClear: true, showTodayButton: true };

        var info = "" + $rootScope.useri.userid + "," + 4;
        
        var fail = function (serviceResp) {

            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
        }

        Apilink.getlink(2, info).then(function (serviceResp) {
            $scope.links = serviceResp.data.data;
        }, fail);

        Apimenu.getmenu(26, $rootScope.useri.userid).then(function (serviceResp) {
            $scope.empresas = serviceResp.data.data;
			console.log("26"); var myArr1 = [serviceResp.data.data, serviceResp.data];
                    console.log(myArr1);

        });

        $scope.openlink2 = function (seId, servicio) {

            $scope.tab = 1;

         

            var emId = $scope.form.selectedEmpresa1.eM_ID;
            $scope.EmpresaChange = true;
            $scope.idServicio = seId;
            $scope.nombreServicio = servicio;

            $scope.form.selectedEmpresa10 = { eM_ID: $scope.form.selectedEmpresa1.eM_ID, eM_NOMBRE: $scope.form.selectedEmpresa1.eM_NOMBRE };

            $scope.form.selectedEmpresa11 = { eM_ID: $scope.form.selectedEmpresa1.eM_ID, eM_NOMBRE: $scope.form.selectedEmpresa1.eM_NOMBRE };
            

            var fecha = new Date();
            $scope.form.years = fecha.getFullYear();

            if (servicio == "Incidentes" || servicio == "Requerimientos") {

                var vtipo;
                if (servicio == "Incidentes") {
                    vtipo = "I";
                } else {
                    vtipo = "R";
                }
                $state.go('indicadoresDash', { idServicio: $scope.idServicio, nombreServicio: $scope.nombreServicio, idEmpresa: $scope.form.selectedEmpresa1.eM_ID, nombreEmpresa: $scope.form.selectedEmpresa1.eM_NOMBRE, tipo: vtipo, atendido: 0 });

            } else {
			
         
                Apimenu.getmenu(23, 'R,Atendidos,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.REatendidosNro = serviceResp.data.data[0].cantidad;  console.log("Apiget");var myArr1 = [seId, emId, $scope];
                    console.log(myArr1);
                }, fail);

                Apimenu.getmenu(23, 'R,Pendientes,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.REpendientesNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'I,Atendidos,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.INatendidosNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'I,Pendientes,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.INpendientesNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(24, seId + "," + emId).then(function (serviceResp) {
                    var emId = $scope.form.selectedEmpresa1.eM_ID;
                    var links = serviceResp.data.data;
                    var linksArreglo = [];
                    $scope.link4 = links;

                    angular.forEach(links, function (linki, key) {
                        //this.push(key + ': ' + value);
                        var body = JSON.stringify({
                            token: localStorage.getItem('Token').toString(),
                            url: linki.link,
                            idUsuario: emId
                        }
                        )

                        if (linki.flag == 0) {
                            
                            Apifonfe.loginswrt(body).success(function (data) {
                                console.log("carga links 2")
                                console.log(data);
                                //var demo = data + "&output=embed";
                                //console.log(demo);
                                linksArreglo.push({
                                    orden: linki.orden,
                                    link: data, flag: linki.flag
                                });
                                debugger;
                            });
                        } else {

                            if (linki.link == 3) {
                                linksArreglo.push({
                                    orden: linki.orden,
                                    link: getAbsolutePath() + "app/views/components/dashboard/Capacidad.html?idEmpresa=" + emId,
                                    flag: 3
                                });

                            } else {
                                linksArreglo.push({
                                    orden: linki.orden,
                                    link: linki.link,
                                    flag: linki.flag
                                });
                            }

                        }


                    });


                    $scope.links = linksArreglo;

                }, fail);

                //$("#theArt").hide();
                $("#panelDetalle").show();
            }
        }

        $scope.cargarDash = function (emId) {
            debugger;

            if (emId.eM_ID == 33 || emId.eM_ID == 61) {

                $rootScope.visibledash = 0;
            } else {
                $rootScope.visibledash = 1;
            }
			console.log("A14");
            //console.log(emId.eM_ID);
            Apimenu.getmenu(14, $rootScope.useri.userid + "," + emId.eM_ID).then(function (serviceResp) {
                debugger;
                $scope.dash = serviceResp.data.data;
                if ($scope.dash.length!==0) {
                    $scope.openlink2($scope.dash[0].sE_ID, $scope.dash[0].sE_NOMBRE);
                    $("#panelDetalle").show();
                } else {
                    Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se encontraron servicios asociados.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                    $("#panelDetalle").hide();
                }
              
          
            }, fail);
        };
console.log("A26");
        Apimenu.getmenu(26, $rootScope.useri.userid).then(function (serviceResp) {
            var index = Apidashboard.indexOfespecial(serviceResp.data.data);
            if (index !== -1) {
                if ($stateParams.activar == undefined)
                {
                    $scope.form.selectedEmpresa1 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa10 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa11 = serviceResp.data.data[index];
                }
                else
                {
                    $scope.form.selectedEmpresa1 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa10 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa11 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    if ($stateParams.atendido != 0) { $scope.EmpresaChange = true; }
                }
                $scope.cargarDash($scope.form.selectedEmpresa1);
            }
            $scope.empresas = serviceResp.data.data;
        }, fail);
console.log("A06");
        $scope.getsection = function (idser) {
            Apimenu.getmenu(6, $rootScope.useri.userid + "," + idser).then(function (serviceResp) {
                $scope.sec = serviceResp.data.data;
                $scope.linkserv = serviceResp.data.data1;

            }, fail);
        };
console.log("A07");
        $scope.getlink = function (idlink, idservicio) {
            Apimenu.getmenu(7, idlink).then(function (serviceResp) {
                $scope.subsection = serviceResp.data.data;
            }, fail);
            Apimenu.getmenu(8, idlink + "," + $rootScope.useri.empresa_id + "," + idservicio).then(function (serviceResp) {
                $scope.links = serviceResp.data.data;
            }, fail);
        };

        $scope.getlink2 = function (idlink, idservicio) {
            
            Apimenu.getmenu(8, idlink + "," + $rootScope.useri.empresa_id + "," + idservicio).then(function (serviceResp) {

                $scope.links2 = serviceResp.data;

            }, fail);

        }

        $scope.expandCallback = function (index, id) {
            $scope.getsection(id);
        };

        $scope.expandCallback2 = function (index, id, servicio) {
            $scope.getlink(id, servicio);
        }

        $scope.expandCallback3 = function (index, id, servicio) {
            $scope.getlink2(id, servicio);
        }

        $scope.cargarAtendido = function (tipo) {
            
            //alert("atendido");
            var vtipo;
            if (tipo == 1) {
                vtipo = "R";
            } else {
                vtipo = "I";
            }

            $state.go('indicadoresDash', { idServicio: $scope.idServicio, nombreServicio: $scope.nombreServicio, idEmpresa: $scope.form.selectedEmpresa1.eM_ID, nombreEmpresa: $scope.form.selectedEmpresa1.eM_NOMBRE, tipo: vtipo, atendido: 1 });
        }

        $scope.cargarPendiente = function (tipo) {
            
            //alert("pendiente");
            var vtipo;
            if (tipo == 1) {
                vtipo = "R";
            } else {
                vtipo = "I";
            }
            $state.go('indicadoresDash', { idServicio: $scope.idServicio, nombreServicio: $scope.nombreServicio, idEmpresa: $scope.form.selectedEmpresa1.eM_ID, nombreEmpresa: $scope.form.selectedEmpresa1.eM_NOMBRE, tipo: vtipo, atendido: 2 });
        }

        $scope.openlink3 = function (url) {
            $scope.EmpresaChange = false;
            $("#theArt").show();
            //$("#panelDetalle").hide();
        }

        $scope.isSelected = function (checktab) {
            return $scope.tab === checktab;
        };

        $scope.selectTab = function (setTab, link) {
            //alert(link);

            if(link==1) {
                $scope.getchart(2, $scope.form.selectedEmpresa10.eM_ID, $scope.dash10, $scope.form.years);
            }

            if(link==2) {
                $scope.getchart(2, $scope.form.selectedEmpresa11.eM_ID, $scope.dash11, $scope.form.expirationdate);
            }
            
            $scope.tab = setTab;

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/chart2.png",
                backgroundColor: 'rgba(41, 127, 130, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando Dashboard...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

            });
console.log("A24");
            //probando lineas borrar miguel
            Apimenu.getmenu(24, $scope.idServicio + "," + $scope.form.selectedEmpresa1.eM_ID).then(function (serviceResp) {
                var emId = $scope.form.selectedEmpresa1.eM_ID;
                var links = serviceResp.data.data;
                var linksArreglo = [];
                $scope.link4 = links;

                angular.forEach(links, function (linki, key) {
                    //this.push(key + ': ' + value);
                    var body = JSON.stringify({
                        token: localStorage.getItem('Token').toString(),
                        url: linki.link,
                        idUsuario: emId
                    }
                    )

                    if (linki.flag == 0) {
                        
                        Apifonfe.loginswrt(body).success(function (data) {
                            console.log("carga links 3")
                            console.log(data);
                            linksArreglo.push({
                                orden: linki.orden,
                                link: data, flag: linki.flag
                            });
                            debugger;
                        });
                    } else {

                        if (linki.link == 3) {
                            linksArreglo.push({
                                orden: linki.orden,
                                link: getAbsolutePath() + "app/views/components/dashboard/Capacidad.html?idEmpresa=" + emId,
                                flag: 3
                            });

                        } else {
                            linksArreglo.push({
                                orden: linki.orden,
                                link: linki.link,
                                flag: linki.flag
                            });
                        }

                    }


                });


                $scope.links = linksArreglo;

                $window.loading_screen.finish();

            }, fail);




        };

      

        function getAbsolutePath() {
            var loc = window.location;
            var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
            return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
        }

        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        }

        $scope.openlink = function (url) {
            var dati = {
                token: localStorage.getItem('Token').toString(),
                url: url,
                idUsuario: $scope.empresas.data.eM_ID
            };

            //debugger;
            var body = JSON.stringify(dati);
            Apifonfe.loginswrt(body).success(function (data) {
                //debugger;
                console.log("carga links")
                console.log(data);
                console.log(url);
                if (url.substring(0, 1) == "h") {

                    $window.open(data, 'Open', 'width=1500,height=700');
                } else {
                    $window.open(url, 'Open', 'width=1500,height=700');
                }
            })
                .error(function (data, status, header, config) {
                    Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                });
        }

        if ($stateParams.activar != undefined) {

            //alert("prueba");

            var emId = $stateParams.idEmpresa;
            var seId = $stateParams.idServicio;

            $scope.idServicio = $stateParams.idServicio;
            $scope.nombreServicio = $stateParams.nombreServicio;

            $scope.form.selectedEmpresa1 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };

            $scope.form.selectedEmpresa10 = { eM_ID: $scope.form.selectedEmpresa1.eM_ID, eM_NOMBRE: $scope.form.selectedEmpresa1.eM_NOMBRE };

            $scope.form.selectedEmpresa11 = { eM_ID: $scope.form.selectedEmpresa1.eM_ID, eM_NOMBRE: $scope.form.selectedEmpresa1.eM_NOMBRE };

            var fecha = new Date();
            $scope.form.years = fecha.getFullYear();


            if ($stateParams.atendido == 0) {


            } else {

                Apimenu.getmenu(23, 'R,Atendidos,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.REatendidosNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'R,Pendientes,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.REpendientesNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'I,Atendidos,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.INatendidosNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'I,Pendientes,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.INpendientesNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(24, seId + "," + emId).then(function (serviceResp) {
                    var emId = $scope.form.selectedEmpresa1.eM_ID;
                    //$scope.links = serviceResp.data.data;
                    var links = serviceResp.data.data;
                    var linksArreglo = [];

                    $scope.link4 = links;
                    angular.forEach(links, function (linki, key) {
                        //this.push(key + ': ' + value);
                        var body = JSON.stringify({
                            token: localStorage.getItem('Token').toString(),
                            url: linki.link,
                            idUsuario: emId
                        }
                        )

                        if (linki.flag == 0) {
                            
                            Apifonfe.loginswrt(body).success(function (data) {
                                console.log("carga links 4")
                                console.log(data);
                                linksArreglo.push({
                                    orden: linki.orden,
                                    link: data, flag: linki.flag
                                });
                                debugger;
                            });
                        } else {

                            linksArreglo.push({
                                orden: linki.orden,
                                link: linki.link,
                                flag: linki.flag
                            });
                        }
                      
                    });

                    //$scope.$broadcast('linkArreglo', linksArreglo);
                    
                    $scope.links = linksArreglo;

                }, fail);

                //$("#theArt").hide();
                $("#panelDetalle").show();
            }
        }

        //metodos globales
        $scope.getchart = function (periodo, em, idchart, servicio, prioridad, sla, estado) {
            if (periodo !== 1) {
                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/chart2.png",
                    backgroundColor: 'rgba(41, 127, 130, 0.79)',
                    loadingHtml: "<p class='loading-message'>Cargando indicador...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
                });

                if (em == undefined) {
                    em = -1;
                }

                if (idchart === '0010' ) {
                    periodo = servicio;
                    $scope.anio = servicio;
                    $scope.empresa = em;
                }
                if (idchart === '0011') {
                    periodo = moment(servicio).format("DD/MM/YYYY");
                    $scope.anio = periodo;
                    servicio = '-1';
                    $scope.empresa = em;
                }


                Apidashboard.getdashi(periodo, em, idchart, servicio, prioridad, sla, estado).then(function (serviceResp) {

                    var the_string = "nodata" + idchart;
                    var model = $parse(the_string);

                    if (serviceResp.data.length > 0) {

                        if (idchart === "0010") {
                            $scope.value10 = false;
                            $scope.periodo10 = periodo;
                            $scope.ff10 = serviceResp.data;
                            $scope.fullkeys10 = Object.keys(serviceResp.data[0]);
                            var obj10 = Apidashboard.makedashboard10(serviceResp.data);
                            $scope.$broadcast('builDash10', obj10);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0011") {
                            debugger;
                            $scope.value11 = false;
                            $scope.periodo11 = periodo;
                            $scope.ff11 = serviceResp.data;
                            $scope.fullkeys11 = Object.keys(serviceResp.data[0]);
                            var obj11 = Apidashboard.makedashboard10(serviceResp.data);
                            $scope.$broadcast('builDash11', obj11);
                            $window.loading_screen.finish();
                        }
                    } else {
                        model.assign($scope, true);
                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $window.loading_screen.finish();
                    }

                }, fail)
            } else {
                if (idchart === "0010") {
                    $scope.periodo10 = periodo;
                    $scope.value10 = true;
                }
                if (idchart === "0011") {
                    $scope.periodo11 = periodo;
                    $scope.value11 = true;
                }
            }
        };

    })

.config(function ($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function (date) {
        return moment(date).format('DD-MM-YYYY');
    };
});

angular.module('masterGenerysV1App')
    .controller("reporte10", ['$scope', '$http', '$uibModal', function ($scope, $http, $uibModal) {

        $scope.$on('builDash10', function (key, data) {
            $scope.labels = data.labels;
            $scope.data = data.data;
            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                legend: { display: false }
            };

        });

        $scope.onClick = function (points, evt) {

            $scope.empresa;
            $scope.anio;
            //$scope.label = points[0]._model.label;
            console.log("empresa: " + $scope.empresa + " año: " + $scope.anio + " mes: " + $scope.label);

            var valor = $scope.empresa + ',' + $scope.anio + ',' + $scope.label;

            $http.get("api/fogettiemposap/" + valor).then(function (serviceResp) {

                $scope.tiemposap = serviceResp.data;
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/ExtensionView/DashTiempoSAP/modals/tiemposap/tiemposap.html',
                    controller: 'ModalTiempoSAP',
                    size: 'md',
                    resolve: {
                        tiemposap: function () {
                            var array = [];
                            array.push($scope.tiemposap);
                            return array;
                        }
                    }
                });
            });

        };

    }]);

angular.module('masterGenerysV1App')
    .controller("reporte20", ['$scope', '$http', '$uibModal', 'Apimenu', '$window', '$rootScope', 'Notification', function ($scope, $http, $uibModal, Apimenu, $window, $rootScope, Notification) {
        $scope.$on('builDash10', function (key, data) {
            $scope.labels = data.labels;
            $scope.data = data.data;
            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                legend: { display: false }
            };

        });

        var vm = this;
        
        vm.CurrentDate = new Date();
        vm.CurrentDate.setDate(vm.CurrentDate.getDate() - 1);

        vm.promedioSAP = promedioSAP;
        vm.formateaFecha = formateaFecha;
        vm.SetearFecha = SetearFecha;
        $rootScope.trueorfalse = vm.intervaloinicio;

        vm.dialogoDetail = dialogoDetail;
        
        
        function dialogoDetail(number) {
            debugger;

            vm.number = number;
            var valor = $rootScope.valorCapturado + ';' + vm.number;

          

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/chart2.png",
                backgroundColor: 'rgba(41, 127, 130, 0.79)',
                loadingHtml: "<p class='loading-message'>Obteniendo detalle...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            Apimenu.getmenu(34, valor).then(function (serviceResp) {
                vm.detalle = serviceResp.data.data;
             


                if (vm.number == 1) {
                    $scope.dialogo(vm.detalle, 1);
                } else if (vm.number == 2) {
                    $scope.dialogosinz(vm.detalle, 2);
                } else if (vm.number == 3) {
                    $scope.total(vm.detalle, 3);
                } else if (vm.number == 4) {
                    $scope.totalsinz(vm.detalle, 4);
                } else if (vm.number == 5) {
                    $scope.dialogo2(vm.detalle, 5);
                } else if (vm.number == 6) {
                    $scope.dialogosinz2(vm.detalle, 6);
                } else if (vm.number == 7) {
                    $scope.total2(vm.detalle, 7);
                } else if (vm.number == 8) {
                    $scope.totalsinz2(vm.detalle, 8);
                }

                $window.loading_screen.finish();

            }, fail)

           
            

            var fail = function (serviceResp) {

                Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                $window.loading_screen.finish();
            }


            }





          
        $scope.dialogo = function (detalle,number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/dialogo.html',
                controller: 'ModalDialogo',
                controllerAs: '$dialogo',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }

        $scope.dialogosinz = function (detalle, number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/dialogosinz.html',
                controller: 'ModalDialogosinz',
                controllerAs: '$dialogosinz',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }

        $scope.total = function (detalle, number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/totales.html',
                controller: 'ModalTotales',
                controllerAs: '$totales',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }

        $scope.totalsinz = function (detalle, number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/totalessinz.html',
                controller: 'ModalTotalessinz',
                controllerAs: '$totalessinz',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }


        //---------------------------------------------------------------------------------------------
        $scope.dialogo2 = function (detalle, number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/dialogo2.html',
                controller: 'ModalDialogo2',
                controllerAs: '$dialogo2',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }

        $scope.dialogosinz2 = function (detalle, number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/dialogosinz2.html',
                controller: 'ModalDialogosinz2',
                controllerAs: '$dialogosinz2',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }

        $scope.total2 = function (detalle, number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/totales2.html',
                controller: 'ModalTotales2',
                controllerAs: '$totales2',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }

        $scope.totalsinz2 = function (detalle, number) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/dashboard/modals/totalessinz2.html',
                controller: 'ModalTotalessinz2',
                controllerAs: '$totalessinz2',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push(detalle);
                        array.push(number);


                        return array;
                    }
                }
            });


        }


       





       vm.intervalos = [{ ID: '00', NOMBRE: '00 hr' },
                        { ID: '01', NOMBRE: '01 hr' },
                        { ID: '02', NOMBRE: '02 hr' },
                        { ID: '03', NOMBRE: '03 hr' },
                        { ID: '04', NOMBRE: '04 hr' },
                        { ID: '05', NOMBRE: '05 hr' },
                        { ID: '06', NOMBRE: '06 hr' },
                        { ID: '07', NOMBRE: '07 hr' },
                        { ID: '08', NOMBRE: '08 hr' },
                        { ID: '09', NOMBRE: '09 hr' },
                        { ID: '10', NOMBRE: '10 hr' },
                        { ID: '11', NOMBRE: '11 hr' },
                        { ID: '12', NOMBRE: '12 hr' },
                        { ID: '13', NOMBRE: '13 hr' },
                        { ID: '14', NOMBRE: '14 hr' },
                        { ID: '15', NOMBRE: '15 hr' },
                        { ID: '16', NOMBRE: '16 hr' },
                        { ID: '17', NOMBRE: '17 hr' },
                        { ID: '18', NOMBRE: '18 hr' },
                        { ID: '19', NOMBRE: '19 hr' },
                        { ID: '20', NOMBRE: '20 hr' },
                        { ID: '21', NOMBRE: '21 hr' },
                        { ID: '22', NOMBRE: '22 hr' },
                        { ID: '23', NOMBRE: '23 hr' },
                        { ID: '24', NOMBRE: '24 hr' }];

       function formateaFecha(fecha) {
           var date = new Date();
           date = fecha;
           var dateParts = date.split("-");
           var fechaformateada = dateParts[1] + ',' + dateParts[2].replace('T00:00:00', '') + ',' + dateParts[0];
           return fechaformateada;
       }

       function SetearFecha(fecha) {
           //Ejemplo
           //Wed Feb 01 2017 00:00:00 GMT-0500 (SA Pacific Standard Time)        
           fecha = fecha.toString();
           var dia = fecha.split(' ')[2];
           var meslipio = '';
           var messucio = '';
           messucio = fecha.split(' ')[1];
           var anio = fecha.split(' ')[3];
           if (messucio === 'Jan') { meslipio = '01'; }
           else if (messucio === 'Feb') { meslipio = '02'; }
           else if (messucio === 'Mar') { meslipio = '03'; }
           else if (messucio === 'Apr') { meslipio = '04'; }
           else if (messucio === 'May') { meslipio = '05'; }
           else if (messucio === 'Jun') { meslipio = '06'; }
           else if (messucio === 'Jul') { meslipio = '07'; }
           else if (messucio === 'Aug') { meslipio = '08'; }
           else if (messucio === 'Sep') { meslipio = '09'; }
           else if (messucio === 'Oct') { meslipio = '10'; }
           else if (messucio === 'Nov') { meslipio = '11'; }
           else if (messucio === 'Dec') { meslipio = '12'; }

           //console.log(dia);
           //console.log(messucio);
           //console.log(meslipio);
           //console.log(anio);        
           return anio + '-' + meslipio + '-' + dia;

       }

       function promedioSAP(fecini, fecfin, horaini, horafin, empresa) {
           
       
           debugger;


           
           var hi = horaini.replace(" hr", "");
           var hf = horafin.replace(" hr", "");
        
           $window.loading_screen = $window.pleaseWait({
               logo: "app/images/chart2.png",
               backgroundColor: 'rgba(41, 127, 130, 0.79)',
               loadingHtml: "<p class='loading-message'>Calculando Tiempo Promedio...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
           });


           vm.fechainicio = fecini;
           vm.fechafin = fecfin;
           

           vm.horario = 'desde ' + horaini + ' hasta ' + horafin;
           vm.horario2 = 'desde ' + horafin + ' hasta ' + horaini;
          

           var valor = SetearFecha(fecini) + ';' + SetearFecha(fecfin) + ';' + hi + ';' + hf + ';' + empresa.eM_ID;

           $rootScope.valorCapturado = valor;

       
           Apimenu.getmenu(33, valor).then(function (serviceResp) {
               debugger;
               if (serviceResp.data.data[0].respuesta == 'false') {
                   $window.loading_screen.finish();
                   Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.data.data[0].mensaje + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });


               } else {
                   vm.promedioSAPCalculado = serviceResp.data.data;

                   //-----------------horaini<horafin

                   vm.nropasosdialogo = vm.promedioSAPCalculado[0].nropasosdialogo;
                   vm.tiempodialogo = vm.promedioSAPCalculado[0].tiempodialogo;

                   vm.nropadosdialogo_sz = vm.promedioSAPCalculado[0].nropadosdialogO_SZ;
                   vm.tiempodialogo_sz = vm.promedioSAPCalculado[0].tiempodialogO_SZ;

                   vm.nropasostotales = vm.promedioSAPCalculado[0].nropasostotales;
                   vm.tiempototal = vm.promedioSAPCalculado[0].tiempototal;

                   vm.nropasostotales_sz = vm.promedioSAPCalculado[0].nropasostotaleS_SZ;
                   vm.tiempototal_sz = vm.promedioSAPCalculado[0].tiempototaL_SZ;

                   //------------------horaini>horafin

                   vm.nropasosdialogo2 = vm.promedioSAPCalculado[0].nropasosdialogO2;
                   vm.tiempodialogo2 = vm.promedioSAPCalculado[0].tiempodialogO2;

                   vm.nropadosdialogo_sz2 = vm.promedioSAPCalculado[0].nropadosdialogO_SZ2;
                   vm.tiempodialogo_sz2 = vm.promedioSAPCalculado[0].tiempodialogO_SZ2;

                   vm.nropasostotales2 = vm.promedioSAPCalculado[0].nropasostotaleS2;
                   vm.tiempototal2 = vm.promedioSAPCalculado[0].tiempototaL2;

                   vm.nropasostotales_sz2 = vm.promedioSAPCalculado[0].nropasostotaleS_SZ2;
                   vm.tiempototal_sz2 = vm.promedioSAPCalculado[0].tiempototaL_SZ2;

                   $window.loading_screen.finish();
               }

     

                 

               }, fail)

           
        
       

        

           var fail = function (serviceResp) {

               Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
               $window.loading_screen.finish();
           }


       }





    

    }]);


angular.module('masterGenerysV1App')
    .controller("Reporte11", ['$scope', function ($scope) {

        $scope.$on('builDash11', function (key, data) {

            $scope.labels = data.labels;
            $scope.series = data.series;
            $scope.legend = data.series;

            $scope.data = data.data;

            $scope.options = {
                legend: { display: false }
            };

        });

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

    }]);