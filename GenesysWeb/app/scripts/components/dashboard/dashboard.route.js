﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('dashboard', {
              parent: 'main',
              url: 'dashboard?idServicio&nombreServicio&idEmpresa&nombreEmpresa&atendido&activar',
              views: {
                  'content@main': {
                      templateUrl: 'app/views/components/dashboard/dashboard2.html',
                      controller: 'DashboardsCtrl',
                      controllerAs: 'dashboard',
                      authorize: true

                  }
              }
          });
});