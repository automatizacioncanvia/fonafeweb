﻿/// <reference path="../../views/modals/newcolaborador.html" />
/// <reference path="../../views/modals/newcolaborador.html" />
'use estrict';
angular.module('masterGenerysV1App')
    .controller('AdministracionCrl', function($scope, $http, Apimenu, Notification,Apifonfe, ngTableParams, $filter, $rootScope, SweetAlert, $uibModal,$window) {




        $rootScope.$on('to_parent', function(event, data) {
            $scope.tabla = data;
            ngtablei($scope.tabla);
        });

        $rootScope.$on('to_parent2', function(event, data) {
            $scope.tabla2 = data;
            ngtablei2($scope.tabla2);
        });

        $scope.form = {};
        $scope.variable = [];
        $scope.editableInput = false;
        $scope.valuei = true;
        $scope.tab = 1;
        $scope.selectTab = function(setTab) {

            $scope.tab = setTab;
        };
        $scope.isSelected = function(checktab) {
            return $scope.tab === checktab;
        };

        var items = function(serviceResp) {
            $scope.values = serviceResp.data;
        };

        var error = function(serviceResp) {
            $scope.values = "Error";
        };

        $scope.cargaServicio = function(empre) {

            var clear = [];
            ngtablei2(clear);
            if (empre == undefined) {

            } else {
                $http.get("api/fogetserviciobyempresa/" + empre).then(function(data) {

                    $scope.servicios = data.data;
                    if ($scope.servicios[1] == undefined) {
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>No se encontraron servicios.</span>', delay: 1500, positionY: 'bottom', positionX: 'right' });
                    }

                });
            }
        }





        $scope.muestralinks = function(empresa, servicio) {
            // if($scope.form.selectedEmpresa==undefined){
            $scope.empre = empresa;
            $scope.servi = servicio;
            var clear = [];
            ngtablei2(clear);




            // }


            if (empresa == undefined || servicio == undefined) {

            } else {
                var valor = empresa + ',' + servicio;
                $http.get("api/fogetlinkbyempresaservicio/" + valor).then(function(data) {


                    var colab = data.data;

                    ngtablei2(colab);
                    //console.log(colab);


                });

            }





        }


        $http.get("api/tablasMaestras").then(function(data) {

            $scope.tablas = data.data;
        });


        $scope.getBusqueda = function(idfiltro, valor) {

            var cadena = idfiltro + ',' + valor;
            $scope.filtrito = idfiltro;
            $scope.valorcito = valor;


            if (idfiltro === 0) {
                $scope.result = "Porfavor elige un filtro";

            } else if (idfiltro === 1) {
                $scope.values = [];
                if (valor === "") {
                    $scope.result = "Ingrese colaborador";
                } else {
                    $http.get("api/fogetbynamefiltro/" + cadena).then(function(serviceResp) {
                        $scope.values = serviceResp.data;
                        $scope.result = "";

                    });
                }
            } else if (idfiltro === 2) {

                $http.get("api/fogetbynamefiltro/" + cadena).then(function(serviceResp) {
                    var colabByEmpresa = serviceResp.data;
                    $scope.variable = colabByEmpresa;

                    ngtablei(colabByEmpresa);
                });
            }
        };


        $scope.getcombo = function(id) {

            $scope.buscarbyname = "";


            if (id === 1) {
                $scope.empresa = "";
                $scope.valuei = true;
                $scope.editableInput = true;
                $scope.variable = [];
                $scope.values = [];
                $scope.result = "";

                var mclear = $scope.values;
                ngtablei(mclear);

            } else if (id === 2) {
                $scope.valuei = false;
                $scope.variable = [];
                $scope.values = [];
                $scope.result = "";

                var mclear = $scope.values;
                ngtablei(mclear);


            } else {

                $scope.values = [];
                var mclear = $scope.values;
                ngtablei(mclear);
            }
        };



        $scope.clickmostrar = function(item) {

            $scope.buscarbyname = item.cO_NOMBRE + " " + item.cO_APELLIDO;

            $scope.values = [];
            $scope.variable = [];

            $http.get("api/fogetbynametotal/" + item.cO_ID).then(function(serviceResp) {
                var colab = serviceResp.data;
                $scope.variable = colab;
                ngtablei(colab);

            });
        };


        var ngtablei = function(array) {
            $scope.tableParams = new ngTableParams({
                page: 1,
                count: 8
            }, {
                total: array.length,
                getData: function($defer, params) {
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(array, params.orderBy()) :
                        array;

                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

        };

        var ngtablei2 = function(array) {
            $scope.tableParams2 = new ngTableParams({
                page: 1,
                count: 8
            }, {
                total: array.length,
                getData: function($defer, params) {
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(array, params.orderBy()) :
                        array;

                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

        };


        $scope.clickEliminarColab = function(id) {
            var refreshtable = $scope.filtrito + ',' + $scope.valorcito;

            SweetAlert.swal({
                    title: "Estás seguro?",
                    text: "Se procederá  a eliminarlo del sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, continúa!",
                    cancelButtonText: "No, retrocede!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $http.post('api/foupdateestadocolaborador/' + id)
                            .success(function(data, status, headers, config) {
                                $scope.PostDataResponse = data;
                                SweetAlert.swal("Eliminado!", "Se eliminó al colaborador satisfactoriamente.", "success");

                                if ($scope.filtrito === 1) {
                                    $http.get("api/fogetbynamefiltro/" + refreshtable).then(function(serviceResp) {
                                        $scope.blank = [];
                                        var mclear = $scope.blank;
                                        ngtablei(mclear);


                                    });
                                } else if ($scope.filtrito === 2) {
                                    $http.get("api/fogetbynamefiltro/" + refreshtable).then(function(serviceResp) {
                                        var mclear = serviceResp.data;
                                        $scope.variable = mclear;
                                        ngtablei(mclear);

                                    });
                                }


                            }).error(function(data, status, header, config) {
                                SweetAlert.swal("Error!", "No se pudo eliminar el colaborador", "error");
                            });

                    } else {
                        SweetAlert.swal("Cancelado", "Operación cancelada", "error");
                    }
                });

        };


        $scope.clickEliminarLink = function(id) {
            var valor = $scope.empre + ',' + $scope.servi;

            SweetAlert.swal({
                    title: "Estás seguro?",
                    text: "Se procederá  a eliminarlo del sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, continúa!",
                    cancelButtonText: "No, retrocede!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $http.post('api/foupdateestadolink/' + id)
                            .success(function(data, status, headers, config) {
                                $scope.PostDataResponse = data;
                                SweetAlert.swal("Eliminado!", "Se eliminó el link satisfactoriamente.", "success");


                                $http.get("api/fogetlinkbyempresaservicio/" + valor).then(function(data) {



                                    var colab = data.data;

                                    ngtablei2(colab);
                                    //console.log(colab);

                                });



                            }).error(function(data, status, header, config) {
                                SweetAlert.swal("Error!", "No se pudo eliminar el link", "error");
                            });

                    } else {
                        SweetAlert.swal("Cancelado", "Operación cancelada", "error");
                    }
                });

        };

        $scope.openlink2 = function(url) {
            var dati = {
                token: localStorage.getItem('Token').toString(),
                url: url
            };

            var body = JSON.stringify(dati);
            Apifonfe.loginswrt(body).success(function(data) {

                    if (url.substring(0, 1) == "h") {
                        $window.open(data, 'Open', 'width=1500,height=700');
                    } else {
                        $window.open(url, 'Open', 'width=1500,height=700');
                    }

                })
                .error(function(data, status, header, config) {
                    Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                });

        }


        Apimenu.getmenu(16, $rootScope.useri.userid).then(function(serviceResp) {

            $scope.getempresas = serviceResp.data.data;


        });



        $http.get("api/fogetbydominio/").then(function(serviceResp) {

            $scope.dominios = serviceResp.data;
        });

        $http.get("api/fogetbyempresa/").then(function(serviceResp) {

            $scope.empresas = serviceResp.data;

        });


        $http.get("api/fogetbyperfil/").then(function(serviceResp) {

            $scope.perfiles = serviceResp.data;

        });

        $http.get("api/fogetbyalcance").then(function(serviceResp) {

            $scope.alcances = serviceResp.data;

        });


        $http.get("api/fogetbycargo/").then(function(serviceResp) {

            $scope.cargos = serviceResp.data;

        });


        $http.get("api/fogetbyfiltros/0003").then(function(serviceResp) {

            $scope.filtros = serviceResp.data;





        });



        $scope.newcolaborador = function() {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/administracion/modals/newcolaborador.html',
                controller: 'ModalNewColaborador',
                size: 'lg',
                resolve: {
                    items: function() {
                        var array = [];
                        array.push($scope.dominios);
                        array.push($scope.perfiles);
                        array.push($scope.alcances);
                        array.push($scope.empresas);
                        array.push($scope.cargos);

                        return array;
                    }
                }
            });


        }

        $scope.editcolaborador = function(idcolab) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/administracion/modals/editcolaborador.html',
                controller: 'ModalEditColaborador',
                size: 'lg',
                resolve: {
                    items: function () {
                        var array = [];
                        array.push($scope.dominios);
                        array.push($scope.perfiles);
                        array.push($scope.alcances);
                        array.push($scope.empresas);
                        array.push($scope.cargos);

                        array.push(idcolab);
                        array.push($scope.filtrito);
                        array.push($scope.valorcito);

                        return array;
                    }
                }

            });



        }


        $scope.newlink = function() {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/administracion/modals/newlink.html',
                controller: 'ModalNewLink',
                size: 'lg',
                resolve: {
                    items: function() {
                        var array = [];
                        array.push($scope.dominios);
                        array.push($scope.perfiles);
                        array.push($scope.alcances);
                        array.push($scope.empresas);
                        array.push($scope.cargos);

                        return array;
                    }
                }
            });


        }



        $scope.editlink = function(idlink) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/views/components/administracion/modals/editlink.html',
                controller: 'ModalEditLink',
                size: 'lg',
                resolve: {
                    link: function() {

                        var array = [];

                        array.push(idlink);
                        array.push($scope.empre);
                        array.push($scope.servi);


                        return array;

                    }

                }
            });



        }




    });
