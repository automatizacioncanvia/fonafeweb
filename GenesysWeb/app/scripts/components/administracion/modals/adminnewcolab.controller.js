﻿angular.module('masterGenerysV1App').controller('ModalNewColaborador', function($uibModalInstance, $scope, items, SweetAlert, $http, $rootScope) {

    $scope.modal = {};
    $scope.dominios = items[0];
    $scope.perfiles = items[1];
    $scope.alcances = items[2];
    $scope.empresas = items[3];
    $scope.cargos = items[4];

    $scope.close = function() {

        $uibModalInstance.close();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.clickInsertar = function() {


        console.log($scope.modal);

        $scope.modal.dni = ($scope.modal.dni === undefined || $scope.modal.dni === null  ? $scope.modal.dni = "" : $scope.modal.dni);
        $scope.modal.fijo = ($scope.modal.fijo === undefined || $scope.modal.fijo === null  ? $scope.modal.fijo = "" : $scope.modal.fijo);
        $scope.modal.anexo = ($scope.modal.anexo === undefined || $scope.modal.anexo === null  ? $scope.modal.anexo = "" : $scope.modal.anexo);
        $scope.modal.celular = ($scope.modal.celular === undefined || $scope.modal.celular === null  ? $scope.modal.celular = "" : $scope.modal.celular);

        var body = JSON.stringify({
            nombre: $scope.modal.nombre,
            apellido: $scope.modal.apellido,
            empresa: $scope.modal.empresa,
            perfil: $scope.modal.perfil,
            alcance: $scope.modal.alcance,
            cuenta: $scope.modal.cuenta,
            email: $scope.modal.email,
            dominio: $scope.modal.dominio,
            cargo: $scope.modal.cargo,
            dni: $scope.modal.dni,
            fijo: $scope.modal.fijo,
            anexo: $scope.modal.anexo,
            celular: $scope.modal.celular,
            iduser: $rootScope.useri.userid,

        });

        console.log(body)

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };


        SweetAlert.swal({
                title: "Estás seguro?",
                text: "Se procederá a insertar al sistema!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, continúa!",
                cancelButtonText: "No, retrocede!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {

                    $http.post('api/foinsertcolaborador', body, config)
                        .success(function(data) {
                            $scope.PostDataResponse = data;
                            console.log($scope.PostDataResponse);
                            SweetAlert.swal("Insertado!", "Se insertó al colaborador satisfactoriamente.", "success");
                            $uibModalInstance.close();

                        }).error(function(data) {
                            $scope.error = data
                            SweetAlert.swal("Error!", $scope.error, "error");
                        });

                } else {
                    SweetAlert.swal("Cancelado", "Operación cancelada", "error");
                }
            });

    };


});
