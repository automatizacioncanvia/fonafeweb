﻿angular.module('masterGenerysV1App').controller('ModalEditColaborador', function($uibModalInstance, $scope, items, SweetAlert, $http, $rootScope,ngTableParams) {

    $scope.modal = {};
    $scope.dominios = items[0];
    $scope.perfiles = items[1];
    $scope.alcances = items[2];
    $scope.empresas = items[3];
    $scope.cargos = items[4];
    var idcolab = items[5];
    var filtro = items[6];
    var valor = items[7];


    // var idcolab = items[0];
    // var filtro = items[1];
    // var valor = items[2];


    $scope.close = function() {

        $uibModalInstance.close();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $http.get("api/fogetbynametotal/" + idcolab).then(function(serviceResp) {

        $scope.colab = serviceResp.data;


        $scope.form = {
            id: $scope.colab[0].cO_ID,
            name: $scope.colab[0].cO_NOMBRE,
            apellido: $scope.colab[0].cO_APELLIDO,
            selectedEmpresa: $scope.colab[0].eM_ID,
            selectedPerfil: $scope.colab[0].pE_ID,
            alcance: $scope.colab[0].aL_ID,
            cuenta: $scope.colab[0].cO_CUENTA,
            email: $scope.colab[0].cO_EMAIL,
            selectedDominio: $scope.colab[0].dO_ID,
            selectedCargo: $scope.colab[0].cR_ID,
            dni: $scope.colab[0].cO_DNI,
            fijo: $scope.colab[0].cO_TELEFONOFIJO,
            anexo: $scope.colab[0].cO_ANEXO,
            celular: $scope.colab[0].cO_CELULAR,

        };

        $scope.values = [];


    });

    $scope.clickActualizar = function(form) //Tercera funcion
        {
            $scope.tabla = [];

            var refreshtable = filtro + ',' + valor;

            $scope.form.dni = ($scope.form.dni === undefined || $scope.form.dni === null ? $scope.form.dni = "" : $scope.form.dni);
            $scope.form.fijo = ($scope.form.fijo === undefined || $scope.form.fijo === null? $scope.form.fijo = "" : $scope.form.fijo);
            $scope.form.anexo = ($scope.form.anexo === undefined || $scope.form.anexo === null ? $scope.form.anexo = "" : $scope.form.anexo);
            $scope.form.celular = ($scope.form.celular === undefined || $scope.form.celular === null ? $scope.form.celular = "" : $scope.form.celular);



            var body = JSON.stringify({
                nombre: form.name,
                apellido: form.apellido,
                empresa: form.selectedEmpresa,
                perfil: form.selectedPerfil,
                alcance: form.alcance,
                cuenta: form.cuenta,
                email: form.email,
                dominio: form.selectedDominio,
                cargo: form.selectedCargo,
                dni: form.dni,
                fijo: form.fijo,
                anexo: form.anexo,
                celular: form.celular,
                iduser: $rootScope.useri.userid,

            });


            console.log('Array lleno: '+body);

            var config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };


            SweetAlert.swal({
                    title: "Estás seguro?",
                    text: "Se procederá a actualizar la información!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, continúa!",
                    cancelButtonText: "No, retrocede!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        $http.put('api/foupdatecolaborador/' + form.id, body, config)
                            .success(function(data) {

                                $scope.PutDataResponse = data;
                                SweetAlert.swal("Actualizado!", "Se actualizó la información satisfactoriamente.", "success");

                                if (filtro === 1) {
                                    $http.get("api/fogetbynamefiltro/" + refreshtable).then(function(serviceResp) {
                                         $scope.tabla = serviceResp.data;
                                         debugger;
                                         $rootScope.$emit('to_parent', $scope.tabla);
                                         $uibModalInstance.close();

                                    });
                                } else if (filtro === 2) {
                                    $http.get("api/fogetbynamefiltro/" + refreshtable).then(function(serviceResp) {
                                        $scope.tabla = serviceResp.data;
                                        $rootScope.$emit('to_parent', $scope.tabla);
                                        $uibModalInstance.close();

                                    });
                                }


                            }).error(function(data) {
                                SweetAlert.swal("Error!", "No se pudo actualizar la información", "error");
                            });

                    } else {
                        SweetAlert.swal("Cancelado", "Operación cancelada", "error");
                    }

                });

        };

});
