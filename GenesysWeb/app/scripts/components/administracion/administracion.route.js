﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('administracion', {
              parent: 'main',
              url: 'administracion',
              views: {
                  'content@main': {
                      templateUrl: 'app/views/components/administracion/administracion.html',
                      controller: 'AdministracionCrl',
                      controllerAs: 'administracion',
                      authorize: true
                      
                  }
              }
          });
});