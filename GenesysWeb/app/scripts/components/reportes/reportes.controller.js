﻿'use strict';


angular.module('masterGenerysV1App')
    .controller('ReportesCtrl', ['$scope', 'Apimenu', '$rootScope', 'Apidashboard', 'Notification', '$uibModal', '$http', '$parse', '$window', '$stateParams', '$state', function ($scope, Apimenu, $rootScope, Apidashboard, Notification, $uibModal, $http, $parse, $window, $stateParams, $state) {

        $scope.nodata0001 = false;
        $scope.nodata0002 = false;
        $scope.nodata0003 = false;
        $scope.nodata0004 = false;
        $scope.nodata0005 = false;
        $scope.nodata0006 = false;
        $scope.nodata0007 = false;
        $scope.nodata0008 = false;
        $scope.nodata0010 = false;
        $scope.nodata0016 = false;
        $scope.nodata0017 = false;
        $scope.nodata0018 = false;
        $scope.nodata0019 = false;
        $scope.nodata0021 = false;
        

        $scope.bucarticket = function () {
            var categoria = '0006';
            $http.get("api/fogetbyfiltros/" + categoria).then(function (serviceResp) {

                $scope.filtros = serviceResp.data;

                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/buscarticket.html',
                    controller: 'ModalBusquedaTicket',
                    size: 'lg',
                    resolve: {
                        filtros: function () {
                            var array = [];
                            array.push($scope.filtros);
                            return array;
                        }
                    }
                });
            });
        }

        $scope.getallticket = function (periodo, empresa, idchart, servicio, prioridad, fecha, estado,tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando tickets...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

            });

            if (periodo !== 1) {

                if (periodo === undefined) {

                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione periodo Dashboard</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                    $window.loading_screen.finish();
                } else {

                    if (empresa !== undefined) {

                        servicio = (servicio === undefined || servicio === null ? -1 : servicio);
                        prioridad = (prioridad === undefined || prioridad === null ? -1 : prioridad);
                        estado = (estado === undefined || estado === null ? -1 : estado);
                        tipo = (tipo === undefined || tipo === null ? -1 : tipo);

                        var vista = idchart + "," + periodo + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad + "," + fecha + "," + estado + "," + tipo;
                        var info = "x,x";
                        console.log("getallticketReportes"); var myArr1 = [vista, info]; console.log(myArr1);
                        $http.get("api/fogetvertickets/" + vista + "/" + info).then(function (serviceResp) {

                            $scope.ticketsobtenidos = serviceResp.data;
                            $window.loading_screen.finish();
                            var modalInstance = $uibModal.open({
                                animation: true,
                                ariaLabelledBy: 'modal-title',
                                ariaDescribedBy: 'modal-body',
                                templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                                controller: 'ModalViewDetail',
                                size: 'lg',
                                resolve: {
                                    filtrosobtenidos: function () {
                                        var array = [];
                                        array.push($scope.ticketsobtenidos);
                                        return array;
                                    }
                                }
                            });

                        });
  
                    } else {
                        
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione empresa</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        // $scope.$scope.servs = [];
                        $window.loading_screen.finish();

                    }
                }
            } else {
                if (empresa !== undefined) {


                    servicio = (servicio === undefined || servicio === null ? -1 : servicio);
                    prioridad = (prioridad === undefined || prioridad === null ? -1 : prioridad);
                    estado = (estado === undefined || estado === null ? -1 : estado);
                    tipo = (tipo === undefined || tipo === null ? -1 : tipo);
 

                    var vista = idchart + "," + 1 + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad + "," + fecha + "," + estado + "," + tipo;

                    var desde = moment($scope.from).format("YYYYMMDD");
                    var hasta = moment($scope.to).format("YYYYMMDD");
                    var info = "" + desde + "," + hasta;
                    //console.log("vista: " + vista + "  info: " + info);
                    $http.get("api/fogetvertickets/" + vista + "/" + info).then(function (serviceResp) {

                        $scope.ticketsobtenidos = serviceResp.data;
                        $window.loading_screen.finish();


                        var modalInstance = $uibModal.open({
                            animation: true,
                            ariaLabelledBy: 'modal-title',
                            ariaDescribedBy: 'modal-body',
                            templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                            controller: 'ModalViewDetail',
                            size: 'lg',
                            resolve: {
                                filtrosobtenidos: function () {
                                    var array = [];
                                    array.push($scope.ticketsobtenidos);
                                    return array;
                                }
                            }
                        });


                    });



                }
            }


        };


        $scope.dash1 = "0001";
        $scope.dash2 = "0002";
        $scope.dash3 = "0003";
        $scope.dash4 = "0004";
        $scope.dash5 = "0005";
        $scope.dash6 = "0006";
        $scope.dash7 = "0007";
        $scope.dash8 = "0008";
        $scope.dash16 = "0016";
        $scope.dash17 = "0017";
        $scope.dash18 = "0018";
        $scope.dash19 = "0019";
        $scope.dash21 = "0021";

        $scope.form = {};


        $scope.openRegresar = function () {

            if ($stateParams.atendido == 1) {
                $state.go('dashboard', { idServicio: $stateParams.idServicio, nombreServicio: $stateParams.nombreServicio, idEmpresa: $stateParams.idEmpresa, nombreEmpresa: $stateParams.nombreEmpresa, atendido: 1, activar: 0 });
            } else {
                $state.go('dashboard', { idServicio: $stateParams.idServicio, nombreServicio: $stateParams.nombreServicio, idEmpresa: $stateParams.idEmpresa, nombreEmpresa: $stateParams.nombreEmpresa, atendido: 0, activar: 1 });
            }


        };

        $scope.selectTab = function (setTab) {

            //console.log($rootScope.useri.empresa_id)
            switch (setTab) {
                case 1:
                    if ($scope.ff1 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash1, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 2:
                    if ($scope.ff2 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash2, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 3:
                    if ($scope.ff3 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash3, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 4:
                    if ($scope.ff4 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash4, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 5:
                    if ($scope.ff5 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash5, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 6:
                    if ($scope.ff6 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash6, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 7:
                    if ($scope.ff7 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash7, undefined, undefined, 'AT_VALOROBJ,AT_UMOBJ', undefined, undefined);
                    }
                    break;

                case 8:
                    if ($scope.ff8 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash8, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 16:
                    if ($scope.ff16 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash16, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 17:
                    if ($scope.ff17 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash17, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 18:
                    if ($scope.ff18 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash18, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 19:
                    if ($scope.ff19 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash19, undefined, undefined, undefined, undefined, undefined);
                    }
                    break;
                case 21:
                    if ($scope.ff21 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash21, undefined, undefined, 'AT_VALOROBJ,AT_UMOBJ', undefined, undefined);
                    }
                    break;
            }
            $scope.tab = setTab;
        };

        $scope.isSelected = function (checktab) {
            return $scope.tab === checktab;
        };

        var fail = function (serviceResp) {

            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

        };

        Apimenu.getmenu(32, $rootScope.useri.userid).then(function (serviceResp) {

            var index = Apidashboard.indexOfespecial(serviceResp.data.data);

            if (index !== -1) {

                Apimenu.getmenu(5, $rootScope.useri.userid).then(function (serviceResp) {

                    $scope.servicios = serviceResp.data.data;
                    //console.log($scope.servicios);

                }, fail);
 
                if ($stateParams.idServicio == undefined) {
                    $scope.form.selectedEmpresa1 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa2 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa3 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa4 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa5 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa6 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa7 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa8 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa16 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa17 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa18 = serviceResp.data.data[index];
                    $scope.form.selectedEmpresa19 = serviceResp.data.data[index];
                }
            }
            $scope.empresas = serviceResp.data.data;
        }, fail);

        Apimenu.getmenu(32, $rootScope.useri.userid).then(function (serviceResp) {

            $scope.empresas2 = serviceResp.data.data;

        });

        Apimenu.getmenu(18, '-').then(function (serviceResp) {
            $scope.resolutor = serviceResp.data.data;
            $scope.resolutor2 = serviceResp.data.data;
        }, fail)

        Apimenu.getmenu(9, '-').then(function (serviceResp) {

            $scope.periodos = serviceResp.data.data;

        }, fail)

        Apimenu.getmenu(30, '-').then(function (serviceResp) {

            $scope.tipotickets = serviceResp.data.data;
 
        }, fail);

        Apimenu.getmenu(25, $rootScope.useri.userid).then(function (serviceResp) {

            $scope.estados = serviceResp.data.data;
            console.log($scope.servicios);

        }, fail);

        //metodos globales
        $scope.getchart = function (periodo, em, idchart, servicio, prioridad, sla, estado, tipoticket,grupo) {
            if (periodo !== 1) {
                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/chart2.png",
                    backgroundColor: 'rgba(41, 127, 130, 0.79)',
                    loadingHtml: "<p class='loading-message'>Cargando indicador...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
                });

                if (em == undefined) {
                    em = 0;
                }

                Apidashboard.getdashi(periodo, em, idchart, servicio, prioridad, sla, estado, tipoticket, grupo).then(function (serviceResp) {

                    var the_string = "nodata" + idchart;
                    var model = $parse(the_string);

                    if (serviceResp.data.length > 0) {

                        if (idchart === "0001") {
                            model.assign($scope, false);
                            $scope.value1 = false;
                            $scope.periodo1 = periodo;
                            var obj = Apidashboard.makedashboard1(serviceResp.data);
                            $scope.ff1 = obj.data;
                            $scope.fullkeys1 = obj.labels;
                            $scope.series1 = obj.series
                            $scope.totales1 = obj.totales;
                            $scope.$broadcast('builDash', obj);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0016") {
                            model.assign($scope, false);
                            $scope.value16 = false;
                            $scope.periodo16 = periodo;
                            var obj = Apidashboard.makedashboard16(serviceResp.data);
                            $scope.ff16 = obj.data;
                            $scope.fullkeys16 = obj.labels;
                            $scope.series16 = obj.series
                            $scope.totales16 = obj.totales;
                            $scope.$broadcast('builDash', obj);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0002") {
                            model.assign($scope, false);
                            $scope.value2 = false;
                            $scope.periodo2 = periodo;
                            $scope.ff2 = serviceResp.data;
                            $scope.fullkeys2 = Object.keys(serviceResp.data[0]);
                            var obj2 = Apidashboard.makedashboard2(serviceResp.data);
                            $scope.$broadcast('builDash2', obj2);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0017") {
                            model.assign($scope, false);
                            $scope.value17 = false;
                            $scope.periodo17 = periodo;
                            $scope.ff17 = serviceResp.data;
                            $scope.fullkeys17 = Object.keys(serviceResp.data[0]);
                            var obj17 = Apidashboard.makedashboard17(serviceResp.data);
                            $scope.$broadcast('builDash17', obj17);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0003") {
                            model.assign($scope, false);
                            $scope.value3 = false;
                            $scope.periodo3 = periodo;
                            $scope.ff3 = serviceResp.data;
                            $scope.fullkeys3 = Object.keys(serviceResp.data[0]);
                            var obj3 = Apidashboard.makedashboard3(serviceResp.data);
                            $scope.$broadcast('builDash3', obj3);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0018") {
                            model.assign($scope, false);
                            $scope.value18 = false;
                            $scope.periodo18 = periodo;
                            $scope.ff18 = serviceResp.data;
                            $scope.fullkeys18 = Object.keys(serviceResp.data[0]);
                            var obj18 = Apidashboard.makedashboard18(serviceResp.data);
                            $scope.$broadcast('builDash18', obj18);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0004") {
                            $scope.ff4 = []
                            model.assign($scope, false);
                            $scope.value4 = false;
                            $scope.periodo4 = periodo;

                            //$scope.ff4 = serviceResp.data;
                            //$scope.fullkeys4 = Object.keys(serviceResp.data[0]);
                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff4.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys4 = headGrid

                            var obj4 = Apidashboard.makedashboard4(serviceResp.data);
                            $scope.$broadcast('builDash4', obj4);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0019") {
                            $scope.ff19 = []
                            model.assign($scope, false);
                            $scope.value19 = false;
                            $scope.periodo19 = periodo;

                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff19.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys19 = headGrid

                            var obj19 = Apidashboard.makedashboard4(serviceResp.data);
                            $scope.$broadcast('builDash19', obj19);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0005") {
                            $scope.ff5 = []
                            model.assign($scope, false);
                            $scope.value5 = false;
                            $scope.periodo5 = periodo;

                            //$scope.ff5 = serviceResp.data;
                            //$scope.fullkeys5 = Object.keys(serviceResp.data[0]);
                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff5.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys5 = headGrid


                            var obj5 = Apidashboard.makedashboard5(serviceResp.data);
                            $scope.$broadcast('builDash5', obj5);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0006") {
                            $scope.ff6 = []
                            model.assign($scope, false);
                            $scope.value6 = false;
                            $scope.periodo6 = periodo;

                            //$scope.ff6 = serviceResp.data;
                            //$scope.fullkeys6 = Object.keys(serviceResp.data[0]);
                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff6.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys6 = headGrid

                            var obj6 = Apidashboard.makedashboard6(serviceResp.data);
                            $scope.$broadcast('builDash6', obj6);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0007") {
                            model.assign($scope, false);
                            $scope.value7 = false;
                            $scope.periodo7 = periodo;
                            $scope.ff7 = serviceResp.data;
                            console.log($scope.ff7);
                            $scope.fullkeys7 = Object.keys(serviceResp.data[0]);
                            var obj7 = Apidashboard.makedashboard7(serviceResp.data);

                            $scope.IncAt = obj7.IncAt;
                            $scope.ReqAt = obj7.ReqAt;
                            $scope.IncRes = obj7.IncRes;
                            $scope.ReqRes = obj7.ReqRes;

                            $scope.$broadcast('builDash7', obj7);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0008") {
                            $scope.ff8 = []
                            model.assign($scope, false);
                            $scope.value8 = false;
                            $scope.periodo8 = periodo;

                            //$scope.ff8 = serviceResp.data;
                            //$scope.fullkeys8 = Object.keys(serviceResp.data[0]);
                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff8.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys8 = headGrid


                            var obj8 = Apidashboard.makedashboard8(serviceResp.data);
                            $scope.$broadcast('builDash8', obj8);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0021") {
                            model.assign($scope, false);
                            $scope.value21 = false;
                            $scope.periodo21 = periodo;
                            $scope.ff21 = serviceResp.data;
                            console.log($scope.ff21);
                            $scope.fullkeys21 = Object.keys(serviceResp.data[0]);
                            var obj21 = Apidashboard.makedashboard21(serviceResp.data);

                            $scope.$broadcast('builDash21', obj21);
                            $window.loading_screen.finish();
                        }

                    } else {


                        model.assign($scope, true);


                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $window.loading_screen.finish();
                    }

                }, fail)
            } else {

                if (idchart === "0001") {
                    $scope.periodo1 = periodo;
                    $scope.value1 = true;
                }
                if (idchart === "0016") {
                    $scope.periodo16 = periodo;
                    $scope.value16 = true;
                }
                if (idchart === "0002") {
                    $scope.periodo2 = periodo;
                    $scope.value2 = true;
                }
                if (idchart === "0017") {
                    $scope.periodo17 = periodo;
                    $scope.value17 = true;
                }
                if (idchart === "0003") {
                    $scope.periodo3 = periodo;
                    $scope.value3 = true;
                }
                if (idchart === "0018") {
                    $scope.periodo18 = periodo;
                    $scope.value18 = true;
                }
                if (idchart === "0004") {
                    $scope.periodo4 = periodo;
                    $scope.value4 = true;
                }
                if (idchart === "0019") {
                    $scope.periodo19 = periodo;
                    $scope.value19 = true;
                }
                if (idchart === "0005") {
                    $scope.periodo5 = periodo;
                    $scope.value5 = true;
                }
                if (idchart === "0006") {
                    $scope.periodo6 = periodo;
                    $scope.value6 = true;
                }
                if (idchart === "0007") {
                    $scope.periodo7 = periodo;
                    $scope.value7 = true;
                }
                if (idchart === "0008") {
                    $scope.periodo8 = periodo;
                    $scope.value8 = true;
                }
                if (idchart === "0021") {
                    $scope.periodo21 = periodo;
                    $scope.value21 = true;
                }
            }
        };

        $scope.onSelected12 = function (empresa, idchart, periodo, servicio, prioridad, sla) {
            var sla = (sla === undefined ? '-1' : sla);
            if (periodo !== 1) {

                if (periodo === undefined) {

                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Periodo Dashboard ' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                } else {

                    if (empresa !== undefined) {

                        var servicio = (servicio === undefined ? -1 : servicio.sE_ID);
                        var prioridad = (prioridad === undefined ? -1 : prioridad);

                        $scope.getchart(periodo, empresa.eM_ID, idchart, servicio, prioridad, sla, undefined, undefined);
                        Apimenu.getmenu(10, empresa.eM_ID).then(function (serviceResp) {
                            $scope.servicios = serviceResp.data.data;
                        });

                    } else {
                        
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Empresa' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $scope.servicios = [];
                    }
                }
            }



        };


        $scope.onSelected3 = function (empresa, idchart, periodo, servicio, prioridad, estado, tipoticket) {

            if (periodo !== 1) {

                if (periodo === undefined) {

                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Periodo Dashboard ' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                } else {

                    if (empresa !== undefined) {

                        var servicio = (servicio === undefined ? -1 : servicio.sE_ID);
                        var prioridad = (prioridad === undefined ? -1 : prioridad.sym);
                        var tipoticket = (tipoticket === undefined ? -1 : tipoticket.tA_COD);

                        $scope.getchart(periodo, empresa.eM_ID, idchart, servicio, prioridad, undefined, estado, tipoticket);
                        Apimenu.getmenu(10, empresa.eM_ID).then(function (serviceResp) {
                            $scope.servicios = serviceResp.data.data;
                        });

                    } else {
                        
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Empresa' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $scope.servicios = [];
                    }
                }
            }
        };

        $scope.onSelected = function (empresa, idchart, periodo, servicio, prioridad, sla, tipoticket,grupo) {
            //console.log($rootScope.useri.empresa_id)
            var sla = (sla === undefined ? '-1' : sla);
            var grupo = (grupo === undefined ? '-1' : grupo);

            if (periodo !== 1) {

                if (periodo === undefined) {

                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Periodo Dashboard ' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                } else {

                    if (empresa !== undefined) {

                        var servicio = (servicio === undefined ? -1 : servicio.sE_ID);
                        var prioridad = (prioridad === undefined ? -1 : prioridad.sym);
                        var tipoticket = (tipoticket === undefined ? -1 : tipoticket.tA_COD);

                        $scope.getchart(periodo, empresa.eM_ID, idchart, servicio, prioridad, sla, undefined, tipoticket, grupo);
                        Apimenu.getmenu(10, empresa.eM_ID).then(function (serviceResp) {
                            $scope.servicios = serviceResp.data.data;
                        });

                    } else {
                        
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Empresa' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $scope.servicios = [];
                    }
                }
            }
        };

        $scope.entrefechas = function (from, to, em, idchart, servicio, prioridad, sla,estado, tipoticket,grupo) {

            $scope.from = from;
            $scope.to = to;


            var sla = (sla === undefined ? '-1' : sla);
            var tipoticket = (tipoticket === undefined ? -1 : tipoticket);
            var grupo = (grupo === undefined ? -1 : grupo);

            if (em !== undefined) {

              $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/chart2.png",
                    backgroundColor: 'rgba(41, 127, 130, 0.79)',
                    loadingHtml: "<p class='loading-message'>Cargando indicador...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });


              Apidashboard.getdashbydate(from, to, em, idchart, servicio, prioridad, sla, estado, tipoticket, grupo).then(function (serviceResp) {

                    var the_string = "nodata" + idchart;
                    var model = $parse(the_string);

                    if (serviceResp.data.length > 0) {

                        if (idchart === "0001") {
                            model.assign($scope, false);
                            var obj = Apidashboard.makedashboard1(serviceResp.data);
                            $scope.ff1 = obj.data;
                            $scope.fullkeys1 = obj.labels;
                            $scope.series1 = obj.series;
                            $scope.totales1 = obj.totales;
                            $scope.$broadcast('builDash', obj);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0016") {
                            model.assign($scope, false);
                            var obj = Apidashboard.makedashboard16(serviceResp.data);
                            $scope.ff16 = obj.data;
                            $scope.fullkeys16 = obj.labels;
                            $scope.series16 = obj.series;
                            $scope.totales16 = obj.totales;
                            $scope.$broadcast('builDash', obj);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0002") {
                            model.assign($scope, false);
                            $scope.ff2 = serviceResp.data;
                            $scope.fullkeys2 = Object.keys(serviceResp.data[0]);
                            var obj2 = Apidashboard.makedashboard2(serviceResp.data);
                            $scope.$broadcast('builDash2', obj2);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0017") {
                            model.assign($scope, false);
                            $scope.ff17 = serviceResp.data;
                            $scope.fullkeys17 = Object.keys(serviceResp.data[0]);
                            var obj17 = Apidashboard.makedashboard17(serviceResp.data);
                            $scope.$broadcast('builDash17', obj17);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0003") {
                            model.assign($scope, false);
                            $scope.ff3 = serviceResp.data;
                            $scope.fullkeys3 = Object.keys(serviceResp.data[0]);
                            var obj3 = Apidashboard.makedashboard3(serviceResp.data);
                            $scope.$broadcast('builDash3', obj3);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0018") {
                            model.assign($scope, false);
                            $scope.ff18 = serviceResp.data;
                            $scope.fullkeys18 = Object.keys(serviceResp.data[0]);
                            var obj18 = Apidashboard.makedashboard18(serviceResp.data);
                            $scope.$broadcast('builDash18', obj18);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0004") {
                            $scope.ff4 = []
                            model.assign($scope, false);

                            //$scope.ff4 = serviceResp.data;
                            //$scope.fullkeys4 = Object.keys(serviceResp.data[0]);
                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff4.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys4 = headGrid

                            var obj4 = Apidashboard.makedashboard4(serviceResp.data);
                            $scope.$broadcast('builDash4', obj4);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0019") {
                            $scope.ff19 = []
                            model.assign($scope, false);

                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff19.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys19 = headGrid

                            var obj19 = Apidashboard.makedashboard4(serviceResp.data);
                            $scope.$broadcast('builDash19', obj19);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0005") {
                            $scope.ff5 = []
                            model.assign($scope, false);

                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff5.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys5 = headGrid

                            //$scope.ff5 = serviceResp.data;
                            //$scope.fullkeys5 = Object.keys(serviceResp.data[0]);
                            var obj5 = Apidashboard.makedashboard5(serviceResp.data);
                            $scope.$broadcast('builDash5', obj5);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0006") {
                            $scope.ff6 = []
                            model.assign($scope, false);

                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff6.push(a)
                            }

                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys6 = headGrid
                            //$scope.ff6 = serviceResp.data;
                            //$scope.fullkeys6 = Object.keys(serviceResp.data[0]);
                            var obj6 = Apidashboard.makedashboard6(serviceResp.data);
                            $scope.$broadcast('builDash6', obj6);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0007") {
                            model.assign($scope, false);
                            $scope.ff7 = serviceResp.data;
                            console.log($scope.ff7);
                            $scope.fullkeys7 = Object.keys(serviceResp.data[0]);
                            var obj7 = Apidashboard.makedashboard7(serviceResp.data);

                            $scope.$broadcast('builDash7', obj7);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0008") {
                            $scope.ff8 = []
                            model.assign($scope, false);
                            var dataGrid = serviceResp.data;
                            var objGrid = []
                            for (objGrid in dataGrid) {
                                var a = dataGrid[objGrid];
                                delete a.mes
                                $scope.ff8.push(a)
                                $window.loading_screen.finish();
                            }
                            var headGrid = Object.keys(serviceResp.data[0]);
                            headGrid.splice(1, 0)
                            $scope.fullkeys8 = headGrid
                            var obj8 = Apidashboard.makedashboard8(serviceResp.data);
                            $scope.$broadcast('builDash8', obj8);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0021") {
                            model.assign($scope, false);
                            $scope.ff21 = serviceResp.data;
                            console.log($scope.ff21);
                            $scope.fullkeys21 = Object.keys(serviceResp.data[0]);
                            var obj21 = Apidashboard.makedashboard21(serviceResp.data);

                            $scope.$broadcast('builDash21', obj21);
                            $window.loading_screen.finish();
                        }
                    } else {
                        model.assign($scope, true);
                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $window.loading_screen.finish();
                    }
                }, fail);
            } else {
                Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Porfavor seleccione una empresa.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
            }
        };

        if ($stateParams.idServicio != undefined) {

            $("#panelRegresar").show();

            if ($stateParams.atendido == 1) {

                $("#tabReqEstado").hide();
                $("#tabInciEstado").hide();
                $("#tabReqTiempo").hide();
                $("#tabInciTiempo").hide();
                $("#tabInciServicio").hide();
                $("#tabReqServicio").hide();
                $("#tabInciPrioridad").hide();
                $("#tabReqPrioridad").hide();

                $("#tabCambio").hide();
                $("#tabTendenciaTicket").hide();

                $("#tabPendientes").hide();

                $("#tabSla").show();

                $scope.tab = 7;


                $scope.form.selectedEmpresa7 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                $scope.form.selectedServicio7 = { sE_ID: $stateParams.idServicio, sE_NOMBRE: $stateParams.nombreServicio };

                $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash7, $stateParams.idServicio, undefined, 'AT_VALOROBJ,AT_UMOBJ', undefined, undefined);

                if ($stateParams.tipo == "R") {
                    $("#panelRequerimientos").show();
                    $("#panelIncidentes").hide();
                } else {
                    $("#panelIncidentes").show();
                    $("#panelRequerimientos").hide();
                }

            } else {
                if ($stateParams.tipo == "R") {
                    $("#tabReqEstado").show();
                    $("#tabInciEstado").hide();
                    $("#tabReqTiempo").show();
                    $("#tabInciTiempo").hide();
                    $("#tabInciServicio").hide();
                    $("#tabReqServicio").show();
                    $("#tabInciPrioridad").hide();
                    $("#tabReqPrioridad").show();

                    $("#tabCambio").hide();
                    $("#tabTendenciaTicket").show();
                    $("#tabPendientes").show();
                    $("#tabSla").show();

                    $("#panelRequerimientos").show();
                    $("#panelIncidentes").hide();

                    $("#TituloSLA").html("Requerimientos Atendidos");

                    $scope.tab = 1;

                    $scope.form.selectedEmpresa1 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa2 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa3 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa4 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa5 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa6 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa7 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa8 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa16 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa17 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa18 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa19 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };

                    $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash1, undefined, undefined, undefined, undefined, undefined);

                } else {
                    $("#tabReqEstado").hide();
                    $("#tabInciEstado").show();
                    $("#tabReqTiempo").hide();
                    $("#tabInciTiempo").show();
                    $("#tabInciServicio").show();
                    $("#tabReqServicio").hide();
                    $("#tabInciPrioridad").show();
                    $("#tabReqPrioridad").hide();

                    $("#tabCambio").hide();
                    $("#tabTendenciaTicket").show();
                    $("#tabPendientes").show();
                    $("#tabSla").show();

                    $("#panelRequerimientos").hide();
                    $("#panelIncidentes").show();
                    $("#TituloSLA").html("Incidentes Atendidos");
                    $scope.tab = 16;

                    $scope.form.selectedEmpresa2 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa3 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa4 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa5 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa6 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa7 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa8 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa16 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa17 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa18 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };
                    $scope.form.selectedEmpresa19 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };

                    $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash16, undefined, undefined, undefined, undefined, undefined);

                }
            }

            $scope.columna = false;

        } else {
            $scope.columna = false;
            $scope.tab = 21;
            $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash21, undefined, undefined, undefined, undefined, undefined);
        }

    }]);

angular.module('masterGenerysV1App')
    .controller('StackBar', ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash', function (key, data) {

            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861', '#EDFF7C', '#FFB318', '#25FFB7', '#6316E8', '#857BFF']
            $scope.labels = data.labels;
            $scope.type = 'StackedBar';
            $scope.series = data.series;

            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            };

            $scope.data = data.data;
        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, servicio, prioridad, estado, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo1 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }
            if (tipo == undefined) { tipo = '-1'; }
            
            var vista = $scope.periodo1 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + estado + "," + tipo;

            $http.get("api/fogetdetalleR1/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

    }]);

angular.module('masterGenerysV1App')
    .controller('Reporte16', ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash', function (key, data) {


            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861', '#EDFF7C', '#FFB318', '#25FFB7', '#6316E8', '#857BFF']
            //if (data.series[0] === 'Pendientes') {
            //    $scope.colors = ['#e74c3c', '#27ae60'];
            //} else {
            //    $scope.colors = ['#27ae60', '#e74c3c'];
            //}

            $scope.labels = data.labels;
            $scope.type = 'StackedBar';
            $scope.series = data.series;

            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            };

            $scope.data = data.data;


        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, servicio, prioridad, estado, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo16 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }

            var vista = $scope.periodo16 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + estado + "," + tipo;

            $http.get("api/fogetdetalleR1/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

    }]);

angular.module('masterGenerysV1App')
    .controller("LineCtrl", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash2', function (key, data) {

            $scope.labels = data.labels;
            $scope.legend = data.legend;

            $scope.series = data.series;
            $scope.data = data.data;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861']
            $scope.onClick = function (points, evt) {
                //console.log(points, evt);
            };

            $scope.datasetOverride = [{
                yAxisID: 'y-axis-1'
            }, {
                yAxisID: 'y-axis-1'
            },
            {
                yAxisID: 'y-axis-1'
            }];
            $scope.options = {
                scales: {
                    yAxes: [{
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }, {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }]
                },
                legend: {
                    display: true
                }
            };


        });

        $scope.opendetalle = function (empresa, servicio, prioridad, fecha, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo2 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }
            if (tipo == undefined) { tipo = '-1'; }

            var vista = $scope.periodo2 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + fecha + "," + tipo;

            $http.get("api/fogetdetalleR2/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte17", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash17', function (key, data) {

            $scope.labels = data.labels;
            $scope.legend = data.legend;

            $scope.series = data.series;
            $scope.data = data.data;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861']

            $scope.onClick = function (points, evt) {
                //console.log(points, evt);
            };

            $scope.datasetOverride = [{
                yAxisID: 'y-axis-1'
            }, {
                yAxisID: 'y-axis-1'
            },
            {
                yAxisID: 'y-axis-1'
            }];
            $scope.options = {
                scales: {
                    yAxes: [{
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }, {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }]
                },
                legend: {
                    display: true
                }
            };
        });

        $scope.opendetalle = function (empresa, servicio, prioridad, fecha, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo17 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }

            var vista = $scope.periodo17 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + fecha + "," + tipo;

            $http.get("api/fogetdetalleR2/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };
    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte3", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash3', function (key, data) {

            $scope.labels = data.labels;
            $scope.series = data.series;
            $scope.legend = data.series;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861']

            $scope.data = data.data;

            $scope.options = {
                legend: { display: true }
            };

        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, prioridad, servicio, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo3 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }
            if (tipo == undefined) { tipo = '-1'; }

            var vista = $scope.periodo3 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo;

            $http.get("api/fogetdetalleR3/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte18", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash18', function (key, data) {
            $scope.labels = data.labels;
            $scope.series = data.series;
            $scope.legend = data.series;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861']
            $scope.data = data.data;

            $scope.options = {
                legend: { display: true }
            };

        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, prioridad, servicio, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo18 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }

            var vista = $scope.periodo18 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo;

            $http.get("api/fogetdetalleR3/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte4", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash4', function (key, data) {

            $scope.labels = data.labels;
            $scope.series = data.series;
            $scope.legend = data.series;

            $scope.data = data.data;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861', '#EDFF7C', '#FFB318']
            $scope.options = {
                legend: { display: true }
            };

        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, servicio, prioridad, fecha, anio, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo4 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }
            if (tipo == undefined) { tipo = '-1'; }

            var vista = $scope.periodo4 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + fecha + "," + anio + "," + tipo;

            $http.get("api/fogetdetalleR6/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };
    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte19", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash19', function (key, data) {

            $scope.labels = data.labels;
            $scope.series = data.series;
            $scope.legend = data.series;

            $scope.data = data.data;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861', '#EDFF7C', '#FFB318']
            $scope.options = {
                legend: { display: true }
            };

        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, servicio, prioridad, fecha, anio, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo19 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }

            var vista = $scope.periodo19 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + fecha + "," + anio + "," + tipo;

            $http.get("api/fogetdetalleR6/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };
    }]);

angular.module('masterGenerysV1App')
    .controller("reporte5", ['$scope', function ($scope) {

        $scope.$on('builDash5', function (key, data) {

            $scope.labels = data.labels;
            $scope.series = data.series;

            $scope.data = data.data;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861']
        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

    }]);


angular.module('masterGenerysV1App')
    .controller("reporte6", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash6', function (key, data) {

            $scope.labels = data.labels;
            $scope.legend = data.series;
            $scope.series = data.series;
            $scope.data = data.data;
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861' ]
            $scope.onClick = function (points, evt) {
                //console.log(points, evt);
            };
            $scope.datasetOverride = [{
                yAxisID: 'y-axis-1'
            }, {
                yAxisID: 'y-axis-1'
            },
            {
                yAxisID: 'y-axis-1'
            } 
            ];
            $scope.options = {
                scales: {
                    yAxes: [{
                        id: 'y-axis-1',
                        type: 'bar',
                        display: true,
                        position: 'left'
                    }, {
                        id: 'y-axis-1',
                        type: 'bar',
                        display: true,
                        position: 'left'
                    }, {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }]
                },
                legend: {
                    display: true
                }
            };

        });

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, servicio, prioridad, fecha, anio, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo6 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }
            if (tipo == undefined) { tipo = '-1'; }

            var vista = $scope.periodo6 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + fecha + "," + anio + "," + tipo;

            $http.get("api/fogetdetalleR4/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

    }]);


angular.module('masterGenerysV1App')
    .controller('Reporte7', ['$scope', '$http', '$rootScope', '$window', '$uibModal', function ($scope, $http, $rootScope, $window, $uibModal) {
        $scope.tabsol = true;

        $scope.form1 = {

        };

        $scope.form1.filtros = {
            fI_CODIGO: 1,
            fI_NOMBRE: "Valor Objetivo",
            fI_VALOR: "AT_VALOROBJ,AT_UMOBJ"
        };

        $scope.$on('builDash7', function (key, data) {

            if (data.series[0] === 'Vencidos') {
                $scope.colors = ['#e74c3c', '#27ae60'];
            } else {
                $scope.colors = ['#27ae60', '#e74c3c'];
            }

            $scope.labels = data.labels;
            $scope.type = 'StackedBar';
            $scope.series = data.series;
 
            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            };

            $scope.data = data.data; 
        });
 
        $scope.onClick = function (points, evt) {
        };

        $scope.ActivarTab = function (checktab) {
            console.log(checktab);
            if (checktab === 1) {
                $scope.tabsol = true;
                $scope.tabres = false;
            } else {
                $scope.tabsol = false;
                $scope.tabres = true;
            }

        };

        $scope.opendetalle = function (empresa, tipo, prioridad, sla, servicio, grupo, valorsla, gruposla) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo7 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (grupo == undefined) { grupo = '-1'; }
            if (valorsla == undefined) { valorsla = '-1'; }
            
             
            var vista = $scope.periodo7 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla + "," + servicio + "," + grupo + "," + gruposla + "," + valorsla;

            $http.get("api/fogetdetalle/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $scope.opendetallerespuesta = function (empresa, tipo, prioridad, sla, servicio, grupo, valorsla) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo7 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (grupo == undefined) { grupo = '-1'; }
            if (valorsla == undefined) { valorsla = '-1'; }


            var vista = $scope.periodo7 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla + "," + servicio + "," + grupo + "," + valorsla;

            $http.get("api/fogetdetalleres/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $scope.opendetallesla = function (empresa, tipo, prioridad, sla, servicio, vm, grupo, valorsla, gruposla) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo7 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (grupo == undefined) { grupo = '-1'; }
            if (valorsla == undefined) { valorsla = '-1'; }

            var vista = $scope.periodo7 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla + "," + servicio + "," + vm + "," + grupo + "," + gruposla + "," + valorsla;

            $http.get("api/fogetdetallesla/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            array.push(vm);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $scope.opendetalleslarespuesta = function (empresa, tipo, prioridad, sla, servicio, vm, grupo, valorsla) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo7 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (grupo == undefined) { grupo = '-1'; }
            if (valorsla == undefined) { valorsla = '-1'; }

            var vista = $scope.periodo7 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla + "," + servicio + "," + vm + "," + grupo + "," + valorsla;

            $http.get("api/fogetdetalleslares/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            array.push(vm);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $http.get("api/fogetbyfiltros/0007").then(function (serviceResp) {
            $scope.filtros = serviceResp.data;
        });
    }]);

angular.module('masterGenerysV1App')
    .controller('Reporte8', ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {

        $scope.$on('builDash8', function (key, data) {

            $scope.labels = data.labels;
            $scope.series = data.series;
            $scope.legend = data.series
            $scope.colors = ['#259CFF', '#7D16E8', '#0AE861']
            $scope.data = data.data;

            $scope.options = {
                legend: { display: true }
            };

        });
        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, servicio, prioridad, fecha, anio, tipo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo8 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }

            var vista = $scope.periodo8 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + fecha + "," + anio + "," + tipo;

            $http.get("api/fogetdetalleR5/" + vista + "/" + info + "/" + servicio).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verticket.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };
    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte21", ['$scope', '$window', '$uibModal', '$rootScope', '$http', function ($scope, $window, $uibModal, $rootScope, $http) {
        $scope.chart1 = true;
        $scope.chart2 = false;
        $scope.Titulo21 = 'Tendencia de SLA Tiempo de Solución';

        $scope.filtros = {
            fI_CODIGO: 1,
            fI_NOMBRE: "Valor Objetivo",
            fI_VALOR: "AT_VALOROBJ,AT_UMOBJ"
        };

        $scope.$on('builDash21', function (key, data) {

            $scope.labels = data.labels;
            $scope.labels2 = data.labels2;
            $scope.legend = data.legend;

            $scope.series = data.series;
            $scope.data = data.data;
            $scope.data2 = data.data2;
            $scope.colors2 = ['#33cd33', '#ff0101'];
            $scope.colors  = ['#33cd33', '#ffa501', '#ff0101', '#808080'];
            $scope.onClick = function (points, evt) {
                //console.log(points, evt);
            };

            $scope.ActivarChart = function (checktab) {
                console.log(checktab);
                if (checktab === 1) {
                    $scope.chart1 = true;
                    $scope.chart2 = false;
                    $scope.Titulo21 = 'Tendencia de SLA Tiempo de Solución';
                } else {
                    $scope.chart1 = false;
                    $scope.chart2 = true;
                    $scope.Titulo21 = 'Porcentaje de Cumplimiento';
                }
            };

            $scope.datasetOverride = [{
                yAxisID: 'y-axis-1'
            }, {
                yAxisID: 'y-axis-1'
            }];

            $scope.options = {
                tooltips: {
                    enabled: false
                },
                hover: {
                    animationDuration: 0
                },
                animation: {
                    duration: 1,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
                                ctx.fillStyle = '#444';
                                var y_pos = model.y - 5;
                                // Make sure data value does not get overflown and hidden
                                // when the bar's value is too close to max value of scale
                                // Note: The y value is reverse, it counts from top down
                                if ((scale_max - model.y) / scale_max >= 0.93)
                                    y_pos = model.y + 20;
                                ctx.fillText(dataset.data[i], model.x, y_pos);
                            }
                        });
                    }
                },
                scales: {
                     
                    yAxes: [{
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left',
                        gridLines: {
                            display: false
                        }
                    }, {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left',
                        gridLines: {
                            display: false
                        }
                    }]
                },
                legend: {
                    display: true
                } 
            };
        });

        $scope.opendetalle = function (empresa, tipo, prioridad, sla, servicio, grupo, valorsla, gruposla, periodo) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo21 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            
            if (prioridad == undefined) { prioridad = '-1'; }
            if (servicio == undefined) { servicio = '-1'; }
            if (grupo == undefined) { grupo = '-1'; }
            if (valorsla == undefined) { valorsla = '-1'; }
            if (gruposla == undefined) { gruposla = '-1'; }
            if (tipo == undefined) { tipo = '-1'; }
            if (periodo == undefined) { periodo = '-1'; }
            

            var vista = $scope.periodo21 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla + "," + servicio + "," + grupo + "," + gruposla + "," + periodo + "," + valorsla;

            $http.get("api/fogetdetalle/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $scope.opendetallesla = function (empresa, tipo, prioridad, sla, servicio, vm, grupo, valorsla, gruposla,periodo) {
            console.log(periodo);
            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo21 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            if (servicio == undefined) { servicio = '-1'; }
            if (grupo == undefined) { grupo = '-1'; }
            if (valorsla == undefined) { valorsla = '-1'; }
            if (prioridad == undefined) { prioridad = '-1'; }
            if (gruposla == undefined) { gruposla = '-1'; }
            if (tipo == undefined) { tipo = '-1'; }
            if (periodo == undefined) { periodo = '-1'; }
            
            
            var vista = $scope.periodo21 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla + "," + servicio + "," + vm + "," + grupo + "," + gruposla + "," + periodo + "," + valorsla;

            $http.get("api/fogetdetallesla/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            array.push(vm);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

    }]);