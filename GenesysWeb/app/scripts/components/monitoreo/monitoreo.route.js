﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
	$stateProvider
          .state('monitoreo', {
          	parent: 'main',
          	url: 'monitoreo?idServicio&nombreServicio&idEmpresa&nombreEmpresa&tipo',
          	views: {
          		'content@main': {
          			templateUrl: 'app/views/components/monitoreo/monitoreo.html',
          			controller: 'MonitoreoCtrl',
          			controllerAs: 'monitoreo',
          			authorize: true

          		}
          	}
          });
});