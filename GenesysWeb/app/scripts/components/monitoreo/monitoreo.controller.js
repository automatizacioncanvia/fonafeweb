﻿'use strict';

angular.module('masterGenerysV1App')
    .controller('MonitoreoCtrl', ['$scope', 'Apimenu', '$rootScope', 'Apidashboard', 'Notification', '$uibModal', '$http', '$parse', '$window', 'ngTableParams', '$filter', 'Apifonfe', '$stateParams', '$state', function ($scope, Apimenu, $rootScope, Apidashboard, Notification, $uibModal, $http, $parse, $window, ngTableParams, $filter, Apifonfe, $stateParams, $state) {

        $scope.form = {};
        $scope.columna = true;

        $scope.nodata0012 = false;
        $scope.nodata0013 = false;
        $scope.nodata0014 = false;
        $scope.nodata0015 = false;

        $scope.tab = 12;

        $scope.dash12 = "0012";
        $scope.dash13 = "0013";
        $scope.dash14 = "0014";
        $scope.dash15 = "0015";



        var ngtablei = function (array) {

            $scope.tableParams = new ngTableParams({
                page: 1,
                count: 15
            }, {
                total: array.length,
                getData: function ($defer, params) {
                    var filteredData = params.filter() ?
                        $filter('filter')(array, params.filter()) :
                        array;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(filteredData, params.orderBy()) :
                        array;

                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
        };

        $scope.selectTab = function (setTab) {
            switch (setTab) {
                case 12:
                    if ($scope.ff12 === undefined) {
                        $scope.getchart(2, '-1', $scope.dash12, undefined, undefined, undefined);
                    }
                    break;
                case 13:
                    if ($scope.ff13 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash13, undefined, undefined, undefined);
                    }
                    break;
                case 14:
                    if ($scope.ff14 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash14, undefined, undefined, 'AT_VALOROBJ,AT_UMOBJ');

                    }
                    break;
                case 15:
                    if ($scope.ff15 === undefined) {
                        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash15, undefined, undefined, undefined);
                    }
                    break;
            }
            $scope.tab = setTab;
        };

        $scope.isSelected = function (checktab) {
            return $scope.tab === checktab;
        };

        var fail = function (serviceResp) {
            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
        };

        Apimenu.getmenu(32, $rootScope.useri.userid).then(function (serviceResp) {
            var index = Apidashboard.indexOfespecial(serviceResp.data.data);

            if (index !== -1) {
                Apimenu.getmenu(5, $rootScope.useri.userid).then(function (serviceResp) { $scope.servicios = serviceResp.data.data; }, fail);

                if ($stateParams.idServicio != undefined) {
                    //$scope.form.selectedEmpresa12 = $stateParams.idEmpresa;

                } else {
                    $scope.form.selectedEmpresa12 = { eM_ID: '-1', eM_NOMBRE: 'Todos' };
                }

                $scope.form.selectedEmpresa13 = serviceResp.data.data[index];
                $scope.form.selectedEmpresa14 = serviceResp.data.data[index];
                $scope.form.selectedEmpresa15 = serviceResp.data.data[index];
            }
            $scope.empresas = serviceResp.data.data;
        }, fail);

        Apimenu.getmenu(31, $rootScope.useri.userid).then(function (serviceResp) {
            $scope.empresas2 = serviceResp.data.data;
        }, fail);


        Apimenu.getmenu(9, '-').then(function (serviceResp) {
            $scope.periodos = serviceResp.data.data;
        }, fail)

        Apimenu.getmenu(18, '-').then(function (serviceResp) {
            $scope.resolutor = serviceResp.data.data;
        }, fail)

        //metodos globales
        $scope.getchart = function (periodo, em, idchart, servicio, prioridad, sla, tipo) {
            if (tipo == undefined) {
                tipo = '-1';
            }

            if (em == undefined) {
                em = -1;
            }

            if (periodo !== 1) {
                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/chart2.png",
                    backgroundColor: 'rgba(41, 127, 130, 0.79)',
                    loadingHtml: "<p class='loading-message'>Cargando Monitoreo...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
                });

                debugger;
                Apidashboard.getdashi(periodo, em, idchart, servicio, prioridad, sla, tipo).then(function (serviceResp) {

                    var the_string = "nodata" + idchart;
                    var model = $parse(the_string);

                    if (serviceResp.data.length > 0) {

                        if (idchart === "0012") {
                            model.assign($scope, false);
                            $scope.value12 = false;
                            $scope.periodo12 = periodo;
                            $scope.ff12 = serviceResp.data;
                            $scope.fullkeys12 = Object.keys(serviceResp.data[0]);
                            ngtablei($scope.ff12);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0013") {
                            model.assign($scope, false);
                            $scope.value13 = false;
                            $scope.periodo13 = periodo;
                            $scope.ff13 = serviceResp.data;
                            $scope.fullkeys13 = Object.keys(serviceResp.data[0]);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0014") {
                            model.assign($scope, false);
                            $scope.value14 = false;
                            $scope.periodo14 = periodo;
                            $scope.ff14 = serviceResp.data;
                            $scope.fullkeys14 = Object.keys(serviceResp.data[0]);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0015") {
                            model.assign($scope, false);
                            $scope.value15 = false;
                            $scope.periodo15 = periodo;
                            $scope.ff15 = serviceResp.data;
                            $scope.fullkeys15 = Object.keys(serviceResp.data[0]);
                            $window.loading_screen.finish();
                        }
                    } else {
                        ngtablei(['']);
                        model.assign($scope, true);
                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $window.loading_screen.finish();
                    }
                }, fail)
            }
            else {
                if (idchart === "0012") {
                    $scope.periodo12 = periodo;
                    $scope.value12 = true;
                }
                if (idchart === "0013") {
                    $scope.periodo13 = periodo;
                    $scope.value13 = true;
                }
                if (idchart === "0014") {
                    $scope.periodo14 = periodo;
                    $scope.value14 = true;
                }
                if (idchart === "0015") {
                    $scope.periodo15 = periodo;
                    $scope.value15 = true;
                }
            }
        };

        //$scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash12, undefined, undefined, undefined);

        $scope.openticket = function (ticket) {
            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando Monitoreo...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            $http.get("api/fogetgrupos/" + ticket).then(function (serviceResp) {
                $scope.ticketsobtenidos = serviceResp.data;
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/monitoreo/modals/vergrupo.html',
                    controller: 'ModalVerGrupo',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketsobtenidos);
                            return array;
                        }
                    }
                });
            });
        };

        $scope.openSD = function (ticket) {

            Apifonfe.linkticket(ticket).success(function (data) {
                var dati = data.split('#');
                var ind = dati[0].indexOf('CAisd');
                var final = dati[0].substring(0, ind);
                $window.open(final + dati[1], 'OpenTicket', 'width=1500,height=700');
            }).error(function (data) {
                Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
            })
        }

        $scope.entrefechas = function (from, to, em, idchart, servicio, prioridad, sla) {
            $scope.from = from;
            $scope.to = to;
            var sla = (sla === undefined ? '-1' : sla);

            if (em !== undefined) {
                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/chart2.png",
                    backgroundColor: 'rgba(41, 127, 130, 0.79)',
                    loadingHtml: "<p class='loading-message'>Cargando Monitoreo...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
                });

                Apidashboard.getdashbydate(from, to, em, idchart, servicio, prioridad, sla).then(function (serviceResp) {
                    var the_string = "nodata" + idchart;
                    var model = $parse(the_string);

                    if (serviceResp.data.length > 0) {
                        if (idchart === "0012") {
                            model.assign($scope, false);
                            $scope.ff12 = serviceResp.data;
                            $scope.fullkeys12 = Object.keys(serviceResp.data[0]);
                            ngtablei($scope.ff12);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0013") {
                            model.assign($scope, false);
                            $scope.ff13 = serviceResp.data;
                            $scope.fullkeys13 = Object.keys(serviceResp.data[0]);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0014") {
                            model.assign($scope, false);
                            $scope.ff14 = serviceResp.data;
                            $scope.fullkeys14 = Object.keys(serviceResp.data[0]);
                            $window.loading_screen.finish();
                        }
                        if (idchart === "0015") {
                            model.assign($scope, false);
                            $scope.ff15 = serviceResp.data;
                            $scope.fullkeys15 = Object.keys(serviceResp.data[0]);
                            $window.loading_screen.finish();
                        }
                    } else {
                        ngtablei(['']);
                        model.assign($scope, true);
                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $window.loading_screen.finish();
                    }
                }, fail);
            } else {
                Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Porfavor seleccione una empresa.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
            }
        };


        if ($stateParams.idServicio != undefined) {
            $("#panelRegresar").show();
            $("#tabTableroAccion").hide();
            $("#tabSLA").hide();

            $("#tabPendientes").hide();
            $("#divAutoRefresco1").hide();
            $("#divAutoRefresco2").hide();


            $scope.columna = false;
            $scope.form.selectedServicio12 = { sE_ID: $stateParams.idServicio, sE_NOMBRE: $stateParams.nombreServicio };

            $scope.form.selectedEmpresa12 = { eM_ID: $stateParams.idEmpresa, eM_NOMBRE: $stateParams.nombreEmpresa };

            $scope.getchart(2, $stateParams.idEmpresa, $scope.dash12, $stateParams.idServicio, undefined, undefined, $stateParams.tipo);

        } else {
            $scope.getchart(2, '-1', $scope.dash12, undefined, undefined, undefined, undefined);
        }


        $scope.openRegresar = function () {

            $state.go('dashboard', { idServicio: $stateParams.idServicio, nombreServicio: $stateParams.nombreServicio, idEmpresa: $stateParams.idEmpresa, nombreEmpresa: $stateParams.nombreEmpresa, activar: 1 });

        };


    }]);

angular.module('masterGenerysV1App')
    .controller('Reporte12', ['$scope', 'Apimenu', '$interval', '$http', 'Notification', '$window', '$uibModal', function ($scope, Apimenu, $interval, $http, Notification, $window, $uibModal) {
        var timer;
        var c;
        $scope.status = false;
        $scope.count = 1;

        $scope.prueba = function (x) {
            console.log(x);
        };

        $scope.changeStatus = function (empresa, idchart, periodo, servicio, prioridad, sla) {
            $scope.status = !$scope.status;


            if ($scope.status) {
                c = 0;

                $scope.empresai = empresa;
                $scope.idcharti = idchart;
                $scope.periodoi = periodo;
                $scope.servicioi = servicio;
                $scope.prioridadi = prioridad;
                $scope.slai = sla;

                $scope.timer = $interval(function (argument) {

                    c++;
                    console.log(c);
                    if (c == 60) {
                        var sla = ($scope.slai === undefined ? '-1' : $scope.slai);
                        if (periodo !== 1) {

                            if (periodo === undefined) {

                                Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Periodo Dashboard ' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                            } else {

                                if (empresa !== undefined) {

                                    var servicio = ($scope.servicioi === undefined ? -1 : $scope.servicioi.sE_ID);
                                    var prioridad = ($scope.prioridadi === undefined ? -1 : $scope.prioridadi);

                                    $scope.getchart(periodo, empresa.eM_ID, idchart, servicio, prioridad, sla);
                                    Apimenu.getmenu(10, empresa.eM_ID).then(function (serviceResp) {
                                        $scope.servicios = serviceResp.data.data;
                                    });

                                } else {
                                    // $scope.getchart(periodo, undefined, idchart,servicio,prioridad);
                                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Empresa' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                                    $scope.servicios = [];
                                }
                            }
                        }
                    }
                    if (c > 60) { c = 0; }
                }, 1000);

            } else {
                if (angular.isDefined($scope.timer)) {
                    $interval.cancel($scope.timer);
                    $scope.timer = undefined;
                }
            }
        };

        $scope.onSelected12 = function (empresa, idchart, periodo, servicio, prioridad, sla) {
            var sla = (sla === undefined ? '-1' : sla);
            if (periodo !== 1) {

                if (periodo === undefined) {

                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Periodo Dashboard ' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });


                } else {

                    if (empresa !== undefined) {

                        var servicio = (servicio === undefined ? -1 : servicio.sE_ID);
                        var prioridad = (prioridad === undefined ? -1 : prioridad);

                        $scope.empresai = empresa;
                        $scope.idcharti = idchart;
                        $scope.periodoi = periodo;
                        $scope.servicioi = servicio;
                        $scope.prioridadi = prioridad;
                        $scope.slai = sla;

                        $scope.getchart(periodo, empresa.eM_ID, idchart, servicio, prioridad, sla);

                        Apimenu.getmenu(10, empresa.eM_ID).then(function (serviceResp) {
                            $scope.servicios = serviceResp.data.data;
                        });

                    } else {
                        // $scope.getchart(periodo, undefined, idchart,servicio,prioridad);
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Empresa' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $scope.servicios = [];
                    }
                }
            }
        };

		
        var comboState = function () {
            var estado = [{ id: "-1", description: "Todas" }, { id: "2", description: "Vencido" }, { id: "1", description: "Por Vencer" }, { id: "0", description: "Pendiente" }];
            return estado;
        };

        $scope.states = comboState();
        $scope.form1 = {

        };
        $scope.form1.filtros = {
            fI_CODIGO: 1,
            fI_NOMBRE: "Valor Objetivo",
            fI_VALOR: "ATVAOBJ,ATUMOBJ" // AT_VALOROBJ,AT_UMOBJ
        };

        $http.get("api/fogetbyfiltros/0007").then(function (serviceResp) {
            $scope.filtros = serviceResp.data;
            //console.log(serviceResp.data);
        });

        $scope.openticket = function (ticket) {
            debugger;
            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            $http.get("api/fogetgrupos/" + ticket).then(function (serviceResp) {
                $scope.ticketsobtenidos = serviceResp.data;
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/monitoreo/modals/vergrupo.html',
                    controller: 'ModalVerGrupo',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketsobtenidos);
                            return array;
                        }
                    }
                });
            });
        };

        $scope.openstate = function (ticket) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            $http.get("api/fogetstatus/" + ticket).then(function (serviceResp) {
                $scope.ticketsobtenidos = serviceResp.data;
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/monitoreo/modals/verstatus.html',
                    controller: 'ModalVerStatus',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketsobtenidos);
                            return array;
                        }
                    }
                });
            });
        };

    }]);

angular.module('masterGenerysV1App')
    .controller('Reporte13', ['$scope', '$http', '$rootScope', '$window', '$uibModal', function ($scope, $http, $rootScope, $window, $uibModal) {

        $scope.form1 = {

        };

        $scope.form1.filtros = {
            fI_CODIGO: 1,
            fI_NOMBRE: "Valor Objetivo",
            fI_VALOR: "ATVAOBJ,ATUMOBJ" //AT_VALOROBJ,AT_UMOBJ
        };

        $http.get("api/fogetbyfiltros/0007").then(function (serviceResp) {
            $scope.filtros = serviceResp.data;
        });

        $scope.$on('builDash13', function (key, data) {

            if (data.series[0] === 'Vencidos') {
                $scope.colors = ['#e74c3c', '#27ae60'];
            } else {
                $scope.colors = ['#27ae60', '#e74c3c'];
            }

            $scope.labels = data.labels;
            $scope.type = 'StackedBar';
            $scope.series = data.series;



            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            };

            $scope.data = data.data;
            //$scope.labels = data.labels;
            //$scope.series = data.series;

            //$scope.data = data.data;

        });

        //$scope.colors = ['#e74c3c', '#27ae60'];

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalleMonitoreo = function (empresa, tipo, prioridad, sla) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando Monitoreo...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo13 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }
			console.log("opendetalleMonitoreo"); var myArr1 = [empresa, tipo, prioridad, sla, info]; console.log(myArr1);
            var vista = $scope.periodo13 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla;
			
            $http.get("api/fogetdetmon/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $http.get("api/fogetbyfiltros/0013").then(function (serviceResp) {
            $scope.filtros = serviceResp.data;
            //console.log(serviceResp.data);
        });

    }]);

angular.module('masterGenerysV1App')
    .controller('Reporte14', ['$scope', '$http', '$rootScope', '$window', '$uibModal', function ($scope, $http, $rootScope, $window, $uibModal) {

        $scope.form1 = {

        };

        $scope.form1.filtros = {
            fI_CODIGO: 1,
            fI_NOMBRE: "Valor Objetivo",
            fI_VALOR: "ATVAOBJ,ATUMOBJ" //AT_VALOROBJ,AT_UMOBJ
        };

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, tipo, prioridad, sla) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando Monitoreo...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo14 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            var vista = $scope.periodo14 + "," + $rootScope.useri.userid + "," + empresa + "," + prioridad + "," + tipo + "," + sla;

            $http.get("api/fogetdetalle/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/indicadores/modals/verdetalle.html',
                    controller: 'ModalViewDetail',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $http.get("api/fogetbyfiltros/0014").then(function (serviceResp) {
            $scope.filtros = serviceResp.data;
            //console.log(serviceResp.data);
        });

    }]);

angular.module('masterGenerysV1App')
    .controller('Reporte15', ['$scope', '$http', '$rootScope', '$window', '$uibModal', function ($scope, $http, $rootScope, $window, $uibModal) {

        $scope.form1 = {

        };

        $scope.form1.filtros = {
            fI_CODIGO: 1,
            fI_NOMBRE: "Valor Objetivo",
            fI_VALOR: "ATVAOBJ,ATUMOBJ" //AT_VALOROBJ,AT_UMOBJ
        };

        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.opendetalle = function (empresa, estado) {

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando Monitoreo...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            var desde
            var hasta
            var info

            if ($scope.periodo15 == 1) {
                desde = moment($scope.from).format("YYYYMMDD");
                hasta = moment($scope.to).format("YYYYMMDD");
                info = "" + desde + "," + hasta;
            }
            else {
                info = "-";
            }

            var vista = $scope.periodo15 + "," + $rootScope.useri.userid + "," + empresa + "," + estado;

            $http.get("api/fogetdetailstate/" + vista + "/" + info).then(function (serviceResp) {
                $scope.ticketdetail = serviceResp.data;
                $scope.Empresa = $rootScope.useri.empresa_id
                $window.loading_screen.finish();
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/views/components/monitoreo/modals/verdetallemon.html',
                    controller: 'ModalViewDetailMon',
                    size: 'lg',
                    resolve: {
                        filtrosobtenidos: function () {
                            var array = [];
                            array.push($scope.ticketdetail);
                            //array.push($scope.Empresa);
                            return array;
                        }
                    }
                });
            });
        };

        $http.get("api/fogetbyfiltros/0015").then(function (serviceResp) {
            $scope.filtros = serviceResp.data;
            //console.log(serviceResp.data);
        });

    }]);


angular.module('masterGenerysV1App')
    .controller('InformeMensual', ['$scope', 'Apimenu', '$interval', '$http', 'Notification', '$window', '$uibModal', 'SweetAlert', function ($scope, Apimenu, $interval, $http, Notification, $window, $uibModal, SweetAlert) {

        Apimenu.getmenu(29, 0).then(function (serviceResp) {
            $scope.anio = serviceResp.data.data;
        });

        $scope.ocultar = function (panel) {

            switch (panel) {
                case 1:
                    if ($("#row-1").is(":hidden")) {
                        $("#row-1").show("slow");
                    } else {
                        $("#row-1").slideUp();
                    }
                    break;
                case 2:
                    if ($("#row-2").is(":hidden")) {
                        $("#row-2").show("slow");
                    } else {
                        $("#row-2").slideUp();
                    }
                    break;
                case 3:
                    if ($("#row-3").is(":hidden")) {
                        $("#row-3").show("slow");
                    } else {
                        $("#row-3").slideUp();
                    }
                    break;
                case 4:
                    if ($("#row-4").is(":hidden")) {
                        $("#row-4").show("slow");
                    } else {
                        $("#row-4").slideUp();
                    }
                    break;

            }


        }


        $scope.descargarInforme = function () {
            //var NWin = window.open("app/views/components/monitoreo/informe.html", '', 'height=1,width=1');

            //if (window.focus)
            //{
            //    NWin.focus();   
            //}

            var empresa = $scope.form.selectedEmpresa4.eM_ID;

            var anio = $scope.form.selectedAnio.anio;

            var mes = $scope.form.selectedMes.fI_NOMBRE;

            debugger;

            //window.open("app/views/components/monitoreo/informe.html?empresa="+empresa+"&anio="+anio+"&mes="+mes);
            window.open("app/views/components/monitoreo/informe.html?empresa=" + empresa + "&anio=" + anio + "&mes=" + mes, '_blank', 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,left=90000, top=90000, width=10, height=10, visible=none', '');
        };


        $http.get("api/fogetbyfiltros/0010").then(function (serviceResp) {
            $scope.meses = serviceResp.data;
            //console.log(serviceResp.data);
        });

        $scope.verInforme = function () {


            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 170, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando información...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
            });

            $("#btnDescargar").show();

            var empresa = $scope.form.selectedEmpresa4;
            var error = 0;

            if (empresa == undefined) {
                error = 1;
            }

            var anio = $scope.form.selectedAnio;
            if (anio == undefined) {
                if (error == 1) {
                    error = 2;
                } else {
                    error = 5;
                }
            }

            var mes = $scope.form.selectedMes;

            if (mes == undefined) {
                if (error == 1) {
                    error = 3;
                } else {
                    if (error == 2) {
                        error = 4;
                    } else {
                        error = 6;
                    }
                }
            }

            switch (error) {
                case 1:
                    SweetAlert.swal("Error!", "Por favor seleccione la empresa.", "error");
                    break;
                case 2:
                    SweetAlert.swal("Error!", "Por favor seleccione la empresa y el año.", "error");
                    break;
                case 3:
                    SweetAlert.swal("Error!", "Por favor seleccione la empresa y el mes.", "error");
                    break;
                case 4:
                    SweetAlert.swal("Error!", "Por favor seleccione la empresa, el año y el mes.", "error");
                    break;
                case 5:
                    SweetAlert.swal("Error!", "Por favor seleccione el año.", "error");
                    break;
                case 6:
                    SweetAlert.swal("Error!", "Por favor seleccione el año y el mes.", "error");
                    break;
            }

            var filtro = anio.anio + "," + mes.fI_NOMBRE + "," + empresa.eM_ID;

            Apimenu.getmenu(28, filtro).then(function (serviceResp) {
                $scope.dataInforme = serviceResp.data.data;
                $scope.dataInforme2 = serviceResp.data.data1;
                $scope.dataInforme3 = serviceResp.data.data2;
                $scope.dataInforme4 = serviceResp.data.data3;
                debugger;

                var html = "<tr style='background-color:red;color:white;text-align:center;'>";
                html += "<th>Empresas</th>";
                html += "<th>Metodo Reporte</th>";
                html += "<th>Incidentes</th>";
                html += "<th>Requerimientos</th>";
                html += "<th>Total</th>";
                html += "</tr>";

                var nombreEmpresa = "";

                for (var i = 0; i < serviceResp.data.data3.length; i++) {

                    if (serviceResp.data.data3[i].totalFila > 1) {
                        if (nombreEmpresa == serviceResp.data.data3[i].empresa) {
                            html += "<tr>";
                            html += "<th>" + serviceResp.data.data3[i].metodoInforme + "</th>";
                            html += "<th>" + serviceResp.data.data3[i].incidentes + "</th>";
                            html += "<th>" + serviceResp.data.data3[i].requerimientos + "</th>";
                            html += "<th>" + serviceResp.data.data3[i].total + "</th>";
                            html += "</tr>";
                        } else {
                            nombreEmpresa = serviceResp.data.data3[i].empresa;
                            html += "<tr>";
                            html += "<th rowspan='" + serviceResp.data.data3[i].totalFila + "'>" + serviceResp.data.data3[i].empresa + "</th>";
                            html += "<th>" + serviceResp.data.data3[i].metodoInforme + "</th>";
                            html += "<th>" + serviceResp.data.data3[i].incidentes + "</th>";
                            html += "<th>" + serviceResp.data.data3[i].requerimientos + "</th>";
                            html += "<th>" + serviceResp.data.data3[i].total + "</th>";

                            html += "</tr>";
                        }
                    } else {
                        if (nombreEmpresa != serviceResp.data.data3[i].empresa) {
                            nombreEmpresa = serviceResp.data.data3[i].empresa;
                        }
                        html += "<tr>";
                        html += "<th rowspan='1'>" + serviceResp.data.data3[i].empresa + "</th>";
                        html += "<th>" + serviceResp.data.data3[i].metodoInforme + "</th>";
                        html += "<th>" + serviceResp.data.data3[i].incidentes + "</th>";
                        html += "<th>" + serviceResp.data.data3[i].requerimientos + "</th>";
                        html += "<th>" + serviceResp.data.data3[i].total + "</th>";
                        html += "</tr>";
                    }
                }

                $("#tblCantInciReq").html(html);

                var html2 = "";
                html2 += "<tr style='background-color: #4072E6;color: white;'>";
                html2 += "<th>Empresas</th>";
                html2 += "<th>Tipo</th>";
                html2 += "<th>Servicio</th>";
                html2 += "<th>Prioridad</th>";
                html2 += "<th>Cerrados</th>";
                html2 += "<th>Total</th>";
                html2 += "</tr>";

                var nombreEmp2 = "";
                var tipo = "";
                var serv = "";

                for (var i = 0; i < serviceResp.data.data4.length; i++) {
                    if (nombreEmp2 != serviceResp.data.data4[i].empresa) {

                        if (serviceResp.data.data4[i].empTotal > 1) {
                            nombreEmp2 = serviceResp.data.data4[i].empresa;
                            tipo = serviceResp.data.data4[i].tipo;
                            serv = "";

                            if (serviceResp.data.data4[i].tipTotal > 1) {
                                if (serviceResp.data.data4[i].servTotal > 1) {
                                    serv = serviceResp.data.data4[i].servicio;
                                    html2 += "<tr>";
                                    html2 += "<th rowspan='" + serviceResp.data.data4[i].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].empresa + "</th>";
                                    html2 += "<th rowspan='" + serviceResp.data.data4[i].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].tipo + "</th>";
                                    html2 += "<th rowspan='" + serviceResp.data.data4[i].servTotal + "'>" + serviceResp.data.data4[i].servicio + "</th>";
                                    html2 += "<th>" + serviceResp.data.data4[i].prioridad + "</th>";
                                    html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                    html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                    html2 += "</tr>";
                                } else {
                                    html2 += "<tr>";
                                    html2 += "<th rowspan='" + serviceResp.data.data4[i].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].empresa + "</th>";
                                    html2 += "<th rowspan='" + serviceResp.data.data4[i].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].tipo + "</th>";
                                    html2 += "</tr>";
                                }
                            } else {

                                html2 += "<tr>";
                                html2 += "<th rowspan='" + serviceResp.data.data4[i].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].empresa + "</th>";
                                html2 += "<th rowspan='" + serviceResp.data.data4[i].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].tipo + "</th>";
                                html2 += "<th rowspan='" + serviceResp.data.data4[i].servTotal + "'>" + serviceResp.data.data4[i].servicio + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].prioridad + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                html2 += "</tr>";
                            }


                        } else {
                            html2 += "<tr>";
                            html2 += "<th rowspan='" + serviceResp.data.data4[i].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].empresa + "</th>";
                            html2 += "<th rowspan='" + serviceResp.data.data4[i].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].tipo + "</th>";
                            html2 += "<th rowspan='" + serviceResp.data.data4[i].servTotal + "'>" + serviceResp.data.data4[i].servicio + "</th>";
                            html2 += "<th>" + serviceResp.data.data4[i].prioridad + "</th>";
                            html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                            html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                            html2 += "</tr>";
                        }

                    } else {
                        if (tipo != serviceResp.data.data4[i].tipo) {
                            tipo = serviceResp.data.data4[i].tipo;
                            serv = serviceResp.data.data4[i].servicio;
                            html2 += "<tr>";
                            html2 += "<th rowspan='" + serviceResp.data.data4[i].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data4[i].tipo + "</th>";
                            html2 += "<th rowspan='" + serviceResp.data.data4[i].servTotal + "'>" + serviceResp.data.data4[i].servicio + "</th>";
                            html2 += "<th>" + serviceResp.data.data4[i].prioridad + "</th>";
                            html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                            html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                            html2 += "</tr>";
                        } else {
                            if (serv != serviceResp.data.data4[i].servicio) {
                                serv = serviceResp.data.data4[i].servicio;
                                html2 += "<tr>";
                                html2 += "<th rowspan='" + serviceResp.data.data4[i].servTotal + "'>" + serviceResp.data.data4[i].servicio + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].prioridad + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                html2 += "</tr>";
                            } else {
                                html2 += "<tr>";
                                html2 += "<th>" + serviceResp.data.data4[i].prioridad + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                html2 += "<th>" + serviceResp.data.data4[i].cerrado + "</th>";
                                html2 += "</tr>";
                            }
                        }
                    }//hasta aqui
                }

                $("#tblCantServicio").html(html2);

                var html3 = "";
                html3 += "<tr style='background-color: #4072E6;color: white;'>";
                html3 += "<th>Empresas</th>";
                html3 += "<th>Tipo</th>";
                html3 += "<th>Servicio</th>";
                html3 += "<th>Prioridad</th>";
                html3 += "<th>Promedio de tiempo de Solución(Horas)</th>";
                html3 += "<th>Máximo de tiempo de Solución(Horas)</th>";
                html3 += "<th>Mínimo de tiempo de Solución(Horas)</th>";
                html3 += "</tr>";

                var nombreEmp3 = "";
                var tipo2 = "";
                var serv2 = "";

                debugger;
                for (var z = 0; z < serviceResp.data.data5.length; z++) {
                    if (nombreEmp3 != serviceResp.data.data5[z].empresa) {

                        if (serviceResp.data.data5[z].empTotal > 1) {

                            nombreEmp3 = serviceResp.data.data5[z].empresa;
                            tipo2 = serviceResp.data.data5[z].tipo;
                            serv2 = "";

                            if (serviceResp.data.data5[z].tipTotal > 1) {
                                if (serviceResp.data.data5[z].servTotal > 1) {
                                    serv2 = serviceResp.data.data5[z].servicio;
                                    html3 += "<tr>";
                                    html3 += "<th rowspan='" + serviceResp.data.data5[z].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].empresa + "</th>";
                                    html3 += "<th rowspan='" + serviceResp.data.data5[z].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].tipo + "</th>";
                                    html3 += "<th rowspan='" + serviceResp.data.data5[z].servTotal + "'>" + serviceResp.data.data5[z].servicio + "</th>";
                                    html3 += "<th>" + serviceResp.data.data5[z].prioridad + "</th>";
                                    html3 += "<th>" + serviceResp.data.data5[z].tiempoPromedio + "</th>";
                                    html3 += "<th>" + serviceResp.data.data5[z].tiempoMaximo + "</th>";
                                    html3 += "<th>" + serviceResp.data.data5[z].tiempoMinimo + "</th>";
                                    html3 += "</tr>";
                                } else {
                                    html3 += "<tr>";
                                    html3 += "<th rowspan='" + serviceResp.data.data5[z].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].empresa + "</th>";
                                    html3 += "<th rowspan='" + serviceResp.data.data5[z].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].tipo + "</th>";
                                    html3 += "</tr>";
                                }
                            } else {
                                html3 += "<tr>";
                                html3 += "<th rowspan='" + serviceResp.data.data5[z].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].empresa + "</th>";
                                html3 += "<th rowspan='" + serviceResp.data.data5[z].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].tipo + "</th>";
                                html3 += "<th rowspan='" + serviceResp.data.data5[z].servTotal + "'>" + serviceResp.data.data5[z].servicio + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].prioridad + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoPromedio + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoMaximo + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoMinimo + "</th>";
                                html3 += "</tr>";
                            }

                        } else {
                            html3 += "<tr>";
                            html3 += "<th rowspan='" + serviceResp.data.data5[z].empTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].empresa + "</th>";
                            html3 += "<th rowspan='" + serviceResp.data.data5[z].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].tipo + "</th>";
                            html3 += "<th rowspan='" + serviceResp.data.data5[z].servTotal + "'>" + serviceResp.data.data5[z].servicio + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].prioridad + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].tiempoPromedio + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].tiempoMaximo + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].tiempoMinimo + "</th>";
                            html3 += "</tr>";
                        }
                    } else {
                        if (tipo2 != serviceResp.data.data5[z].tipo) {
                            tipo2 = serviceResp.data.data5[z].tipo;
                            serv2 = serviceResp.data.data5[z].servicio;
                            html3 += "<tr>";
                            html3 += "<th rowspan='" + serviceResp.data.data5[z].tipTotal + "' style='background-color: #ADCAE7;color: white;'>" + serviceResp.data.data5[z].tipo + "</th>";
                            html3 += "<th rowspan='" + serviceResp.data.data5[z].servTotal + "'>" + serviceResp.data.data5[z].servicio + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].prioridad + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].tiempoPromedio + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].tiempoMaximo + "</th>";
                            html3 += "<th>" + serviceResp.data.data5[z].tiempoMinimo + "</th>";
                            html3 += "</tr>";
                        } else {
                            if (serv2 != serviceResp.data.data5[z].servicio) {
                                serv2 = serviceResp.data.data5[z].servicio;
                                html3 += "<tr>";
                                html3 += "<th rowspan='" + serviceResp.data.data5[z].servTotal + "'>" + serviceResp.data.data5[z].servicio + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].prioridad + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoPromedio + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoMaximo + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoMinimo + "</th>";
                                html3 += "</tr>";
                            } else {
                                html3 += "<tr>";
                                html3 += "<th>" + serviceResp.data.data5[z].prioridad + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoPromedio + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoMaximo + "</th>";
                                html3 += "<th>" + serviceResp.data.data5[z].tiempoMinimo + "</th>";
                                html3 += "</tr>";
                            }
                        }
                    }

                }

                $("#tblTiempoReq").html(html3);

            });


            //fin
            setTimeout(
              function () {
                  //do something special
                  $window.loading_screen.finish();
              }, 4000);
        };


    }]);