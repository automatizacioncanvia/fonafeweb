﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('inventarios', {
              parent: 'main',
              url: 'inventarios',
              views: {
                  'content@main': {
                      templateUrl: 'app/views/components/inventarios/inventarios.html',
                      controller: 'InventarioCtrl',
                      controllerAs: 'inventario',
                      authorize: true

                  }
              }
          });
});