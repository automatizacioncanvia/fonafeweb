﻿angular.module('masterGenerysV1App')
    .config(ValidacionFechas)
    .controller('NewInventario', NuevoEquipo);

function ValidacionFechas($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function (date) {
        return date ? moment(date).format('YYYY/MM/DD') : '';
    };
}

function NuevoEquipo($uibModalInstance, $scope, filtrosobtenidos, $rootScope, $http, Apifonfe, Notification, $window, ngTableParams, $filter, Apiinventario, SweetAlert) {
    var vm = this;
    $scope.Titulo = 'Registrar Equipo'
    vm.ID = filtrosobtenidos[1];
    vm.OP = filtrosobtenidos[0];

    console.log(filtrosobtenidos[0]);
    Onload();

    function Onload() {
        console.log(vm.OP);
        console.log("operacion");
        Apiinventario.getOption(JSON.stringify(new Vista(1, 2, $rootScope.useri.userid))).then(CargaDatos, Error);
        if (vm.OP == 2)
        {
            $scope.Titulo = 'Actualizar Equipo'
            Apiinventario.getOption(JSON.stringify(new Vista(vm.ID, 6, 1))).then(CargaInventario, Error);
        }
    };

    function CargaInventario(serviceResp) {

        console.log('Cargamos los datos del equipo');
        console.log(serviceResp);
        var falta = (ValidaNull(serviceResp.data.data[0].iteM_FECINST) !== 0 ? parseDate(serviceResp.data.data[0].iteM_FECINST) : undefined);
        var fbaja = (ValidaNull(serviceResp.data.data[0].iteM_FECBAJA) !== 0 ? parseDate(serviceResp.data.data[0].iteM_FECBAJA) : undefined);
        var sdesde = (ValidaNull(serviceResp.data.data[0].iteM_CONTFINI) !== 0 ? parseDate(serviceResp.data.data[0].iteM_CONTFINI) : undefined);
        var shasta = (ValidaNull(serviceResp.data.data[0].iteM_CONTFFIN) !== 0 ? parseDate(serviceResp.data.data[0].iteM_CONTFFIN) : undefined);
        var gdesde = (ValidaNull(serviceResp.data.data[0].iteM_GARANFINI) !== 0 ? parseDate(serviceResp.data.data[0].iteM_GARANFINI) : undefined);
        var ghasta = (ValidaNull(serviceResp.data.data[0].iteM_GARANFFIN) !== 0 ? parseDate(serviceResp.data.data[0].iteM_GARANFFIN) : undefined);

        var Tipo = [];
        var Vcenter = [];
        var EstadoE = [];
        var Empresa = [];
        var Propiedad = [];
        var Ubicacion = [];
        var Admin = [];
        var Sbase = [];
        var Sversion = [];
        var Rol = [];
        var Ambiente = [];
        var Cricidad = [];
        var Familia = [];
        var Clase = [];
        var Elicencia = [];
        var Esoporte = [];
        var Egarantia = [];
        var Marca = [];
        var Modelo = [];
        var ip = [];

        Tipo = { tipO_ID: serviceResp.data.data[0].tipO_ID, tipO_DESC: serviceResp.data.data[0].tipO_DESC };
        Vcenter = { vcid: serviceResp.data.data[0].vcid, vC_NOMBRE: serviceResp.data.data[0].vC_NOMBRE };
        EstadoE = { esT_ID: serviceResp.data.data[0].esT_ID, esT_DESC: serviceResp.data.data[0].esT_DESC };
        Empresa = { empresA_ID: serviceResp.data.data[0].empresA_ID, eM_NOMBRE: serviceResp.data.data[0].eM_NOMBRE };
        Propiedad = { proP_ID: serviceResp.data.data[0].proP_ID, proP_DESC: serviceResp.data.data[0].proP_DESC };
        Ubicacion = { ubiC_ID: serviceResp.data.data[0].ubiC_ID, ubiC_DESC: serviceResp.data.data[0].ubiC_DESC };
        Admin = { adminID: serviceResp.data.data[0].adminID, adminDesc: serviceResp.data.data[0].adminDesc };
        Sbase = { sO_ID: serviceResp.data.data[0].sO_ID, sO_DESC: serviceResp.data.data[0].sO_DESC };
        Sversion = { vE_ID: serviceResp.data.data[0].vE_ID, vE_DESC: serviceResp.data.data[0].vE_DESC };
        Rol = { roL_USOID: serviceResp.data.data[0].roL_USOID, roL_USODESC: serviceResp.data.data[0].roL_USODESC };
        Ambiente = { amB_ID: serviceResp.data.data[0].amB_ID, amB_DESC: serviceResp.data.data[0].amB_DESC };
        Cricidad = { criticaL_ID: serviceResp.data.data[0].criticaL_ID, criticaL_DESC: serviceResp.data.data[0].criticaL_DESC };
        Familia = { faM_ID: serviceResp.data.data[0].faM_ID, faM_DESC: serviceResp.data.data[0].faM_DESC };
        Clase = { clasE_ID: serviceResp.data.data[0].clasE_ID, clasE_ALIAS: serviceResp.data.data[0].clasE_ALIAS };
        Elicencia = { licenciaid: serviceResp.data.data[0].licenciaid, licenciaDesc: serviceResp.data.data[0].licenciaDesc };
        Esoporte = { expirA_ID: serviceResp.data.data[0].expirA_ID, expirA_DESC: serviceResp.data.data[0].expirA_DESC };
        Egarantia = { garanEstadoId: serviceResp.data.data[0].garanEstadoId, garanEstadoDesc: serviceResp.data.data[0].garanEstadoDesc };
        Marca = { marcA_ID: serviceResp.data.data[0].marcA_ID, marcA_DESC: serviceResp.data.data[0].marcA_DESC };
        Modelo = { modelO_ID: serviceResp.data.data[0].modelO_ID, mO_MODELO: serviceResp.data.data[0].mO_MODELO };
        ip = { iteM_IP: serviceResp.data.data[0].iteM_IP, mO_MODELO: serviceResp.data.data[0].mO_MODELO };
        
        console.log(serviceResp.data.data[0]);

        $scope.em = {
            hostname: serviceResp.data.data[0].iteM_HOSTNAME,
            vmname: serviceResp.data.data[0].iteM_VMNAME,
            fechaalta: falta,
            selectedTipo: (ValidaNull(serviceResp.data.data[0].tipO_ID) != 0 ? Tipo : undefined),
            selectedvcenter: (ValidaNull(serviceResp.data.data[0].vcid) != 0 ? Vcenter : undefined),
            selectedestadoe: (ValidaNull(serviceResp.data.data[0].esT_ID) != 0 ? EstadoE : undefined),
            fechabaja: fbaja,
            selectedempresa: (ValidaNull(serviceResp.data.data[0].empresA_ID) != 0 ? Empresa : undefined),
            selectedpropiedad: (ValidaNull(serviceResp.data.data[0].proP_ID) != 0 ? Propiedad : undefined),
            selectedubicacion: (ValidaNull(serviceResp.data.data[0].ubiC_ID) != 0 ? Ubicacion : undefined),
            selectedadministrado: (ValidaNull(serviceResp.data.data[0].adminID) != 0 ? Admin : undefined),
            selectedsoftbase: (ValidaNull(serviceResp.data.data[0].sO_ID) != 0 ? Sbase : undefined),
            selectedsoftversion: (ValidaNull(serviceResp.data.data[0].vE_ID) != 0 ? Sversion : undefined),
            selectedroluso: (ValidaNull(serviceResp.data.data[0].roL_USOID) != 0 ? Rol : undefined),
            ambienteselected: (ValidaNull(serviceResp.data.data[0].amB_ID) != 0 ? Ambiente : undefined),
            selectedcriticidad: (ValidaNull(serviceResp.data.data[0].criticaL_ID) != 0 ? Cricidad : undefined),
            descripcion: serviceResp.data.data[0].iteM_COMENTA,
            selectedfamilia: (ValidaNull(serviceResp.data.data[0].faM_ID) != 0 ? Familia : undefined),
            selectedclase: (ValidaNull(serviceResp.data.data[0].clasE_ID) != 0 ? Clase : undefined),
            selectedestadolicencia: (ValidaNull(serviceResp.data.data[0].licenciaid) != 0 ? Elicencia : undefined),
            licencianro: serviceResp.data.data[0].licencianro,
            selectedestadosoporte: (ValidaNull(serviceResp.data.data[0].expirA_ID) != 0 ? Esoporte : undefined),
            soportedesde: sdesde,
            soportehasta: shasta,
            selectedestadogarantia: (ValidaNull(serviceResp.data.data[0].garanEstadoId) != 0 ? Egarantia : undefined),
            garantiadesde: gdesde,
            garantiahasta: ghasta,
            nroactivo : serviceResp.data.data[0].iteM_NROACTIVO,
            nroserie: serviceResp.data.data[0].iteM_SERIE,
            partnumber : serviceResp.data.data[0].iteM_PARTNUM,
            cid : serviceResp.data.data[0].cid,
            rudesde : serviceResp.data.data[0].rudesde,
            ruhasta : serviceResp.data.data[0].ruhasta,
            bahiadesde : serviceResp.data.data[0].bahiaDesde,
            bahiahasta : serviceResp.data.data[0].bahiaHasta,
            selectedmarca: (ValidaNull(serviceResp.data.data[0].marcA_ID) != 0 ? Marca : undefined),
            selectedmodelo: (ValidaNull(serviceResp.data.data[0].modelO_ID) != 0 ? Modelo : undefined),
            itemrack: serviceResp.data.data[0].iteM_RACK,
            ram: serviceResp.data.data[0].iteM_RAM,
            socket: serviceResp.data.data[0].iteM_SOCKET,
            nrocore: serviceResp.data.data[0].iteM_CORE,
            nrocpu: serviceResp.data.data[0].iteM_CPU,
            // nrocpu: serviceResp.data.data[0].iteM_CPU,
            discoasign: serviceResp.data.data[0].iteM_DISK,
            discoaprov: serviceResp.data.data[0].iteM_DISKAPROV,
            ip: serviceResp.data.data[0].iteM_IP //Agregado en la consulta SP_INVOPTIONS
        };

       

    }

    function Error(serviceResp) {
        console.log(serviceResp.data);
    };

    function CargaDatos(serviceResp) {
        console.log(serviceResp.data)
 
        $scope.tipoequipo = serviceResp.data.data;
        $scope.vcenter = serviceResp.data.data1;
        $scope.estadoequipo = serviceResp.data.data2;
        $scope.empresa = serviceResp.data.data3;
        $scope.propiedad = serviceResp.data.data4;
        $scope.ubicacion = serviceResp.data.data5;
        $scope.administrado = serviceResp.data.data6;
        $scope.softbase = serviceResp.data.data7;
        //software version
        $scope.roluso = serviceResp.data.data8;
        $scope.ambiente = serviceResp.data.data9;
        $scope.criticidad = serviceResp.data.data10;
        $scope.familia = serviceResp.data.data11;
        //clase
        $scope.estadolicencia = serviceResp.data.data12;
        $scope.estadosoporte = serviceResp.data.data13;
        $scope.estadogarantia = serviceResp.data.data14;
        $scope.marca = serviceResp.data.data15;
        //modelo

    };

    $scope.Softchange = function (id_software) {

        Apiinventario.getOption(JSON.stringify(new Vista(id_software, 3, $rootScope.useri.userid))).then(CargaVersionSO, Error);

    };

    $scope.Familiachange = function (id_familia) {

        Apiinventario.getOption(JSON.stringify(new Vista(id_familia, 4, $rootScope.useri.userid))).then(CargaClase, Error);

    };

    $scope.Marcachange = function (id_marca) {

        Apiinventario.getOption(JSON.stringify(new Vista(id_marca, 5, $rootScope.useri.userid))).then(CargaModelo, Error);

    };

    $scope.clickInsertar = function (hostname, vmname, tipoequipo, vcenter, estadoequipo, fechaalta, fechabaja, empresa, propiedad, ubicacion, administrado, softbase, softversion, roluso, ambiente, criticidad, descripcion, familia, clase, estadolicencia, licencianro, estadosoporte, soportedesde, soportehasta, estadogarantia, garantiadesde, garantiahasta, nroactivo, nroserie, partnumber, cid, rudesde, ruhasta, bahiadesde, bahiahasta, marca, modelo, itemrack, ram, socket, nrocore, nrocpu, discoasign, discoaprov) {

        console.log(fechaalta);

        var fhasta = moment(fechaalta).format("YYYYMMDD");
        var fbaja = moment(fechabaja).format("YYYYMMDD");
        var sdesde = moment(soportedesde).format("YYYYMMDD");
        var shasta = moment(soportehasta).format("YYYYMMDD");
        var gdesde = moment(garantiadesde).format("YYYYMMDD");
        var ghasta = moment(garantiahasta).format("YYYYMMDD");


        if (hostname == null || hostname == undefined) {
            SweetAlert.swal("Error!", "Debe ingresar el nombre del equipo", "error");
        }
        if (vmname == null || vmname == undefined) {
            SweetAlert.swal("Error!", "Debe ingresar el nombre virtual del equipo", "error");
        }
        else if (tipoequipo == null || tipoequipo == undefined) {
            SweetAlert.swal("Error!", "Debe ingresar el tipo de equipo", "error");
        }
        else if (estadoequipo == null || estadoequipo == undefined) {
            SweetAlert.swal("Error!", "Debe ingresar el estado de equipo", "error");
        }
        else {

            SweetAlert.swal({
            title: "Estás seguro?",
            text: "Se procederá a registrar al sistema!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, continúa!",
            cancelButtonText: "No, retrocede!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {

                    hostname = (ValidaNull(hostname) != 0 ? hostname : null);
                    vmname = (ValidaNull(vmname) != 0 ? vmname : null);
                    tipoequipo = (ValidaNull(tipoequipo) != 0 ? tipoequipo : null);
                    vcenter = (ValidaNull(vcenter) != 0 ? vcenter : null);
                    estadoequipo = (ValidaNull(estadoequipo) != 0 ? estadoequipo : null);
                    fhasta = (ValidaNull(fechaalta) != 0 ? fhasta : null);
                    fbaja = (ValidaNull(fechabaja) != 0 ? fbaja : null);
                    empresa = (ValidaNull(empresa) != 0 ? empresa : null);
                    propiedad = (ValidaNull(propiedad) != 0 ? propiedad : null);
                    ubicacion = (ValidaNull(ubicacion) != 0 ? ubicacion : null);
                    administrado = (ValidaNull(administrado) != 0 ? administrado : null);
                    softbase = (ValidaNull(softbase) != 0 ? softbase : null);
                    softversion = (ValidaNull(softversion) != 0 ? softversion : null);
                    roluso = (ValidaNull(roluso) != 0 ? roluso : null);
                    ambiente = (ValidaNull(ambiente) != 0 ? ambiente : null);
                    criticidad = (ValidaNull(criticidad) != 0 ? criticidad : null);
                    descripcion = (ValidaNull(descripcion) != 0 ? descripcion : null);
                    familia = (ValidaNull(familia) != 0 ? familia : null);
                    clase = (ValidaNull(clase) != 0 ? clase : null);
                    estadolicencia = (ValidaNull(estadolicencia) != 0 ? estadolicencia : null);
                    licencianro = (ValidaNull(licencianro) != 0 ? licencianro : null);
                    estadosoporte = (ValidaNull(estadosoporte) != 0 ? estadosoporte : null);
                    sdesde = (ValidaNull(soportedesde) != 0 ? sdesde : null);
                    shasta = (ValidaNull(soportehasta) != 0 ? shasta : null);
                    estadogarantia = (ValidaNull(estadogarantia) != 0 ? estadogarantia : null);
                    gdesde = (ValidaNull(garantiadesde) != 0 ? gdesde : null);
                    ghasta = (ValidaNull(garantiahasta) != 0 ? ghasta : null);
                    nroactivo = (ValidaNull(nroactivo) != 0 ? nroactivo : null);
                    nroserie = (ValidaNull(nroserie) != 0 ? nroserie : null);
                    partnumber = (ValidaNull(partnumber) != 0 ? partnumber : null);
                    cid = (ValidaNull(cid) != 0 ? cid : null);
                    rudesde = (ValidaNull(rudesde) != 0 ? rudesde : null);
                    ruhasta = (ValidaNull(ruhasta) != 0 ? ruhasta : null);
                    bahiadesde = (ValidaNull(bahiadesde) != 0 ? bahiadesde : null);
                    bahiahasta = (ValidaNull(bahiahasta) != 0 ? bahiahasta : null);
                    marca = (ValidaNull(marca) != 0 ? marca : null);
                    modelo = (ValidaNull(modelo) != 0 ? modelo : null);
                    itemrack = (ValidaNull(itemrack) != 0 ? itemrack : null);
                    ram = (ValidaNull(ram) != 0 ? ram : null);
                    socket = (ValidaNull(socket) != 0 ? socket : null);
                    nrocore = (ValidaNull(nrocore) != 0 ? nrocore : null);
                    nrocpu = (ValidaNull(nrocpu) != 0 ? nrocpu : null);
                    discoasign = (ValidaNull(discoasign) != 0 ? discoasign : null);
                    discoaprov = (ValidaNull(discoaprov) != 0 ? discoaprov : null);


                    Apiinventario.insertItem(JSON.stringify(new VistaInventario(vm.OP, vm.ID, hostname, tipoequipo, vmname, estadoequipo, fhasta, fbaja, familia, clase, nroactivo, nroserie, partnumber, modelo, marca, ubicacion, empresa, propiedad, administrado, roluso, ambiente, criticidad, descripcion, softbase, softversion, estadolicencia, licencianro, estadosoporte, sdesde, shasta, estadogarantia, gdesde, ghasta, ram, socket, nrocore, nrocpu, discoaprov, discoasign, itemrack, rudesde, ruhasta, bahiadesde, bahiahasta, cid, vcenter, $rootScope.useri.userid))).success(function (data) {
                        if (vm.OP == "1") {
                            SweetAlert.swal("Insertado!", "Se insertó al equipo satisfactoriamente.", "success");
                        } else {
                            SweetAlert.swal("Acualizado!", "Se Acualizó el equipo satisfactoriamente.", "success");
                        }
                        
                        $uibModalInstance.close();
                        $uibModalInstance.close();

                    }).error(function (data) {
                        $scope.error = data
                        SweetAlert.swal("Error!", $scope.error, "error");
                    });

                } else {
                    SweetAlert.swal("Cancelado", "Operación cancelada", "error");
                }
            });
        }
    };


    function CargaVersionSO(serviceResp) {
        $scope.softversion = serviceResp.data.data;


    };

    function CargaClase(serviceResp) {
        $scope.clase = serviceResp.data.data;
        console.log("Clases")
        console.log($scope.clase);
    };

    function CargaModelo(serviceResp) {
        $scope.modelo = serviceResp.data.data;

    };

    function Vista(id, vista, colab_id) {
        this.id = id;
        this.vista = vista;
        this.colab_id = colab_id;
    };
    //ncontrato
    function VistaInventario(op, eciid, nombre, tipos, nombrev, estadoe, feca, fecb, familia, clase, codactivo, serie, partnum, modelo, marca, ubicacion, proyecto, propiedad, admin, rol, ambiente, criticidad, desc, soft, version, elicencia, nlicencia, esoporte, soported, soporteh, egarantia, garantiad, garantiah, ram, socket, core, cpu, discoap, discoas, rack, rud, ruh, bad, bah, cid, vcenter, colab_id) {

        this.op = op;
        this.eci_id = eciid;
        this.nombre = nombre;
        this.tipos = tipos;
        this.nombrev = nombrev;
        this.estadoe = estadoe;
        this.fec_alta = feca;
        this.fec_baja = fecb;
        this.fam_id = familia;
        this.clase_id = clase;
        this.cod_ca = codactivo;
        this.serie = serie;
        this.partnum = partnum;
        this.modelo = modelo;
        this.marca = marca;
        this.cod_ubi = ubicacion;
        this.proyecto = proyecto;
        this.propiedad = propiedad;
        this.adminid = admin;
        this.cod_rol = rol;
        this.cod_amb = ambiente;
        this.cod_crit = criticidad;
        this.desc = desc;
        this.soid = soft;
        this.version = version;
        this.elicencia = elicencia;
        this.nlicencia = nlicencia;
        this.esoporte = esoporte;

        this.soported = soported;
        this.soporteh = soporteh;
        this.egarantia = egarantia;
        this.garantiad = garantiad;
        this.garantiah = garantiah;
        this.ram = ram;
        this.socket = socket;
        this.core = core;
        this.cpu = cpu;
        this.discoap = discoap;
        this.discoas = discoas;
        this.rack = rack;
        this.rud = rud;
        this.ruh = ruh;
        this.bad = bad;
        this.bah = bah;
        this.cid = cid;
        this.vcenter = vcenter;
        this.colab_id = colab_id;
    };

    function ValidaNull(dato) {
        var dat = dato;
        if (dat == null || dat == "" || dat == undefined) { dat = 0 }
        return dat;
    };

    function parseDate(datestring) {
        var m = moment(datestring, 'DD/MM/YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $scope.close = function () {
        $uibModalInstance.close();

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
};