angular.module('masterGenerysV1App').controller('ModalBusquedaTicket', function($uibModalInstance, $scope, $rootScope, $http, filtros, Apifonfe, Notification, $window) {

    $scope.count = 0;
    $scope.filtrosticket = filtros[0];
    $scope.sortReverse = false;

    $scope.filtrosticket.selected = [];
    $scope.ordenamiento = 'x,x';


    $scope.close = function() {
        $uibModalInstance.close();
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.getbyid = function(filtros, searchidsoli) {


        $scope.ordenamiento = 'x,x';
        $scope.filtrosseleccionados = filtros;
        if (angular.isUndefined(searchidsoli)) {
            searchidsoli = '';
        }
        if (searchidsoli.length <= 4 || searchidsoli == '') {
            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span>Para realizar una búsqueda debe escribir más de cuatro caracteres.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
        } else {
            $scope.buscar = searchidsoli;
            $scope.pageChanged(1);
            $scope.getData(1);
        }


    };

    $scope.ordenardata = function(sortType, sortReverse) {

        $scope.elementotabla = sortType;
        $scope.ordenarpor = sortReverse;
        $scope.ordenamiento = sortType + ',' + sortReverse
        console.log($scope.ordenamiento);

        $window.loading_screen = $window.pleaseWait({
            logo: "app/images/search.png",
            backgroundColor: 'rgba(41, 127, 112, 0.79)',
            loadingHtml: "<p class='loading-message'>Ordenando resultados...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

        });


        $http.get("api/fogettickets/" + $scope.valor + "/" + $scope.ordenamiento)
            .success(function(data) {
                $scope.count = data.data1[0].table_count;
                $scope.tickets = data.data;
                $window.loading_screen.finish();

            }).error(function(data) {
                $window.loading_screen.finish();

            });
    };

    $scope.getData = function(pageno) {

        if ($scope.buscar == undefined || $scope.buscar == "") {
            $scope.tickets = [];
            $scope.pageChanged(1);
            $scope.count = 0;
            $scope.sortType = '';
            $scope.sortReverse = false;


        } else {
            var cadenafiltros = "";
            var idcolab = $rootScope.useri.userid;

            if ($scope.filtrosseleccionados.length == 0 || $scope.filtrosseleccionados == undefined) {
                $scope.filtrosseleccionados = [];

            } else {
                for (var i = 0; i < $scope.filtrosseleccionados.length; i++) {
                    $scope.filtrosseleccionados[i].fI_CODIGO;
                    console.log($scope.filtrosseleccionados[i].fI_CODIGO);
                    cadenafiltros = cadenafiltros + ',' + $scope.filtrosseleccionados[i].fI_VALOR;
                }
            }

            var datos = idcolab + ',' + $scope.buscar + ',' + pageno;
            $scope.valor = datos + '-' + cadenafiltros

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/search.png",
                backgroundColor: 'rgba(41, 127, 177, 0.79)',
                loadingHtml: "<p class='loading-message'>Cargando resultados...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

            });


            // $http.get("api/fogettickets/" + $scope.valor + "/" + $scope.ordenamiento).then(function(serviceResp) {

            //     $scope.count = serviceResp.data.data1[0].table_count;
            //     $scope.tickets = serviceResp.data.data;

            //     console.log($scope.tickets);
            //     console.log($scope.count);
            // });
            $http.get("api/fogettickets/" + $scope.valor + "/" + $scope.ordenamiento)
                .success(function(data) {
                    $scope.count = data.data1[0].table_count;
                    $scope.tickets = data.data;
                    $window.loading_screen.finish();

                }).error(function(data) {
                    $window.loading_screen.finish();

                });

        }
    };





    $scope.pageChanged = function(page) {
        $scope.currentPage = page;
    };


    $scope.limpiartabla = function(searchidsoli) {
        if (searchidsoli.length == 0) {
            $scope.tickets = [];
            $scope.pageChanged(1);
            $scope.count = 0;
            $scope.sortType = 'fechaApertura';
            $scope.sortReverse = false;
        }
    };



    $scope.openticket = function(ticket) {
        
        debugger;
        Apifonfe.linkticket(ticket).success(function(data) {

            var dati = data.split('#');

            var ind = dati[0].indexOf('CAisd');
            var final = dati[0].substring(0, ind);
            console.log(final + dati[1]);

            debugger;

            $window.open(final + dati[1], 'OpenTicket', 'width=1500,height=700');
        }).error(function(data) {




            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
        })

    }



});
