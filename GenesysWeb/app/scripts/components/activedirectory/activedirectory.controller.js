﻿
'use estrict';
angular.module('masterGenerysV1App')
    .controller('ActiveDirectoryCrl', function ($scope, AD, $rootScope, Apimenu, Notification, Apidashboard, $uibModal, Message, $window, $mdDialog) {

        $scope.form = {};
        $scope.tab = 1;
        $scope.selectTab = function (setTab) {

            $scope.tab = setTab;
        };
        $scope.isSelected = function (checktab) {
            return $scope.tab === checktab;
        };


        var fail = function (serviceResp) {

            Message.result(2, serviceResp.data);

           // Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

        };

     
        Apimenu.getmenu(11, $rootScope.useri.userid).then(function (serviceResp) {

            var index = Apidashboard.indexOfespecial(serviceResp.data.data);

            if (index !== -1) {

                $scope.form.selectedEmpresa1 = serviceResp.data.data[index];

            }
            $scope.empresas = serviceResp.data.data;
        }, fail);


        var flowstatus = function (serviceResp) {
            var datai = JSON.parse(serviceResp.data);
           

            if (datai.executionSummary.status === 'COMPLETED') {


                if (datai.executionSummary.resultStatusType != 'ERROR') {

                    $window.loading_screen.finish();

                    if (datai.flowOutput.result !== undefined) {


                        $scope.userad = JSON.parse(datai.flowOutput.result);
                        //console.log($scope.userad);

                      
                        if ($scope.userad.length === 0) {

                            Message.result(4, "No se encontro el Usuario");
                        }

                    }

                    else {

                        Message.result(4, "Usuario no encontrado,intentelo denuevo");


                    }
                }
                else {

                    Message.result(1, datai.flowOutput.result);
                    $window.loading_screen.finish();
                }
                


            } else {

                AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatus, fail);
            }

        }


        var correctgetuser = function (serviceResp) {

            var result = JSON.parse(serviceResp.data);

          
            var idejecution = result.executionId;

            AD.getFlowStatus(idejecution, 'execution-log').then(flowstatus, fail)

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/user.png",
                backgroundColor: 'rgba(216, 216, 216, 0.61)',
                loadingHtml: "<p class='loading-message'>Buscando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

            });

            

        };

        $scope.searchuser = function (empresa, name) {


            var obj = JSON.stringify({
                mtoken :'1000001',
                input: '7,'+ empresa.eM_ID+','+name
            })

            AD.getuser(obj).then(correctgetuser, fail);

        };

        
        

        $scope.newuser = function (ev) {


            //var modalInstance = $uibModal.open({
            //    animation: $scope.animation,
            //    ariaLabelledBy: 'modal-title',
            //    ariaDescribedBy: 'modal-body',
            //    templateUrl: 'app/views/components/activedirectory/modals/newuserad.html',
            //    controller: 'NewUserCtrl',
            //    size: 'mm',
            //    backdrop:'static',
            //    keyboard:false,
            //    resolve: {
            //        data: function () {

            //            return $scope.empresas;
            //        }

            //    }
            //    });



            $mdDialog.show({
                controller: "NewADUserCrl",
                controllerAs: "$user",
                templateUrl: 'app/views/components/activedirectory/modals/newaduser.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true,// Only for -xs, -sm breakpoints.
                locals: {
                    empresas: $scope.empresas
                }
            })






        };



        //metodos del menu desplegable


        var originatorEv;

        $scope.openMenu = function ($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };



        $scope.showuser = function (ev, user,value) {
           
            user.edit = value;
          

            $mdDialog.show({
                controller: "ShowUserCtrl",
                templateUrl: 'app/views/components/activedirectory/modals/showuser.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true ,
                locals:{
                    user:user
                }
            })
    
        };






        $scope.bajauser = function (ev, user, index) {

            $scope.id_baja = index;

            var confirm = $mdDialog.confirm()
              .title('Baja Usuario')
              .textContent('¿Esta Seguro que quiere de Baja al Usuario?')
              .targetEvent(ev)
              .ok('Aceptar')
              .cancel('Cancelar');


            $mdDialog.show(confirm).then(function () {



                var office = (user.office === undefined || user.office === '' ? null : user.office);
                var title = (user.title === undefined || user.title === '' ? null : user.title);
                var department = (department === undefined || user.department === '' ? null : user.department);
                var company = (user.company === undefined || user.company === '' ? null : user.company);
                var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
                var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null : user.mobilephone);
                var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);

                var path = user.DistinguishedName;
                var iruta = path.indexOf("\\");

                if (iruta !== -1) {
                    user.DistinguishedName = user.DistinguishedName.replace("\\", "");
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")

                } else {
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")
                }


          

                var buser = JSON.stringify({
                    u_adid: user.SID,
                    o_id: 12,
                    em_id: null, //no necesito enviar
                    u_name: user.givenname,
                    u_surname: user.surname,
                    u_samaname: user.SamAccountName,
                    u_password: null,
                    ou_path: user.DistinguishedName,
                    u_office: office,
                    u_title: title,
                    u_departament: department,
                    u_company: company,
                    u_homephone: homephone,
                    u_mobilephone: mobilephone,
                    u_officephone: officephone,
                    u_enable: '0',
                    u_accountexpirationdate: user.AccountExpirationDate,
                    co_id: $rootScope.useri.userid,
                    info: '-'
                })




                AD.operation(buser).then(useroperation_baja, errorflows);


                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/user.png",
                    backgroundColor: 'rgba(42, 67, 94, 0.54)',
                    loadingHtml: "<p class='loading-message'>Baja Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });




            }, function () {


            });



        }



        var useroperation_baja = function (serviceResp) {
            var datai = JSON.parse(serviceResp.data);
            var idejecution = datai.executionId;
            AD.getFlowStatus(idejecution, 'execution-log').then(flowstatubaja, errorflows)


        }


        var flowstatubaja = function (serviceResp) {

            var datai = JSON.parse(serviceResp.data);


            if (datai.executionSummary.status === 'COMPLETED') {

                if (datai.executionSummary.resultStatusType != 'ERROR') {

                    if (datai.flowOutput.result !== undefined) {

                        var result = JSON.parse(datai.flowOutput.result);
                        switch (parseInt(result[0].status_id)) {
                            case 1:// $scope.SID = result[0].data.SID;
                                // $scope.tab = 2;

                                $scope.userad[$scope.id_baja].Enabled = false;
                                Message.result(3, "Baja Usuario");
                                $window.loading_screen.finish();
                                break;
                            case 2: Message.result(1, result[0].data);
                                $window.loading_screen.finish();
                                break;

                        }

                    }

                    else {

                        Message.result(1, "No se recibio resultado del servidor");
                        $window.loading_screen.finish();
                    }
                } else {

                    Message.result(1, datai.flowOutput.result);
                    $window.loading_screen.finish();
                }

            }

            else {

                AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatubaja, errorflows);
            }




        }








        $scope.disableuser = function (ev, user, index) {

            $scope.id_disable = index;

            var confirm = $mdDialog.confirm()
              .title('Deshabilitar')
              .textContent('¿Esta Seguro que quiere deshabilitar al usuario?')
              .targetEvent(ev)
              .ok('Aceptar')
              .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {


                 
                var office = (user.office === undefined || user.office === '' ? null : user.office);
                var title = (user.title === undefined || user.title === '' ? null : user.title);
                var department = (department === undefined || user.department === '' ? null :  user.department);
                var company = (user.company === undefined || user.company === '' ? null : user.company);
                var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
                var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null :  user.mobilephone);
                var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);

                var path = user.DistinguishedName;
                var iruta = path.indexOf("\\");

                if (iruta !== -1) {
                    user.DistinguishedName = user.DistinguishedName.replace("\\", "");
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")
                   
                } else {
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "") 
                }


               // var enable = (user.Enabled === true ? '1' : '0');
                // var expiration = ($scope.user.AccountExpirationDate === null ? null : $scope.user.AccountExpirationDate);


                var disableuser = JSON.stringify({
                    u_adid: user.SID,
                    o_id: 5, 
                    em_id: null, //no necesito enviar
                    u_name: user.givenname,
                    u_surname: user.surname,
                    u_samaname: user.SamAccountName,
                    u_password: null,
                    ou_path: user.DistinguishedName,
                    u_office: office,
                    u_title: title,
                    u_departament: department,
                    u_company: company,
                    u_homephone: homephone,
                    u_mobilephone: mobilephone,
                    u_officephone: officephone,
                    u_enable: '0',
                    u_accountexpirationdate: user.AccountExpirationDate,
                    co_id: $rootScope.useri.userid,
                    info:'-'
                })




                AD.operation(disableuser).then(useroperation_disable, errorflows);


                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/user.png",
                    backgroundColor: 'rgba(42, 67, 94, 0.54)',
                    loadingHtml: "<p class='loading-message'>Desabilitando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });


                 
             
            }, function () {
            
              
            });
        };


        var useroperation_disable = function (serviceResp) {
    
           
            var datai = JSON.parse(serviceResp.data);
            var idejecution = datai.executionId;
            AD.getFlowStatus(idejecution, 'execution-log').then(flowstatusdisable, errorflows)


        }


        var flowstatusdisable = function (serviceResp) {

            var datai = JSON.parse(serviceResp.data);
          

            if (datai.executionSummary.status === 'COMPLETED') {

                if (datai.executionSummary.resultStatusType != 'ERROR') {

                    if (datai.flowOutput.result !== undefined) {

                        var result = JSON.parse(datai.flowOutput.result);
                        switch (parseInt(result[0].status_id)) {
                            case 1: //$scope.SID = result[0].data.SID;
                                // $scope.tab = 2;

                                $scope.userad[$scope.id_disable].Enabled = false;
                                Message.result(3, "Usuario Desabiliado");
                                $window.loading_screen.finish();
                                break;
                            case 2: Message.result(1, result[0].data);
                                $window.loading_screen.finish();
                                break;

                        }

                    }

                    else {

                        Message.result(1, "No se recibio resultado del servidor");
                        $window.loading_screen.finish();
                    }
                } else {

                    Message.result(1, datai.flowOutput.result);
                    $window.loading_screen.finish();
                }

            }

            else {

                AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatusdisable, errorflows);
            }




        }






        $scope.enableuser = function (ev, user, index) {

            $scope.id_disable = index;

            var confirm = $mdDialog.confirm()
              .title('Habilitar')
              .textContent('¿Esta Seguro que quiere Habilitar al usuario?')
              .targetEvent(ev)
              .ok('Aceptar')
              .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {



                var office = (user.office === undefined || user.office === '' ? null : user.office);
                var title = (user.title === undefined || user.title === '' ? null : user.title);
                var department = (department === undefined || user.department === '' ? null : user.department);
                var company = (user.company === undefined || user.company === '' ? null : user.company);
                var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
                var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null : user.mobilephone);
                var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);

                var path = user.DistinguishedName;
                var iruta = path.indexOf("\\");

                if (iruta !== -1) {
                    user.DistinguishedName = user.DistinguishedName.replace("\\", "");
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")

                } else {
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")
                }


                // var enable = (user.Enabled === true ? '1' : '0');
                // var expiration = ($scope.user.AccountExpirationDate === null ? null : $scope.user.AccountExpirationDate);


                var enableuser = JSON.stringify({
                    u_adid: user.SID,
                    o_id: 4,
                    em_id: null, //no necesito enviar
                    u_name: user.givenname,
                    u_surname: user.surname,
                    u_samaname: user.SamAccountName,
                    u_password: null,
                    ou_path: user.DistinguishedName,
                    u_office: office,
                    u_title: title,
                    u_departament: department,
                    u_company: company,
                    u_homephone: homephone,
                    u_mobilephone: mobilephone,
                    u_officephone: officephone,
                    u_enable: '1',
                    u_accountexpirationdate: user.AccountExpirationDate,
                    co_id: $rootScope.useri.userid,
                    info: '-'
                })




                AD.operation(enableuser).then(useroperation_enable, errorflows);


                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/user.png",
                    backgroundColor: 'rgba(42, 67, 94, 0.54)',
                    loadingHtml: "<p class='loading-message'>Habilitando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });




            }, function () {
              

            });
        };


        var useroperation_enable = function (serviceResp) {
            var datai = JSON.parse(serviceResp.data);         
            var idejecution = datai.executionId;
            AD.getFlowStatus(idejecution, 'execution-log').then(flowstatusenable, errorflows)


        }


        var flowstatusenable = function (serviceResp) {

            var datai = JSON.parse(serviceResp.data);
           

            if (datai.executionSummary.status === 'COMPLETED') {

                if (datai.executionSummary.resultStatusType != 'ERROR') {

                    if (datai.flowOutput.result !== undefined) {

                        var result = JSON.parse(datai.flowOutput.result);
                        switch (parseInt(result[0].status_id)) {
                            case 1:// $scope.SID = result[0].data.SID;
                                // $scope.tab = 2;

                                $scope.userad[$scope.id_disable].Enabled = true;
                                Message.result(3, "Usuario Habilitado");
                                $window.loading_screen.finish();
                                break;
                            case 2: Message.result(1, result[0].data);
                                $window.loading_screen.finish();
                                break;

                        }

                    }

                    else {

                        Message.result(1, "No se recibio resultado del servidor");
                        $window.loading_screen.finish();
                    }
                } else {

                    Message.result(1, datai.flowOutput.result);
                    $window.loading_screen.finish();
                }

            }

            else {

                AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatusenable, errorflows);
            }




        }





        $scope.unlockuser = function (ev, user, index) {

            $scope.id_unlock = index;

            var confirm = $mdDialog.confirm()
              .title('Desbloquear')
              .textContent('¿Esta Seguro que quiere Desbloquear al usuario?')
              .targetEvent(ev)
              .ok('Aceptar')
              .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {

                var office = (user.office === undefined || user.office === '' ? null : user.office);
                var title = (user.title === undefined || user.title === '' ? null : user.title);
                var department = (department === undefined || user.department === '' ? null : user.department);
                var company = (user.company === undefined || user.company === '' ? null : user.company);
                var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
                var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null : user.mobilephone);
                var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);
                var enable = (user.Enabled === true ? '1' : '0');

                var path = user.DistinguishedName;
                var iruta = path.indexOf("\\");

                if (iruta !== -1) {
                    user.DistinguishedName = user.DistinguishedName.replace("\\", "");
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")

                } else {
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")
                }


                // var enable = (user.Enabled === true ? '1' : '0');
                // var expiration = ($scope.user.AccountExpirationDate === null ? null : $scope.user.AccountExpirationDate);


                var enableuser = JSON.stringify({
                    u_adid: user.SID,
                    o_id: 3,
                    em_id: null, //no necesito enviar
                    u_name: user.givenname,
                    u_surname: user.surname,
                    u_samaname: user.SamAccountName,
                    u_password: null,
                    ou_path: user.DistinguishedName,
                    u_office: office,
                    u_title: title,
                    u_departament: department,
                    u_company: company,
                    u_homephone: homephone,
                    u_mobilephone: mobilephone,
                    u_officephone: officephone,
                    u_enable: enable,
                    u_accountexpirationdate: user.AccountExpirationDate,
                    co_id: $rootScope.useri.userid,
                    info: '-'
                })




                AD.operation(enableuser).then(useroperation_unlock, errorflows);


                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/user.png",
                    backgroundColor: 'rgba(42, 67, 94, 0.54)',
                    loadingHtml: "<p class='loading-message'>Desbloqueando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });




            }, function () {
              

            });
        };


        var useroperation_unlock = function (serviceResp) {

            var datai = JSON.parse(serviceResp.data);          
            var idejecution = datai.executionId;
            AD.getFlowStatus(idejecution, 'execution-log').then(flowstatusunlock, errorflows)


        }


        var flowstatusunlock = function (serviceResp) {

            var datai = JSON.parse(serviceResp.data);
            if (datai.executionSummary.status === 'COMPLETED') {
                if (datai.executionSummary.resultStatusType != 'ERROR') { 
                                    if (datai.flowOutput.result !== undefined) {

                                        var result = JSON.parse(datai.flowOutput.result);
                                        switch (parseInt(result[0].status_id)) {
                                            case 1: //$scope.SID = result[0].data.SID;
                                                // $scope.tab = 2;

                                                $scope.userad[$scope.id_unlock].LockedOut = false;
                                                Message.result(3, "Usuario Desbloqueado");
                                                $window.loading_screen.finish();
                                                break;
                                            case 2: Message.result(1, result[0].data);
                                                $window.loading_screen.finish();
                                                break;

                                        }

                                    }

                                    else {

                                        Message.result(1, "No se recibio resultado del servidor");
                                        $window.loading_screen.finish();
                                    }

                }
                else {
                    Message.result(1, datai.flowOutput.result);
                    $window.loading_screen.finish();
                }

            }

            else {

                AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatusunlock, errorflows);
            }




        }







        $scope.deleteuser = function (ev, user, index) {

            $scope.id_delete = index;

            var confirm = $mdDialog.confirm()
              .title('Eliminar')
              .textContent('¿Esta Seguro que quiere Eliminar al usuario?')
              .targetEvent(ev)
              .ok('Aceptar')
              .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {

                var office = (user.office === undefined || user.office === '' ? null : user.office);
                var title = (user.title === undefined || user.title === '' ? null : user.title);
                var department = (department === undefined || user.department === '' ? null : user.department);
                var company = (user.company === undefined || user.company === '' ? null : user.company);
                var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
                var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null : user.mobilephone);
                var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);
                var enable = (user.Enabled === true ? '1' : '0');

                var path = user.DistinguishedName;
                var iruta = path.indexOf("\\");

                if (iruta !== -1) {
                    user.DistinguishedName = user.DistinguishedName.replace("\\", "");
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")

                } else {
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")
                }


                // var enable = (user.Enabled === true ? '1' : '0');
                // var expiration = ($scope.user.AccountExpirationDate === null ? null : $scope.user.AccountExpirationDate);


                var enableuser = JSON.stringify({
                    u_adid: user.SID,
                    o_id: 6,
                    em_id: null, //no necesito enviar
                    u_name: user.givenname,
                    u_surname: user.surname,
                    u_samaname: user.SamAccountName,
                    u_password: null,
                    ou_path: user.DistinguishedName,
                    u_office: office,
                    u_title: title,
                    u_departament: department,
                    u_company: company,
                    u_homephone: homephone,
                    u_mobilephone: mobilephone,
                    u_officephone: officephone,
                    u_enable: enable,
                    u_accountexpirationdate: user.AccountExpirationDate,
                    co_id: $rootScope.useri.userid,
                    info: '-'
                })




                AD.operation(enableuser).then(useroperation_delete, errorflows);


                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/user.png",
                    backgroundColor: 'rgba(42, 67, 94, 0.54)',
                    loadingHtml: "<p class='loading-message'>Eliminando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });




            }, function () {
              

            });
        };


        var useroperation_delete = function (serviceResp) {

          
            var datai = JSON.parse(serviceResp.data);
            var idejecution = datai.executionId;
            AD.getFlowStatus(idejecution, 'execution-log').then(flowstatusdelete, errorflows)


        }


        var flowstatusdelete = function (serviceResp) {

            var datai = JSON.parse(serviceResp.data);
         

            if (datai.executionSummary.status === 'COMPLETED') {

                if (datai.executionSummary.resultStatusType != 'ERROR'){

                                    if (datai.flowOutput.result !== undefined) {

                                        var result = JSON.parse(datai.flowOutput.result);
                                        switch (parseInt(result[0].status_id)) {
                                            case 1: 
                                                $scope.userad.splice($scope.id_delete, 1)
                                                Message.result(3, "Usuario Eliminado");
                                                $window.loading_screen.finish();
                                                break;
                                            case 2: Message.result(1, result[0].data);
                                                $window.loading_screen.finish();
                                                break;

                                        }

                                    }

                                    else {

                                        Message.result(1, "No se recibio resultado del servidor");
                                        $window.loading_screen.finish();
                                    }
                }
                else {

                    Message.result(1, datai.flowOutput.result);
                    $window.loading_screen.finish();
                }

            }

            else {

                AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatusdelete, errorflows);
            }




        }





       
        $scope.changepassword = function (ev, user, index) {

         

            $mdDialog.show({
                controller: "ChangePasswordCtrl",
                templateUrl: 'app/views/components/activedirectory/modals/changepassword.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: false,// Only for -xs, -sm breakpoints.
                locals: {
                    user: user
                }
            })



        }

        $scope.changeou = function (ev, user, index,empresa) {
          
            user.em_id = empresa.eM_ID;

            $mdDialog.show({
                controller: "ChangeOUCtrl",
                controllerAs:"ou",
                templateUrl: 'app/views/components/activedirectory/modals/changeou.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: false,// Only for -xs, -sm breakpoints.
                locals: {
                    user: user
                }
            })

        }






        var errorflows = function (serviceResp) {

            Message.result(4, serviceResp.data);
            $window.loading_screen.finish();

        }


        $scope.permisos=function(ev, user, index,empresa){

            user.em_id = empresa.eM_ID;

            $mdDialog.show({
                controller: "PermisionCtrl",
                controllerAs:"$per",
                templateUrl: 'app/views/components/activedirectory/modals/permisos.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true,// Only for -xs, -sm breakpoints.
                locals: {
                    user: user
                }
            })

        }
     

    })