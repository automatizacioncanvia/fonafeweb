﻿angular.module('masterGenerysV1App')
.controller('ChangeOUCtrl', function ($scope, $mdDialog, user, $rootScope, AD, $window, Message, Apimenu) {

    $rootScope.zindexvalue = true;
    var path = user.DistinguishedName;
    var iruta = path.indexOf("\\");


    var vm = this;
    vm.form = {};
  

    if (iruta !== -1) {
        user.DistinguishedName = user.DistinguishedName.replace("\\", "");
        user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")

    } else {
        user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")
    }

    vm.indexofpath = function (path, arreglo) {
        var final;
        var i = 0
        var len = arreglo.length;

        for (i; i < len; i++) {

            if (path === arreglo[i].uO_RUTA) {

                final= i;
                break;
            }

        }

        return final;
    }

    vm.menudata = function (serviceResp) {
       

        var id_ouactual = vm.indexofpath(user.DistinguishedName, serviceResp.data.data);

        vm.form.unidad_actual = serviceResp.data.data[id_ouactual];

        vm.unidades = serviceResp.data.data;
        //vm.unidades2 = serviceResp.data.data;



    }

    vm.fail = function (serviceResp) {


        Message.result(1, serviceResp.data);

    }

    vm.getou=function(empresa_id){

        //$scope.form.unidadorganizativa = undefined;
        Apimenu.getmenu(12, empresa_id).then(vm.menudata, vm.fail);
    }

    vm.getou(user.em_id);

    vm.cancel = function () {
        $rootScope.zindexvalue = false;
        $mdDialog.cancel();
    };

    vm.changeou = function (ev) {

        if (vm.form.unidad_actual.uO_RUTA === vm.form.unidad_nueva.uO_RUTA) {



            Message.result(4, 'No puedes elegir la misma OU');




        }
        else {

           var confirm = $mdDialog.confirm()
          .title('Cambiar OU')
          .textContent('¿Esta Seguro que quiere cambiale de OU al usuario?')
          .targetEvent(ev)
          .ok('Aceptar')
          .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {

                var office = (user.office === undefined || user.office === '' ? null : user.office);
                var title = (user.title === undefined || user.title === '' ? null : user.title);
                var department = (department === undefined || user.department === '' ? null : user.department);
                var company = (user.company === undefined || user.company === '' ? null : user.company);
                var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
                var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null : user.mobilephone);
                var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);
                var enable = (user.Enabled === true ? '1' : '0');

                var path = vm.form.unidad_nueva.uO_RUTA;
             
                var changeou_user = JSON.stringify({
                    u_adid: user.SID,
                    o_id: 11,
                    em_id: user.em_id, //no necesito enviar
                    u_name: user.givenname,
                    u_surname: user.surname,
                    u_samaname: user.SamAccountName,
                    u_password: null,
                    ou_path: path,
                    u_office: office,
                    u_title: title,
                    u_departament: department,
                    u_company: company,
                    u_homephone: homephone,
                    u_mobilephone: mobilephone,
                    u_officephone: officephone,
                    u_enable: enable,
                    u_accountexpirationdate: user.AccountExpirationDate,
                    co_id: $rootScope.useri.userid,
                    info: vm.form.unidad_actual.uO_RUTA
                })
                AD.operation(changeou_user).then(vm.useroperation_changeou, vm.errorflows);


                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/user.png",
                    backgroundColor: 'rgba(42, 67, 94, 0.54)',
                    loadingHtml: "<p class='loading-message'>Cambiando UO Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });




            }, function () {
               

            });





        }



      

    }

    vm.errorflows = function (serviceResp) {
        $window.loading_screen.finish();
        Message.result(1, serviceResp.data);
       

    }

    vm.useroperation_changeou = function (serviceResp) {


        var datai = JSON.parse(serviceResp.data); 
        var idejecution = datai.executionId;
        AD.getFlowStatus(idejecution, 'execution-log').then(vm.flowstatuschangeou, vm.errorflows)


    }

    vm.flowstatuschangeou = function (serviceResp) {

        var datai = JSON.parse(serviceResp.data);
      

        if (datai.executionSummary.status === 'COMPLETED') {

            if (datai.executionSummary.resultStatusType != 'ERROR') {

                if (datai.flowOutput.result !== undefined) {

                    var result = JSON.parse(datai.flowOutput.result);
                    switch (parseInt(result[0].status_id)) {
                        case 1:
                            // $scope.userad.splice($scope.id_delete, 1)
                            Message.result(3, "UO Cambiada Exitosamente!");
                            $window.loading_screen.finish();
                            break;
                        case 2: Message.result(1, result[0].data);
                            $window.loading_screen.finish();
                            break;

                    }

                }

                else {

                    Message.result(1, "No se recibio resultado del servidor");
                    $window.loading_screen.finish();
                }

            } else {
                Message.result(1, datai.flowOutput.result);
                $window.loading_screen.finish();
            }

        }

        else {

            AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(vm.flowstatuschangeou, vm.errorflows);
        }




    }






})