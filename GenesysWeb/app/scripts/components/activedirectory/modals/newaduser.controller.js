﻿
'use estrict';
angular.module('masterGenerysV1App')
    .controller('NewADUserCrl', function ($scope, AD, $rootScope, Apimenu, Apidashboard, $uibModal, Message, $window, $mdDialog, empresas, $timeout, $q,$log) {


        //Declareacion de Variable Para en Angular
        vm = this;
        vm.form = {};
        vm.edit = false;
        vm.tab = 0;
        $scope.form = {};
        vm.groupsf = [];
        //$scope.groupsf = [];
        vm.groupsi = [];
        vm.groupsc = [];
      
        $scope.expire = true;
        $rootScope.zindexvalue = true;
        vm.memberof = [];





        






        //Funciones '()' inizalizacion 
        vm.cancel = Cancel;
        vm.menusdata = GetUnidades;
        vm.fail = Fail
        vm.getou = GetOu;
        vm.setou = SetYoutEmpresaAndGetOU();
        vm.empresas = empresas;
        vm.selectdate = SelecDate;
        vm.expiration= NeverExpire;
        vm.selectempresa = SelecEmpresa;
        vm.filesystem = GetFSGroups;
        vm.bd = GetBDGroups;
        vm.internet = GetInternetGroups;
        vm.citrix = GetCitrixGroups;
      
        vm.crearusuario = CrearUsuario;
        vm.flowerror = Error;
        vm.usercreated = WhenUserCratedSucces;
        vm.flowstatus = FlowStatus;



      




        // Watch de algunas variables utilizando $scope

        $scope.$watch('form.empresa', function () {

            console.log($scope.form.empresa);

            if ($scope.form.empresa !== undefined) {

                var empresa_id = $scope.form.empresa.eM_ID;
                vm.getou(empresa_id);
                //Apimenu.getmenu(13, empresa_id + ",1").then(vm.filesystem, vm.fail)
                //Apimenu.getmenu(13, empresa_id + ",2").then(vm.bd, vm.fail);
                //Apimenu.getmenu(13, empresa_id + ",3").then(vm.internet, vm.fail);
                //Apimenu.getmenu(13, empresa_id + ",4").then(vm.citrix, vm.fail);
                Apimenu.getmenu(27, empresa_id).then(GetALLGroupt, vm.fail)

                Apimenu.getmenu(22, empresa_id).then(GetAtributos, vm.fail)

            }


        })







        $scope.$watch('form.samaname', function () {

            if ($scope.form.samaname !== undefined) {
                $scope.form.samaname = $scope.form.samaname.toLowerCase().replace(/\s+/g, '');
            }

        })


        //Funciones

        function GetALLGroupt(serviceResp) {


            var resuldb = JSON.parse(serviceResp.data.data[0].result);
            console.log(resuldb);
            $scope.arrayfinal = resuldb;


        }



         function Cancel () {

            $rootScope.zindexvalue = false;
            $mdDialog.cancel();
        }

         function GetUnidades (serviceResp) {
            vm.unidades = serviceResp.data.data;
        }

         function Fail (serviceResp) {

            Message.result(1, serviceResp.status);

        }

         function GetOu (empresa_id) {


            vm.form.unidadorganizativa = undefined;
            Apimenu.getmenu(12, empresa_id).then(vm.menusdata, vm.fail);

        }

         function SetYoutEmpresaAndGetOU(empresa)
        {
            var index = Apidashboard.indexOfespecial(empresas);

            if (index !== -1) {

                $scope.form.empresa = empresas[index];
                vm.getou(empresas[index].eM_ID);
            }
        }

         function SelecDate(date) {

             if (date !== null) {
                 $scope.expire = false;
                 $scope.off = false;
             }

        }

         function NeverExpire(expire) {
             
             if (expire !== true) {
                 $scope.expire = true;
                 $scope.off = true;
                 vm.user.AccountExpirationDate = null;
            }
        }

         function SelecEmpresa (empresa) {

            console.log(empresa);
        }

         function GetFSGroups (serviceResp) {

            vm.form.filesystem = undefined;
            vm.gfilesystem = serviceResp.data.data;
            console.log(vm.gfilesystem)

        }

         function GetBDGroups (serviceResp) {

            vm.form.bg = undefined;
            vm.gbd = serviceResp.data.data;


        }

         function GetInternetGroups (serviceResp) {

            vm.form.internet = undefined;
            vm.ginternet = serviceResp.data.data;




         }

         function GetCitrixGroups (serviceResp) {

            vm.form.citrix = undefined;
            vm.gcitrix = serviceResp.data.data;

        }

         function AddFsGroups (index) {  
            var  adig = vm.gfilesystem.splice(index, 1) 
            vm.gruposf.push(adig[0]);
            vm.form.filesystem = undefined;
        }

         function DelFsGroups (index) {
            var delg = vm.gruposf.splice(index, 1)
            vm.gfilesystem.push(delg[0]);
           
        }

         function CrearUsuario() {

            var office = (vm.user.office === undefined || vm.user.office === '' ? null : vm.user.office);
            var title = (vm.user.title === undefined || vm.user.title === '' ? null : vm.user.title);
            var department = (vm.user.department === undefined || vm.user.department === '' ? null : vm.user.department);
            var company = (vm.user.company === undefined || vm.user.company === '' ? null : vm.user.company);
            var homephone = (vm.user.homephone === undefined || vm.user.homephone === '' ? null : vm.user.homephone);
            var mobilephone = (vm.user.mobilephone === undefined || vm.user.mobilephone === '' ? null : vm.user.mobilephone);
            var officephone = (vm.user.Officephone === undefined || vm.user.Officephone === '' ? null : vm.user.Officephone);
            var passowrd = (vm.user.password);
            var empresa_id = $scope.form.empresa.eM_ID;
            var expiration = (vm.user.AccountExpirationDate === null || vm.user.AccountExpirationDate === '' ? null : vm.user.AccountExpirationDate);
        
            var newuser = new AD.User(null, 1, empresa_id, vm.user.givenname, vm.user.surname, $scope.form.samaname, passowrd, vm.form.unidadorganizativa.uO_RUTA,
                 office, title, department, company, homephone, mobilephone, officephone, '1', expiration, $rootScope.useri.userid, '-' ,$scope.allat)

           
             console.log(newuser)

            AD.operation(newuser).then(vm.usercreated, vm.flowerror);

           // console.log($scope.allat);

            $window.loading_screen = $window.pleaseWait({
                logo: "app/images/user.png",
                backgroundColor: 'rgba(42, 67, 94, 0.54)',
                loadingHtml: "<p class='loading-message'>Creando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

            });


        };

         function Error (serviceResp) {

            Message.result(1, serviceResp.data);
            $window.loading_screen.finish();

        }

         function WhenUserCratedSucces (serviceResp) {

            var datai = JSON.parse(serviceResp.data);

            var idejecution = datai.executionId;

            AD.getFlowStatus(idejecution, 'execution-log').then(vm.flowstatus, vm.flowerror)


        }

         function FlowStatus(serviceResp) {

            var datai = JSON.parse(serviceResp.data);


            if (datai.executionSummary.status === 'COMPLETED') {


                if (datai.executionSummary.resultStatusType != 'ERROR') {

                    if (datai.flowOutput.result !== undefined) {

                        var result = JSON.parse(datai.flowOutput.result);
                        switch (parseInt(result[0].status_id)) {
                            case 1: vm.user.SID = result[0].data.SID;
                                Message.result(3, "Usuario Creado");
                                vm.edit = true;
                                vm.tab = 1;
                                $window.loading_screen.finish();
                                break;
                            case 2: Message.result(1, result[0].data);
                                $window.loading_screen.finish();
                                break;

                        }

                    }

                    else {

                        Message.result(1, "No se recibio resultado del servidor");
                        $window.loading_screen.finish();
                    }

                } else {

                    Message.result(1, datai.flowOutput.result);
                    $window.loading_screen.finish();
                }

            }

            else {

                AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(vm.flowstatus, vm.flowerror);
            }


         }



         vm.addgroudinternet = function () {

             if (vm.groupsi.length < 1) {


                 vm.groupsi.push({
                     g_ID: vm.form.internet.g_ID,
                     g_NOMBRE: vm.form.internet.g_NOMBRE,
                     operacion: 9
                 });



             } else {

                 Message.result(1, "Solo Puede haber un grupo")

             }
         }

         vm.deletegroupi = function (index) {
            vm.groupsi.splice(index, 1);

         }

         vm.addgroudctrix = function () {


             if (arrayObjectIndexOf(vm.groupsc, vm.form.citrix.g_ID, "g_ID") === -1) {


                 vm.groupsc.push({
                     g_ID: vm.form.citrix.g_ID,
                     g_NOMBRE: vm.form.citrix.g_NOMBRE,
                     operacion: 9
                 });



             } else {

                 Message.result(1, "Ya exite el grupo");
             }

         }


         vm.deletegroupc = function (index) {

             vm.groupsc.splice(index, 1);
         }


         var arrayObjectIndexOf = function (myArray, searchTerm, property) {

             for (var i = 0, len = myArray.length; i < len; i++) {

                 if (myArray[i][property] === searchTerm) return i;
             }
             return -1;
         }

       
         vm.addgroud = function () {

            // debugger;
             if (arrayObjectIndexOf(vm.groupsf, vm.form.filesystem.g_ID, "g_ID") === -1) {


                 vm.groupsf.push({
                     g_ID: vm.form.filesystem.g_ID,
                     g_NOMBRE: vm.form.filesystem.g_NOMBRE,
                     operacion: 9
                 });



             } else {

                 Message.result(1, "Ya exite el grupo");
             }





         }


         vm.deletegroup = function (index) {

             vm.groupsf.splice(index, 1);



         }



         function RequestGroups(serviceResp) {

             var datai = JSON.parse(serviceResp.data);

             if (datai.executionSummary.status === 'COMPLETED') {

                 if (datai.executionSummary.resultStatusType != 'ERROR') {

                     if (datai.flowOutput.result !== undefined) {

                         var result = JSON.parse(datai.flowOutput.result);
                         switch (parseInt(result[0].status_id)) {
                             case 1:
                                 $mdDialog.cancel();
                                 Message.result(3, "Permisos Agregados");
                                 $window.loading_screen.finish();
                                 break;
                             case 2: Message.result(1, result[0].data);
                                 $window.loading_screen.finish();
                                 break;

                         }

                     }

                     else {

                         Message.result(1, "No se recibio resultado del servidor");
                         $window.loading_screen.finish();
                     }
                 } else {

                     Message.result(1, datai.flowOutput.result);
                     $window.loading_screen.finish();

                 }

             }

             else {

                 AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(RequestGroups, vm.flowerror);
             }




         }




         function GroupCorrect(serviceResp) {


             var datai = JSON.parse(serviceResp.data);
             var idejecution = datai.executionId;
             AD.getFlowStatus(idejecution, 'execution-log').then(RequestGroups, vm.flowerror)

         }







         vm.addpermision = function () {

             debugger;

             console.log($scope.arrayfinal);

             var datafinal=$scope.arrayfinal;

             var final = [];

             for (var t = 0; t < datafinal.length; t++) {


                 if (datafinal[t].gadd.length > 0) {
                     final = final.concat(datafinal[t].gadd)
                 }

                 if (datafinal[t].gdelete.length > 0) {

                     final = final.concat(datafinal[t].gdelete)

                 }

                             
             }

             if (!final.length > 0) {

                 Message.result(4, 'No ha hecho ningun cambio');

             }

             else {

                 var fullgroups = JSON.stringify({
                                     SID: vm.user.SID,
                                     Groups: final,
                                     co_id: $rootScope.useri.userid

                 })


                 AD.groupOperation(fullgroups).then(GroupCorrect, vm.flowerror)

                             $window.loading_screen = $window.pleaseWait({
                                 logo: "app/images/user.png",
                                 backgroundColor: 'rgba(34, 204, 202, 0.43)',
                                 loadingHtml: "<p class='loading-message'>Agregando Permisos</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                             });

       
             }



             //var grouptotal = vm.groupsc.concat(vm.groupsi, vm.groupsf);

             //if(grouptotal.length>0){
             //            var fullgroups = JSON.stringify({
             //                SID: vm.user.SID,
             //                Groups: grouptotal,
             //                co_id: $rootScope.useri.userid

             //            })

             //            AD.groupOperation(fullgroups).then(GroupCorrect, vm.flowerror)

             //            $window.loading_screen = $window.pleaseWait({
             //                logo: "app/images/user.png",
             //                backgroundColor: 'rgba(34, 204, 202, 0.43)',
             //                loadingHtml: "<p class='loading-message'>Agregando Permisos</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

             //            });

             //} else {


             //    Message.result(1, "No ha Elegido Ningun Grupo");
             //}




         }


         function GetAtributos(serviceResp) {

             //console.log(serviceResp);

             $scope.allat = serviceResp.data.data;
             console.log($scope.allat);


         }
        



        //vm.select_empresa = function () {

        //    console.log($scope.form.empresa,'desde metodo');

        //}





});