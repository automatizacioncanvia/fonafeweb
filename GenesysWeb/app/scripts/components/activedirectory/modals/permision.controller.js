﻿angular.module('masterGenerysV1App')
.controller('PermisionCtrl', function ($scope, $mdDialog, user, $rootScope, AD, $window, Message, Apimenu) {

    $rootScope.zindexvalue = true;


    $window.loading_screen = $window.pleaseWait({
        logo: "app/images/user.png",
        backgroundColor: 'rgba(42, 67, 94, 0.54)',
        loadingHtml: "<p class='loading-message'>Cargando Permisos</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

    });



    var empresa_id = user.em_id;
    var membergroups = (user.memberof!== undefined ? JSON.parse(user.memberof) : []);
    var path = user.DistinguishedName;
    var iruta = path.indexOf("\\");

    var pself = this;
    var fail = fail;

    pself.memberof = membergroups;


  

    Apimenu.getmenu(27, empresa_id).then(GetALLGroupt, fail)


    if (iruta !== -1) {

        user.DistinguishedName = user.DistinguishedName.replace("\\", "");
        user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")


    } else {
        user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")

    }






    pself.cancel = Cancel;
   
    pself.addpermision = PermisionFinalTran;



    function GetALLGroupt(serviceResp) {
        

        var resuldb = JSON.parse(serviceResp.data.data[0].result);
        console.log(resuldb);
        $scope.arrayfinal1 = resuldb;
        $window.loading_screen.finish();
    }

   function Cancel() {
        $rootScope.zindexvalue = false;
        $mdDialog.cancel();


    }

    function fail(serviceResp) {

        Message.result(1, serviceResp.data);


    }

    


    function PermisionFinalTran(){

        console.log($scope.arrayfinal1);

        var datafinal=$scope.arrayfinal1;

        var final = [];

        for (var t = 0; t < datafinal.length; t++) {


            if (datafinal[t].gadd.length > 0) {
                final = final.concat(datafinal[t].gadd)
            }

            if (datafinal[t].gdelete.length > 0) {

                final = final.concat(datafinal[t].gdelete)

            }

                             
        }

        if (!final.length > 0) {

            Message.result(4, 'No ha hecho ningun cambio');

        }

        else

        {

            var sid = user.SID;
            var co_id = $rootScope.useri.userid;

            var office = (user.office === undefined || user.office === '' ? null : user.office);
            var title = (user.title === undefined || user.title === '' ? null : user.title);
            var department = (user.department === undefined || user.department === '' ? null : user.department);
            var company = (user.company === undefined || user.company === '' ? null : user.company);
            var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
            var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null : user.mobilephone);
            var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);
            var enable = (user.Enabled === true ? '1' : '0');
            

            var fullg = new FullGroups(sid,
                                      final,
                                      co_id,
                                      user.em_id,
                                      user.givenname,
                                      user.surname,
                                      user.SamAccountName,
                                      null,
                                      user.DistinguishedName,
                                      office,
                                      title,
                                      department,
                                      company,
                                      homephone,
                                      mobilephone,
                                      officephone,
                                      enable,
                                      user.AccountExpirationDat);


            AD.groupOperation(fullg).then(GroupCorrect, FlowError)
            $window.loading_screen = $window.pleaseWait({
                        logo: "app/images/user.png",
                        backgroundColor: 'rgba(34, 204, 202, 0.43)',
                        loadingHtml: "<p class='loading-message'>Modificando Permisos</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                    });

        }

       
      

    }


    function GroupCorrect(serviceResp) {

        var datai = JSON.parse(serviceResp.data);
        var idejecution = datai.executionId;
        AD.getFlowStatus(idejecution, 'execution-log').then(RequestGroups, FlowError)

    }

    function RequestGroups(serviceResp) {

        var datai = JSON.parse(serviceResp.data);

        if (datai.executionSummary.status === 'COMPLETED') {

            if (datai.executionSummary.resultStatusType != 'ERROR') {

                if (datai.flowOutput.result !== undefined) {

                    var result = JSON.parse(datai.flowOutput.result);
                    switch (parseInt(result[0].status_id)) {
                        case 1:
                            $mdDialog.cancel();
                            Message.result(3, "Permisos Modificados");
                            $window.loading_screen.finish();
                            break;
                        case 2: Message.result(1, result[0].data);
                            $window.loading_screen.finish();
                            break;

                    }

                }

                else {

                    Message.result(1, "No se recibio resultado del servidor");
                    $window.loading_screen.finish();
                }
            } else {

                Message.result(1, datai.flowOutput.result);
                $window.loading_screen.finish();

            }

        }

        else {

            AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(RequestGroups, FlowError);
        }


    }



    function FlowError(serviceResp) {

        Message.result(1, serviceResp.data);
        $window.loading_screen.finish();

    }


    function FullGroups(sid,groups,co_id,em_id,u_name,u_surname,u_samaname,u_password,ou_path,u_office,u_title,u_departament,u_company,u_homephone,u_mobilephone,u_officephone,u_enable,u_accountexpirationdate) {
        this.SID = sid;
        this.Groups = groups;
        this.co_id = co_id;
        this.em_id = em_id;
        this.u_name = u_name;
        this.u_surname = u_surname;
        this.u_samaname = u_samaname;
        this.u_password = u_password;
        this.ou_path = ou_path;
        this.u_office = u_office;
        this.u_title = u_title;
        this.u_departament = u_departament;
        this.u_company = u_company;
        this.u_homephone = u_homephone;
        this.u_mobilephone = u_mobilephone;
        this.u_officephone = u_officephone;
        this.u_enable = u_enable;
        this.u_accountexpirationdate = u_accountexpirationdate;

    }


})