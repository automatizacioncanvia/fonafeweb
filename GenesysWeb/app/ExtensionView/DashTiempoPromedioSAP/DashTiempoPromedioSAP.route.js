﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
           .state('dashTiempopromediosap', {
            parent:'main',
               url: 'tiempopromediosap',
               views: {
                   'content@main': {
                       templateUrl: 'app/ExtensionView/DashTiempoPromedioSAP/DashTiempoPromedioSAP.html',
                       controller: 'DashTiempoPromedioSAPCtrl',
                       controllerAs: 'DashTiempoPromedioSAP',
                       authorize: true
                   }
               }
           });
});