﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
           .state('dashExtension', {
               url: '/reportextension',
               views: {
                   'dashExtension@': {
                       templateUrl: 'app/ExtensionView/DashExtension/DashExtension.html',
                       controller: 'DashExtensionCtrl',
                       controllerAs: 'DashExtension',
                       authorize: true
                   }
               }
           });
});