﻿angular.module('masterGenerysV1App')
    .controller('DashExtensionCtrl', ['$scope', 'Apimenu', '$rootScope', 'Apidashboard', 'Notification', '$uibModal', '$http', function($scope, Apimenu, $rootScope, Apidashboard, Notification, $uibModal, $http) {

        $scope.tab = 1;
        $scope.dash9 = "0009";

        $scope.form = {};

        $scope.nodata0009 = false;

        $scope.selectTab = function(setTab) {
            //debugger;
            //si se requiere ng-if
            //var empresa = ($scope.empresas.selected !== undefined ? $scope.empresas.selected.eM_ID :undefined);
            $scope.tab = setTab;
        };

        $scope.isSelected = function(checktab) {
            return $scope.tab === checktab;
        };

        var fail = function(serviceResp) {

            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

        };

        Apimenu.getmenu(3, $rootScope.useri.userid).then(function(serviceResp) {

            var index = Apidashboard.indexOfespecial(serviceResp.data.data);

            if (index !== -1) {

                Apimenu.getmenu(5, $rootScope.useri.userid).then(function(serviceResp) {


                    $scope.servicios = serviceResp.data.data;
                    //console.log($scope.servicios);

                }, fail);


                $scope.form.selectedEmpresa9 = serviceResp.data.data[index];


            }
            $scope.empresas = serviceResp.data.data;
        }, fail);

        Apimenu.getmenu(9, '-').then(function(serviceResp) {

                $scope.periodos = serviceResp.data.data;

            }, fail)
            //metodos globales
        $scope.getchart = function(periodo, em, idchart, servicio, prioridad, sla) {

            var sla = (sla === undefined ? '-1' : sla);

            // var periodo_ver=periodo;
            // var em_ver=em;
            // var idchart_ver=idchart;

            // var servicio_ver = (servicio_ver === undefined ? -1 : servicio_ver);

            // var prioridad_ver = (prioridad_ver === undefined ? -1 : prioridad_ver);

            if (periodo !== 1) {
                var empresa = em;

                var servicio = (servicio === undefined ? -1 : servicio);
                var prioridad = (prioridad === undefined ? -1 : prioridad);

                var vista = idchart + "," + periodo + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad;
                var info = "-";


                $http.get("api/fogetvertickets/" + vista + "/" + info).then(function(serviceResp) {
                    $scope.ticketsobtenidos = serviceResp.data;
                    $http.get("api/fogetvertickets/" + vista + "/" + info).then(function(serviceResp) {
                        $scope.ticketsobtenidos = serviceResp.data;



                    });


                });

            }
            if (periodo !== 1) {

                Apidashboard.getdashi(periodo, em, idchart, servicio, prioridad, sla).then(function(serviceResp) {

                debugger;
                    if (serviceResp.data.length > 0) {

                        if (idchart === "0009") {
                            $scope.value9 = false;
                            $scope.periodo9 = periodo;
                            $scope.ff9 = serviceResp.data;
                            console.log($scope.ff9);
                            $scope.fullkeys9 = Object.keys(serviceResp.data[0]);
                            var obj9 = Apidashboard.makedashboard9(serviceResp.data);
                            $scope.$broadcast('builDash9', obj9);
                        }
                    } else {

                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                    }

                }, fail)
            } else {

                //debugger;
                if (idchart === "0009") {
                    $scope.periodo9 = periodo;
                    $scope.value9 = true;
                }
            }
        };

        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash9, undefined, undefined, undefined);

        $scope.onSelected = function(empresa, idchart, periodo, servicio, prioridad, sla) {
            var sla = (sla === undefined ? '-1' : sla);
            if (periodo !== 1) {

                if (periodo === undefined) {

                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Periodo Dashboard ' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                } else {

                    if (empresa !== undefined) {

                        var servicio = (servicio === undefined ? -1 : servicio.sE_ID);
                        var prioridad = (prioridad === undefined ? -1 : prioridad.sym);

                        $scope.getchart(periodo, empresa.eM_ID, idchart, servicio, prioridad, sla);
                        Apimenu.getmenu(10, empresa.eM_ID).then(function(serviceResp) {
                            $scope.servicios = serviceResp.data.data;
                        });

                    } else {
                        // $scope.getchart(periodo, undefined, idchart,servicio,prioridad);
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Empresa' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $scope.servicios = [];
                    }
                }
            }



        };

        $scope.entrefechas = function(from, to, em, idchart, servicio, prioridad, sla) {

            var sla = (sla === undefined ? '-1' : sla);

            if (em !== undefined) {

                var empresa = em;
                var servicio = (servicio === undefined ? -1 : servicio);
                var prioridad = (prioridad === undefined ? -1 : prioridad);

                var vista = idchart + "," + 1 + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad;

                var desde = moment(from).format("YYYYMMDD");
                var hasta = moment(to).format("YYYYMMDD");
                var info = "" + desde + "," + hasta;
                $http.get("api/fogetvertickets/" + vista + "/" + info).then(function(serviceResp) {
                    $scope.ticketsobtenidos = serviceResp.data;
                    $http.get("api/fogetvertickets/" + vista + "/" + info).then(function(serviceResp) {
                        $scope.ticketsobtenidos = serviceResp.data;



                    });


                });





                Apidashboard.getdashbydate(from, to, em, idchart, servicio, prioridad).then(function(serviceResp) {
                debugger;
                    if (serviceResp.data.length > 0) {

                        if (idchart === "0009") {

                            $scope.ff9 = serviceResp.data;
                            $scope.fullkeys9 = Object.keys(serviceResp.data[0]);
                            var obj9 = Apidashboard.makedashboard9(serviceResp.data);
                            $scope.$broadcast('builDash9', obj9);
                        }
                    } else {
                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                    }

                }, fail);

            } else {

                Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Porfavor seleccione una empresa.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

            }
        };

    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte9", ['$scope', '$http', '$uibModal', function($scope, $http, $uibModal) {

        $scope.$on('builDash9', function(key, data) {
            $scope.labels = data.labels;
            $scope.data = data.data;
            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                legend: { display: true }
            };

        });

        $scope.onClick = function(points, evt) {
            console.log(points, evt);
        };

    }]);



angular.module('masterGenerysV1App').controller('dtpickerController', function() {

    var vm = this;
    var d = new Date(2016, 0, 1);
    var n = moment();


    vm.date = moment().add(-1, 'd');
    vm.date2 = moment();


    //vm.options = { format: "YYYY/MM/DD HH:mm" };

    vm.update = function(date, date2) {
        vm.optionsFrom = { format: 'DD/MM/YYYY', maxDate: date2, minDate: d };
        vm.optionsTo = { format: 'DD/MM/YYYY', minDate: date };

        //vm.optionsFrom = { format: 'DD/MM/YYYY' };
        //vm.optionsTo = { format: 'DD/MM/YYYY' };
    };
    vm.update(vm.date, vm.date2);

    vm.updateOnly = function () {
        //vm.optionsFrom = { format: 'DD/MM/YYYY', maxDate: date2, minDate: d };
        vm.optionsReg = { format: 'DD/MM/YYYY', minDate: d };

        //vm.optionsFrom = { format: 'DD/MM/YYYY' };
        //vm.optionsTo = { format: 'DD/MM/YYYY' };
    };

    vm.updateOnly();


    vm.getTime = function() {
        alert('Selected time is:' + vm.date.format("YYYY/MM/DD"));
    };



    vm.addTime = function(val, selector) {
        vm.date = moment(vm.date.add(val, selector));
    };


});
