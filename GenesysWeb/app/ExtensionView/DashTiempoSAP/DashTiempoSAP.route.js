﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
           .state('dashTiempo', {
               url: '/reporttiempo',
               views: {
                   'dashTiempo@': {
                       templateUrl: 'app/ExtensionView/DashTiempoSAP/DashTiempoSAP.html',
                       controller: 'DashTiempoCtrl',
                       controllerAs: 'DashTiempo',
                       authorize: true
                   }
               }
           });
});