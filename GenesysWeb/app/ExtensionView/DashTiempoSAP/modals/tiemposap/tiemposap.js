﻿angular.module('masterGenerysV1App').controller('ModalTiempoSAP', function($uibModalInstance, $scope, tiemposap, $rootScope, $http, Apifonfe, Notification, $window, ngTableParams, $filter) {

    $scope.close = function() {
        $uibModalInstance.close();

    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.tiemposap = tiemposap[0];
    var gettiemposap = $scope.tiemposap;
    // var gettiemposap=JSON.stringify($scope.tiemposap);
    $scope.cantidad=$scope.tiemposap.length;
    console.log(gettiemposap)
    console.log($scope.tiemposap.length);

    $scope.exportarexcel = function() {
        alasql('SELECT * INTO XLSX("Reporte.xlsx",{headers:true}) FROM ?', [$scope.tiemposap]);
    };

    var ngtablei = function(array) {
        $scope.tableParams = new ngTableParams({
            page: 1,
            count: 7
        }, {
            total: array.length,
            getData: function($defer, params) {
                var filteredData = params.filter() ?
                    $filter('filter')(array, params.filter()) :
                    array;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    array;

                // var orderedData = params.sorting() ?
                //         $filter('orderBy')(array, params.orderBy()) :
                //         array;


                params.total(orderedData.length);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

    };

    ngtablei(gettiemposap);






});
