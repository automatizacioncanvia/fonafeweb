﻿angular.module('masterGenerysV1App')
    .controller('DashTiempoCtrl', ['$scope', 'Apimenu', '$rootScope', 'Apidashboard', 'Notification', '$uibModal', '$http', function($scope, Apimenu, $rootScope, Apidashboard, Notification, $uibModal, $http) {



        var comboanio = function() {
            var date = new Date,
                years = [],
                year = date.getFullYear();

            for (var i = year; i > year - 5; i--) {
                years.push(i);
            }

            console.log("entro");
            return years;
        };

        $scope.years = comboanio();

        console.log("salio");

        $scope.dash10 = "0010";

        $scope.form = {};

        $scope.nodata0010 = false;

        $scope.selectTab = function(setTab) {
            $scope.tab = setTab;
        };

        $scope.isSelected = function(checktab) {
            return $scope.tab === checktab;
        };

        var fail = function(serviceResp) {

            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

        };

        Apimenu.getmenu(26, $rootScope.useri.userid).then(function(serviceResp) {

            var index = Apidashboard.indexOfespecial(serviceResp.data.data);

            if (index !== -1) {

                $scope.form.selectedEmpresa10 = serviceResp.data.data[index];

            }
            $scope.empresas = serviceResp.data.data;
        }, fail);




        Apimenu.getmenu(9, '-').then(function(serviceResp) {

                $scope.periodos = serviceResp.data.data;

            }, fail)
            //metodos globales
        $scope.getchart = function(periodo, em, idchart, anio) {

            $scope.empresa = em;
            $scope.anio = anio;


            var em = (em === undefined ? -1 : em);
            var sla = '-1';
            var prioridad = -1;
            var servicio = -1;

            if (periodo !== 1) {

                Apidashboard.getdashi(anio, em, idchart, servicio, prioridad, sla).then(function(serviceResp) {


                    if (serviceResp.data.length > 0) {

                        if (idchart === "0010") {
                            $scope.value10 = false;
                            $scope.periodo10 = periodo;
                            $scope.ff10 = serviceResp.data;
                            $scope.fullkeys10 = Object.keys(serviceResp.data[0]);
                            var obj10 = Apidashboard.makedashboard10(serviceResp.data);
                            $scope.$broadcast('builDash10', obj10);
                        }
                    } else {

                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                    }

                }, fail)
            } else {

                //debugger;
                if (idchart === "0010") {
                    $scope.periodo10 = periodo;
                    $scope.value10 = true;
                }
            }
        };

        $scope.getchart(2, $rootScope.useri.empresa_id, $scope.dash10, undefined, undefined, undefined);

        $scope.onSelected = function(empresa, idchart, periodo, servicio, prioridad, sla) {
            var sla = (sla === undefined ? '-1' : sla);
            if (periodo !== 1) {

                if (periodo === undefined) {

                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Periodo Dashboard ' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                } else {

                    if (empresa !== undefined) {

                        var servicio = (servicio === undefined ? -1 : servicio.sE_ID);
                        var prioridad = (prioridad === undefined ? -1 : prioridad.sym);

                        $scope.getchart(periodo, empresa.eM_ID, idchart, servicio, prioridad, sla);
                        Apimenu.getmenu(10, empresa.eM_ID).then(function(serviceResp) {
                            $scope.servicios = serviceResp.data.data;
                        });

                    } else {
                        // $scope.getchart(periodo, undefined, idchart,servicio,prioridad);
                        Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Seleccione Empresa' + idchart + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                        $scope.servicios = [];
                    }
                }
            }



        };

        $scope.entrefechas = function(em, idchart, anio) {
            console.log(anio);
            var sla = '-1'
            var from = "";
            var to = "";
            if (em !== undefined) {

                var empresa = em;
                var servicio = -1;
                var prioridad = -1;

                var vista = idchart + "," + anio + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad;

                Apidashboard.getdashbydate(from, to, em, idchart, servicio, prioridad).then(function(serviceResp) {

                    if (serviceResp.data.length > 0) {

                        if (idchart === "0010") {

                            $scope.ff9 = serviceResp.data;
                            $scope.fullkeys9 = Object.keys(serviceResp.data[0]);
                            var obj9 = Apidashboard.makedashboard9(serviceResp.data);
                            $scope.$broadcast('builDash10', obj9);
                        }
                    } else {
                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                    }

                }, fail);

            } else {

                Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Porfavor seleccione una empresa.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

            }
        };

    }]);

angular.module('masterGenerysV1App')
    .controller("reporte10", ['$scope', '$http', '$uibModal', function($scope, $http, $uibModal) {

        $scope.$on('builDash10', function(key, data) {
            $scope.labels = data.labels;
            $scope.data = data.data;
            $scope.options = {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                legend: { display: false }
            };

        });

        $scope.onClick = function(points, evt) {

            $scope.empresa;
            $scope.anio;
            $scope.label = points[0]._model.label;
            console.log("empresa: " + $scope.empresa + " año: " + $scope.anio + " mes: " + $scope.label);

            var valor = $scope.empresa + ',' + $scope.anio + ',' + $scope.label;
            
            $http.get("api/fogettiemposap/" + valor).then(function(serviceResp) {

                $scope.tiemposap = serviceResp.data;

                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/ExtensionView/DashTiempoSAP/modals/tiemposap/tiemposap.html',
                    controller: 'ModalTiempoSAP',
                    size: 'md',
                    resolve: {
                        tiemposap: function() {
                            var array = [];
                            array.push($scope.tiemposap);
                            return array;
                        }
                    }
                });
            });

        };

    }]);
