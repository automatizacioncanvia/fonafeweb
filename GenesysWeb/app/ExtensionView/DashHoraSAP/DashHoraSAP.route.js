﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
           .state('dashHorasap', {
               url: '/reporthoras',
               views: {
                   'dashHorasap@': {
                       templateUrl: 'app/ExtensionView/DashHoraSAP/DashHoraSAP.html',
                       controller: 'DashHoraSAPCtrl',
                       controllerAs: 'DashHoraSAP',
                       authorize: true
                   }
               }
           });
});