﻿angular.module('masterGenerysV1App')
    .controller('DashHoraSAPCtrl', ['$scope', 'Apimenu', '$rootScope', 'Apidashboard', 'Notification', '$uibModal', '$http', function ($scope, Apimenu, $rootScope, Apidashboard, Notification, $uibModal, $http) {

        $scope.tab = 1;
        $scope.dash11 = "0011";

        $scope.form = {};

        var today = moment();

        $scope.form.expirationdate = today;

        $scope.options = { format: 'DD/MM/YYYY', showClear: true, showTodayButton: true };

        $scope.selectTab = function (setTab) {
            //debugger;
            //si se requiere ng-if
            //var empresa = ($scope.empresas.selected !== undefined ? $scope.empresas.selected.eM_ID :undefined);
            $scope.tab = setTab;
        };

        $scope.isSelected = function (checktab) {
            return $scope.tab === checktab;
        };

        var fail = function (serviceResp) {

            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

        };

        //metodos globales
        $scope.getchart = function (periodo, em, idchart, anio) {

            var em = (em === undefined ? -1 : em);
            var sla = '-1';
            var prioridad = -1;
            var servicio = -1;
            var anio = moment(anio).format("DD/MM/YYYY");

            if (periodo !== 1) {

                Apidashboard.getdashi(anio, em, idchart, servicio, prioridad, sla).then(function (serviceResp) {


                    if (serviceResp.data.length > 0) {

                        if (idchart === "0011") {
                            debugger;
                            $scope.value11 = false;
                            $scope.periodo11 = periodo;
                            $scope.ff11 = serviceResp.data;
                            $scope.fullkeys11 = Object.keys(serviceResp.data[0]);
                            var obj11 = Apidashboard.makedashboard10(serviceResp.data);
                            $scope.$broadcast('builDash11', obj11);
                        }
                    } else {

                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                    }

                }, fail)
            } else {

                //debugger;
                if (idchart === "0011") {
                    $scope.periodo11 = periodo;
                    $scope.value11 = true;
                }
            }
        };

        Apimenu.getmenu(3, $rootScope.useri.userid).then(function (serviceResp) {

            var index = Apidashboard.indexOfespecial(serviceResp.data.data);

            if (index !== -1) {

                Apimenu.getmenu(5, $rootScope.useri.userid).then(function (serviceResp) {


                    $scope.servicios = serviceResp.data.data;
                    //console.log($scope.servicios);

                }, fail);


                $scope.form.selectedEmpresa11 = serviceResp.data.data[index];


            }
            $scope.empresas = serviceResp.data.data;
        }, fail);

        Apimenu.getmenu(9, '-').then(function (serviceResp) {

            $scope.periodos = serviceResp.data.data;

        }, fail)
 

        $scope.entrefechas = function (from, em, idchart) {

            var sla = (sla === undefined ? '-1' : sla);

            if (em !== undefined) {

                var empresa = em;
                var servicio = (servicio === undefined ? -1 : servicio);
                var prioridad = (prioridad === undefined ? -1 : prioridad);

                var vista = idchart + "," + 1 + "," + $rootScope.useri.userid + "," + empresa + "," + servicio + "," + prioridad;

                var desde = moment(from).format("YYYYMMDD");
                var to = "";
   
 
                Apidashboard.getdashbydate(from, to, em, idchart, servicio, prioridad).then(function (serviceResp) {

                    if (serviceResp.data.length > 0) {

                        if (idchart === "0011") {

                            $scope.ff11 = serviceResp.data;
                            $scope.fullkeys11 = Object.keys(serviceResp.data[0]);
                            var obj9 = Apidashboard.makedashboard11(serviceResp.data);
                            $scope.$broadcast('builDash11', obj11);
                        }
                    } else {
                        Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atención! -</b> No hay información para el filtro selecionado.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                    }

                }, fail);

            } else {

                Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>Porfavor seleccione una empresa.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

            }
        };

    }]);

angular.module('masterGenerysV1App')
    .controller("Reporte11", ['$scope', function ($scope) {
        debugger;
        $scope.$on('builDash11', function (key, data) {
//            debugger;
            $scope.labels = data.labels;
            $scope.series = data.series;
            $scope.legend = data.series;

            $scope.data = data.data;

            $scope.options = {
                legend: { display: false }
            };

        });

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

    }]);

 