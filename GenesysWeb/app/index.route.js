﻿angular.module('masterGenerysV1App').config(function ($stateProvider, $urlRouterProvider, $mdIconProvider, $mdThemingProvider) {

   $urlRouterProvider.otherwise('/')
   
    $mdIconProvider
     .iconSet("acciones", 'app/images/acciones/acciones.svg', 24)
     .iconSet("agreagar", 'app/images/acciones/agregar.svg', 24)
     .iconSet("buscar", 'app/images/acciones/buscar.svg',24)
     .iconSet("deshabilitar", 'app/images/acciones/deshabilitar.svg', 24)
     .iconSet("desbloquear", 'app/images/acciones/desbloquear.svg', 24)
     .iconSet("editar", 'app/images/acciones/editar.svg', 24)
     .iconSet("eliminar", 'app/images/acciones/eliminar.svg', 24)
     .iconSet("habilitar", 'app/images/acciones/habilitar.svg', 24)
     .iconSet("password", 'app/images/acciones/password.svg', 24)
     .iconSet("user", 'app/images/svg/svg/user-23.svg', 24)

    
    $mdThemingProvider.theme('default')
   .primaryPalette('deep-orange')
   .backgroundPalette('grey');

});