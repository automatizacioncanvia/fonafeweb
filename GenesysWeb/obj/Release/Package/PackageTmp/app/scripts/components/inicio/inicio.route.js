﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('inicio', {
              parent: 'main',
              url: 'inicio',
              views: {
                  'content@main': {
                      templateUrl: 'app/views/components/inicio/inicio.html',
                      controller: 'InicioCtrl',
                      controllerAs: 'inicio',
                      authorize: true

                  }
              }
          });
});