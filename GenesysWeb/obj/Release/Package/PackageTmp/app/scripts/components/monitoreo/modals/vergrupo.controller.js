﻿angular.module('masterGenerysV1App').controller('ModalVerGrupo', function ($uibModalInstance, $scope, filtrosobtenidos, $rootScope, $http, Apifonfe, Notification, $window, ngTableParams, $filter) {

	$scope.ticketobtenidosmodal = filtrosobtenidos[0].data;
	$scope.totalTickets = filtrosobtenidos[0].data.length;
	var gettickets = $scope.ticketobtenidosmodal;

	console.log(gettickets);

	$scope.close = function () {
		$uibModalInstance.close();

	};


	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.exportarexcel = function () {
		alasql('SELECT * INTO XLSX("Reporte.xlsx",{headers:true}) FROM ?', [$scope.ticketobtenidosmodal]);
	};

	var ngtablei = function (array) {
		$scope.tableParams = new ngTableParams({
			page: 1,
			count: 7
		}, {
			total: array.length,
			getData: function ($defer, params) {
				var filteredData = params.filter() ?
                    $filter('filter')(array, params.filter()) :
                    array;
				var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    array;

				params.total(orderedData.length);
				$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
			}
		});

	};
	console.log(gettickets);
	ngtablei(gettickets);

});
