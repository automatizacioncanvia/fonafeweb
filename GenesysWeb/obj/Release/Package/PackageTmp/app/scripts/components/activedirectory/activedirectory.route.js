﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
	$stateProvider
			  .state('activedirectory', {
			  	parent: 'main',
			  	url: 'activedirectory',
			  	views: {
			  		'content@main': {
			  			templateUrl: 'app/views/components/activedirectory/activedirectory.html',
			  			controller: 'ActiveDirectoryCrl',
			  			controllerAs: 'activedirectory',
			  			authorize: true
			  		}
			  	}
			  });
});