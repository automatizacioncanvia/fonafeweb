﻿angular.module('masterGenerysV1App')
.controller('ShowUserCtrl', function ($scope, $mdDialog, user, $rootScope, AD, $window, Message,Apimenu) {




    $scope.edit = user.edit;
    $scope.samavieja = user.SamAccountName;
    $rootScope.zindexvalue = true;
    $scope.user = user;
    $scope.datetime = user.AccountExpirationDate;
    var path=user.DistinguishedName;
    var iruta = path.indexOf("\\");






    Apimenu.getmenu(22, user.em_id).then(GetAtributos, error)

    function GetAtributos(serviceResp) {

       var allat = serviceResp.data.data;
      
       $scope.showattrs = (user.atributos === null ? allat : JSON.parse(user.atributos));

    }


    



    if (iruta !== -1) {
      
        $scope.user.DistinguishedName= $scope.user.DistinguishedName.replace("\\","");         
        $scope.user.DistinguishedName = ($scope.user.DistinguishedName).replace("CN=" + user.cn + ",", "")
      

    } else {
        $scope.user.DistinguishedName = ($scope.user.DistinguishedName).replace("CN="+user.cn+",", "")
       
    }
 
    if ($scope.user.AccountExpirationDate === null) {
        $scope.expire = true;
    } else {
        $scope.expire = false;
    }

    $scope.editar = function () {

        $scope.edit = true;
    }

    $scope.selectdate = function () {

        $scope.expire = false;
    }

    $scope.change = function (tab) {
        
        switch (tab) {

            case 0: $scope.tab = 1;
                break;
            case 1: $scope.tab = 0;
                break;
        }

    }

    $scope.cancel = function () {
        $rootScope.zindexvalue = false;
        $mdDialog.cancel();
    };

    $scope.show = function (ev) {
        $mdDialog.show({
            templateUrl: 'temp.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: false
        });
    }

    $scope.expiration = function (expire) {


        if (expire !== true) {
            $scope.user.AccountExpirationDate = null;
        }
        

    }

    $scope.modificar = function () {

        var office = ($scope.user.office === undefined || $scope.user.office === '' ? null : $scope.user.office);
        var title = ($scope.user.title === undefined || $scope.user.title === '' ? null : $scope.user.title);
        var department = ($scope.user.department === undefined || $scope.user.department === '' ? null : $scope.user.department);
        var company = ($scope.user.company === undefined || $scope.user.company === '' ? null : $scope.user.company);
        var homephone = ($scope.user.homephone === undefined || $scope.user.homephone === '' ? null : $scope.user.homephone);
        var mobilephone = ($scope.user.mobilephone === undefined || $scope.user.mobilephone === '' ? null : $scope.user.mobilephone);
        var officephone = ($scope.user.Officephone === undefined || $scope.user.Officephone === '' ? null : $scope.user.Officephone);
        var enable = ($scope.user.Enabled === true ? '1' : '0');
       // var expiration = ($scope.user.AccountExpirationDate === null ? null : $scope.user.AccountExpirationDate);


      var edituser = JSON.stringify({
            u_adid: $scope.user.SID,
            o_id: 2,
            em_id: null,
            u_name: $scope.user.givenname,
            u_surname: $scope.user.surname,
            u_samaname: $scope.user.SamAccountName,
            u_password: null,
            ou_path: $scope.user.DistinguishedName,
            u_office: office,
            u_title: title,
            u_departament: department,
            u_company: company,
            u_homephone: homephone,
            u_mobilephone: mobilephone,
            u_officephone: officephone,
            u_enable: enable,
            u_accountexpirationdate: $scope.user.AccountExpirationDate,
            co_id: $rootScope.useri.userid,
            info: $scope.samavieja,
            atributos: $scope.showattrs
      })


      console.log(edituser);


     


      AD.operation(edituser).then(usermodified, error);


      $window.loading_screen = $window.pleaseWait({
          logo: "app/images/user.png",
          backgroundColor: 'rgba(42, 67, 94, 0.54)',
          loadingHtml: "<p class='loading-message'>Modificando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

      });



    }

    var usermodified = function (serviceResp) {
     
        var datai = JSON.parse(serviceResp.data);
        var idejecution = datai.executionId;
        AD.getFlowStatus(idejecution, 'execution-log').then(flowstatusmodified, error)

    }

    var flowstatusmodified = function (serviceResp) {

        var datai = JSON.parse(serviceResp.data);
      

        if (datai.executionSummary.status === 'COMPLETED' ) {


            if (datai.executionSummary.resultStatusType != 'ERROR')
                            {
                              
                                                if (datai.flowOutput.result !== undefined) {

                                                    var result = JSON.parse(datai.flowOutput.result);
                                                    switch (parseInt(result[0].status_id)) {
                                                        case 1: $scope.SID = result[0].data.SID;
                                                            // $scope.tab = 2;
                                                            Message.result(3, "Usuario Modificado");
                                                            $window.loading_screen.finish();
                                                            break;
                                                        case 2: Message.result(1, result[0].data);
                                                            $window.loading_screen.finish();
                                                            break;

                                                    }

                                                }

                                                else {

                                                    Message.result(1, "No se recibio resultado del servidor");
                                                    $window.loading_screen.finish();
                                                }
                                }
                                      else {
                                        
                                            Message.result(1, datai.flowOutput.result);
                                            $window.loading_screen.finish();

                                }



        }

        else {

            AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatusmodified, error);
        }




    }

    var error = function (serviceResp) {

        Message.result(4, serviceResp.data);
        $window.loading_screen.finish();

     


    }

    $scope.cancelar = function () {

        $scope.edit = false;



    }


})