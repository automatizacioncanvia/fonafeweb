﻿angular.module('masterGenerysV1App').controller('ModalNewLink', function($uibModalInstance, $scope, Apimenu,Notification, SweetAlert, $http, $rootScope) {

    $scope.close = function() {

        $uibModalInstance.close();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.modal = {};

    Apimenu.getmenu(16, $rootScope.useri.userid).then(function(serviceResp) {

        $scope.getempresas2 = serviceResp.data.data;


    });


    $scope.cargaServicio = function(empre) {


        if (empre == undefined) {

        } else {
            $http.get("api/fogetserviciobyempresa/" + empre).then(function(data) {

                $scope.servicios2 = data.data;
                if ($scope.servicios2[1] == undefined) {
                    Notification.info({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>No se encontraron servicios.</span>', delay: 1500, positionY: 'bottom', positionX: 'right' });
                }

            });
        }
    }

    $scope.clickInsertar = function() {


        console.log($scope.modal);

        $scope.modal.nombre = ($scope.modal.nombre === undefined || $scope.modal.nombre === null ? $scope.modal.nombre = "" : $scope.modal.nombre);
        $scope.modal.descripcion = ($scope.modal.descripcion === undefined || $scope.modal.descripcion === null ? $scope.modal.descripcion = "" : $scope.modal.descripcion);

    debugger;
        var body = JSON.stringify({
            nombre: $scope.modal.nombre,
            descripcion: $scope.modal.descripcion,
            idempresa:$scope.modal.selectedEmpresa.eM_ID,
            idservicio:$scope.modal.selectedServicio.sE_ID,
            iduser:$rootScope.useri.userid,
        });

        console.log(body)

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };


        SweetAlert.swal({
                title: "Estás seguro?",
                text: "Se procederá a insertar al sistema!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, continúa!",
                cancelButtonText: "No, retrocede!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {

                    $http.post('api/foinsertlink', body, config)
                        .success(function(data) {
                            $scope.PostDataResponse = data;
                            console.log($scope.PostDataResponse);
                            SweetAlert.swal("Insertado!", "Se insertó el link satisfactoriamente.", "success");
                            $uibModalInstance.close();

                        }).error(function(data) {
                            $scope.error = data
                            SweetAlert.swal("Error!", $scope.error, "error");
                        });

                } else {
                    SweetAlert.swal("Cancelado", "Operación cancelada", "error");
                }
            });

    };


});
