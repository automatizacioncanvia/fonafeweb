'use strict';

/**
 * @ngdoc function
 * @name masterGenerysV1App.controller:AppCtrl
 * @description
 * # AppCtrl
 * Controller of the masterGenerysV1App
 */

angular.module('masterGenerysV1App')
  .controller('AppCtrl', function ($scope, Apifonfe, $rootScope, Apilink, Notification, $window, $state) {
      
      var info = "" + $rootScope.useri.userid + "," + 2;

      var getlink1 = function (serviceResp) {
          debugger;
          $scope.apps = serviceResp.data.data;
      }

      var fail = function (serviceResp) {
          Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status+ '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
      }

      Apilink.getlink(2, info).then(getlink1, fail);

      $scope.Absolutelogin = function (id, url) {
   
          $scope.disable = true;
         
          if (id === 2) {

              var body = JSON.stringify({
                  token: localStorage.getItem('Token').toString(),
                  url:url
              });
              Apifonfe.login(body).success(function (data) {
                  debugger;
                  var ind = url.indexOf('CAisd');
                  var final = url.substring(0, ind);
                  
                  if (data.indexOf('CAisd/pdmweb.exe?SID') === -1) {
                  
                      $scope.disable = false;
                      Notification.warning({ message: ' <span class="pe-7s-attention" ></span><span><b>  Atencion! -</b>No se puede logear al Service Desk.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                  } else {
                  
                      $scope.disable = false;
                      $window.open(final+data, 'Open', 'width=1500,height=700');
                  }

              })
                 .error(function (data) {
                     $scope.disable = false;
                     Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });

                 });
          }
          else if (id === 3) {
              var dati = {
                  token: localStorage.getItem('Token').toString(),
                  url: url,
                  idUsuario: $rootScope.useri.empresa_id
              };
          
              var body = JSON.stringify(dati);

              Apifonfe.loginswrt(body).success(function (data) {
                  //debugger;
                  console.log("carga links")
                  console.log(data);
                  console.log(url);
                  if (url.substring(0, 1) == "h") {
                      $scope.disable = false;
                      $window.open(data, 'Open', 'width=1500,height=700');
                  } else {
                      $scope.disable = false;
                      $window.open(url, 'Open', 'width=1500,height=700');
                  }
              })
                .error(function (data, status, header, config) {
                    $scope.disable = false;
                    Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                });

          } 
          if(id===1) {

                var bodyi = JSON.stringify( {
                    token: localStorage.getItem('Token').toString(),
                    url: url,
                    idUsuario: $rootScope.useri.userid
                });


              Apifonfe.loginPortalCloud(bodyi).success(function (data) {
                  debugger;
                  $scope.disable = false;
                  $window.open(data, 'Open', 'width=1500,height=700');
               }).error(function (data, status, header, config) {
                          Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                       });

          }

          if (id === 168) {

             
              $state.go('activedirectory');
          }

      }
     

  });
