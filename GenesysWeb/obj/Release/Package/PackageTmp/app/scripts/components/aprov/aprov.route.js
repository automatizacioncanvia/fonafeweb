﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('aprovisionamiento', {
              parent: 'main',
              url: 'aprovisionamiento',
              views: {
                  'content@main': {
                      templateUrl: 'app/views/components/aprov/aprovisionamiento.html',
                      controller: 'AprovCtrl',
                      controllerAs: 'aprov',
                      authorize: true

                  }
              }
          });
});