﻿/// <reference path="../../../../views/components/indicadores/modals/verstatus.html" />
/// <reference path="../../../../views/components/indicadores/modals/verstatus.html" />
/// <reference path="../../../../views/components/indicadores/modals/verstatus.html" />
angular.module('masterGenerysV1App').controller('ModalViewDetail', function ($uibModalInstance, $scope, filtrosobtenidos, $rootScope, $http, Apifonfe, Notification, $window, ngTableParams, $filter,$uibModal) {
    
	$scope.ticketdetail = filtrosobtenidos[0].data;
	$scope.totalTickets = filtrosobtenidos[0].data.length;
	$scope.sla = filtrosobtenidos[1]
	var gettickets = $scope.ticketdetail;
	if ($scope.sla == undefined) { $scope.sla = 'SLA'; }
	if ($scope.sla == 'VO') { $scope.sla = 'VO'; }
	if ($scope.sla == 'LI') { $scope.sla = 'LI'; }
	if ($scope.sla == 'VM') { $scope.sla = 'VM'; }
	if ($scope.sla == 'EX') { $scope.sla = 'VM'; }
	console.log($scope.sla);console.log("opendetalle01"); var myArr1 = [$scope.ticketdetail, $scope.totalTickets]; console.log(myArr1);

	$scope.close = function () {
		$uibModalInstance.close();

	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.exportarexcel = function () {
		alasql('SELECT * INTO XLSX("Reporte.xlsx",{headers:true}) FROM ?', [$scope.ticketdetail]);
	};

	var ngtablei = function (array) {
		$scope.tableParams = new ngTableParams({
			page: 1,
			count: 7
		}, {
			total: array.length,
			getData: function ($defer, params) {
				var filteredData = params.filter() ?
                    $filter('filter')(array, params.filter()) :
                    array;
				var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    array;

				params.total(orderedData.length);
				$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
			}
		});

	};
	
	ngtablei(gettickets);

	$scope.openSD = function (numero) {

	    Apifonfe.linkticket(numero).success(function (data) {
	        var dati = data.split('#');

	        var ind = dati[0].indexOf('CAisd');
	        var final = dati[0].substring(0, ind);
			console.log("Opens Sd" + data);
	        $window.open(final + dati[1], 'OpenTicket', 'width=1500,height=700');
	    }).error(function (data) {
	        Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
	    })
	}


	$scope.openticket = function (ticket) {
	    debugger;
	    $window.loading_screen = $window.pleaseWait({
	        logo: "app/images/search.png",
	        backgroundColor: 'rgba(41, 127, 170, 0.79)',
	        loadingHtml: "<p class='loading-message'>Cargando información1...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
	    });

	    $http.get("api/fogetgrupos/" + ticket).then(function (serviceResp) {
	        $scope.ticketsobtenidos = serviceResp.data;
	        $window.loading_screen.finish();
	        var modalInstance = $uibModal.open({
	            animation: true,
	            ariaLabelledBy: 'modal-title',
	            ariaDescribedBy: 'modal-body',
	            templateUrl: 'app/views/components/monitoreo/modals/vergrupo.html',
	            controller: 'ModalVerGrupo',
	            size: 'lg',
	            resolve: {
	                filtrosobtenidos: function () {
	                    var array = [];
	                    array.push($scope.ticketsobtenidos);
	                    return array;
	                }
	            }
	        });
	    });
	};

	$scope.openstate = function (ticket) {

	    $window.loading_screen = $window.pleaseWait({
	        logo: "app/images/search.png",
	        backgroundColor: 'rgba(41, 127, 170, 0.79)',
	        loadingHtml: "<p class='loading-message'>Cargando información2...</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"
	    });

	    $http.get("api/fogetstatus/" + ticket).then(function (serviceResp) {
	        $scope.ticketsobtenidos = serviceResp.data;
	        $window.loading_screen.finish();
	        var modalInstance = $uibModal.open({
	            animation: true,
	            ariaLabelledBy: 'modal-title',
	            ariaDescribedBy: 'modal-body',
	            templateUrl: 'app/views/components/monitoreo/modals/verstatus.html',
	            controller: 'ModalVerStatus',
	            size: 'lg',
	            resolve: {
	                filtrosobtenidos: function () {
	                    var array = [];
	                    array.push($scope.ticketsobtenidos);
	                    return array;
	                }
	            }
	        });
	    });
	};
});
