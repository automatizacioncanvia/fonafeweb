﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('documentos', {
              parent: 'main',
              url: 'documentos',
              views: {
                  'content@main': {
                      templateUrl: 'app/views/components/documentos/documentos.html',
                      controller: 'DocumentosCtrl',
                      controllerAs: 'documentos',
                      authorize: true

                  }
              }
          });
});