﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
	$stateProvider
          .state('indicadoresDash', {
          	parent: 'main',
          	url: 'indicadoresDash?idServicio&nombreServicio&idEmpresa&nombreEmpresa&tipo&atendido',
          	views: {
          		'content@main': {
          			templateUrl: 'app/views/components/dashboard/indicadoresDash.html',
          			controller: 'IndicadoresDashCtrl',
          			controllerAs: 'indicadoresDash',
          			authorize: true

          		}
          	}
          });
});