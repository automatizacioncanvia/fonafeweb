﻿"use strict";
/**
 * @ngdoc function
 * @name masterGenerysV1App.controller:DocumentosCtrl
 * @description
 * # DocumentosCtrl
 * Controller of the masterGenerysV1Appp
 */
angular.module('masterGenerysV1App')
  .controller('InventarioCtrl', function (Upload, $parse, $scope, $rootScope, $http, $timeout, SweetAlert, $uibModal, Apiinventario, ngTableParams, $stateParams, $filter)
  {
      var vm = this;


      Onload();

      var ngtablei = function (array) {

          $scope.tableParams = new ngTableParams({
              page: 1,
              count: 15
          }, {
              total: array.length,
              getData: function ($defer, params) {
                  var filteredData = params.filter() ?
                      $filter('filter')(array, params.filter()) :
                      array;
                  var orderedData = params.sorting() ?
                      $filter('orderBy')(filteredData, params.orderBy()) :
                      array;

                  params.total(orderedData.length);
                  $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
          });
      };
       




      function Onload() {
          
          Apiinventario.getOption(JSON.stringify(new Vista(1, 1, $rootScope.useri.userid))).then(CargaDatos, Error);

      };

      function Error(serviceResp) {
          console.log(serviceResp.data);
      };

      function CargaDatos(serviceResp) {
          
          $scope.empresas = serviceResp.data.data;
          $scope.estado = serviceResp.data.data1;
          $scope.admpor = serviceResp.data.data2;
          //vm.categoriass = serviceResp.data[4];
          //vm.administrados = serviceResp.data[5];
          //vm.sadministrados = vm.administrados;
          //vm.estados = serviceResp.data[6];
          //vm.sestados = vm.estados;
          //vm.torres = serviceResp.data[7];
          //vm.storres = vm.torres;


          //vm.marcas = loadALL(serviceResp.data[8], 'MARCA_DESC');
          //vm.modelos = loadALL(serviceResp.data[9], 'MO_MODELO');
          //$scope.modelos = vm.modelos;

          //vm.COLAB_ID = (vm.COLAB_ID === null ? serviceResp.data[3][0].COLAB_ID : vm.COLAB_ID);
          //vm.Cliente = loadALL(vm.clientess, 'CLIENT_DESC');
          //vm.Proyecto = loadALL(vm.proyectoss, 'PROY_DESC');
          //vm.Servicios = loadALL(vm.servicioss, 'SERV_DESC');
          //vm.Categorias = loadALL(vm.categoriass, 'CATE_DESC');

          //Cancel();
      };

      $scope.NuevoInventario = function (operacion, id) {
      
          var modalInstance = $uibModal.open({
              animation: true,
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              templateUrl: 'app/views/components/inventarios/modals/NuevoInventario.html',
              controller: 'NewInventario',
              size: 'lg',
              resolve: {
                  filtrosobtenidos: function () {
                      var array = [];
                      array.push(operacion);
                      array.push(id);
                      return array;
                  }
              }
          });

      };

      $scope.BuscarInventario = function (hostname, serie, ip, empresa, estado, admin) {

          if (empresa === undefined) {
              empresa = -1
          }

          if (estado === undefined) {
              estado = -1
          }

          if (admin === undefined) {
              admin = -1
          }

          if (ip === undefined) {
              ip = -1
          }

          if (serie === undefined) {
              serie = -1
          }

          if (hostname === undefined) {
              hostname = -1
          }

          var vista = empresa + "|" + estado + "|" + admin + "|" + hostname + "|" + serie + "|" + ip + "|" + $rootScope.useri.userid;
          var info = "";
          console.log("Inventario");
            var myArr1 = [vista, info];
            console.log(myArr1);
          var body = JSON.stringify({
              vista: vista,
              info: info
          });


          Apiinventario.getinventario(body).success(function (serviceResp) {
              console.log(serviceResp);
              console.log(vista);
              ngtablei(serviceResp);

          });

      }


      function Vista(id, vista, colab_id) {
          this.id = id;
          this.vista = vista;
          this.colab_id = colab_id;
      };
  });