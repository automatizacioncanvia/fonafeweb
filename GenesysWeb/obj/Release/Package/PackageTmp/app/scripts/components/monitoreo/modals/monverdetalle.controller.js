﻿/// <reference path="../../../../views/components/indicadores/modals/verstatus.html" />
/// <reference path="../../../../views/components/indicadores/modals/verstatus.html" />
/// <reference path="../../../../views/components/indicadores/modals/verstatus.html" />
angular.module('masterGenerysV1App').controller('ModalViewDetailMon', function ($uibModalInstance, $scope, filtrosobtenidos, $rootScope, $http, Apifonfe, Notification, $window, ngTableParams, $filter, $uibModal) {
    console.log($rootScope.useri.empresa_id)
    $scope.ticketdetail = filtrosobtenidos[0].data;
    $scope.totalTickets = filtrosobtenidos[0].data.length;
    var gettickets = $scope.ticketdetail;

    console.log(gettickets);

    $scope.close = function () {
        $uibModalInstance.close();

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.exportarexcel = function () {
        alasql('SELECT * INTO XLSX("Reporte.xlsx",{headers:true}) FROM ?', [$scope.ticketdetail]);
    };

    var ngtablei = function (array) {
        $scope.tableParams = new ngTableParams({
            page: 1,
            count: 7
        }, {
            total: array.length,
            getData: function ($defer, params) {
                var filteredData = params.filter() ?
                    $filter('filter')(array, params.filter()) :
                    array;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    array;

                params.total(orderedData.length);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

    };
    
    ngtablei(gettickets);

    $scope.openSD = function (ticket) {

        Apifonfe.linkticket(ticket).success(function (data) {
            var dati = data.split('#');

            var ind = dati[0].indexOf('CAisd');
            var final = dati[0].substring(0, ind);

            $window.open(final + dati[1], 'OpenTicket', 'width=1500,height=700');
        }).error(function (data) {
            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
        })
    }
 
});
