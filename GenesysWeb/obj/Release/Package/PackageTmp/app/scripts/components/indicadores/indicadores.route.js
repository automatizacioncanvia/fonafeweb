﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('indicadores', {
              parent: 'main',
              url: 'indicadores?idServicio&nombreServicio&idEmpresa&nombreEmpresa&tipo&atendido',
              views: {
                  'content@main': {
                      templateUrl: 'app/views/components/indicadores/indicadores.html',
                      controller: 'IndicadoresCtrl',
                      controllerAs: 'indicadores',
                      authorize: true

                  }
              }
          });
});