﻿angular.module('masterGenerysV1App')
.controller('NewUserCtrl', function ($uibModalInstance, $scope, Notification, data, Apidashboard, Apimenu, $rootScope, AD, Message, $window) {


    $scope.form = {};

    $scope.user = {};

    $scope.tab = 1;

    $scope.groupsf = [];
    $scope.groupsi = [];
    $scope.groupsc = [];

    $scope.isSelected = function (checktab) {
        return $scope.tab === checktab;
    };

    var today=  moment();

    $scope.form.expirationdate = today;

    $scope.options = { format: 'YYYY/MM/DD HH:mm', showClear: true, showTodayButton: true, minDate: today };

    $scope.ok = function () {

        $uibModalInstance.close();
    };

    var flowstatus = function (serviceResp) {
        var datai = JSON.parse(serviceResp.data);
    

        if (datai.executionSummary.status === 'COMPLETED') {


            if (datai.executionSummary.resultStatusType != 'ERROR') {

                if (datai.flowOutput.result !== undefined) {

                    var result = JSON.parse(datai.flowOutput.result);
                    switch (parseInt(result[0].status_id)) {
                        case 1: $scope.SID = result[0].data.SID;
                            $scope.tab = 2;
                            Message.result(3, "Usuario Creado");
                            $window.loading_screen.finish();
                            break;
                        case 2: Message.result(1, result[0].data);
                            $window.loading_screen.finish();
                            break;

                    }

                }

                else {

                    Message.result(1, "No se recibio resultado del servidor");
                    $window.loading_screen.finish();
                }

            } else {

                Message.result(1, datai.flowOutput.result);
                $window.loading_screen.finish();
            }

        }

        else {

            AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatus, fail);
        }

    }

    var usercreated = function (serviceResp) {
        var datai = JSON.parse(serviceResp.data);

        var idejecution = datai.executionId;

        AD.getFlowStatus(idejecution, 'execution-log').then(flowstatus, error)
       

    }

    var error = function (serviceResp) {

        $window.loading_screen.finish();

        Message.result(1, "Error con el servidor");

    }

    $scope.createuser = function (empresa_id, name, surname, samaname, password, ou_path, expirationdate, office, title, department, company, officephone, mobilephone, homephone) {

        var office = (office === undefined ? null : office);
        var title = (title === undefined ? null  : title);
        var department = (department === undefined ? null : department);
        var company = (company === undefined ? null : company);
        var homephone = (homephone === undefined ? null : homephone);
        var mobilephone = (mobilephone === undefined ? null : mobilephone);
        var officephone = (officephone === undefined ? null : officephone);

        var user = JSON.stringify({
            u_adid: null,
            o_id: 1,
            em_id: empresa_id,
            u_name: name,
            u_surname: surname,
            u_samaname: samaname,
            u_password: password,
            ou_path: ou_path,
            u_office: office,
            u_title: title,
            u_departament: department,
            u_company: company,
            u_homephone: homephone,
            u_mobilephone: mobilephone,
            u_officephone: officephone,
            u_enable: "1",
            u_accountexpirationdate: moment(expirationdate).format("YYYY-MM-DD HH:mm"),
            co_id: $rootScope.useri.userid,
            info:'-'
        })

  

       


        AD.operation(user).then(usercreated, error);
    

    $window.loading_screen = $window.pleaseWait({
        logo: "app/images/user.png",
        backgroundColor: 'rgba(42, 67, 94, 0.54)',
        loadingHtml: "<p class='loading-message'>Creando Usuario</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

    });
            
       


        
    }

    var menusdata = function (serviceResp) {
        $scope.unidades = serviceResp.data.data;
    }

    var fail = function (serviceResp) {
        Message.result(1, serviceResp.data);
    }

    var filesystem = function (serviceResp) {
        $scope.form.filesystem = undefined;
        $scope.gfilesystem = serviceResp.data.data;
    }

    var bd = function (serviceResp) {
        $scope.form.bg = undefined;
        $scope.gbd = serviceResp.data.data;

    }

    var internet = function (serviceResp) {
        $scope.form.internet = undefined;
        $scope.ginternet = serviceResp.data.data;
        


    }

    var citrix = function (serviceResp) {
        $scope.form.citrix = undefined;
        $scope.gcitrix = serviceResp.data.data;
      
    }
    
    var getou = function (empresa_id) {


        $scope.form.unidadorganizativa = undefined;
        Apimenu.getmenu(12, empresa_id).then(menusdata, fail);
     
    }

    var index = Apidashboard.indexOfespecial(data);
    if (index !== -1) {

        $scope.form.selectedEmpresa1 = data[index];
        getou(data[index].eM_ID)
    }

    $scope.empresas = data;

    $scope.onselectcompany = function(empresa_id) {

        if (empresa_id !== undefined) {

            getou(empresa_id);
            Apimenu.getmenu(13, empresa_id + ",1").then(filesystem, fail)
            Apimenu.getmenu(13, empresa_id + ",2").then(bd, fail);
            Apimenu.getmenu(13, empresa_id + ",3").then(internet, fail);
            Apimenu.getmenu(13, empresa_id + ",4").then(citrix, fail);
        }

    }

    $scope.selectTab = function (setTab) {

        $scope.tab = setTab;
    };

    $scope.isSelected = function (checktab) {
        return $scope.tab === checktab;
    };

    $scope.$watch('form.field', function () {
        if($scope.form.samaname!==undefined){
                $scope.form.samaname = $scope.form.samaname.toLowerCase().replace(/\s+/g, '');
        }
        
    });

    $scope.addgroud = function () {


        if (arrayObjectIndexOf($scope.groupsf, $scope.form.filesystem.g_ID, "g_ID") === -1) {


            $scope.groupsf.push({
                g_ID: $scope.form.filesystem.g_ID,
                g_NOMBRE: $scope.form.filesystem.g_NOMBRE,
                operacion: 9
            });


       
        } else {

            Message.result(1, "Ya exite el grupo");
        }

     



    }

    var arrayObjectIndexOf = function (myArray, searchTerm, property) {
      
        for (var i = 0, len = myArray.length; i < len; i++) {
           
            if (myArray[i][property] === searchTerm) return i;
        }
        return -1;
    }

    $scope.deletegroup = function (index) {

        $scope.groupsf.splice(index, 1);

      

    }

    $scope.addgroudinternet = function () {

        if ($scope.groupsi.length < 1) {

        
            $scope.groupsi.push({g_ID:$scope.form.internet.g_ID,
                                 g_NOMBRE: $scope.form.internet.g_NOMBRE,
                                 operacion: 9
            });



        } else {
             
            Message.result(1,"Solo Puede haber un grupo")
            
        }
    }

    $scope.deletegroupi = function (index) {
        $scope.groupsi.splice(index, 1);

    }

    $scope.addgroudctrix = function () {
        if ($scope.groupsc.length < 1) {

           //$scope.groupsc.push($scope.form.citrix);

            $scope.groupsc.push({
                g_ID: $scope.form.citrix.g_ID,
                g_NOMBRE: $scope.form.citrix.g_NOMBRE,
                operacion: 9
            });


        }
        else {
            Message.result(1, "Solo Puede haber un grupo");
        }


        $scope.deletegroupc = function (index) {

            $scope.groupsc.splice(index, 1);
        }
    }

    $scope.addpermision = function () {

     

        $scope.grouptotal = $scope.groupsc.concat($scope.groupsi, $scope.groupsf);

      

        var fullgroups = JSON.stringify({
            SID: $scope.SID,
            Groups: $scope.grouptotal,
            co_id: $rootScope.useri.userid

        })

        AD.groupOperation(fullgroups).then(groupcorrect,error)
        
        $window.loading_screen = $window.pleaseWait({
            logo: "app/images/user.png",
            backgroundColor: 'rgba(34, 204, 202, 0.43)',
            loadingHtml: "<p class='loading-message'>Agregando Permisos</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

        });



    }

    var flowstatus2 = function (serviceResp) {
        var datai = JSON.parse(serviceResp.data);
     

        if (datai.executionSummary.status === 'COMPLETED') {


           

            if (datai.executionSummary.resultStatusType != 'ERROR') {

                if (datai.flowOutput.result !== undefined) {

                    var result = JSON.parse(datai.flowOutput.result);
                    switch (parseInt(result[0].status_id)) {
                        case 1:
                            $scope.ok();
                            Message.result(3, "Permisos Agregados");
                            $window.loading_screen.finish();
                            break;
                        case 2: Message.result(1, result[0].data);
                            $window.loading_screen.finish();
                            break;

                    }

                }

                else {

                    Message.result(1, "No se recibio resultado del servidor");
                    $window.loading_screen.finish();
                }
            } else {

                Message.result(1, datai.flowOutput.result);
                $window.loading_screen.finish();

            }

        }

        else {

            AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatus2, fail);
        }

    }

    var groupcorrect = function (serviceResp) {

        var datai = JSON.parse(serviceResp.data);
        var idejecution = datai.executionId;
        AD.getFlowStatus(idejecution, 'execution-log').then(flowstatus2, error)

    }



});