﻿angular.module('masterGenerysV1App').config(function ($stateProvider) {
    $stateProvider
          .state('reportes', {
              parent: 'main',
              url: 'reportes?idServicio&nombreServicio&idEmpresa&nombreEmpresa&tipo&atendido',
              views:{
                  'content@main':{
                      templateUrl: 'app/views/components/reportes/reportes.html',
                      controller: 'ReportesCtrl',
                      controllerAs: 'reportes',
                      authorize: true
                                }
                    }
          });
});