﻿angular.module('masterGenerysV1App')
    .controller('InicioCtrl', function ($scope, $state, Apilink, $rootScope, Apimenu, $parse, Notification, Apifonfe, $window, Apidashboard, $sce, $stateParams) {

        Apimenu.getmenu(26, $rootScope.useri.userid).then(function (serviceResp) {
            $scope.empresas = serviceResp.data.data;
        });

        $scope.cargarDash = function (emId) {
            //console.log(emId.eM_ID);
            Apimenu.getmenu(30, $rootScope.useri.userid + "," + emId.eM_ID).then(function (serviceResp) {
                $scope.dash = serviceResp.data.data; console.log("Menu30");var myArr1 = [serviceResp.data.data, serviceResp.data];
                    console.log(myArr1);

            }, fail);
        };

        var fail = function (serviceResp) {

            Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Status: ' + serviceResp.status + '</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
        }



        $scope.openlink2 = function (seId, servicio) {

            $scope.tab = 1;

            var emId = $scope.form.selectedEmpresa1.eM_ID;
            $scope.EmpresaChange = true;
            $scope.idServicio = seId;
            $scope.nombreServicio = servicio;

            $scope.form.selectedEmpresa10 = { eM_ID: $scope.form.selectedEmpresa1.eM_ID, eM_NOMBRE: $scope.form.selectedEmpresa1.eM_NOMBRE };

            $scope.form.selectedEmpresa11 = { eM_ID: $scope.form.selectedEmpresa1.eM_ID, eM_NOMBRE: $scope.form.selectedEmpresa1.eM_NOMBRE };
            console.log(form.selectedEmpresa10);
            var fecha = new Date();
            $scope.form.years = fecha.getFullYear();

            if (servicio == "Incidentes" || servicio == "Requerimientos") {

                var vtipo;
                if (servicio == "Incidentes") {
                    vtipo = "I";
                } else {
                    vtipo = "R";
                }
                $state.go('indicadoresDash', { idServicio: $scope.idServicio, nombreServicio: $scope.nombreServicio, idEmpresa: $scope.form.selectedEmpresa1.eM_ID, nombreEmpresa: $scope.form.selectedEmpresa1.eM_NOMBRE, tipo: vtipo, atendido: 0 });

            } else {

                Apimenu.getmenu(23, 'R,Atendidos,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.REatendidosNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'R,Pendientes,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.REpendientesNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'I,Atendidos,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.INatendidosNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(23, 'I,Pendientes,' + seId + "," + emId).then(function (serviceResp) {
                    $scope.INpendientesNro = serviceResp.data.data[0].cantidad;
                }, fail);

                Apimenu.getmenu(24, seId + "," + emId).then(function (serviceResp) {
                    var emId = $scope.form.selectedEmpresa1.eM_ID;
                    var links = serviceResp.data.data;
                    var linksArreglo = [];
                    $scope.link4 = links;

                    angular.forEach(links, function (linki, key) {
                        //this.push(key + ': ' + value);
                        var body = JSON.stringify({
                            token: localStorage.getItem('Token').toString(),
                            url: linki.link,
                            idUsuario: emId
                        }
                        )

                        if (linki.flag == 0) {

                            Apifonfe.loginswrt(body).success(function (data) {
                                linksArreglo.push({
                                    orden: linki.orden,
                                    link: data, flag: linki.flag
                                });
                                debugger;
                            });
                        } else {

                            if (linki.link == 3) {
                                linksArreglo.push({
                                    orden: linki.orden,
                                    link: getAbsolutePath() + "app/views/components/dashboard/Capacidad.html?idEmpresa=" + emId,
                                    flag: 3
                                });

                            } else {
                                linksArreglo.push({
                                    orden: linki.orden,
                                    link: linki.link,
                                    flag: linki.flag
                                });
                            }

                        }


                    });


                    $scope.links = linksArreglo;

                }, fail);

                $("#theArt").hide();
                $("#panelDetalle").show();
            }
        }

 });