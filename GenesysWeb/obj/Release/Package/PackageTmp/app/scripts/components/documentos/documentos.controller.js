﻿
"use strict";
/**
 * @ngdoc function
 * @name masterGenerysV1App.controller:DocumentosCtrl
 * @description
 * # DocumentosCtrl
 * Controller of the masterGenerysV1Appp
 */
angular.module('masterGenerysV1App')
  .controller('DocumentosCtrl', function (Upload, $parse, $scope, $rootScope, $http, $timeout, Apimenu, SweetAlert, Apidocumento, ngTableParams) {
  	$scope.final = false;
  	$scope.init = function () {
  		// check if there is query in url
  		$http.get("api/getElementoControl/" + 'DocumentosCtrl' + ',' + $rootScope.useri.empresa_id).then(function (data) {
  			var letra;
  			var elemento;
  			var controles;
  			for (var i = 0; i < data.data.length; i++) {
  				letra = data.data[i].accion.substring(0, 1);
  				elemento = data.data[i].idValor + "_" + letra;
  				controles = $parse(elemento);
  				controles.assign($scope, String(data.data[i].valor) == "true");
  			}
  			$scope.final = true;
  		});
  	};

  	//$scope.spinner = {
  	//    active: true
  	//};

  	$scope.tab = 1;
  	$scope.selectTab = function (setTab) {

  		$scope.tab = setTab;
  	};
  	$scope.isSelected = function (checktab) {
  		return $scope.tab === checktab;
  	};

  	var vm2 = this;
  	vm2.myDate = new Date();
  	vm2.files = [];

  	vm2.upload = [];
  	vm2.UploadedFiles = [];
  	$scope.fechavisible = false;
  	$scope.submit = function () {
  		var empId = $rootScope.useri.empresa_id;
  		vm2.startUploading($scope.file);
  	};

  	//Carga de categorias al combo
  	var categoria = "0004";
  	Apidocumento.getbyfiltros(categoria).success(function (data) {
  		$scope.categoriaModal = data
  		$scope.categoria = data

  	});


  	Apimenu.getmenu(26, $rootScope.useri.userid).then(function (serviceResp) {
  	    $scope.empresas = serviceResp.data.data;
  	});

  	//$http.get("api/fogetbyempresa/").then(function (serviceResp) {

  	//	$scope.empresas = serviceResp.data;
  	//	console.log("api/fogetbyempresa/");
  	//});

  	$http.get("api/fogetbyalcance").then(function (serviceResp) {

  		$scope.alcances = serviceResp.data;
  		console.log("api/fogetbyalcance/");
  	});

  	$http.get("api/servicios").then(function (serviceResp) {
  		$scope.servicios = serviceResp.data;
  		$scope.servicios2 = serviceResp.data;
  		$scope.periodos = [{ value: 1, text: 'Mes Actual' },
        { value: 2, text: 'Ultimos 3 Meses' },
        { value: 3, text: 'Ultimos 6 Meses' },
        { value: 4, text: 'Ultimos 12 Meses' },
        { value: 5, text: 'Fechas' }
  		];
  		console.log("api/servicios/");
  	});

  	$scope.onSelected = function (empresa) {
  		if (empresa != undefined) {
  			$scope.getchart($scope.periodo, empresa.eM_ID);
  		}
  		else {
  			$scope.getchart($scope.periodo);
  		}
  	};

  	$scope.SeleccionarPeriodo = function (periodo) {
  	    if (periodo == 5) {
  	        $scope.fechavisible = true;
  	    } else {
  	        $scope.fechavisible = false;
  	    }
  	}

  	$scope.entrefechas = function (from, to, em, description, categoria, servicio, periodo)
  	{
  		var desde = moment(from).format("YYYYMMDD");
  		var hasta = moment(to).format("YYYYMMDD");
  		console.log("fechas")
  		console.log(periodo)

  		if (em === undefined) {
  			em = -1
  		}

  		if (categoria === undefined) {
  			categoria = -1
  		}

  		if (servicio === undefined) {
  			servicio = -1
  		}

  		if (description === undefined) {
  			description = "";
  		}

  		var vista = em + "," + servicio + "," + categoria + "," + description + "," + periodo;
  		var info = "" + desde + "," + hasta;
  		console.log(info);

  		var body = JSON.stringify({
  			vista: vista,
  			info: info
  		});

  		Apidocumento.getdash(body).success(function (data) {
  			$scope.tableParams = new ngTableParams({
  				page: 1, // show first page
  				count: 5
  				// count per page
  			}, {
  				total: data.length, // length of data
  				getData: function ($defer, params) {
  					$scope.data = data.slice((params.page() - 1) * params.count(), params.page() * params.count());
  					$defer.resolve($scope.data);
  				}
  			});
  		});
  	};
  	

  	function buscarDocumentos(em, nombre, categoria) {

  		var fecha = new Date();
  		var ayer = new Date(fecha.getTime() - 24 * 60 * 60 * 1000);

  		var desde = fecha.getFullYear() + '' + (fecha.getMonth() + 1) + '' + fecha.getDate();
  		var hasta = ayer.getFullYear() + '' + (ayer.getMonth() + 1) + '' + ayer.getDate();

  		var vista = "-1" + "," + "-1" + "," + "-1" + "," + nombre;
  		var info = "" + desde + "," + hasta;
  		var body = JSON.stringify({
  			vista: vista,
  			info: info
  		});

  		var nombreArchivo = nombre.split('.')[0];
  		debugger;

  		$http.get("api/DocumentoNombre/" + nombreArchivo).then(function (data) {
  			$scope.tableParams = new ngTableParams({
  				page: 1, // show first page
  				count: 5
  				// count per page
  			}, {
  				total: data.data.length, // length of data
  				getData: function ($defer, params) {
  					$scope.data = data.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
  					$defer.resolve($scope.data);
  				}
  			});
  		});
  	}

  	function startUploading($file) {
  	    //$scope.spinner.active = true;
  	    debugger;
  	    var desde2 = moment(vm2.myDate).format("DD/MM/YYYY");
  	    $("#panelLoad").show();
  	    //var nombreArchivo = $scope.modal.empresa + "_" + $scope.modal.servicios + "_" + $scope.modal.categoria + "_" + $scope.modal.nombre;// + ".pdf";
  	    var d = new Date();

  	    var n = $file.name.indexOf(".");
  	    var x = $file.name;  	    
  	    var extension = x.substr(n, x.length - n);

  	    console.log(extension);

  	    var nombreArchivo = $scope.modal.nombre   + extension;

  		$file = Upload.rename($file, nombreArchivo);

  		vm2.upload[0] = Upload.upload({
  			url: "./api/fileupload", // webapi url
  			method: "POST",
  			data: { file: $file }

  		}).progress(function (evt) {
  		}).success(function (data, status, headers, config) {
  			// file is uploaded successfully
  			if (data.error == '-1') {
  				vm2.UploadedFiles.push({ FileName: data.FileName, FilePath: data.LocalFilePath, FileLength: data.FileLength });
  				console.log(desde2);

  				var body = JSON.stringify({
  				    nombre: nombreArchivo,
  				    fecha: desde2,
  					em_id: $scope.modal.empresa,
  					se_id: $scope.modal.servicios,
  					codigo: $scope.modal.categoria,
  					usuario: $rootScope.useri.userid
  				});

  				Apidocumento.insertDocumento(body).success(function (d) {
  				    //$scope.spinner.active = false;
  				    $("#panelLoad").hide();
  					SweetAlert.swal("Agregado!", "Se agrego documento exitosamente.", "success");
  					$('#modalAgregarDocumento').modal('hide');

  					buscarDocumentos($scope.modal.empresa, nombreArchivo, $scope.modal.categoria);
  				}).error(function (d) {
  					$scope.error = d.error;
  					SweetAlert.swal("Error!", $scope.error, "error");
  				});
  			} else {
  				SweetAlert.swal("Error!", data.error, "error");
  			}


  		}).error(function (data, status, headers, config) {
  		    debugger;
  		    $("#panelLoad").hide();
  		    SweetAlert.swal("Error!", data.error, "error");
  		});

  	}

  	$scope.modalClear = function () {
  	    if ($scope.modal.empresa != undefined) $scope.modal.empresa = "";
  	    if ($scope.modal.servicios != undefined) $scope.modal.servicios = "";
  	    if ($scope.modal.categoria != undefined) $scope.modal.categoria = "";
  	    if ($scope.modal.nombre != undefined) $scope.modal.nombre = "";
  	    if ($scope.modal.fechaR != undefined) $scope.modal.fechaR = "";
  	    $scope.file = "";

  	    $("#modalAgregarDocumento .form-control").val("");

  	    return false;
  	}

  	function abortUpload(index) {
  		vm2.upload[index].abort();
  	}


  	//Download document
  	$scope.clickShowfile = function (archivo) {
  	   
  	    var body = JSON.stringify({
  	        nombre: archivo
  	    });

  	    //window.open("\\FNFPRTLFS01\Portal\Fonafe\Archivos\3_6_1_Reporte.PDF");

  	    //if (archivo.split("_").length > 0) {
  	    //    window.open("content/PDF/" + archivo.split("_")[3]);
  	    //} else {

  	        window.open("content/PDF/" + archivo);
  	    //}

  	    

  	    //Apidocumento.copiarDocumento(body).success(function (d) {
  	    //    window.open("content/PDF/" + archivo);

  	    //    var document = JSON.stringify({
  	    //        nombre: archivo,
  	    //        codigo: "EliminarDocumento"
  	    //    });

  	    //    //Apidocumento.deletebypath(document).success(function (data) {

  	    //    //}).error(function (d) {
  	    //    //    SweetAlert.swal("Error!", $scope.error, "error");
  	    //    //});

  	    //}).error(function (d) {
  	    //    SweetAlert.swal("Error!", $scope.error, "El FileServer no esta disponible");
  	    //});

  	};

  	//Delete document
  	$scope.clickEliminarDoc = function (archivo) {

  	    SweetAlert.swal({
  	        title: "Estás seguro?",
  	        text: "Se procederá  a eliminarlo del sistema!",
  	        type: "warning",
  	        showCancelButton: true,
  	        confirmButtonColor: "#DD6B55",
  	        confirmButtonText: "Si, continúa!",
  	        cancelButtonText: "No, retrocede!",
  	        closeOnConfirm: false,
  	        closeOnCancel: false
  	    },
         function (isConfirm) {
             if (isConfirm) {

                 var document = JSON.stringify({
                     nombre: archivo,
                     codigo: "DesactivarDocumento"
                 });
                 Apidocumento.deletebypath(document).success(function (data) {
                     SweetAlert.swal("Eliminado!", "Se elimino documento exitosamente.", "success");
                 }).error(function (d) {
                     $scope.error = d.error;
                     SweetAlert.swal("Error!", $scope.error, "error");
                 });
             } else {
                 SweetAlert.swal("Cancelado", "Operación cancelada", "error");
             }
         }
        );

  	};

  	//Set scope 
  	vm2.startUploading = startUploading;
  	vm2.abortUpload = abortUpload;

  });