﻿angular.module('masterGenerysV1App')
.controller('ChangePasswordCtrl', function ($scope, $mdDialog, user, $rootScope, AD, $window, Message) {

    $rootScope.zindexvalue = true;

    $scope.cancel = function () {
        $rootScope.zindexvalue = false;
        $mdDialog.cancel();
    };

    user.ul_changepasswordatlogon = false;


    $scope.checkchangepassword = function (value) {
        user.ul_changepasswordatlogon = !value;
    };

    $scope.change = function (ev) {
            var confirm = $mdDialog.confirm()
              .title('Cambiar Contraseña')
              .textContent('¿Esta Seguro que quiere cambiarle la contraseña al usuario?')
              .targetEvent(ev)
              .ok('Aceptar')
              .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {


              

                var office = (user.office === undefined || user.office === '' ? null : user.office);
                var title = (user.title === undefined || user.title === '' ? null : user.title);
                var department = (department === undefined || user.department === '' ? null : user.department);
                var company = (user.company === undefined || user.company === '' ? null : user.company);
                var homephone = (user.homephone === undefined || user.homephone === '' ? null : user.homephone);
                var mobilephone = (user.mobilephone === undefined || user.mobilephone === '' ? null : user.mobilephone);
                var officephone = (user.Officephone === undefined || user.Officephone === '' ? null : user.Officephone);
                var enable = (user.Enabled === true ? '1' : '0');
                var changepasswordatlogon = user.ul_changepasswordatlogon;

                var path = user.DistinguishedName;
                var iruta = path.indexOf("\\");

                if (iruta !== -1) {
                    user.DistinguishedName = user.DistinguishedName.replace("\\", "");
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")

                } else {
                    user.DistinguishedName = (user.DistinguishedName).replace("CN=" + user.cn + ",", "")
                }




                var user_password_change = JSON.stringify({
                    u_adid: user.SID,
                    o_id: 10,
                    em_id: null, 
                    u_name: user.givenname,
                    u_surname: user.surname,
                    u_samaname: user.SamAccountName,
                    u_password: $scope.password1,
                    ou_path: user.DistinguishedName,
                    u_office: office,
                    u_title: title,
                    u_departament: department,
                    u_company: company,
                    u_homephone: homephone,
                    u_mobilephone: mobilephone,
                    u_officephone: officephone,
                    u_enable: enable,
                    u_accountexpirationdate: user.AccountExpirationDate,
                    co_id: $rootScope.useri.userid,
                    info: '-',
                    ul_changepasswordatlogon:changepasswordatlogon
                })


            


                AD.operation(user_password_change).then(useroperation_changepassword, errorflows);


                $window.loading_screen = $window.pleaseWait({
                    logo: "app/images/user.png",
                    backgroundColor: 'rgba(42, 67, 94, 0.54)',
                    loadingHtml: "<p class='loading-message'>Cambiando Contraseña</p> <div class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>"

                });




            }, function () {
                $mdDialog.cancel();

            });
        };

    var useroperation_changepassword = function (serviceResp) {

     
        var datai = JSON.parse(serviceResp.data);
        var idejecution = datai.executionId;
        AD.getFlowStatus(idejecution, 'execution-log').then(flowstatuspassword, errorflows)




    }

    var flowstatuspassword = function (serviceResp) {

        var datai = JSON.parse(serviceResp.data);
      

        if (datai.executionSummary.status === 'COMPLETED') {

                                if (datai.executionSummary.resultStatusType != 'ERROR'){

                                                            if (datai.flowOutput.result !== undefined) {

                                                                var result = JSON.parse(datai.flowOutput.result);
                                                                switch (parseInt(result[0].status_id)) {
                                                                    case 1:
                                                                      // $scope.userad.splice($scope.id_delete, 1)
                                                                        Message.result(3, "Password Cambiada Exitosamente!");
                                                                        $window.loading_screen.finish();
                                                                        break;
                                                                    case 2: Message.result(1, result[0].data);
                                                                        $window.loading_screen.finish();
                                                                        break;

                                                                }

                                                            }

                                                            else {

                                                                Message.result(1, "No se recibio resultado del servidor");
                                                                $window.loading_screen.finish();
                                                            }
                                            } else {

                                                 
                                                    Message.result(1, datai.flowOutput.result);
                                                    $window.loading_screen.finish();
                                      

                                            }
                


                                }

        else {

            AD.getFlowStatus(datai.executionSummary.executionId, 'execution-log').then(flowstatuspassword, errorflows);
        }


    }

    var errorflows = function (serviceResp) {

        $window.loading_screen.finish();
        Message.result(1, serviceResp.data);


    }
    



})