﻿angular.module('masterGenerysV1App').service('Message', function (Notification) {


    this.result = function (status, message) {


        switch (status) {
            case 1:
                Notification.error({ message: ' <span class="pe-7s-attention" ></span><span><b>  Error! -</b>No se pudo procesar la peticion. Error: ' + message + '</span>', delay: 10000, positionY: 'bottom', positionX: 'right' });
                break;

            case 2:
                Notification.info({ message: ' <span class="pe-7s-info" ></span><span><b>  Atencion! -</b>  ' + message + '.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                break;
            case 3:
                Notification.success({ message: ' <span class="fa fa-check" ></span><span><b>  Success! -</b>  ' + message + '.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                break;
            case 4:
                Notification.warning({ message: ' <span class="fa fa-check" ></span><span><b>  Warning! -</b>  ' + message + '.</span>', delay: 4000, positionY: 'bottom', positionX: 'right' });
                break;


        }

    }








})