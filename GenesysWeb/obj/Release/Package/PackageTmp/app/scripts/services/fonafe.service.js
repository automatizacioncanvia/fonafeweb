﻿angular.module('masterGenerysV1App').factory('Apifonfe', function ($http) {

    
    var config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    return {

        login: function (data) {
            //console.log('entre');
            return $http.post('api/loginsd',data,config);
        },
        loginsw: function () {
            return $http.post('api/loginsw',config);
        },
        loginswrt: function (data) {
            //console.log('entre');
            return $http.post('api/loginswrt', data, config);
        },
        loginPortalCloud: function (data) {
            return $http.post('api/loginPortalCloud', data, config);
        },
        linkticket: function (ticket) {

            var token = localStorage.getItem('Token').toString();

            var data = JSON.stringify({
                ticket_id: ticket,
                token:token
            })

            return $http.post('api/getsdlink', data, config);

         
        }

    }

})