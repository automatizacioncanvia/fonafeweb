﻿angular.module('masterGenerysV1App').factory('Apidocumento', function ($http) {


    var config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    return {

        getdash: function (data) {
            //console.log('entre');
            return $http.post('api/documento', data, config);
        },
        getbyfiltros: function (data) {
            return $http.get("api/fogetbyfiltros/" + data);
        },

        deletebypath: function (data) {
            return $http.post("api/fodeletebypath", data, config);
        },
        insertDocumento: function (data){
            return $http.post("api/insertDocumento", data, config);
        },
        copiarDocumento: function (data){
            return $http.post("api/copiarDocumento/", data, config);
    }

    }

})