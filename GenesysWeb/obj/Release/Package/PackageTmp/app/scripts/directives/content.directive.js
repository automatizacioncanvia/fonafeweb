﻿/// <reference path="D:\FonafeGITProyecto\GenesysWeb\Index.html" />
angular.module('masterGenerysV1App')
.directive('contentItem', function ($compile) {

    var dateitem = '<sm-date-time-picker ng-model="content.model"'+
    'fname="{{content.name}}"'+
    'label="{{content.title}}"'+
    'flex="100"'+
    'flex-sm="100"'+
    'flex-xs="100"'+
    'format="YYYY/MM/DD"' +
    'mode="date"' +
    'week-start-day="Monday"' +
      '>'+
   '</sm-date-time-picker>';


    var textitem = '<md-input-container  style="width:100%;">' +
        '<label>{{content.title}}</label>' +
        '<input type="text" ng-model="content.model" md-maxlength="{{content.max}}" name="{{content.name}}" ng-requiered="false">' +
      '</md-input-container>';


    var intitem = '<md-input-container  style="width:100%;">' +
        '<label>{{content.title}}</label>' +
        '<input  ng-model="content.model" md-maxlength="{{content.max}}" name="{{content.name}}" ng-pattern="/^[0-9]{8}$/" max="{{content.max}}">' +
      '</md-input-container>';



    var getTemplate = function (contenType) {

        var template = '';

        switch (contenType) {

            case 'date':
                template = dateitem;
                break;
            case 'int':
                template = intitem;
                break;
            case 'text':
                template = textitem;
        }

        return template;
    }
    

    var linker = function (scope, element, attrs) {

        element.html(getTemplate(scope.content.content_type)).show();
        $compile(element.contents())(scope);
    }



    return {

        restrict: "E",
        link: linker,
        scope: {
            content: '='
        }
    };




});