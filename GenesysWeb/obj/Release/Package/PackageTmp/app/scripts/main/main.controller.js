﻿angular.module('masterGenerysV1App')
.controller('MainCtrl', function ($scope, Auth, $rootScope, Apimenu) {

    $scope.navi = true;
    $rootScope.zindexvalue = false;

    $scope.open = function () {
        //var myEl = angular.element(document.querySelector('html'));
        //myEl.addClass('nav-open');
    }

    $scope.colapsar = function () {

        $('#minimizeSidebar').click();
    }

})


angular.module('masterGenerysV1App').controller('optionsharepoint', function ($scope, Auth, $rootScope) {


    $scope.CambiarUsuario = function () {

        $rootScope.valueauth = false;
        Auth.logout();
    };

    $scope.CerrarSesion = function () {
     
        $rootScope.valueauth = false;
        Auth.logout();
    };

    $scope.DescargarManual = function (archivo) {
 
        var body = JSON.stringify({
            nombre: archivo
        }); 

        window.open("content/PDF/" + archivo);

       

    };

});


function tokenInterceptor() {
    return {
        'request': function (config) {

            var token = localStorage.getItem('Token');
            if (token) {
                config.headers['Authorization'] = /*'Bearer ' */ +token;
            }
            return config;
        }
    };
}

function interceptors($httpProvider) {
    $httpProvider.interceptors.push('tokenInterceptor');
}

interceptors.$inject = ['$httpProvider'];

angular.module('masterGenerysV1App')
    .factory('tokenInterceptor', tokenInterceptor)
    .config(interceptors);


angular.module('masterGenerysV1App').controller('MyCtrl', function ($scope, $location, $rootScope) {

    $scope.isActive = function (route) {
        return route === $location.path();
    };

    $scope.valor = false;

    $scope.collapse = function () {

        if ($scope.valor === false) {

            $scope.valor = true;
        }
        else {

            $scope.valor = false;
        }
    };



});


angular.module('masterGenerysV1App').filter('shpdatefilter', function () {
    return function (string) {
        if (!angular.isString(string)) {
            return string;
        }
        var result = new Date(parseInt(string.substr(6)));
        //moment(datefrom).format("YYYY-MM-DDTHH:mm");
        var resulti = moment(result).format("YYYY-MM-DD HH:mm");

        return resulti;
    };

});


angular.module('masterGenerysV1App').directive('usSpinner', ['$http', '$rootScope', function ($http, $rootScope) {
    return {
        link: function (scope, elm, attrs) {
            $rootScope.spinnerActive = false;
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (loading) {
                $rootScope.spinnerActive = loading;
                if (loading) {
                    elm.removeClass('ng-hide');
                } else {
                    elm.addClass('ng-hide');
                }
            });
        }
    };

}]);

