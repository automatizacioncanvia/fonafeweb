﻿angular.module('masterGenerysV1App').controller('LoginController', FuncionPrincipal)

function FuncionPrincipal($http, $scope, Auth, $window, $location, $rootScope, Apimenu, $state) {


    Apimenu.getmenu(4, '0').success(function (data) {

        $scope.testAccounts = data.data;
        $scope.testAccounts.selected = { dO_ID: 1, dO_NOMBRE: 'GMDSA' };
        //console.log('Ok - Dominios Cargados');


    }).error(function (data) {
        //console.log('Error - Capturar Dominios');
    })

    //Cargando
    $scope.loader = {
        loading: false,
    };

    //Loguearse
    $scope.Loguearse = function (usuario, contrasena) {

  
        var value = usuario.indexOf('@');
        var usua = usuario.substring(0, value);
        var domi = usuario.substring(value+1);


       // var usua = $scope.usuario;
        var contra = $scope.contrasena;
        //var domi = $scope.testAccounts.selected.dO_NOMBRE;
        var condi = false;

        //Loading-Icon
        $scope.loader.loading = true;
        //...

        var data = {
            dominio: domi,
            usuario: usua,
            contrasena: contra
        };

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        var body = JSON.stringify(data);

        Auth.login(body, config).success(function (data) {

            $scope.PostDataResponse = data;
  
            if (data.otherDetails === "true") {

                localStorage.setItem("Perfiles", data.data);
                localStorage.setItem("Token", data.token);
                localStorage.setItem("Respuesta", data.otherDetails);
                localStorage.setItem("Usuario", data.usuario);

                $scope.loader.loading = true;
                $scope.condi = true;
                //console.log('Ok - Logueo');
                //console.log('Ok - Perfiles-Token-Respuesta-Usuario en LocalStorage');
                $state.go('dashboard');
                event.preventDefault();


            }
             
            else {
                $scope.loader.loading = false;
                $scope.condi = false;
                $scope.PostDataResponse = data;
                //console.log('Error - Logueo');
            }

        }).error(function (data) {
            //console.log('Error - Data-Web-Service');
        })
    };
}
