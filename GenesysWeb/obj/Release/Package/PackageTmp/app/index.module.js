﻿
'use strict';

/**
 * @ngdoc overviewW
 * @name masterGenerysV1App
 * @description
 * # masterGenerysV1App
 *
 * Main module of the application.
 */
angular
  .module('masterGenerysV1App', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'angularUtils.directives.dirPagination',
    '720kb.tooltips',
    'oitozero.ngSweetAlert',
    'chart.js',
    'ae-datetimepicker',
    'ui.select',
    'ngTable',
    'ui.bootstrap',
    'angularSpinner',
    'ngHandsontable',
    'angular.backtop',
    'slick',
    'ngFileUpload',
    'ui-notification',
    'ngMaterial',
    'ngLoader',
    'ngInput',
    'smDateTimeRangePicker',
    'uiSwitch',
    'nickel.minStrength'
  ])


angular.module('masterGenerysV1App').run(function ($rootScope, $state, Auth, $location, Apimenu) {
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
  
        $rootScope.stati = toState.name;

        if (Auth.islogin()) {

            var result = Auth.setUserinfo();

            Apimenu.getmenu(1, $rootScope.useri.userid.toString()).success(function (data) {
                $rootScope.menuItems = data.data;
                debugger;
                if (toState.name === 'dashExtension' || toState.name === 'dashTiempo' || toState.name === 'dashHorasap' || toState.name === 'activedirectory' || toState.name === 'indicadoresDash' || toState.name === 'dashTiempopromediosap' || toState.name === 'aprovisionamiento') {
                    $state.go(toState.url);
                } else
                {
                    var oli = Apimenu.validroute(toState.name);
                    console.log(oli);
                    if (Apimenu.validroute(toState.name)) {
                        console.log("entro primero")
                        $state.go(toState.name);
                    } else {
                        $state.go($rootScope.menuItems[0]["path"]);
                    }
                }
            });
        }
        else {

            console.log("entro a login");
            $location.path("login");
            

        }

    });

});