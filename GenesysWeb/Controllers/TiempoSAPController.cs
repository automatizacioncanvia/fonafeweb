﻿using GenesysUtil;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GenesysWeb.Controllers
{
    public class TiempoSAPController : ApiController
    {

        [Route("api/fogettiemposap/{valor}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetByNameTotal(String valor)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);

                List<string> parametros = new List<string>();
                parametros.Add("@VALOR");

                using (DataTable ds = SqlConector.getDataTable("FO_GET_TIEMPORESP_SAP_DETALLE", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {

                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

    }





}

