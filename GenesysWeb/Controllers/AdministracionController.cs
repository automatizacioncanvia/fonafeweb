﻿using GenesysUtil;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GenesysBL;

namespace GenesysWeb.Controllers
{
    public class AdministracionController : ApiController
    {



        [Route("api/tablasMaestras")]
        [HttpGet]
        public HttpResponseMessage GetTablasMaestras()
        {
            try
            {
                HttpResponseMessage response = null;

                UtilBL obj_DocumentoBL = new UtilBL();
                DataTable dt = obj_DocumentoBL.GetTablas();
                response = Request.CreateResponse(HttpStatusCode.OK, dt);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }

        }

        [Route("api/getElementoControl/{parametro}")]
        [HttpGet]
        public HttpResponseMessage GetElementoControl(String parametro)
        {
            try
            {
                HttpResponseMessage response = null;
                AdministracionBL obj_DocumentoBL = new AdministracionBL();
                DataTable dt = obj_DocumentoBL.GetElementoControl(parametro);
                response = Request.CreateResponse(HttpStatusCode.OK, dt);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }
        }

        [Route("api/fogetbyname/{name}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetByName(String name) 
        {
            if (name != null || name != "")
            {
                List<string> list = new List<string>(); 
                list.Add(name); 

                List<string> parametros = new List<string>();
                parametros.Add("@NAME");

                using (DataTable ds = SqlConector.getDataTable("FO_GETINFOCOLAB", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        } 




        [Route("api/fogetbynametotal/{idcolab}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetByNameTotal(String idcolab)
        {
            if (idcolab != null || idcolab != "")
            {
                List<string> list = new List<string>(); 
                list.Add(idcolab);

                List<string> parametros = new List<string>();
                parametros.Add("@ID");

                using (DataTable ds = SqlConector.getDataTable("FO_GETINFOCOLABTOTAL", list, parametros))
                {   
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {

                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }


        [Route("api/fogetserviciobyempresa/{idempresa}")]
        [HttpGet]
        public HttpResponseMessage GetServicioByEmpresa(String idempresa)
        {
            if (idempresa != null || idempresa != "")
            {
                List<string> list = new List<string>();
                list.Add(idempresa);

                List<string> parametros = new List<string>();
                parametros.Add("@EM_ID");

                using (DataTable ds = SqlConector.getDataTable("FO_GETSERVICIOBYEMPRESA", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }



        [Route("api/fogetlinkbyempresaservicio/{valor}")]
        [HttpGet]
        public HttpResponseMessage GetServicioByEmpresaServicio(String valor)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);

                List<string> parametros = new List<string>();
                parametros.Add("@VALOR");

                using (DataTable ds = SqlConector.getDataTable("FO_GETLINKBYEMPRESASERVICIO", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }



        [Route("api/fogetcolaboradorbyempresa/{idempresa}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetByColabArea(String idempresa)
        {
            if (idempresa != null || idempresa != "")
            {
                List<string> list = new List<string>(); 
                list.Add(idempresa); 

                List<string> parametros = new List<string>();
                parametros.Add("@IDEMPRESA");

                using (DataTable ds = SqlConector.getDataTable("FO_GETCOLABORADORBYEMPRESA", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }


        [Route("api/fogetbynamefiltro/{valor}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetByNameFiltro(String valor) 
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>(); 
                list.Add(valor); 

                List<string> parametros = new List<string>();
                parametros.Add("@VALOR");

                using (DataTable ds = SqlConector.getDataTable("FO_GETBYFILTRONAME", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }
  

        [Route("api/foupdatecolaborador/{id?}")]
        [HttpPut]
        public HttpResponseMessage PUTCOLABORADOR(int? id, GenesysBE.focolaborador e)
        {
            if (e != null)
            {
               
                string store = "FO_UPDATECOLABORADOR";
                string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
                SqlConnection myConnection = new SqlConnection(unbrowse);
                SqlCommand sqlCmd = new SqlCommand(store, myConnection);
                sqlCmd.Connection = myConnection;
                myConnection.Open();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@ID", id);
                sqlCmd.Parameters.AddWithValue("@NOMBRE", e.Nombre);
                sqlCmd.Parameters.AddWithValue("@APELLIDO", e.Apellido);
                sqlCmd.Parameters.AddWithValue("@EMPRESA", e.Empresa);
                sqlCmd.Parameters.AddWithValue("@PERFIL", e.Perfil);
                sqlCmd.Parameters.AddWithValue("@ALCANCE", e.Alcance);
                sqlCmd.Parameters.AddWithValue("@CUENTA", e.Cuenta);
                sqlCmd.Parameters.AddWithValue("@EMAIL", e.Email);
                sqlCmd.Parameters.AddWithValue("@DOMINIO", e.Dominio);
                sqlCmd.Parameters.AddWithValue("@CARGO", e.Cargo);
                sqlCmd.Parameters.AddWithValue("@DNI", e.Dni);
                sqlCmd.Parameters.AddWithValue("@FIJO", e.Fijo);
                sqlCmd.Parameters.AddWithValue("@ANEXO", e.Anexo);
                sqlCmd.Parameters.AddWithValue("@CELULAR", e.Celular);
                sqlCmd.Parameters.AddWithValue("@IDUSER", e.Iduser);

                sqlCmd.ExecuteNonQuery();
                myConnection.Close();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, e);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }





        [Route("api/foupdateestadocolaborador/{id:int}")]
        [HttpPost]
        public HttpResponseMessage POSTESTADOCOLABORADOR(int id)
        {


            string store = "FO_UPDATEESTADOCOLABORADOR";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@IDCOLAB", id);
            sqlCmd.ExecuteNonQuery();
            myConnection.Close();

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "eliminado");
            return response;

        }


        [Route("api/foupdatelink/{id?}")]
        [HttpPut]
        public HttpResponseMessage PUTLINK(int? id, GenesysBE.Link e)
        {
            if (e != null)
            {

                string store = "FO_UPDATELINK";
                string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
                SqlConnection myConnection = new SqlConnection(unbrowse);
                SqlCommand sqlCmd = new SqlCommand(store, myConnection);
                sqlCmd.Connection = myConnection;
                myConnection.Open();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@ID", id);
                sqlCmd.Parameters.AddWithValue("@NOMBRE", e.Nombre);
                sqlCmd.Parameters.AddWithValue("@DESCRIPCION", e.Descripcion);
                sqlCmd.Parameters.AddWithValue("@IDUSER", e.Iduser);


                sqlCmd.ExecuteNonQuery();
                myConnection.Close();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, e);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/foupdateestadolink/{id:int}")]
        [HttpPost]
        public HttpResponseMessage POSTESTADOLINK(int id)
        {


            string store = "FO_UPDATEESTADOLINK";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@IDLINK", id);
            sqlCmd.ExecuteNonQuery();
            myConnection.Close();

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "eliminado");
            return response;

        }




        //iNSERT COLABORADOR
        [Route("api/foinsertcolaborador")]
        [HttpPost]
        public HttpResponseMessage PostColaborador(GenesysBE.focolaborador e)
        {

            string result = "";


          
            string store = "FO_INSERTCOLABORADOR";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.AddWithValue("@NOMBRE", e.Nombre);
            sqlCmd.Parameters.AddWithValue("@APELLIDO", e.Apellido);
            sqlCmd.Parameters.AddWithValue("@EMPRESA", e.Empresa);
            sqlCmd.Parameters.AddWithValue("@PERFIL", e.Perfil);
            sqlCmd.Parameters.AddWithValue("@ALCANCE", e.Alcance); 
            sqlCmd.Parameters.AddWithValue("@CUENTA", e.Cuenta);
            sqlCmd.Parameters.AddWithValue("@EMAIL", e.Email);
            sqlCmd.Parameters.AddWithValue("@DOMINIO", e.Dominio);
            sqlCmd.Parameters.AddWithValue("@CARGO", e.Cargo);
            sqlCmd.Parameters.AddWithValue("@DNI", e.Dni);
            sqlCmd.Parameters.AddWithValue("@FIJO", e.Fijo);
            sqlCmd.Parameters.AddWithValue("@ANEXO", e.Anexo);
            sqlCmd.Parameters.AddWithValue("@CELULAR", e.Celular);
            sqlCmd.Parameters.AddWithValue("@IDUSER", e.Iduser);



            object o = sqlCmd.ExecuteScalar();
            if (o != null)
            {
                result = o.ToString();

            }
            myConnection.Close();
            if (result == "REGISTRO EXITOSO")
            {

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, result);

            }
        }


        [Route("api/foinsertlink")]
        [HttpPost]
        public HttpResponseMessage PostLink(GenesysBE.Link e)
        {

            string result = "";



            string store = "FO_INSERTLINK";
            string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(unbrowse);
            SqlCommand sqlCmd = new SqlCommand(store, myConnection);
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.AddWithValue("@NOMBRE", e.Nombre);
            sqlCmd.Parameters.AddWithValue("@DESCRIPCION", e.Descripcion);
            sqlCmd.Parameters.AddWithValue("@EMPRESA", e.Idempresa);
            sqlCmd.Parameters.AddWithValue("@SERVICIO", e.Idservicio);
            sqlCmd.Parameters.AddWithValue("@IDUSER", e.Iduser);
           



            object o = sqlCmd.ExecuteScalar();
            if (o != null)
            {
                result = o.ToString();

            }
            myConnection.Close();
            if (result == "REGISTRO EXITOSO")
            {

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, result);

            }
        }


        [Route("api/fogetbyfiltros/{idcategoria}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetFiltros(String idcategoria)
        {
            List<string> list = new List<string>();
            list.Add(idcategoria);

            List<string> parametros = new List<string>();
            parametros.Add("@pCodigo");
            using (DataTable ds = SqlConector.getDataTable("FO_GETFILTROS", list, parametros))
            {
                list.Clear();
                parametros.Clear();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                return response;
            }
        }

        





    [Route("api/fogetbydominio/")]
        [HttpGet]
        public HttpResponseMessage GetDataSetDominio()
        {
            using (DataTable ds = SqlConector.getDataTable("FO_GETINFODOMINIO"))
            {
                return Request.CreateResponse(HttpStatusCode.OK, ds);
            }
        }

        [Route("api/fogetbyalcance/")]
        [HttpGet]
        public HttpResponseMessage GetDataSetAlcance()
        {
            using (DataTable ds = SqlConector.getDataTable("FO_GETINFOALCANCE"))
            {
                return Request.CreateResponse(HttpStatusCode.OK, ds);
            }
        }


        [Route("api/fogetbyempresa/")]
        [HttpGet]
        public HttpResponseMessage GetDataSetEmpresa()
        {
            using (DataTable ds = SqlConector.getDataTable("FO_GETINFOEMPRESA"))
            {
                return Request.CreateResponse(HttpStatusCode.OK, ds);
            }
        }

        [Route("api/fogetbyperfil/")]
        [HttpGet]
        public HttpResponseMessage GetDataSetPerfil()
        {
            using (DataTable ds = SqlConector.getDataTable("FO_GETINFOPERFIL"))
            {
                return Request.CreateResponse(HttpStatusCode.OK, ds);
            }
        }

        [Route("api/fogetbycargo")]
        [HttpGet]
        public HttpResponseMessage GetDataSetCargo()
        {
            using (DataTable ds = SqlConector.getDataTable("FO_GETINFOCARGO"))
            {
                return Request.CreateResponse(HttpStatusCode.OK, ds);
            }
        }


        [Route("api/fogetperfilbyempresa/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetPerfilByEmpresa(String id) 
        {
            if (id != null || id != "")
            {
                List<string> list = new List<string>(); 
                list.Add(id);

                List<string> parametros = new List<string>();
                parametros.Add("@EM_ID");
                using (DataTable ds = SqlConector.getDataTable("FO_GETPERFILBYEMPRESA", list, parametros))
                {             
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }






       




    }
}
