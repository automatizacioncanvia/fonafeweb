﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GenesysBL;

namespace GenesysWeb.Controllers
{
    public class EmpresasController : ApiController
    {
        [Route("api/GetEmpresaId/{EM_ID:int}")]
        [HttpGet]
        public HttpResponseMessage GetEmpresaId(int EM_ID)
        {
            GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
            DataTable dt = obj_GenesysBL.GetEmpresaId(EM_ID);
            return Request.CreateResponse(HttpStatusCode.OK, dt);
        }

      

    }
}
