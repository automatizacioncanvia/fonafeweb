﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using GenesysBL;
using System.Net.Http;
using System.Web.Http;
using GenesysBE;
using System.IO;
using System.Web;
using System.Data.OleDb;//Agregado
using System.Configuration;//Agregado
using System.Data.SqlClient;//Agregado
using GenesysUtil;

namespace GenesysWeb.Controllers
{
    public class DocumentoController : ApiController
    {

        private readonly string workingFolder = HttpRuntime.AppDomainAppPath + @"\content\PDF";

        [Route("api/DocumentoNombre/{nombre}")]
        [HttpGet]
        public HttpResponseMessage GetBusquedaNombre(string nombre)
        {
            try
            {
                    HttpResponseMessage response = null;
                    Format e = new Format();

                    DateTime Hoy = DateTime.Today;
                    string fecha_actual = Hoy.ToString("yyyyMMdd");
                    DateTime ayer = DateTime.Now.AddDays(-1);
                    string fecha_ayer=ayer.ToString("yyyyMMdd");

                    e.info = fecha_ayer+"," + fecha_actual;
                    e.vista = "-1,-1,-1," + nombre;

                    DocumentoBL obj_DocumentoBL = new DocumentoBL();
                    DataTable dt = obj_DocumentoBL.Getdocumento(e);
                    response = Request.CreateResponse(HttpStatusCode.OK, dt);
                    return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }
        }

        [Route("api/documento")]
        [HttpPost]
        public HttpResponseMessage Getdocument(GenesysBE.Format e)
        {
            try
            {
                HttpResponseMessage response = null;
                if (e != null)
                {
                    DocumentoBL obj_DocumentoBL = new DocumentoBL();
                    DataTable dt = obj_DocumentoBL.Getdocumento(e);
                    response = Request.CreateResponse(HttpStatusCode.OK, dt);
                    return response;
                }
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }

        }




        [Route("api/servicios")]
        [HttpGet]
        public HttpResponseMessage GetServicios()
        {
            try
            {
                HttpResponseMessage response = null;
              
                    UtilBL obj_DocumentoBL = new UtilBL();
                    DataTable dt = obj_DocumentoBL.GetServicios();
                    response = Request.CreateResponse(HttpStatusCode.OK, dt);
                    return response;
                
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }

        }


        [Route("api/descargarDocumento")]
        [HttpPost]
        public HttpResponseMessage DescargarDocumento(Documento documento) {
            HttpResponseMessage response = null;

            string destiPath = @workingFolder;
            string destiFile = Path.Combine(destiPath, documento.nombre);

            System.IO.FileInfo toDownload =
                    new System.IO.FileInfo(destiFile);

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition",
                       "attachment; filename=" + toDownload.Name);
            HttpContext.Current.Response.AddHeader("Content-Length",
                       toDownload.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(destiFile);
            HttpContext.Current.Response.End();

            return response;

        }



        [Route("api/copiarDocumento")]
        [HttpPost]
        public HttpResponseMessage CopiarDocumento(Documento documento)
        {
            HttpResponseMessage response = null;

            string[] cadena = documento.nombre.Split('_');

            UtilBL obj = new UtilBL();
            DataTable dt = new DataTable();
            dt = obj.GetFiltros("0005");

            string nombreRuta = "";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FI_NOMBRE"].ToString() == "RutaArchivo")
                {
                    nombreRuta = dt.Rows[i]["FI_VALOR"].ToString();
                }
            }

            string SourcePath = Path.Combine(@nombreRuta, cadena[0], cadena[1], cadena[2]);
            string sourceFile = Path.Combine(SourcePath, documento.nombre);

            string destiPath = @workingFolder;
            string destiFile = Path.Combine(destiPath, documento.nombre);

            if (!File.Exists(destiFile))
            {
                File.Copy(sourceFile, destiFile, true);
            }

            //string pathCopy = "Uploads/" + documento.nombre;

            //System.IO.FileInfo toDownload =
            //    new System.IO.FileInfo(HttpContext.Current.Server.MapPath(pathCopy));

            //HttpContext.Current.Response.Clear();
            //HttpContext.Current.Response.AddHeader("Content-Disposition",
            //           "attachment; filename=" + toDownload.Name);
            //HttpContext.Current.Response.AddHeader("Content-Length",
            //           toDownload.Length.ToString());
            //HttpContext.Current.Response.ContentType = "application/octet-stream";
            //HttpContext.Current.Response.WriteFile(pathCopy);
            //HttpContext.Current.Response.End();

            response = Request.CreateResponse(HttpStatusCode.OK);

            return response;
        }

        static public void Download(string patch)
        {
            System.IO.FileInfo toDownload =
                       new System.IO.FileInfo(HttpContext.Current.Server.MapPath(patch));

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition",
                       "attachment; filename=" + toDownload.Name);
            HttpContext.Current.Response.AddHeader("Content-Length",
                       toDownload.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(patch);
            HttpContext.Current.Response.End();
        }

        [Route("api/fodeletebypath")]
        [HttpPost]
        public HttpResponseMessage DeleteDocument(Documento documento)
        {
            UtilBL obj = new UtilBL();
            DataTable dt = new DataTable();
            dt = obj.GetFiltros("0005");

            string nombreRuta = "";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FI_NOMBRE"].ToString() == "RutaArchivo")
                {
                    nombreRuta = dt.Rows[i]["FI_VALOR"].ToString();
                }
            }

            HttpResponseMessage response = null;

            string fileName = documento.nombre;
            
            string opcion = documento.codigo;

            if (opcion == "EliminarDocumento")
            {
                string archivo= Path.Combine(@workingFolder, fileName);

                if ((System.IO.File.Exists(archivo)))
                {
                    System.IO.File.Delete(archivo);
                }
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else {
                //cortar archivo en fileserver y copiar a una carpeta eliminados

                string[] cadena = fileName.Split('_');

                string SourcePath = Path.Combine(@nombreRuta, cadena[0], cadena[1], cadena[2]);
                string sourceFile = Path.Combine(SourcePath, fileName);

                string targetPath = Path.Combine(@nombreRuta, "Eliminados", cadena[0], cadena[1], cadena[2]);

                string archivo = "";
                archivo = Path.Combine(targetPath, fileName);

                if (archivo != null || archivo != string.Empty)
                {
                    DocumentoBL obj_DocumentoBL = new DocumentoBL();

                    string result;
                    result = obj_DocumentoBL.eliminarDocumento(fileName);

                    if (result == "ACTUALIZACION EXITOSO")
                    {
                        if (!Directory.Exists(targetPath))
                        {
                            Directory.CreateDirectory(targetPath);
                        }

                        if (!File.Exists(archivo))
                        {
                            File.Copy(sourceFile, archivo, true);
                        }

                        if ((System.IO.File.Exists(sourceFile)))
                        {
                            System.IO.File.Delete(sourceFile);
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.NotFound);
                    }
                }
            }

            return response;
        }

        [Route("api/insertDocumento")]
        [HttpPost]
        public HttpResponseMessage InsertDocumento(Documento documento)
        {

            try
            {
                HttpResponseMessage response = null;
                if (documento != null)
                {
                    DocumentoBL obj_DocumentoBL = new DocumentoBL();

                    string result;
                    result = obj_DocumentoBL.InsertarDocumento(documento);
                    
                    if (result == "REGISTRO EXITOSO")
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else {
                        response = Request.CreateResponse(HttpStatusCode.NotFound);
                    }
                        
                    return response;
                }
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }


        }


        //Web-Service de Carga de Archivos .csv a DB Sql Server 2008
        [Route("api/cargararchivocsv")]
        [HttpPost]
        public HttpResponseMessage CargarArchivoCsv()
        {
            HttpResponseMessage response = null;

            try
            {
                string fileName = "st03_dialog_112016";
                string targetPath = @"\\10.0.5.24\103039 - asop_iso-gmd-mejora continua\Proyectos\Fonafe\SAP\";
                string targetPathComplete = targetPath + fileName;
                
                OleDbConnection OleDbcon = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + targetPathComplete + ";Extended Properties=Excel 12.0;");

                OleDbCommand cmd = new OleDbCommand("SELECT * FROM ["+fileName+"$]", OleDbcon);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(cmd);

                DataSet ds = new DataSet();
                objAdapter.Fill(ds);
                DataTable dt = ds.Tables[0];

                //Agregar a una Tabla temporal o Física en SQL Server 2008--------------------
                string unbrowse = ConfigurationManager.ConnectionStrings["connBDFonafeDev"].ConnectionString;
                string SP = "FO_GETDATADASHBOARD_SAP";
                SqlConnection myConnection = new SqlConnection(unbrowse);
                SqlCommand sqlCmd = new SqlCommand(SP, myConnection);
                sqlCmd.Connection = myConnection;
                myConnection.Open();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Details", dt);

                SqlDataAdapter adapta = new SqlDataAdapter(sqlCmd);
                DataTable tabla = new DataTable();
                adapta.Fill(tabla);
                //-----------------------------------------------------------------------------
                

                response = Request.CreateResponse(HttpStatusCode.OK, tabla);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }
            return response;
        }


        }
}