﻿
using GenesysWeb.Extensions;
using GenesysWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using GenesysBL;
using System.Data;

namespace GenesysWeb.Controllers
{
    public class FileUploadController : ApiController
    {
        private readonly string workingFolder = HttpRuntime.AppDomainAppPath + @"\content\PDF";

        //public async Task<IHttpActionResult> Get()
        //{
        //    var photos = new List<PhotoViewModel>();

        //    var photoFolder = new DirectoryInfo(workingFolder);

        //    await Task.Factory.StartNew(() =>
        //    {
        //        photos = photoFolder.EnumerateFiles()
        //          .Where(fi => new[] { ".jpg", ".bmp", ".png", ".gif", ".tiff" }
        //            .Contains(fi.Extension.ToLower()))
        //          .Select(fi => new PhotoViewModel
        //          {
        //              Name = fi.Name,
        //              Created = fi.CreationTime,
        //              Modified = fi.LastWriteTime,
        //              Size = fi.Length / 1024
        //          })
        //          .ToList();
        //    });

        //    return Ok(new { Photos = photos });
        //}

        ///// <summary>
        /////   Delete photo
        ///// </summary>
        ///// <param name="fileName"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //public async Task<IHttpActionResult> Delete(string fileName)
        //{
        //    if (!FileExists(fileName))
        //    {
        //        return NotFound();
        //    }

        //    try
        //    {
        //        var filePath = Directory.GetFiles(workingFolder, fileName)
        //          .FirstOrDefault();

        //        await Task.Factory.StartNew(() =>
        //        {
        //            if (filePath != null)
        //                File.Delete(filePath);
        //        });

        //        var result = new PhotoActionResult
        //        {
        //            Successful = true,
        //            Message = fileName + "deleted successfully"
        //        };
        //        return Ok(new { message = result.Message });
        //    }
        //    catch (Exception ex)
        //    {
        //        var result = new PhotoActionResult
        //        {
        //            Successful = false,
        //            Message = "error deleting fileName " + ex.GetBaseException().Message
        //        };
        //        return BadRequest(result.Message);
        //    }
        //}


        /// <summary>
        ///   Add a photo
        /// </summary>
        /// <returns></returns>
        /// 

        [MimeMultipart]
        public async Task<FileUploadResult> Post()
        {
            string errorMessage = "-1";
            string _localFileName = "";
            string sourceFile = "";
            string destFile = "";

            try
            {
                // Check if the request contains multipart/form-data.
                var multipartFormDataStreamProvider = new UploadMultipartFormProvider(workingFolder);

                // Read the MIME multipart asynchronously 
                await Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                _localFileName = multipartFormDataStreamProvider
                    .FileData.Select(multiPartData => multiPartData.LocalFileName).FirstOrDefault();

                string fileName = Path.GetFileName(_localFileName);
                string sourcePath = _localFileName;
                string[] cadena = fileName.Split('_');

                //string sourcePath = @"\\10.100.13.74\d$\Demo";
                //string targetPath = @"\\10.100.13.78\d$\Demo";

                //UtilBL obj = new UtilBL();
                //DataTable dt = new DataTable();
                //dt = obj.GetFiltros("0005");

                //string nombreRuta = "";

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    if (dt.Rows[i]["FI_NOMBRE"].ToString() == "RutaArchivo") {
                //        nombreRuta = dt.Rows[i]["FI_VALOR"].ToString();
                //    }
                //}

                //string targetPath = Path.Combine(@nombreRuta, cadena[0], cadena[1], cadena[2]);

                //sourceFile = sourcePath;
                //destFile = Path.Combine(targetPath, fileName);

                //copiar archivo a repositorio

                // Use Path class to manipulate file and directory paths.

                // To copy a folder's contents to a new location:
                // Create a new target folder, if necessary.
                //if (!Directory.Exists(targetPath))
                //{
                //    Directory.CreateDirectory(targetPath);
                //}

                //    // To copy a file to another location and 
                //    // overwrite the destination file if it already exists.
                //    if (!File.Exists(destFile))
                //    {
                //        File.Copy(sourceFile, destFile, true);
                //    }
                //    else {
                //        errorMessage = "Ya existe un archivo con ese mismo nombre";
                //    }

                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                errorMessage = "Servidor de archivos no disponible, intente mas tarde.";

            }
            //finally
            //{
            //    if (File.Exists(_localFileName))
            //    {
            //        File.Delete(_localFileName);
            //    }
            //}

            // To copy all the files in one directory to another directory.
            // Get the files in the source folder. (To recursively iterate through
            // all subfolders under the current directory, see
            // "How to: Iterate Through a Directory Tree.")
            // Note: Check for target path was performed previously
            //       in this code example.
            //if (Directory.Exists(sourcePath))
            //{
            //    string[] files = Directory.GetFiles(sourcePath);

            //    // Copy the files and overwrite destination files if they already exist.
            //    foreach (string s in files)
            //    {
            //        // Use static Path methods to extract only the file name from the path.
            //        fileName = Path.GetFileName(s);
            //        destFile = Path.Combine(targetPath, fileName);
            //        System.IO.File.Copy(s, destFile, true);
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Source path does not exist!");
            //}

            // Create response
            return new FileUploadResult
            {
                //LocalFilePath = destFile,
                //FileName = Path.GetFileName(destFile),
                //FileLength = new FileInfo(destFile).Length,
                //error = errorMessage

                LocalFilePath = _localFileName,
                FileName = Path.GetFileName(_localFileName),
                FileLength = new FileInfo(_localFileName).Length,
                error = errorMessage

            };

            //var archivos =
            //    multipartFormDataStreamProvider.FileData
            //   .Select(file => new FileInfo(file.LocalFileName))
            //   .Select(fileInfo => new FileUploadResult
            //   {
            //       LocalFilePath = _localFileName,
            //       FileName = fileInfo.Name,
            //       Created = fileInfo.CreationTime,
            //       Modified = fileInfo.LastWriteTime,
            //       FileLength = fileInfo.Length / 1024
            //   }).FirstOrDefault();

            //    return archivos;
            //}
        }
        /// <summary>
        ///   Check if file exists on disk
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool FileExists(string fileName)
        {
            var file = Directory.GetFiles(workingFolder, fileName)
              .FirstOrDefault();

            return file != null;
        }

    }
}
