﻿using GenesysBE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace GenesysWeb.Controllers
{ //[Authorize]
	public class InventarioController : ApiController
	{

        GenesysBL.InventarioBL obj_GenesysBL = new GenesysBL.InventarioBL();

        [Route("api/ioption")]
        [HttpPost]
        public HttpResponseMessage GetIOptions([FromBody] Option ioption)
        { 
            try
            {
                DataSet result = obj_GenesysBL.GetOPTIONS(ioption);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }
            catch (Exception e)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, e);
                return response;
            }
        }

        [Route("api/getinventario")]
        [HttpPost]
        public HttpResponseMessage GetInventario(Format inv)
        {
            try
            {
                HttpResponseMessage response = null;
                if (inv != null)
                {

                    DataSet result = obj_GenesysBL.GetInventario(inv);
                    response = Request.CreateResponse(HttpStatusCode.OK, result.Tables[0]);
                    return response;
                }
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }

        }

        [Route("api/createitem")]
        [HttpPost]
        public HttpResponseMessage CreateItem([FromBody] Inventario inv)
        {
            try
            {
                HttpResponseMessage response = null;
                if (inv != null)
                {
                    string result;
                    result =  obj_GenesysBL.PostInventario(inv);
                    if (result == "")
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.NotFound);
                    }

                    return response;
                }
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }

        }

    }
}