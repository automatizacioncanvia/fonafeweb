﻿using GenesysUtil;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GenesysWeb.Controllers
{
    public class VerticketsController : ApiController
    {
        [Route("api/fogetvertickets/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetVerTickets(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETDATADASHBOARDALL", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetgrupos/{valor}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetVerGrupos(String valor)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);

                List<string> parametros = new List<string>();
                parametros.Add("@pTicket");

                using (DataSet ds = SqlConector.getDataset("FO_GETGRUPO", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetstatus/{valor}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetStatus(String valor)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);

                List<string> parametros = new List<string>();
                parametros.Add("@pTicket");

                using (DataSet ds = SqlConector.getDataset("FO_GETESTADO", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalle/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsDetail(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDDETAIL", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/getdetallepanel/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetpanelDetail(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETPANELDETAILALL", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleres/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsDetailRes(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDDETAILRES", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/getdetallepanelsla/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetpanelDetailSla(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETPANELDETAILSLAALL", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetallesla/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsDetailSla(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDDETAILSLA", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleslares/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsDetailSlaRes(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDDETAILSLARES", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleR1/{valor}/{valor2}/{valor3}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsR1(String valor, String valor2, String valor3)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);
                list.Add(valor3);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");
                parametros.Add("@pServicio");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDALL_R1", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleR2/{valor}/{valor2}/{valor3}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsR2(String valor, String valor2, String valor3)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);
                list.Add(valor3);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");
                parametros.Add("@pServicio");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDALL_R2", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleR3/{valor}/{valor2}/{valor3}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsR3(String valor, String valor2, String valor3)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);
                list.Add(valor3);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");
                parametros.Add("@pServicio");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDALL_R3", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleR4/{valor}/{valor2}/{valor3}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsR4(String valor, String valor2, String valor3)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);
                list.Add(valor3);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");
                parametros.Add("@pServicio");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDALL_R4", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleR5/{valor}/{valor2}/{valor3}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsR5(String valor, String valor2, String valor3)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);
                list.Add(valor3);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");
                parametros.Add("@pServicio");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDALL_R5", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetalleR6/{valor}/{valor2}/{valor3}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsR6(String valor, String valor2, String valor3)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);
                list.Add(valor3);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");
                parametros.Add("@pServicio");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDALL_R6", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetmon/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDsTickDetMon(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHDETMONALL", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

        [Route("api/fogetdetailstate/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetTicketsDetailState(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@pIdVista");
                parametros.Add("@pInfo");

                using (DataSet ds = SqlConector.getDataset("FO_GETDASHBOARDETAILALL_15", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }

    }
}
