﻿using GenesysUtil;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GenesysWeb.Controllers
{
    public class BucarticketsController : ApiController
    {
        [Route("api/fogettickets/{valor}/{valor2}")]
        [HttpGet]
        public HttpResponseMessage GetDataSetByNameTotal(String valor, String valor2)
        {
            if (valor != null || valor != "")
            {
                List<string> list = new List<string>();
                list.Add(valor);
                list.Add(valor2);

                List<string> parametros = new List<string>();
                parametros.Add("@VALORTOTAL");
                parametros.Add("@ORDER");

                using (DataSet ds = SqlConector.getDataset("FO_GETTICKETS", list, parametros))
                {
                    list.Clear();
                    parametros.Clear();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ds);
                    return response;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid Parameters");
            }
        }
    }

}
