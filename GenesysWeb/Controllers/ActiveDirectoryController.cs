﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GenesysUtil.FlowConnections;
using GenesysBE.ActiveDirectory;
using Newtonsoft.Json;


namespace GenesysWeb.Controllers
{
    public class ActiveDirectoryController : ApiController
    {

        // string flowuuid_dev = "4fc3d3ab-b42a-43d3-a5a8-2451c839bf54";--dev
         string flowuuid_dev = "84657598-498a-4ef6-8cf4-4f12ac7d1a4a"; //pdr
        //string flowuuid_prd = "84657598 - 498a-4ef6-8cf4-4f12ac7d1a4a";


       [Route("api/execflow")]
        [HttpPost]
        public HttpResponseMessage ExecFlow(ExecFlow e)
        {
            try
            {
                //LE MANDO LA OPERACION Y EL INPUT QUE PUEDE SER UN JSON

                string json = JsonConvert.SerializeObject(e);
                FlowClass oflo = new FlowClass();

                //GenesysBL.ActiveDirectoryDL hp = new GenesysBL.ActiveDirectoryDL();
                //hpooconfiguration hpoo = new hpooconfiguration();
                //hpoo = hp.GetHPOOCONFIGURATION();


                string final = oflo.ExecuteFlowNow(flowuuid_dev, "10.119.1.183", json,"gmd","gmd.2016");
                return Request.CreateResponse(HttpStatusCode.OK, final);

            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }

        }

        [Route("api/getflowsummary/{idejecution}/{sentence}")]
        [HttpGet]

        public HttpResponseMessage getExecutionsummary(string idejecution, string sentence)
        {
            try
            {

                //GenesysBL.ActiveDirectoryDL hp = new GenesysBL.ActiveDirectoryDL();
                //hpooconfiguration hpoo = new hpooconfiguration();
                //hpoo = hp.GetHPOOCONFIGURATION();

                FlowClass oflo = new FlowClass();
                string result = oflo.GetFlow("gmd", "gmd.2016", idejecution, sentence, "10.119.1.183");
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }
        }


        [Route("api/operation")]
        [HttpPost]
        public HttpResponseMessage Operacion(User e)
        {
            try
            {

                //GenesysBL.ActiveDirectoryDL hp = new GenesysBL.ActiveDirectoryDL();
                //hpooconfiguration hpoo = new hpooconfiguration();
                //hpoo = hp.GetHPOOCONFIGURATION();


                Int64 unixTimestamp = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                Random rnd = new Random();
                int ramdomnumber = rnd.Next(1, 100000);
                Int64 m_token = Int64.Parse(unixTimestamp + "" + ramdomnumber);
                e.m_token = m_token;



                GenesysBL.ActiveDirectoryDL objectoperation = new GenesysBL.ActiveDirectoryDL();
                int num;
                string opresult = objectoperation.Operation(e);
                bool result = Int32.TryParse(opresult, out num);

                if (result == false)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, opresult);
                    return response;

                }
                else
                {
                    //insertar atributos si los tenga.


                    if (e.atributos != null) {
                        string correctinsert=objectoperation.InserAtributos(e.atributos, int.Parse(opresult));
                        bool result2 = Int32.TryParse(correctinsert, out num);


                        if (result2 == false)
                        {
                            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, opresult);
                            return response;
                        }

     
                    }
                    

                        int user_Log_id = int.Parse(opresult);
                        //validar si es number si no es nunca pasa flujo y sale al cath
                        ExecFlow oflow = new ExecFlow();
                        oflow.mtoken = m_token.ToString();
                        oflow.input = "-";



                        string json = JsonConvert.SerializeObject(oflow);
                        FlowClass oflo = new FlowClass();
                        string final = oflo.ExecuteFlowNow(flowuuid_dev, "10.119.1.183", json, "gmd", "gmd.2016");
                        return Request.CreateResponse(HttpStatusCode.OK, final);

                    

                }

             

            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }

        }



        [Route("api/operationgroup")]
        [HttpPost]
        public HttpResponseMessage Operaciongroup(FullGroup g)
        {

            try
            {


                Int64 unixTimestamp = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                Random rnd = new Random();
                int ramdomnumber = rnd.Next(1, 100000);
                Int64 m_token = Int64.Parse(unixTimestamp + "" + ramdomnumber);
                g.tokenid = m_token;

                GenesysBL.ActiveDirectoryDL objgroups = new GenesysBL.ActiveDirectoryDL();
                string result = objgroups.GroupOperation(g);


                ExecFlow oflow = new ExecFlow();
                oflow.mtoken = m_token.ToString();
                oflow.input = "-";

                string json = JsonConvert.SerializeObject(oflow);
                FlowClass oflo = new FlowClass();
                string final = oflo.ExecuteFlowNow(flowuuid_dev, "10.119.1.183", json, "gmd", "gmd.2016");
                return Request.CreateResponse(HttpStatusCode.OK, final);

            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }
        }

    }
}
