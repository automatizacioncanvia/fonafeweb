﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data;
using System.DirectoryServices;
using System.Collections;
using System.DirectoryServices.Protocols;

using System.Configuration;
using System.Data.SqlClient;
using GenesysUtil;
using System.DirectoryServices.AccountManagement;
using Newtonsoft.Json;
using System.Web.Http.Results;
using GenesysWeb.Attributes;
using System.Web.Http.Controllers;

using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using System.Web;

using System.Security.Principal;

using FL = GenesysWeb.FlowConection;
using System.IO;
using System.Net.Http.Headers;

namespace GenesysWeb.Controllers
{
    //[Authorize]
    public class FonafeController : ApiController
    {

        public String Tokken;


        private string Algo(System.Web.Mvc.AuthorizationContext actionContext)
        {
            try
            {
                HttpRequestBase request = actionContext.RequestContext.HttpContext.Request;
                Tokken = request.Params["Token"];

                return Tokken;
            }
            catch (Exception)
            {
                return "Error";
            }
        }

        public String GetPasswordColaborador(String str)
        {
            string result = null;
            GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
            result = obj_GenesysBL.GetPasswordColaborador(str);
            return result;
        }


        [Route("api/loginsd")]
        [HttpPost]
        public HttpResponseMessage LoginSD(GenesysBE.TokenClass e)
        {
            /*Este método en otra clase-------------------------------------------------------*/

            string Token = e.token;//Tokken;
            int _expirationMinutes = 1440;//10;
            string usuariotoken = null;
            string clavetoken = null;
            string clavetokenfirst = null;

            try
            {
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(Token));

                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    DateTime timeStamp = new DateTime(ticks);
                    usuariotoken = username;

                    clavetokenfirst = GetPasswordColaborador(username);

                    clavetoken = SecurityManager.DecryptPassword(clavetokenfirst);

                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                    if (!expired)
                    {
                        //sigue adelante (estas a tiempo)
                    }
                    else {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, "Sesión cerrada");
                    }
                }
            }
            catch (Exception ex)
            {
                string error = Convert.ToString(ex);
            }
            /*Este método en otra clase-------------------------------------------------------*/

            try
            {
                FL.LoginSD fll = new FL.LoginSD();
                string str_dominio = String.Empty;
                GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
                str_dominio = obj_GenesysBL.GetDominioColaborador(usuariotoken);

                //string URL = "";
                //string info = e.idUsuario + ",1";

                //DataTable dtCuenta = obj_GenesysBL.GetMenu(20, info).Tables[0];
                //if (dtCuenta.Rows[0][0].ToString() == "SIN CUENTA")
                //{
                //    URL = fll.sdLogin(usuariotoken, clavetoken, e.url, str_dominio);
                //}
                //else
                //{
                //    URL = fll.sdLogin(dtCuenta.Rows[0]["CA_NOMBRE"].ToString(), dtCuenta.Rows[0]["CA_CLAVE"].ToString(), e.url, dtCuenta.Rows[0]["DO_NOMBRE"].ToString());
                //}

                string URL = fll.sdLogin(usuariotoken, clavetoken, e.url, str_dominio);

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URL);
                return response;
            } catch (Exception i)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, i);
                return response;
            }
        }


        
      

        [Route("api/loginsw")]
        [HttpPost]
        public HttpResponseMessage LoginWins()
        {
            List<string> cookies = new List<string>();
            FlowConection.LoginSW ologin = new FlowConection.LoginSW();
            cookies = ologin.getcookie();

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, cookies);
            return response;
        }

        //Este web service se utiliza para retornarte la url del solarwins
        [Route("api/loginswrt")]
        [HttpPost]
        public HttpResponseMessage LoginWinsRt(GenesysBE.TokenClass e)
        {
            /*Este método en otra clase-------------------------------------------------------*/

            string Token = e.token;//Tokken;
            int _expirationMinutes = 1440;//10;
            string usuariotoken = null;
            string clavetoken = null;
            string clavetokenfirst = null;

            try
            {
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(Token));

                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    DateTime timeStamp = new DateTime(ticks);
                    usuariotoken = username;

                    clavetokenfirst = GetPasswordColaborador(username);

                    clavetoken = SecurityManager.DecryptPassword(clavetokenfirst);

                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                    if (!expired)
                    {
                        //sigue adelante (estas a tiempo)
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, "Sesión cerrada");
                    }
                }
            }
            catch (Exception ex)
            {
                string error = Convert.ToString(ex);
            }
            /*Este método en otra clase-------------------------------------------------------*/

            try {
            FL.LoginSW fll = new FL.LoginSW();
            string str_dominio = String.Empty;
            GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
            str_dominio = obj_GenesysBL.GetDominioColaborador(usuariotoken);

            string URL = "";

            string info = e.idUsuario + ",2";
            DataTable dtCuenta = obj_GenesysBL.GetMenu(20, info).Tables[0];

                if (dtCuenta.Rows[0][0].ToString() == "SIN CUENTA")
                {
                    URL = fll.swLogin(usuariotoken, clavetoken, str_dominio, e.url);
                }
                else {
                    
                    URL = fll.swLogin(dtCuenta.Rows[0]["CA_NOMBRE"].ToString(), dtCuenta.Rows[0]["CA_CLAVE"].ToString(), dtCuenta.Rows[0]["DO_NOMBRE"].ToString(), e.url);
                    
                }
            //aqui


            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URL);
                //response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }
            catch(Exception i)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, i);
                return response;

            }
        }


        //Este web service se utiliza para retornarte la url del solarwins
        [Route("api/loginPortalCloud")]
        [HttpPost]
        public HttpResponseMessage LoginPortalCloud(GenesysBE.TokenClass e)
        {

            string Token = e.token;//Tokken;
            int _expirationMinutes = 1440;//10;
            string usuariotoken = null;
            string clavetoken = null;
            string clavetokenfirst = null;

            try
            {
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(Token));

                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    DateTime timeStamp = new DateTime(ticks);
                    usuariotoken = username;

                    clavetokenfirst = GetPasswordColaborador(username);

                    clavetoken = SecurityManager.DecryptPassword(clavetokenfirst);

                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                    if (!expired)
                    {
                        //sigue adelante (estas a tiempo)
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, "Sesión cerrada");
                    }
                }

                FL.LoginSW fll = new FL.LoginSW();
                string str_dominio = String.Empty;
                GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
                str_dominio = obj_GenesysBL.GetDominioColaborador(usuariotoken);

                string info = e.idUsuario + ",4";
                DataTable dtCuenta = obj_GenesysBL.GetMenu(19, info).Tables[0];

                string URL = "";

                if (dtCuenta.Rows[0][0].ToString() == "SIN CUENTA") {
                    URL = e.url + Base64Encode(usuariotoken) + "/" + Base64Encode(clavetoken) + "/" + Base64Encode(str_dominio);
                } else {
                    URL = e.url + Base64Encode(dtCuenta.Rows[0]["CA_NOMBRE"].ToString()) + "/" + Base64Encode(dtCuenta.Rows[0]["CA_CLAVE"].ToString()) + "/" + Base64Encode(dtCuenta.Rows[0]["DO_NOMBRE"].ToString());
                }

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URL);
                return response;

            }
            catch (Exception i)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, i);
                return response;
            }


        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }


        //Este web servuce se utiliza para los indicadores
        [Route("api/dashboard")]
        [HttpPost]
        public HttpResponseMessage DashboardServiceDesk(GenesysBE.Format e)
        {

            try
            {
                HttpResponseMessage response = null;
                if (e != null)
                {

                    GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
                    DataSet result = obj_GenesysBL.GetDash(e);

                    response = Request.CreateResponse(HttpStatusCode.OK, result.Tables[0] );
                    return response;

                }
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }catch(Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }
        }

        //Este web servuce se utiliza para los indicadores
        [Route("api/panelmando")]
        [HttpPost]
        public HttpResponseMessage DashboardPanelMando(GenesysBE.Format e)
        {

            try
            {
                HttpResponseMessage response = null;
                if (e != null)
                {

                    GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
                    DataSet result = obj_GenesysBL.GetDash(e);

                    //response = Request.CreateResponse(HttpStatusCode.OK, result.Tables[0] );
                    response = Request.CreateResponse(HttpStatusCode.OK, result.Tables);
                    return response;

                }
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex);
                return response;
            }
        }


        //Este web service se utiliza para cargar el nav, empresas,dominios
        [Route("api/menu/{vista:int}/{info}")]
        [HttpGet]
        public HttpResponseMessage GetMenu(int vista,string info)
        {

            try { 
            GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
                DataSet result = obj_GenesysBL.GetMenu(vista,info);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }
            catch (Exception e)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, e);
                return response;
            }


        }
        
        [Route("api/auth")]
        [HttpPost]
        public HttpResponseMessage AuthFonafe(GenesysBE.Seguridad seguri)
        {
            int flagcorpac = seguri.dominio.IndexOf("corpac");

            if (flagcorpac != -1)
            {
                seguri.dominio = "corpac.local";
            }

            string result = null;
            GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
            result = obj_GenesysBL.FonafeAuth(seguri);

            string[] separadas;

            separadas = result.Split('¬');

            if (separadas.Length > 1)
            {
                var rptJsonyDescri = new
                {
                    Data = separadas[0],
                    OtherDetails = separadas[1],
                    Token = separadas[2],
                    Usuario = separadas[3],
                };

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, rptJsonyDescri);
                return response;
            }
            else
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }
        }



        [Route("api/getsdlink")]
        [HttpPost]
        public HttpResponseMessage GetUrlLink(GenesysBE.Ticket e)
        {
            /*Este método en otra clase-------------------------------------------------------*/

            string Token = e.token;//Tokken;
            int _expirationMinutes = 1440;//10;
            string usuariotoken = null;
            string clavetoken = null;
            string clavetokenfirst = null;

            try
            {
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(Token));

                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    DateTime timeStamp = new DateTime(ticks);
                    usuariotoken = username;

                    clavetokenfirst = GetPasswordColaborador(username);

                    clavetoken = SecurityManager.DecryptPassword(clavetokenfirst);

                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                    if (!expired)
                    {
                        //sigue adelante (estas a tiempo)
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, "Sesión cerrada");
                    }
                }
            }
            catch (Exception ex)
            {
                string error = Convert.ToString(ex);
            }
            /*Este método en otra clase-------------------------------------------------------*/

            try
            {
                FL.LoginSD fll = new FL.LoginSD();
                string str_dominio = String.Empty;
                string sdlink = String.Empty;
                GenesysBL.SeguridadBL obj_GenesysBL = new GenesysBL.SeguridadBL();
                str_dominio = obj_GenesysBL.GetDominioColaborador(usuariotoken);
                sdlink = obj_GenesysBL.GETsdlink();


                string URL = fll.getlinksd(usuariotoken, clavetoken, sdlink, str_dominio,e.ticket_id);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URL);
                return response;
            }
            catch (Exception i)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, i);
                return response;
            }
        }













    }
}
