﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace GenesysWeb.FlowConection
{
    public class LoginSD
    {

        public string sdLogin(string username, string password,string url,string domain)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            httpWebRequest.Method = "POST";
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36";
         

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                String json = "";

                //if (domain.IndexOf('.') != -1)
                //{
                //json = "PortalSession=&USERNAME=" + username + "@" + domain + "&PIN=" + password + "&PDA=No&OP=LOGIN";
                //}
                //else
                //{
                //json = "PortalSession=&USERNAME=" + domain + "\\" + username + "&PIN=" + password + "&PDA=No&OP=LOGIN";

                json = "PortalSession=&USERNAME=" + username + "&PIN=" + password + "&PDA=No&OP=LOGIN";

                //}



                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var urlfinal = GetString(result);
                return urlfinal;
            }


        }

        public string GetString(string valor)
        {
            String str_url = valor;
            int first_tag = str_url.IndexOf("url =");
            int last_tag = str_url.IndexOf("window.location");
            string text = str_url.Substring((first_tag + 8), (last_tag - (first_tag + 13))).Trim();
            return text;
        }

        public string getlinksd(string username, string password, string url, string domain,string ticke_id)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            httpWebRequest.Method = "POST";
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36";


            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                String json = "";

                if (domain.IndexOf('.') != -1)
                {
                    //json = "PortalSession=&USERNAME=" + username + "@" + domain + "&PIN=" + password + "&PDA=No&OP=LOGIN&INITIAL_LOAD=OP%3DSEARCH%2BFACTORY%3Dcr%2BSKIPLIST%3D1%2BQBE.EQ.ref_num%3D"+ticke_id;
                    json = "PortalSession=&USERNAME=" + username  + "&PIN=" + password + "&PDA=No&OP=LOGIN&INITIAL_LOAD=OP%3DSEARCH%2BFACTORY%3Dcr%2BSKIPLIST%3D1%2BQBE.EQ.ref_num%3D" + ticke_id;
                }
                else
                {
                    json = "PortalSession=&USERNAME=" + username + "&PIN=" + password + "&PDA=No&OP=LOGIN&INITIAL_LOAD=OP%3DSEARCH%2BFACTORY%3Dcr%2BSKIPLIST%3D1%2BQBE.EQ.ref_num%3D" + ticke_id;
                }



                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var urlfinal = GetString(result);
                return url+"#"+urlfinal;
            }


        }

    
    }
}