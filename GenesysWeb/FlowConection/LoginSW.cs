﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using GenesysBL;

namespace GenesysWeb.FlowConection
{
    public class LoginSW
    {

        public List<string> getcookie()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://172.24.10.50/Orion/Login.aspx?autologin=no");
            httpWebRequest.AllowAutoRedirect = false;
            httpWebRequest.CookieContainer = new CookieContainer();
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            httpWebRequest.Method = "POST";
            //String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            //httpWebRequest.Headers.Add("Content-Type","application/json");
            //httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
            //


            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKLTkzMDU4NzM2OA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAgIEDxYCHgZzY3JvbGwFBGF1dG8WAgIBD2QWAgIBD2QWBgIBEA8WBB4IQ3NzQ2xhc3MFI3N3LXBnLWVycm9ydGV4dCBzdy12YWxpZGF0aW9uLWVycm9yHgRfIVNCAgJkZGQCBQ8PFgQfAgUWIHN3LWJ0bi1wcmltYXJ5IHN3LWJ0bh8DAgIWAh4KYXV0b21hdGlvbgUFTG9naW5kAgYPFgIeB1Zpc2libGVoZGSez5K55PHJzNeOF2pEiIQSBH0QZA%3D%3D&ctl00%24BodyContent%24Username=genesys%5CAdministrator&ctl00%24BodyContent%24Password=Pa55w0rd123%24";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            List<string> cookies = new List<string>();

            foreach (Cookie cook in httpResponse.Cookies)

            {

                cookies.Add(cook.ToString());

                // Console.WriteLine("Cookie:");
                // Console.WriteLine("{0} = {1}", cook.Name, cook.Value);
                // Console.WriteLine("Domain: {0}", cook.Domain);
                // Console.WriteLine("Path: {0}", cook.Path);
                // Console.WriteLine("Port: {0}", cook.Port);
                // Console.WriteLine("Secure: {0}", cook.Secure);

                // Console.WriteLine("When issued: {0}", cook.TimeStamp);
                // Console.WriteLine("Expires: {0} (expired? {1})",
                //     cook.Expires, cook.Expired);
                // Console.WriteLine("Don't save: {0}", cook.Discard);
                // Console.WriteLine("Comment: {0}", cook.Comment);
                // Console.WriteLine("Uri for comments: {0}", cook.CommentUri);
                // Console.WriteLine("Version: RFC {0}", cook.Version == 1 ? "2109" : "2965");

                // Show the string representation of the cookie.
                //Console.WriteLine("String: {0}", cook.ToString());
            }

            return cookies;


        }

        public string swLogin(string username, string password,string dominio,string url)
        {
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKLTkzMDU4NzM2OA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAgIEDxYCHgZzY3JvbGwFBGF1dG8WAgIBD2QWAgIBD2QWBgIBEA8WBB4IQ3NzQ2xhc3MFI3N3LXBnLWVycm9ydGV4dCBzdy12YWxpZGF0aW9uLWVycm9yHgRfIVNCAgJkZGQCBQ8PFgQfAgUWIHN3LWJ0bi1wcmltYXJ5IHN3LWJ0bh8DAgIWAh4KYXV0b21hdGlvbgUFTG9naW5kAgYPFgIeB1Zpc2libGVoZGTGrS0%2BsjGnebDtyWugWNVsGu8U3w%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username="+dominio+"%5C"+ username + "&ctl00%24BodyContent%24Password="+ password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKLTkzMDU4NzM2OA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAgIEDxYCHgZzY3JvbGwFBGF1dG8WAgIBD2QWAgIBD2QWBgIBEA8WBB4IQ3NzQ2xhc3MFI3N3LXBnLWVycm9ydGV4dCBzdy12YWxpZGF0aW9uLWVycm9yHgRfIVNCAgJkZGQCBQ8PFgQfAgUWIHN3LWJ0bi1wcmltYXJ5IHN3LWJ0bh8DAgIWAh4KYXV0b21hdGlvbgUFTG9naW5kAgYPFgIeB1Zpc2libGVoZGTcb5OhxctuKRrw3Fdx%2B4rEi0y6wA%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password="+ password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKLTkzMDU4NzM2OA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAgIEDxYCHgZzY3JvbGwFBGF1dG8WAgIBD2QWAgIBD2QWBgIBEA8WBB4IQ3NzQ2xhc3MFI3N3LXBnLWVycm9ydGV4dCBzdy12YWxpZGF0aW9uLWVycm9yHgRfIVNCAgJkZGQCBQ8PFgQfAgUWIHN3LWJ0bi1wcmltYXJ5IHN3LWJ0bh8DAgIWAh4KYXV0b21hdGlvbgUFTG9naW5kAgYPFgIeB1Zpc2libGVoZGTGrS0%2BsjGnebDtyWugWNVsGu8U3w%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username="+username+"&ctl00%24BodyContent%24Password="+password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKLTkzMDU4NzM2OA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAgIEDxYCHgZzY3JvbGwFBGF1dG8WAgIBD2QWAgIBD2QWBgIBEA8WBB4IQ3NzQ2xhc3MFI3N3LXBnLWVycm9ydGV4dCBzdy12YWxpZGF0aW9uLWVycm9yHgRfIVNCAgJkZGQCBQ8PFgQfAgUWIHN3LWJ0bi1wcmltYXJ5IHN3LWJ0bh8DAgIWAh4KYXV0b21hdGlvbgUFTG9naW5kAgYPFgIeB1Zpc2libGVoZBgCBSJjdGwwMCRCb2R5Q29udGVudCRjdGwwMCRMb2dpblZpZXcxDw9kAgFkBSJjdGwwMCRCb2R5Q29udGVudCRjdGwwMCRMb2dpblZpZXcyDw9kAgFkXCe7pxgpxmta%2FEW2NUze3xWIMdQ%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username="+ username + "&ctl00%24BodyContent%24Password="+ password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=evMeAw%2F4wcZxPt7d5joZPheYPpcx9qccuJeRUs9V1mXW0SLkGsEHeeKqe0B6ORxAw%2FcBcA4n6jmt8kfS8H0u3%2BQvhgJQD3mMB7CoKtN0N5Ga7okVNmDbprQU8iDm8EbkKt%2BMj9f%2FiZDSdEEjD4XpR6nAE812y%2BvwBaiY3AP2zWCx6wMekjZTp6HOcVvCNhW1wshF60dnAA5RklLczSc9gCp5%2F4k8ngWFs1VEU960%2Bpte1ua4pZe8vaISn2jqJu%2BDCpc7ZE1xXFstHmXEW%2BVfaRd3KV38OMqDWzmsmTdA1TJvT8Kb9JlXPEx1OJcHeFzVsqG113nGC7v5kzfEsvBmrH0qSsQ%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=E3FOPBSjQfZ93dCIIpyLKEFStsWF9em4pOxRkAu7kTERwWTP%2BFlHiADYh9G66WbGl6JDKH5IX5aknrs2FzKfK4YlgIE4czP%2FtsQhiOAfajL%2Fv1FqHI5kjfF2sejtity0ePp%2Bu%2F7ERbl%2FQJoRm9IuLOywmu1aqklLowXnrKQ5rOzYeB40pzGR64gwGpYXeEm%2FDUL1%2BknBr7bGnUlYCU4rs2SFmSz2o9gC2f5trkfwJeXu7hNOP7m88xIsIJ2r0JB0GHZ5FEmHSVdoRCc5uuGxZ42437ADNSRQhruUi5joN3ko39c6C7%2F3DuYFjjmh34ZqxisVuATOimkJkGPkYaMEq8ECc%2FT7YTB4FPPomhI0uAIoeNjL&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=UuAIFpURWrmZUh0du1kVb%2Bh5vClJX9Eh9O7PzoHgHTSU3MkjfhNYPHSxG%2F3WjozYn0CdFjwIT6RuntWzAobxtpBp1bmfs%2BKRS8el9K1ZuWzCS%2BoGIYY%2Bj3mjLPsCdbmh0NqIBLSCx5NDycpCz4wACCE5OK9TB9usnrAZCkIRpGJK5cZfub0bTosYcSlETPvAyH%2Fw%2Fcwq3pShpoyeATOEXA7i3DXB%2B9Vyqa03Z1wKLiClrNAtmv9sfWAoD6nDiSVpu%2F5R1EfZsS6fAx5JJkYNM5BcZBNnEOjHJtttTQ3T03Y5f61e9lDGKGdZDOJg%2FP%2FZ6ZwSssiTWDrJLg%2B1uer2XvKS5T67pzLu4mUwM1ddJgCGyp15GzHQWUO3EgloXBgHHpHlibyKYpA7Pta9Xag0hq16Jr454dGymr%2BWBuidYbyVC8FJCpq822zfNs0U%2BPBz%2BCaYin%2FkvcqRcy1WMRk%2FU%2F1huXWbed9y8fd1fbO9FmagxC7nntUkhwHZXop2hktc&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username="+username+"&ctl00%24BodyContent%24Password="+password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=XuPtVDldN56KwMnPzdW%2Ba7EJFpIvhJhwxzqlS3dnIEt9ES4hJTu5zTnXQO1iI%2FD9Jp47OAsoMMRJRIBLtIUqwjhBtv2UaN%2FPoe%2B%2BniHGBYTKUMl73HVl6DzQ9Hk64OgzQSE37C8Aifk4tn9Is8Y0kp%2FBSxhQZ3Cb99X%2BxxDMdKBe8YZZQJ4ywbWOscT10PzUT%2BffztyN2aEGRSCnpL6vPb9kfim%2FbJAyIt6GKcR%2BYcXZM14dfpBhMpGZT%2BONuY7han5iLLTsLPdECpyu20Acsv0yZu%2B7iHBPwPlwBm06Efx87G9wfPd7poJ4VnONgiOHmi0HdKwb%2B%2B%2FO%2ByvMnQ%2BBBTM0cFCmq%2Fmm33Z2yAXV1K%2B07WL8SwK0SDmTOqyPQDLWozmb1J2tSxrEytXLxXClFw%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;

            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=%2FcVEp3hRy3QLvOs8TxSfQD7Sr3Np2%2BcSJCYHrnw64pD3chdV094XLDz%2FIa9NosAZgf%2B4thJizzNc0VuDAKVt0uLwJByRoyet7dnQioFcAhzLek76igoUeIvH96byW%2F4GsFNK6Qr0G6cKWtxqdPtbkereg1iyLCQkjcvwJA%2FF9FBTaeSfzQE%2BnkpMyh2ssILMQclD%2BfSeGZclc7%2B13c8ePtpQM%2BNUClNpipYNEIMSyAXYldDHq1xs3JcKWfIlJEnYfpuEuC9ckGiQvVIROCE9ZgMf%2BYQ6tK9LnXfDu1yVKLLd9yLgk%2FMbsmLQkrUwPsxIkD9eK1rjapIWCsympnysW8ScfPuK38p8CBAymQErW4KitR5nQF5JWD1Mv6sBfGyA3nr9eW4glc1Z0Y4KEw043Q%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;

            //string json= "__EVENTTARGET = ctl00 % 24BodyContent % 24ctl04 & __EVENTARGUMENT = &__VIEWSTATE = rvHVLOXSUYS6FjSeE7NZuTas3DWbk8RMXje % 2BFz5SXVV8ViWRESjS774azuxoYUhFUZAtteLYkxkG4kQwvoqCZJFxfK5HT9evmpXa0SxRP0pIcEE6t3DU4FP3YMo % 2FKSaFXJNO4vkDlJPnPpluuQ3vdjV % 2FMmdR98jEg % 2BQEe8 % 2FvX2N20jf2h10yds4pGCPjV % 2B4 % 2Bzz2XfCSyKcUjlq % 2BPWODDE0EOQblskZJIIwcfU4dtEoUxrU3AvQsFR33 % 2BrZKLkudAK0FtR7UmGpTLxCFPsM0IpraVHc0 % 2FyTHLkkamDOvp2so2PSSCS969rtdRuwNwX4yD1e % 2B1hpHawVGhVzjGj5TtmSoI % 2FyALaon4sFrT3zgt1OkBoZtnVsvx4S1bK1M1q31kMBS8acVNdumOnCJ1zHYN2g % 3D % 3D & __VIEWSTATEGENERATOR = 01070692 & ctl00 % 24BodyContent % 24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;

            //string json = "__EVENTTARGET =ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=5NZyfyzwiPwW1Bdgohpr0h4cNJiuSoKEEqYckecLLn4PoWTD5VfDIl3bjvD5wWfPJD1PAAh5FPpBubSETEK1iQOG6IWTKWYC1WQsLXdustiNh%2FOUP5pqqQTJD5cxawkTM3sTsbrvGCOprHPK0d1JkFMJprcXYLQOp%2FlwUH3jdK%2BlVNcPpqRDVBsU5pm2a2iZ%2Fl6D0vj2RYLHjr9Yn6BdSNq6Izi2xjVYdBorsRvhk3acadmagwi9%2BL61tMtHji14MdVYfB%2BcO8oo3jtAxZTZ4rkw0YUPWuQHSJB%2F9DWDBQJrvDZO5Oh6PX7McQJmgydhexrkz7NUMyzuzXuWYmwgAfmt6U6f2mjmTwcP%2BUdq4q4K%2FIn9fQwRjbwIyo5CXFDzyc%2F1aezOuKx42i9rwcytdA%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;
            //string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=fdwGATssyZb5TwyQVZfFvXa7b8Z9UfcqdN4s0P1VQw38c1IXJDzYm3hH3eOHkHdg9Yi8Aw7UprztT08UFCIs17q2pvRyQ4ft9CY2xZgpVKA%2F4cbs1LEqBsJ0bkZNjBg3sxWxsKb113P%2BBpgM0jocC9sH4r7a1979YdNRqtZSuLyrTHU2ulEez2RM%2F6ruV8uPXWGJv62AE10tuRYyaOQ3A%2Bwujt9v878IqpghCLOJUy4vuwALPhedBXzcsZ7yEwhTlQJ0QVL6j2PwHKFvhs%2Fed6c2Ll6qD0BaNQT45OswwTjz9HdgpJCUYsED1kgxbr2RbIYlGyXeMKilkkib1EdrjXWt0e0CWhL%2BRwEC1c84XcqG3kHlWqCo79X%2FJ3ugXg7EaZluO02DWbP0CCnjH0EVew%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;
            string json = "__EVENTTARGET=ctl00%24BodyContent%24ctl04&__EVENTARGUMENT=&__VIEWSTATE=AF2coPO8GzosS4O3LWPxbtbT5uNqBrgy4IkpeP43tqko4JpM%2FdFpooUsSduWkhAiaDhDH6Gw0G9vub4Ep%2F9RW8YoAg2QL1y7wJ7iAIq56myoiXn1SMDcSrKJEsICTQ%2Bfk3a%2BIKznzowMCly1GdD%2B2kmZz5a%2BiwLx1fTNcbyCnIjjfqCvsxevxEKYL66uKR4Gk9en2DqcCd5r4JjCHLFjFJGe9QDvQNKmD8LFiYIGe2k44eNYHCHefv6e1Cj1Qa2ZWkVn4jbspXR1KAZTDOlzQ2a0rPYF1kSAI%2FTYj%2FVm9OkS7f2QIsmkhmroI5BBdmJXz%2BN%2FkorUsf60ZNNERwJR4s6KuZdKD1vhBJ9FA7ml6R97jiczEDMsE4n8CDqeWqiXrZrgaqqWfAnXTzOUaaiO5A%3D%3D&__VIEWSTATEGENERATOR=01070692&ctl00%24BodyContent%24Username=" + username + "&ctl00%24BodyContent%24Password=" + password;
            var str_link = url + "?";

            return str_link +=json;
        }
    }
}