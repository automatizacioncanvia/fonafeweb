﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenesysWeb.Models
{
    public class FileUploadResult
    {
        public string LocalFilePath { get; set; }
        public string FileName { get; set; }
        public long FileLength { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public string error { get; set; }
    }
}