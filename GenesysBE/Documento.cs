﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE
{
    public class Documento
    {
        public string nombre { get; set; }
        public string em_id { get; set; }
        public string se_id { get; set; }
        public string usuario { get; set; }
        public string codigo { get; set; }
        public string fecha { get; set; }

    }
}
