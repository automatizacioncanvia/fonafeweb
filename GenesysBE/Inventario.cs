﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE
{
    public class Inventario
    {
        public string OP { get; set; }
        public string ECI_ID { get; set; }
        public string NOMBRE { get; set; }
        public string TIPOS { get; set; }
        public string NOMBREV { get; set; }
        public string ESTADOE { get; set; }
        public string FEC_ALTA { get; set; }
        public string FEC_BAJA { get; set; }
        public string FAM_ID { get; set; }
        public string CLASE_ID { get; set; }
        public string COD_CA { get; set; }
        public string SERIE { get; set; }
        public string PARTNUM { get; set; }
        public string MODELO { get; set; }
        public string MARCA { get; set; }
        public string COD_UBI { get; set; }
        public string PROYECTO { get; set; }
        public string PROPIEDAD { get; set; }
        public string ADMINID { get; set; }
        public string COD_ROL { get; set; }
        public string COD_AMB { get; set; }
        public string COD_CRIT { get; set; }
        public string DESC { get; set; }
        public string SOID { get; set; }
        public string VERSION { get; set; }
        public string ELICENCIA { get; set; }
        public string NLICENCIA { get; set; }
        public string ESOPORTE { get; set; }
        public string NCONTRATO { get; set; }
        public string SOPORTED { get; set; }
        public string SOPORTEH { get; set; }
        public string EGARANTIA { get; set; }
        public string GARANTIAD { get; set; }
        public string GARANTIAH { get; set; }
        public string RAM { get; set; }
        public string SOCKET { get; set; }
        public string CORE { get; set; }
        public string CPU { get; set; }
        public string DISCOAP { get; set; }
        public string DISCOAS { get; set; }
        public string RACK { get; set; }
        public string RUD { get; set; }
        public string RUH { get; set; }

        public string BAD { get; set; }
        public string BAH { get; set; }
        public string CID { get; set; }

        public string ITEM_IP { get; set; }
        public string EMPRESA { get; set; }
        public string DET_PROPIEDAD { get; set; }


        public int? COLAB_ID { get; set; }
        public string USER { get; set; }
        public int? VCENTER { get; set; }
        public string CODIGOSD { get; set; }
        public string TIPOTICKET { get; set; }
        public string FLAGCARGA { get; set; }

        public string productivo { get; set; }

        public string econfidencialidad { get; set; }
        public string eintegridad { get; set; }
        public string edisponibilidad { get; set; }

        public string edr { get; set; }
        public string antivirus { get; set; }
        public string datastore { get; set; }
        public string winupdate { get; set; }

        public string middleware { get; set; }
        public string middlversion { get; set; }
        public string virtualizador { get; set; }
        public string contingencia { get; set; }
        public string monitoreo { get; set; }
        public string rvtool { get; set; }
        public string endoflife { get; set; }
        public string lineabase { get; set; }
        public string backlvl { get; set; }
        public string fecbacklvl { get; set; }
    }
}
