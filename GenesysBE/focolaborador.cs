﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE
{
    public class focolaborador
    {
        private String nombre;
        private String apellido;
        private int empresa;
        private int perfil;
        private String alcance;
        private String cuenta;
        private String email;
        private int dominio;
        private int cargo;
        private String dni;
        private String fijo;
        private String anexo;
        private String celular;
        private int iduser;

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public int Empresa
        {
            get
            {
                return empresa;
            }

            set
            {
                empresa = value;
            }
        }

        public int Perfil
        {
            get
            {
                return perfil;
            }

            set
            {
                perfil = value;
            }
        }

        public string Alcance
        {
            get
            {
                return alcance;
            }

            set
            {
                alcance = value;
            }
        }

        public string Cuenta
        {
            get
            {
                return cuenta;
            }

            set
            {
                cuenta = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public int Dominio
        {
            get
            {
                return dominio;
            }

            set
            {
                dominio = value;
            }
        }

        public int Cargo
        {
            get
            {
                return cargo;
            }

            set
            {
                cargo = value;
            }
        }

        public string Dni
        {
            get
            {
                return dni;
            }

            set
            {
                dni = value;
            }
        }

        public string Fijo
        {
            get
            {
                return fijo;
            }

            set
            {
                fijo = value;
            }
        }

        public string Anexo
        {
            get
            {
                return anexo;
            }

            set
            {
                anexo = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public int Iduser
        {
            get
            {
                return iduser;
            }

            set
            {
                iduser = value;
            }
        }
    }
}
