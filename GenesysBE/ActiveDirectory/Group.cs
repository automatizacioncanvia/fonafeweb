﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE.ActiveDirectory
{
   public class Group
    {
        public int g_id { get; set; }
        public string g_nombre { get; set; }
        public int operacion { get; set; }
    }
}
