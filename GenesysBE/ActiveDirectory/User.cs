﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE.ActiveDirectory
{
    public class User
    {
        public string u_adid { get; set; }
        public int o_id { get; set; }
        public int em_id { get; set; }
        public string u_name { get; set; }
        public string u_surname { get; set; }
        public string u_samaname { get; set; }
        public string u_password { get; set;}
        //  public int ou_id { get; set; }
        public string ou_path { get; set; }
        public string u_office { get; set;}
        public string u_title { get; set; }
        public string u_departament { get; set; }
        public string u_company { get; set; }
        public string u_homephone { get; set; }
        public string u_mobilephone { get; set; }
        public string u_officephone { get; set; }
        public string u_enable { get; set; }
        public string u_accountexpirationdate { get; set; }
        public int co_id { get; set; }
        public Int64 m_token { get; set; }
        public string info { get; set; }
        public List<UserAttr> atributos { get; set; }
        public bool? ul_changepasswordatlogon { get; set; }

    }
}
