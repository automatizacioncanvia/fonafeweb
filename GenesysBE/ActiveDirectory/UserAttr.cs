﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE.ActiveDirectory
{
    public class UserAttr
    {
        public int aT_ID { get; set; }
        public int dO_ID { get; set; }
        public int eM_ID { get; set; }
        public string model { get; set; }
    }
}
