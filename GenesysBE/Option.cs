﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE
{
    public class  Option
    {
        public string id { get; set; }
        public int vista { get; set; }
        public int count { get; set; }
        public string svista { get; set; }
        public int colab_id { get; set; }
        public string dni { get; set; }
        public string fecha { get; set; }
        public string sensor { get; set; }
        public string area { get; set; }
    }
}
