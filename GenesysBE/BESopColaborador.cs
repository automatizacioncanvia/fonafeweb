﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE
{
    public class BESopColaborador
    {
        private int colab_id;
        private String area_id;
        private String prol_id;

        public int Colab_id
        {
            get
            {
                return colab_id;
            }

            set
            {
                colab_id = value;
            }
        }

        public string Area_id
        {
            get
            {
                return area_id;
            }

            set
            {
                area_id = value;
            }
        }

        public string Prol_id
        {
            get
            {
                return prol_id;
            }

            set
            {
                prol_id = value;
            }
        }
    }
}
