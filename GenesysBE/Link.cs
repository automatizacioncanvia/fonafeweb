﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE
{
    public class Link
    {
        private String nombre;
        private String descripcion;
        private String idempresa;
        private String idservicio;    
        private int iduser;

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public string Idempresa
        {
            get
            {
                return idempresa;
            }

            set
            {
                idempresa = value;
            }
        }

        public string Idservicio
        {
            get
            {
                return idservicio;
            }

            set
            {
                idservicio = value;
            }
        }

        public int Iduser
        {
            get
            {
                return iduser;
            }

            set
            {
                iduser = value;
            }
        }
    }
}
