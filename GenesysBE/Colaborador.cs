﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysBE
{
    public class Colaborador
    {
        public int      id { get; set; } //No se utiliza, generado por db
        public string contrasena { get; set; }
        public string   nombreapellido { get; set; }
        public string   cuenta { get; set; }
        public string   correo { get; set; }
        public string   nombreuo { get; set; }
        public int      estado { get; set; }
        public string   compania { get; set; }
        public string   area { get; set; }
        public string   dominio { get; set; }
        public string   nombre { get; set; }
        public string   apellido { get; set; }
        public string   alias { get; set; }
        public string   oficina { get; set; }
        public string   dni { get; set; }
        public string   departamento { get; set; }
        public string   rol { get; set; }
        public string   fecharegistro { get; set; }
        public string   fechamodificado { get; set; }
    }
}
